module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      spacing: {
        0.2: '0.2rem',
        0.4: '0.4rem',
        0.5: '0.5rem',
        0.7: '0.7rem',
        1: '1rem',
        1.5: '1.5rem',
        1.7: '1.7rem',
        2: '2rem',
        2.5: '2.5rem',
        2.7: '2.7rem',
        3: '3rem',
        3.5: '3.5rem',
        3.7: '3.7rem',
        4: '4rem',
        4.5: '4.5rem',
        4.7: '4.7rem',
        5: '5rem',
        5.7: '5.7rem',
        6: '6rem',
        6.5: '6.5rem',
        6.7: '6.7rem',
        7: '7rem',
        7.5: '7.5rem',
        8: '8rem'
      },
      borderWidth: {
        0: '0px',
        1: '1px',
        2: '2px',
        4: '4px',
        8: '8px'
      },
      minWidth: {
        5: '5rem',
        5.5: '5.5rem',
        6: '6rem',
        6.5: '6.5rem',
        7: '7rem',
        7.5: '7.5rem',
        8: '8rem',
        8.5: '8.5rem',
        9: '9rem',
        9.5: '9.5rem',
        10: '10rem',
        12: '12rem',
        13: '13rem',
        14: '14rem',
        15: '15rem',
        16: '16rem',
        17: '17rem',
        18: '18rem'
      },

      fontSize: {
        small: ['1rem', { lineHeight: '1.2' }],
        'sub-heading': ['1.2rem', { lineHeight: '1.4' }],
        normal: ['1.6rem', { lineHeight: '1.6' }],
        'heading-1': ['3.2rem', { lineHeight: '2.2' }],
        'heading-2': ['2.8rem', { lineHeight: '2' }],
        'heading-3': ['2.2rem', { lineHeight: '1.8' }]
      },
      zIndex: {
        '-1': '-1'
      }
    }
  },
  variants: {
    extend: {
      backgroundColor: ['active']
    }
  },
  plugins: [require('@tailwindcss/forms'), require('daisyui')]
};
