import store from "store";
import { setDataKeno } from "store/lottery";
import BaseAPI from "config/BaseApi";
import moment from "moment";

class KenoServices {
  //
  constructor() {
    this.result = null;
    this.duration = null;
    this.timer = null;
  }

  static async init() {
    const response = await BaseAPI.get("keno/current");
    const { status, data } = response;
    if (status) {
      clearInterval(this.timer);
      this.result = data;
      const nowDate = moment(data.result_date);
      nowDate.seconds(0);
      nowDate.milliseconds(0);
      const d = nowDate.diff(moment(), "seconds");
      this.duration = d;
      data.duration = d;
      store.dispatch(setDataKeno(data));
      this.intervalUpdate();
    }
  }

  static async intervalUpdate() {
    this.timer = setTimeout(() => {
      if (this.duration > 60) {
        this.duration = this.duration - 1;
        store.dispatch(
          setDataKeno({ ...this.result, duration: this.duration })
        );
        this.intervalUpdate();
      } else {
        if (this.duration > 0) {
          this.init();
        }
      }
    }, 1000);
  }
}

export default KenoServices;
