import axios from "axios";
import Cookies from "js-cookie";
import store from "store";
import { logOut } from "store/user";
import Router from "next/router";
import { get } from "lodash";

const BaseAPI = axios.create({
  baseURL: process.env.NEXT_PUBLIC_APP_API,
  timeout: 12000,
});

BaseAPI.interceptors.response.use(
  (config) => {
    const token = Cookies.get("token");
    const authDevice =
      "d04f817cf80fb5cad2eb936fa2aba25ae6d1a77dab4e5bf4662099072cd9f6d1";
    if (!!token) config.headers.Authorization = `Bearer ${token}`;
    config.headers.authdevice = authDevice;
    const { status } = config.data;
    if (status) {
      return config.data;
    }
    return config;
  },
  (error) => {
    if (get(error, "response.status", "") === 401) {
      store.dispatch(logOut());
      Router.push("/auth/login");
    }
    Promise.reject(get(error, "response.data", {}));
  }
);

BaseAPI.interceptors.request.use(
  (config) => {
    const token = Cookies.get("token");
    const authDevice =
      "d04f817cf80fb5cad2eb936fa2aba25ae6d1a77dab4e5bf4662099072cd9f6d1";
    if (!!token) config.headers.Authorization = `Bearer ${token}`;
    config.headers.authdevice = authDevice;
    return config;
  },
  (err) => Promise.reject(err)
);

export default BaseAPI;
