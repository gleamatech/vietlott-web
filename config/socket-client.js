import io from "socket.io-client";
import store from "../store";
import { setRooms } from "store/lottery";
import { setUser } from "store/user";
import BaseApi from "config/BaseApi.js";

class SocketServices {
  socket;
  static initSocket(token) {
    const sk = io(`${process.env.NEXT_PUBLIC_APP_SOCKET_URL}`, {
      query: {
        token: `${token}`,
      },
    });
    const updateInfoUser = async () => {
      const response = await BaseApi.post("/user/me");
      if (response.status) {
        store.dispatch(setUser(response));
      }
    };
    sk.on("connect", () => {
      console.log(`WS Chat Connected`);
    });

    sk.on("GET_ALL_ROOM", (msg) => {
      store.dispatch(setRooms(msg));
      updateInfoUser();
    });

    sk.on("JOIN_ROOM", (msg) => {
      sk.emit("GET_ALL_ROOM");
    });
    sk.on("CLOSE_ROOM", (msg) => {
      sk.emit("GET_ALL_ROOM");
    });
    sk.on("NEW_ROOM", (msg) => {
      sk.emit("GET_ALL_ROOM");
    });
    sk.on("LEAVE_ROOM", (msg) => {
      sk.emit("GET_ALL_ROOM");
    });
    sk.on("JOIN_ROOM_SUCCESS", (msg) => {
      sk.emit("GET_ALL_ROOM");
    });

    sk.on("ERROR", (msg) => {
      const data = JSON.parse(msg);
      console.log("error", data.msg);
      //   Alert.alert("Lỗi", data.msg);
    });

    sk.on("disconnect", () => {
      console.log("disconnect socket");
    });

    this.socket = sk;
  }

  static getSocketInstance() {
    return this.socket;
  }
}

export default SocketServices;
