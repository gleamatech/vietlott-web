import firebase from "firebase/app";
import "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyAWNdt0TWxlzDw658tEgoE-UJS3ZWJ5PY0",
  authDomain: "knclub-e7aec.firebaseapp.com",
  projectId: "knclub-e7aec",
  storageBucket: "knclub-e7aec.appspot.com",
  messagingSenderId: "532095462872",
  appId: "1:532095462872:web:99dc15cc687d2b81aad6ad",
  measurementId: "G-J4Q0NNK48L",
};

!firebase.apps.length ? firebase.initializeApp(firebaseConfig) : firebase.app();
// firebase.analytics();
firebase.auth().useDeviceLanguage();

export default firebase;
