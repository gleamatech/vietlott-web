import Button from "components/share/common/Button";
import Select from "components/share/common/Select";
import React, { useMemo } from "react";
import { Fragment } from "react";
import { useSelector } from "react-redux";
import { LIST_TYPE_LOTTERY } from "constants/lottery";

const LastModalKeno = ({
  listInfoPrice,
  handleSelectPrice,
  handleCheckList,
  handleCancelLastBoxKeno,
  handleContinue,
  data,
}) => {
  const selectPriceKeno = useSelector((state) => state.lottery.selectPriceKeno);
  const listOptions = useMemo(() => {
    return LIST_TYPE_LOTTERY.keno.listOptions;
  }, []);
  return (
    <Fragment>
      <div className="flex">
        <div className="w-full">
          <Select
            options={listInfoPrice}
            label={"Mệnh giá"}
            onChangeSelect={handleSelectPrice}
            defaultValue={selectPriceKeno}
          />
        </div>
      </div>
      <div className="w-full mt-1">
        {listOptions.map((item, index) => (
          <div
            key={index}
            className="flex items-center border-2 mb-0.5 pl-1 cursor-pointer"
          >
            <input
              type="radio"
              id={`${item.name}`}
              name="big small"
              value={item.value}
              className="form-checkbox rounded-none border border-red-600 text-red-500 w-2 h-2"
              onClick={handleCheckList}
              defaultChecked={item.value == data.subGame}
            />
            <div className="flex flex-col ml-1 ">
              <label htmlFor={`${item.name}`} className="cursor-pointer">
                {item.display}
              </label>
              <label htmlFor={`${item.name}`} className="cursor-pointer">
                {item.name}
              </label>
            </div>
          </div>
        ))}
      </div>
      <div className="modal-action flex flex-col items-center w-full">
        <div className="flex flex-col w-full">
          <div className="flex justify-between w-full max-w-full">
            <Button
              className="btn bg-red-500 "
              style={{ width: "48%" }}
              onClick={handleCancelLastBoxKeno}
              buttonColor={"btn-error"}
            >
              Hủy
            </Button>
            <Button style={{ width: "48%" }} onClick={handleContinue}>
              Tiếp theo
            </Button>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default LastModalKeno;
