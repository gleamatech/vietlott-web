import React, { useState } from "react";
import { toast } from "react-toastify";

const TableSelect = ({
  numberList,
  maxSelect,
  numberSelect = [],
  handleUpdateList,
}) => {
  const [listSelected, setListSelected] = useState(numberSelect);

  const handleSelectNumber = (number) => () => {
    const isSelected = listSelected.includes(number);
    if (isSelected) {
      const arrTemp = listSelected.filter((x) => x !== number);
      handleUpdateList(arrTemp);
      return setListSelected(arrTemp);
    }
    if (listSelected.length === maxSelect) {
      return toast.warning("Bạn đã chọn đủ số");
    }
    handleUpdateList([...listSelected, number]);
    setListSelected([...listSelected, number]);
  };

  return (
    <div
      className={`grid grid-cols-${Math.ceil(
        numberList.length / 10
      )} gap-0.5 overflow-y-auto hidden-scrollbar overflow-hidden mb-1`}
      style={{ maxHeight: "30vh", height: "30vh" }}
    >
      {numberList.fill("").map((num, index) => (
        <label
          key={index}
          className={`mx-auto rounded-full h-3 w-3 flex items-center justify-center border-1 border-red-500 ${
            listSelected.includes(index + 1) ? "bg-red-500" : ""
          }`}
          onClick={handleSelectNumber(index + 1)}
        >
          <span
            className={`text-small ${
              listSelected.includes(index + 1) ? "text-gray-50" : ""
            }`}
          >
            {index + 1}
          </span>
        </label>
      ))}
    </div>
  );
};

export default TableSelect;
