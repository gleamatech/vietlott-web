import React, { Fragment } from "react";
import { LIST_TYPE_LOTTERY } from "constants/lottery";

const ListTab = ({ handleClickTab, tabActive }) => {
  const tabs = [LIST_TYPE_LOTTERY.mega, LIST_TYPE_LOTTERY.power];

  return (
    <div className="flex justify-between items-center bg-white rounded-lg w-full h-32">
      {tabs.map((item, index) => (
        <div
          key={index}
          className={`w-1/2 h-full py-1.5 transition ease-in-out duration-300${
            tabActive === item.slug
              ? "text-blue-500 border-4 font-medium border-blue-500"
              : ""
          }`}
          onClick={handleClickTab(item)}
        >
          <img
            className="object-scale-down h-full m-auto "
            src={item.image}
            alt={item.name}
          />
        </div>
      ))}
    </div>
  );
};

export default ListTab;
