import Button from "components/share/common/Button";
import TogetherJoinRoom from "components/share/common/ContentModal/TogetherJoinRoom";
import { flatten, get } from "lodash";
import React from "react";
import { useRef } from "react";
import { useSelector } from "react-redux";
import { formatCurrency } from "utils/functions";

const BoxSelectTogether = ({ item }) => {
  const user = useSelector((state) => state.user.user);
  const sumPriceFunc = (total = 0, current) => {
    return total + parseFloat(current.price);
  };
  const joinInfo = item.join.find((it) => {
    return it.player._id === get(user, "_id", "");
  });
  const sumJoin = item.join
    .slice()
    .filter((it) => it.player._id === get(user, "_id"))
    .reduce((total = 0, current) => {
      return total + parseFloat(current.price);
    }, 0);
  const totalJoin = item.join.reduce(sumPriceFunc, 0);
  const joinNums = item.join.filter((it) => !it.lowCost).map((it) => it.nums);
  const joinLowNums = item.join.filter((it) => it.lowCost).map((it) => it.nums);
  const uniqNums = [...new Set(flatten(joinNums))].sort((a, b) => a - b);
  const remain = item.price - totalJoin;
  const renderListNumber = (list = []) => {
    return (
      <div className="mx-auto">
        <div className="flex w-full flex-wrap">
          {list.map((num, index) => (
            <div
              key={index}
              className="rounded-full h-3 w-3 flex items-center justify-center border-1 border-red-500 mx-1 my-0.5"
            >
              <span className="text-small">{num}</span>
            </div>
          ))}
        </div>
      </div>
    );
  };
  const myRef = useRef();

  const renderRightViewModal = () => {
    return (
      <Button
        onClick={() => myRef.current.onCreateNewRoom()}
        className="normal-case text-lg m-0.5"
      >
        Tham gia
      </Button>
    );
  };
  const handleShowModalJoin = () => {
    return window.openModal({
      content: (
        <TogetherJoinRoom
          item={item}
          renderRightViewModal={renderRightViewModal}
          ref={myRef}
        />
      ),
      rightView: renderRightViewModal(),
    });
  };

  return (
    <div>
      <div className="w-full bg-white p-1.5 rounded-lg mt-1.5">
        <div className="pb-1.5 border-b">
          <div className="flex justify-between pb-0.5">
            <div>Tổng số:</div>
            <div className="sub-text-color">{formatCurrency(item.price)}</div>
          </div>
          <div className="flex justify-between pb-0.5">
            <div>Hoàn thiện:</div>
            <div className="sub-text-color">{formatCurrency(totalJoin)}</div>
          </div>
          <div className="flex justify-between">
            <div>Còn lại:</div>
            <div className="sub-text-color">{formatCurrency(remain)}</div>
          </div>
        </div>
        {joinInfo && (
          <div className="flex justify-between align-center pb-1.5 pt-1.5 border-b">
            <span className="text-blue-500">
              {get(joinInfo, "player.phoneNumber", "")}
            </span>{" "}
            <p>
              Đã tham gia:{" "}
              <span className="sub-text-color">{formatCurrency(sumJoin)}</span>
            </p>
          </div>
        )}

        <div className="flex justify-between p-0.5">
          <p>Các số đã chọn</p>
        </div>
        <div className="flex justify-between pb-0.5 border-b">
          {renderListNumber(uniqNums)}
        </div>
        <div className="flex justify-between p-0.5">
          <p>Dãy số ngẫu nhiên</p>
        </div>
        <div className="flex justify-between pb-0.5 border-b">
          {renderListNumber(item.preNums)}
        </div>

        <div className="flex justify-between py-1 border-b">
          <Button className="w-full" onClick={handleShowModalJoin}>
            Tham gia
          </Button>
        </div>
      </div>
    </div>
  );
};

export default React.memo(BoxSelectTogether);
