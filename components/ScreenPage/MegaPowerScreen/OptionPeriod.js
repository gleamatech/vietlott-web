import React from "react";
import { Fragment } from "react";

const OptionPeriod = ({ tabs, handleClickTab }) => {
  return (
    <Fragment>
      <div className="flex justify-between w-full mt-2 ">
        {tabs.map((item, index) => (
          <div
            key={index}
            onClick={handleClickTab(item, index)}
            className={`capitalize h-full px-2 py-1 transition ease-in-out duration-300 text-center bg-white rounded-lg text-xl${
              item.current ? " border-1 border-red-500 text-center" : ""
            }`}
            style={{ width: "49%" }}
          >
            {item.title}
          </div>
        ))}
      </div>
    </Fragment>
  );
};

export default OptionPeriod;
