import React from "react";
import IconEva from "components/share/common/IconEva";
import ShowPDF from "components/share/common/ContentModal/ShowPDF";

const RuleRandom = () => {
  const handleShowPdf = (event) => {
    event.stopPropagation();
    return window.openModal({
      content: <ShowPDF urlPDF={"139.180.219.58/tc.pdf"} />,
      isShowHeader: false,
    });
  };

  return (
    <div
      onClick={handleShowPdf}
      className="p-0.5 flex items-center bg-white rounded"
    >
      <IconEva name="award-outline" width={"20px"} height={"20px"} />
      Cơ cấu
    </div>
  );
};

export default RuleRandom;
