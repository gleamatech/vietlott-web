import React, { useState, Fragment, useEffect } from "react";
import { Disclosure, Menu, Transition } from "@headlessui/react";
import * as eva from "eva-icons";
import { useRouter } from "next/router";
import Link from "next/link";
import { classNames } from "utils/functions";
import IconEva from "./IconEva";

const NavBar = () => {
  const router = useRouter();
  const navigation = [
    { name: "Trang Chủ", href: "/" },
    { name: "Lịch Sử", href: "/history" },
    { name: "Kết Quả", href: "/result" },
    { name: "Tài Khoản", href: "/profile" },
  ];

  const handleClickNav = (item, index) => () => {
    router.push(item.href);
  };

  const iconEva = (name, color) => {
    return <i data-eva={name} data-eva-fill={color}></i>;
  };
  const iconsNavbar = [iconEva, iconEva, iconEva, iconEva];
  const nameIcons = [
    "home-outline",
    "clock-outline",
    "tv-outline",
    "heart-outline",
  ];

  useEffect(() => {
    eva.replace();
  }, []);

  return (
    <Disclosure
      as="nav"
      className="fixed bottom-0 left-0 right-0 z-40 bg-white"
    >
      <div className="container mx-auto border-b-1 sm:border-0 sm:px-6 lg:px-8">
        <div className="flex ">
          {navigation.map((item, index) => (
            <Link href={item.href} key={index} className="w-1/4">
              <a
                onClick={handleClickNav(item, index)}
                className={classNames(
                  item.href === router.route
                    ? "text-blue-700 border-t-4 border-blue-700 "
                    : "text-gray-500 border-t-4 border-white ",
                  "w-full text-xl py-0.5 text-center font-normal flex flex-col items-center"
                )}
                aria-current={item.current ? "page" : undefined}
              >
                {iconsNavbar[index](
                  nameIcons[index],
                  item.href === router.route ? "#1D4ED8" : "#6b7280"
                )}
                <div className="pt-0.2"> {item.name}</div>
              </a>
            </Link>
          ))}
        </div>
      </div>
    </Disclosure>
  );
};

export default React.memo(NavBar);
