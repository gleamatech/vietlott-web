import React, { useEffect, useState } from "react";

const Select = ({
  options,
  defaultValue,
  label,
  onChangeSelect,
  isGetAllData,
  ...props
}) => {
  const [value, setValue] = useState("");

  useEffect(() => {
    if (isGetAllData) {
      const data = options.find((option) => option.value == defaultValue.value);
      return setValue(data.value);
    }
    return setValue(defaultValue);
  }, [defaultValue]);

  const onChange = () => (e) => {
    const { value } = e.target;
    setValue(value);
    if (isGetAllData) {
      const data = options.find((option) => option.value == value);
      return onChangeSelect(data);
    }
    onChangeSelect(value);
  };

  return (
    <div>
      {label ? <span className="text-gray-700 text-lg">{label}</span> : null}
      <select
        value={value}
        className="form-select block w-full text-xl rounded border-none bg-gray-50 appearance-none"
        onChange={onChange()}
      >
        {options.map((item, index) => (
          <option value={item.value} key={index}>
            {item.text}
          </option>
        ))}
      </select>
    </div>
  );
};

export default Select;
