import React, { Fragment } from "react";

const Notification = ({
  titleModal,
  textModal,
  titleSubmit,
  titleCancel,
  handleOnClickSubmit,
}) => {
  const handleCloseModal = () => {
    window.closeModal();
  };
  return (
    <Fragment>
      <h1 className="text-black text-2xl">{titleModal}</h1>
      <p className="mt-1">{textModal}</p>
      <div className="modal-action">
        {titleCancel ? (
          <Fragment>
            <label onClick={handleCloseModal} className="mr-1">
              {titleCancel}
            </label>
            <label className="mr-1" onClick={handleOnClickSubmit}>
              {titleSubmit}
            </label>
          </Fragment>
        ) : (
          <Fragment>
            <label onClick={handleCloseModal} className="mr-1">
              {titleSubmit}
            </label>
          </Fragment>
        )}
      </div>
    </Fragment>
  );
};

export default React.memo(Notification);
