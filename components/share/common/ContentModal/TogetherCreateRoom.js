import ListTab from "components/ScreenPage/TogetherScreen/ListTab";
import TableSelect from "components/ScreenPage/TogetherScreen/TableSelect";
import Button from "components/share/common/Button";
import BaseAPI from "config/BaseApi";
import SocketServices from "config/socket-client";
import { lotteryType, megaPrice, powerPrice } from "constants/lottery";
import { get } from "lodash";
import React, {
  forwardRef,
  Fragment,
  useImperativeHandle,
  useMemo,
  useState,
} from "react";
import NumberFormat from "react-number-format";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";
import { formatCurrency } from "utils/functions";
import Select from "../Select";

const TogetherCreateRoom = ({ lotteryMega }, ref) => {
  const user = useSelector((state) => state.user.user);
  const [tabActive, setTabActive] = useState(lotteryType.mega);
  const [selectValue, setSelectValue] = useState(lotteryMega[0].value);
  const [price, setPrice] = useState("");
  const [listNumberSelect, setListNumberSelect] = useState([]);

  const selectList = useMemo(() => {
    const arrTemp = tabActive === lotteryType.mega ? megaPrice : powerPrice;
    const listOptions = arrTemp.filter((item) => item.value !== 6);
    return listOptions;
  });

  const packagePrice = useMemo(() => {
    const type = parseInt(selectValue);
    const selectedOption = selectList.find((item) => item.value === type);
    setPrice("");
    return selectedOption.money;
  }, [selectValue]);

  const pricePerNum = useMemo(() => {
    const type = parseInt(selectValue);
    return Math.floor(packagePrice / type);
  }, [selectValue]);

  const isLow = useMemo(() => {
    return parseFloat(price) < parseFloat(pricePerNum);
  }, [selectValue, price]);

  const maxSelect = useMemo(() => {
    let option = Math.floor(parseFloat(price) / parseFloat(pricePerNum));
    return (option = option === 0 ? 1 : option);
  }, [price, pricePerNum]);

  const renderNotes = useMemo(() => {
    if (price && price <= packagePrice) {
      return (
        <p className="text-center text-lg text-yellow-500 mt-1">
          {isLow
            ? `Bạn được chọn tối đa ${maxSelect} số, 
Số của bạn sẽ không được hiển thị do thấp hơn mức tối thiểu`
            : `Bạn được chọn tối đa ${maxSelect} số`}
        </p>
      );
    } else if (price > packagePrice) {
      return (
        <p className="text-center text-lg text-yellow-500 mt-1">
          Bạn đã nhập nhiều hơn số tiền vé
        </p>
      );
    } else {
      return null;
    }
  }, [price]);

  const handleSelectNumber = (list) => {
    setListNumberSelect(list);
  };

  const renderListNumber = useMemo(() => {
    const numberList =
      tabActive === lotteryType.mega ? new Array(45) : new Array(55);
    return (
      <TableSelect
        numberSelect={listNumberSelect}
        numberList={numberList}
        maxSelect={maxSelect}
        handleUpdateList={handleSelectNumber}
      />
    );
  }, [price]);

  const handleOnChangeSelect = (e) => {
    setSelectValue(e);
  };

  const handleClickTab = (tab) => () => {
    setTabActive(tab.slug);
  };

  const handleInput = () => (e) => {
    setPrice(e.floatValue);
  };

  useImperativeHandle(ref, () => ({
    async onCreateNewRoom() {
      if (
        listNumberSelect.length === maxSelect &&
        price &&
        price <= packagePrice
      ) {
        const socketInstance = SocketServices.getSocketInstance();
        const createObject = {
          gameType: tabActive,
          package: parseInt(selectValue),
          createdBy: user._id,
        };
        const response = await BaseAPI.post("/room", createObject);
        const roomId = get(response, "data.doc._id");
        if (response.status && roomId) {
          const joinObject = {
            roomId: roomId,
            price,
            nums: listNumberSelect,
          };
          socketInstance.emit("JOIN_ROOM", JSON.stringify(joinObject));
          window.closeModal();
          toast.success("Tham gia thành công, chúc quý khách may mắn");
        }
      } else {
        return toast.warning("Vui lòng kiểm tra dữ liệu");
      }
    },
  }));
  return (
    <Fragment>
      <ListTab tabActive={tabActive} handleClickTab={handleClickTab} />
      <div className="w-full flex flex-col  mt-1 rounded-md">
        <label className="text-base">Bao</label>
        <Select options={selectList} onChangeSelect={handleOnChangeSelect} />
      </div>
      <div className="w-full flex flex-col  mt-1 rounded-md">
        <label className="text-base mb-0.5">Nhập số tiền bạn muốn gộp</label>
        <NumberFormat
          displayType="input"
          onValueChange={handleInput()}
          value={price}
          className="bg-gray-100 pl-0.5 h-3 input text-lg"
          placeholder={`Mỗi số có trị giá ${formatCurrency(pricePerNum)}`}
          thousandSeparator
        />
      </div>
      {renderNotes}
      <div
        className={`w-full flex flex-col  mt-1 rounded-md ${
          price && price <= packagePrice ? "" : "invisible"
        }`}
      >
        {renderListNumber}
      </div>
    </Fragment>
  );
};

export default forwardRef(TogetherCreateRoom);
