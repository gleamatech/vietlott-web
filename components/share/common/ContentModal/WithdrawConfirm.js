import BaseAPI from "config/BaseApi";
import { useRouter } from "next/router";
import React, { Fragment } from "react";
import { toast } from "react-toastify";
const WithdrawConfirm = ({
  titleModal,
  textModal,
  titleSubmit,
  titleCancel,
  withdrawType,
  withdrawTo,
  info,
  amount,
  password,
}) => {
  const router = useRouter();
  const handleSendWithdraw = async () => {
    try {
      const response = await BaseAPI.post("/withdraw", {
        withdrawType,
        withdrawTo,
        info,
        amount,
        password,
      });
      if (response.status == true) {
        router.reload();
        toast.success("Đã đặt lệnh rút tiền thành công");
      } else {
        return toast.error(`Lỗi ${response.data.msg}`);
      }
    } catch (error) {
      toast.error("Lỗi hệ thống", response.data.msg);
    }
  };
  const handleCloseModal = () => {
    window.close();
  };
  return (
    <Fragment>
      <h1 className="text-black text-2xl">{titleModal}</h1>
      <p className="mt-1">{textModal}</p>
      <div className="modal-action">
        <label onClick={handleCloseModal} className="mr-1">
          {titleCancel}
        </label>
        <label className="mr-1" onClick={handleSendWithdraw}>
          {titleSubmit}
        </label>
      </div>
    </Fragment>
  );
};

export default React.memo(WithdrawConfirm);
