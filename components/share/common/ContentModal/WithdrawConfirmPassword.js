import Button from "components/share/common/Button";
import React, { Fragment, useState } from "react";
import { toast } from "react-toastify";
import IconEva from "../IconEva";
import WithdrawConfirm from "./WithdrawConfirm";
const WithdrawConfirmPassword = ({
  withdrawType,
  withdrawTo,
  info,
  amount,
}) => {
  const [password, setPassword] = useState();
  const handleOnChangePassword = (e) => {
    setPassword(e.target.value);
  };
  const handleConfirmWithdraw = () => {
    if (password) {
      window.openModal({
        content: (
          <WithdrawConfirm
            titleModal={"Xác nhận"}
            textModal={
              "Xác nhận thao tác rút tiền, vui lòng kiểm tra kỹ, không thể hủy bỏ sau khi xác nhận"
            }
            titleSubmit={"Đồng ý"}
            titleCancel={"Quay lại"}
            withdrawType={withdrawType}
            withdrawTo={withdrawTo}
            info={info}
            amount={amount}
            password={password}
          />
        ),
        isShowHeader: false,
      });
    } else {
      toast.error("Vui lòng nhập mật khẩu");
    }
  };
  const closeModal = () => {
    window.closeModal();
  };
  return (
    <Fragment>
      <h1 className="text-black text-2xl">Vui lòng xác nhận mật khẩu</h1>
      <input
        onChange={handleOnChangePassword}
        type="password"
        placeholder="Nhập mật khẩu để rút tiền"
        className="bg-gray-100 flex justify-center mt-1 input input-bordered w-full pl-1.5 text-xl"
      ></input>
      <div className="modal-action">
        <Button onClick={handleConfirmWithdraw} className="w-full">
          Rút tiền
        </Button>
      </div>
      <label
        onClick={closeModal}
        className=" absolute top-1 right-1 cursor-pointer"
      >
        <IconEva name="close-outline" width="20px" height="20px" />
      </label>
    </Fragment>
  );
};

export default React.memo(WithdrawConfirmPassword);
