import { useRouter } from "next/router";
import React, { useEffect, useMemo } from "react";
import IconEva from "../IconEva";
const ShowPDF = ({ urlPDF }) => {
  const handleCloseModal = () => {
    window.closeModal();
  };
  const router = useRouter();
  const renderLinkPdf = useMemo(() => {
    if (urlPDF) return { linkPdf: urlPDF };
    let linkPdf, src;
    switch (router.pathname) {
      case "/keno":
        linkPdf = "139.180.219.58/the-le-keno.pdf";
        break;
      case "/power":
        linkPdf = "139.180.219.58/the-le-power-655.pdf";
        break;
      case "/mega":
        linkPdf = "139.180.219.58/mega-645.pdf";
        break;
      case "/together":
        linkPdf = "139.180.219.58/3mien.pdf";
        break;
      case "/auth/register":
        linkPdf = "139.180.219.58/dieu_khoan_su_dung.pdf";
        break;
    }
    return { linkPdf };
  });

  const renderPdf = useMemo(() => {
    return (
      <object
        className="w-full h-full"
        data={renderLinkPdf.linkPdf}
        type="application/pdf"
      >
        <iframe
          className="w-full h-full"
          src={`https://docs.google.com/viewer?url=${renderLinkPdf.linkPdf}&embedded=true`}
        ></iframe>
      </object>
    );
  }, [router, urlPDF]);

  return (
    <div style={{ height: "60vh" }}>
      <div style={{ height: "90%", marginTop: "5%" }}>
        <div className="text-2xl mb-1 font-medium">Cơ cấu và điều khoản</div>
        <div className="w-full h-full rounded-xl">{renderPdf}</div>
      </div>
      <label
        className="absolute top-1 right-1 cursor-pointer"
        onClick={handleCloseModal}
      >
        <IconEva name="close-outline" width="20px" height="20px" />
      </label>
    </div>
  );
};

export default React.memo(ShowPDF);
