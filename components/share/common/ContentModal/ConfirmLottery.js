import DeliveryInfo from "components/share/lottery/DeliveryInfo";
import BaseAPI from "config/BaseApi";
import { dist, ward } from "constants/infoDelivery";
import { get } from "lodash";
import { useRouter } from "next/router";
import React, { Fragment, useEffect, useState, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import { setUser } from "store/user";
import { formatCurrency } from "utils/functions";
import Button from "../Button";
import Select from "../Select";

const ConfirmLottery = ({
  listTable,
  selectPeriod,
  total,
  gameType,
  tabs,
  isRandom,
  valueTable,
  valueLottery,
}) => {
  const dataProvince = [
    { text: "TP. Hồ Chí Minh", value: 1 },
    { text: "Khu vực khác", value: 2 },
  ];
  const arrDelivery = ["store", "ship", "take"];

  const router = useRouter();
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user.user);
  const lottery = useSelector((state) => state.lottery);
  const { kenoData, megaData, powerData } = lottery;

  const [area, setArea] = useState({ text: "TP. Hồ Chí Minh", value: 1 });
  const [dataDist, setDataDist] = useState([]);
  const [distId, setDistId] = useState(null);
  const [dataWard, setDataWard] = useState([]);
  const [wardId, setWardId] = useState(null);
  const [address, setAddress] = useState("");
  const [showAddressForm, setShowAddressForm] = useState(false);
  const [disableButton, setDisableButton] = useState(true);
  const [valueDelivery, setValueDelivery] = useState(0);
  const [loading, setLoading] = useState(false);
  const [shipType, setShipType] = useState(null);
  const [feeShip, setFeeShip] = useState([
    { name: "Phí giao hàng", price: 18500 },
  ]);
  const [delivery, setDelivery] = useState({});

  const isKeno = useMemo(() => {
    return gameType === "keno";
  }, [gameType]);

  const listService = useMemo(() => {
    if (isKeno) {
      return [{ display: "Lưu tại cửa hàng", value: 1 }];
    }
    return [
      { display: "Lưu tại cửa hàng", value: 1 },
      { display: "Giao hàng", value: 2 },
      { display: "Đến nhận tại cửa hàng", value: 3 },
    ];
  }, [gameType]);

  const getCurrentPeriod = useMemo(() => {
    switch (gameType) {
      case "keno":
        return get(kenoData, "current_ky", "");
      case "power":
        return get(powerData, "power.ky", "");
      case "mega":
        return get(megaData, "mega.ky", "");
    }
  }, [lottery]);

  const handleDelivery = (e) => {
    setValueDelivery(e.target.value);
    const valueShip = e.target.value;
    setShipType(arrDelivery[valueShip - 1]);
    if (valueShip == 2) {
      setFeeShip([{ name: "Phí giao hàng", price: 18500 }]);
      setShowAddressForm(true);
    } else {
      setDelivery({});
      setFeeShip([]);
      setShowAddressForm(false);
    }
  };

  const handleInputInfo = (e) => {
    const addressInfo = e.target.value;
    let nameDistrict = "",
      nameWard = "";
    setAddress(addressInfo);
    if (area.value === 2) {
      setFeeShip([{ name: "Phí giao hàng", price: 28000 }]);
    } else {
      nameDistrict = dist.find(
        (item) => item.DistrictID === distId
      ).DistrictName;
      nameWard = ward.find((item) => item.WardCode == wardId).WardName;

      setFeeShip([{ name: "Phí giao hàng", price: 18500 }]);
    }
    if (e.target.value == "") {
      setDisableButton(true);
    } else {
      setDelivery({
        area: area.value === 2,
        dist: distId,
        ward: wardId,
        address: `${address},${nameWard},${nameDistrict}`,
      });

      setDisableButton(false);
    }
  };

  const onChangeArea = (event) => {
    setArea(dataProvince[event - 1]);
    if (event === "1") {
      setFeeShip([{ name: "Phí giao hàng", price: 18500 }]);
      setDataDist(dist);
      setAddress("");
      setArea({ text: "TP. Hồ Chí Minh", value: 1 });
      setDistId(1442);
      setWardId(20110);
      setDataDist(dist);
      setDataWard(ward.filter((item) => item.DistrictID === 1442));
    } else if (event === "2") {
      setArea({ text: "Khu vực khác", value: 2 });
      setDataDist([]);
      setDataWard([]);
      setAddress("");
      setDistId(null);
      setWardId(null);
      setFeeShip([{ name: "Phí giao hàng", price: 28000 }]);
      setDelivery({
        area: true,
        dist: null,
        ward: null,
        address: "",
      });
    }
  };

  const onChangeDistrict = (event) => {
    const districtId = Number(event);
    const firstWardId = ward.filter((item) => item.DistrictID === districtId)[0]
      .WardCode;
    const nameDistrict = dist.find(
      (item) => item.DistrictID === districtId
    ).DistrictName;
    const nameWard = ward.find((item) => item.WardCode == firstWardId).WardName;
    setDelivery({
      area: area == 2,
      dist: districtId,
      ward: Number(firstWardId),
      address: `${address},${nameWard},${nameDistrict}`,
    });
    setDistId(districtId);
    setDataWard(ward.filter((item) => item.DistrictID === districtId));
  };

  const onChangeWard = (event) => {
    const wardID = Number(event);
    const nameWard = ward.find((item) => item.WardCode == wardID).WardName;
    const districtId = ward.find((item) => item.WardCode == wardID).DistrictID;
    const nameDistrict = dist.find(
      (item) => item.DistrictID === districtId
    ).DistrictName;
    setDelivery({
      area: area.value == 2,
      dist: distId,
      ward: wardID,
      address: `${address},${nameWard}, ${nameDistrict}`,
    });
    setWardId(wardID);
  };

  const updateInfoUser = async () => {
    const response = await BaseAPI.post("/user/me");
    if (response.status) {
      dispatch(setUser(response));
    }
  };

  const handleBuyLottery = async () => {
    setLoading(true);
    const arrLottery = listTable.map((item) => ({
      price: item.money,
      subGame: get(item, "subGame[0]", ""),
      numOfNums: get(item, "listSelect", []).length,
      nums: get(item, "listSelect", []),
      lon: false,
      nho: false,
      chan: false,
      le: false,
      plus: false,
    }));
    try {
      const response = await BaseAPI.post("/lottery", {
        address: `${address}`,
        shipType,
        fee: feeShip,
        delivery,
        isFromWeb: true,
        isPrint: false,
        isUseBalance: true,
        ky: getCurrentPeriod,
        lottery: arrLottery,
        lotteryType: gameType,
        name: user.name,
        phoneNumber: user.phoneNumber,
        nums: Number(selectPeriod),
        price: total,
        isTC: isRandom,
        numsTicket: valueLottery,
        numsTable: valueTable,
        isSame: tabs === "same",
      });
      setLoading(false);
      if (response.status) {
        toast.success("Mua vé thành công");
        router.push("/success");
        window.closeModal();
        updateInfoUser();
      } else toast.error("Lỗi mua vé");
    } catch (error) {
      setLoading(false);
      toast.error("Lỗi hệ thống");
    }
  };
  useEffect(() => {
    //auto check delivery
    setValueDelivery(1);
    setShipType(arrDelivery[0]);

    setArea({ text: "TP. Hồ Chí Minh", value: 1 });
    setDistId(1442);
    setWardId(20110);
    dist.map(
      (item) => (
        (item.value = item.DistrictID), (item.text = item.DistrictName)
      )
    );
    ward.map(
      (item) => ((item.value = item.WardCode), (item.text = item.WardName))
    );
    setDataDist(dist);
    setDataWard(ward.filter((item) => item.DistrictID === 1442));
  }, []);
  return (
    <Fragment>
      <DeliveryInfo arrInfo={listService} onClickDelivery={handleDelivery} />
      <div className={showAddressForm ? "" : "invisible"}>
        <div className="mb-1">
          <Select
            options={dataProvince}
            label={"Khu vực"}
            onChangeSelect={onChangeArea}
          />
        </div>
        <div
          className={`${area.value === 2 ? "flex flex-col-reverse mb-1" : ""}`}
        >
          <div className={`${area.value === 2 ? "invisible" : ""}`}>
            <div className="mb-1">
              <Select
                options={dataDist}
                label={"Quận"}
                onChangeSelect={onChangeDistrict}
              />
            </div>
            <div className="mb-1">
              <Select
                options={dataWard}
                label={"Phường"}
                onChangeSelect={onChangeWard}
              />
            </div>
          </div>

          <div className="mb-1">
            <span className="text-gray-700">Địa chỉ chi tiết</span>
            <input
              className="form-select block w-full mt-1 text-xl rounded border-none bg-gray-50 appearance-none input input-primary focus:border"
              style={{ height: "27px" }}
              value={address}
              onChange={handleInputInfo}
              placeholder="Nhập địa chỉ giao hàng"
            />
          </div>
        </div>
      </div>
      <div className="mt-2">
        <div className="flex justify-between">
          <p>Vé mua</p>
          <span className="text-red-600">{formatCurrency(total)}</span>
        </div>
        <div className="flex justify-between">
          <p>Phí giao hàng</p>
          <span className=" text-red-600">
            {valueDelivery == 2 ? formatCurrency(feeShip[0].price) : 0}
          </span>
        </div>
        <div className="flex justify-between">
          <p>Thành tiền</p>
          <span className=" text-red-600">
            {valueDelivery == 2
              ? formatCurrency(total + feeShip[0].price)
              : formatCurrency(total)}
          </span>
        </div>
      </div>
      <div className="modal-action">
        <Fragment>
          <Button
            className="w-full"
            onClick={handleBuyLottery}
            disabled={(disableButton && showAddressForm) || loading}
          >
            Hoàn tất mua hàng
          </Button>
        </Fragment>
      </div>
    </Fragment>
  );
};

export default React.memo(ConfirmLottery);
