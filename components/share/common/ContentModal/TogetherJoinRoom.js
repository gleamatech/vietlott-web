import TableSelect from "components/ScreenPage/TogetherScreen/TableSelect";
import SocketServices from "config/socket-client";
import { flatten } from "lodash";
import React, {
  forwardRef,
  Fragment,
  useImperativeHandle,
  useMemo,
  useState,
} from "react";
import NumberFormat from "react-number-format";
import { toast } from "react-toastify";
import { formatCurrency } from "utils/functions";

const TogetherJoinRoom = ({ item }, ref) => {
  const [listNumberSelect, setListNumberSelect] = useState([]);
  const [price, setPrice] = useState("");

  const isMega = item.gameType === "mega";
  const joinNums = item.join.filter((it) => !it.lowCost).map((it) => it.nums);
  const uniqNums = [...new Set(flatten(joinNums))].sort((a, b) => a - b);

  const packagePrice = useMemo(() => {
    return item.price;
  });

  const totalJoin = useMemo(() => {
    return item.join.reduce((total, current) => {
      return total + parseFloat(current.price);
    }, 0);
  });

  const priceRemain = useMemo(() => {
    return packagePrice - totalJoin;
  });

  const pricePerNum = useMemo(() => {
    const type = item.package;
    return Math.floor(packagePrice / type);
  });

  const isLow = useMemo(() => {
    return parseFloat(price) < parseFloat(pricePerNum);
  }, [price]);

  const maxSelect = useMemo(() => {
    let option = Math.floor(parseFloat(price) / parseFloat(pricePerNum));
    return (option = option === 0 ? 1 : option);
  }, [price, pricePerNum]);

  const renderNotes = useMemo(() => {
    if (price && price <= priceRemain) {
      return (
        <p className="text-center text-lg text-yellow-500 mt-1">
          {isLow
            ? `Bạn được chọn tối đa ${maxSelect} số, 
Số của bạn sẽ không được hiển thị do thấp hơn mức tối thiểu`
            : `Bạn được chọn tối đa ${maxSelect} số`}
        </p>
      );
    } else if (price > priceRemain) {
      return (
        <p className="text-center text-lg text-yellow-500 mt-1">
          Bạn đã nhập nhiều hơn số tiền vé
        </p>
      );
    } else {
      return null;
    }
  }, [price]);

  const handleInput = () => (e) => {
    setPrice(e.floatValue);
  };

  const handleSelectNumber = (list) => {
    setListNumberSelect(list);
  };

  const renderListNumber = useMemo(() => {
    const numberList = isMega ? new Array(45) : new Array(55);
    return (
      <TableSelect
        numberSelect={listNumberSelect}
        numberList={numberList}
        maxSelect={maxSelect}
        handleUpdateList={handleSelectNumber}
      />
    );
  }, [price]);

  const renderListSelected = (list = []) => {
    return (
      <div className="mx-auto">
        <div className="flex w-full flex-wrap">
          {list.map((num, index) => (
            <div
              key={index}
              className="rounded-full h-3 w-3 flex items-center justify-center border-1 border-red-500 mx-1 my-0.5"
            >
              <span className="text-small">{num}</span>
            </div>
          ))}
        </div>
      </div>
    );
  };

  useImperativeHandle(ref, () => ({
    async onCreateNewRoom() {
      if (
        listNumberSelect.length === maxSelect &&
        price &&
        price <= priceRemain
      ) {
        const res = Math.abs(parseFloat(price) - priceRemain);
        if (parseFloat(price) < 10000) {
          return toast.warning("Vui lòng tham gia tối thiểu 10.000đ!");
        }
        if (res <= 5000 && res > 0) {
          return toast.warning("Vui lòng tham gia với mệnh giá khác!");
        }
        const socketInstance = SocketServices.getSocketInstance();
        const joinObject = {
          roomId: item._id,
          price: price,
          nums: listNumberSelect,
        };
        socketInstance.emit("JOIN_ROOM", JSON.stringify(joinObject));
        window.closeModal();
        toast.success("Tham gia thành công, chúc quý khách may mắn");
      } else {
        return toast.warning("Vui lòng kiểm tra lại dữ liệu");
      }
    },
  }));
  return (
    <Fragment>
      <div className="flex justify-between w-full mt-1 bg-gray-100 pb-1.5 pt-1.5 rounded-md">
        <div className="pl-2 my-auto">
          Tổng số:
          <span className="sub-text-colors ">{formatCurrency(totalJoin)}</span>
        </div>
        <div className="my-auto pr-2">
          Còn lại:
          <span className="sub-text-colors">{formatCurrency(priceRemain)}</span>
        </div>
      </div>
      <div className="flex justify-between flex-col w-full mt-1 bg-gray-100 pb-1.5 pt-1.5 rounded-md">
        <div className="flex justify-between w-full">
          <div className="pl-2 my-auto">Các số đã chọn</div>
          <div className="my-auto pr-2">Bao {item.package}</div>
        </div>
        {renderListSelected(uniqNums)}
      </div>
      <div className="w-full flex flex-col  mt-1 rounded-md">
        <label className="text-base mb-0.5">Nhập số tiền bạn muốn góp</label>
        <NumberFormat
          displayType="input"
          onValueChange={handleInput()}
          value={price}
          className="bg-gray-100 pl-0.5 h-3 input text-lg"
          placeholder={`Mỗi số có trị giá ${formatCurrency(pricePerNum)}`}
          thousandSeparator
        />
      </div>
      {renderNotes}
      {
        <div
          className={`w-full flex flex-col  mt-1 rounded-md ${
            price && price <= priceRemain ? "" : "invisible"
          }`}
        >
          {renderListNumber}
        </div>
      }
    </Fragment>
  );
};

export default forwardRef(TogetherJoinRoom);
