import LastModalKeno from "components/ScreenPage/KenoScreen/LastModalKeno";
import Select from "components/share/common/Select";
import Button from "components/share/common/Button";
import React, { Fragment, useEffect, useRef, useState, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import {
  addCopyLotteryKeno,
  addSelectLotteryKeno,
  addSelectPriceKeno,
} from "store/lottery";
import ListNumbers from "../../lottery/ListNumbers";
import IconEva from "../IconEva";
import { LIST_TYPE_LOTTERY, lotteryType } from "constants/lottery";
import { get } from "lodash";

const BoxSelectNumber = ({
  index,
  data,
  isBigSmall = false,
  handleClickButton,
  gameType,
  moneySelectLottery,
}) => {
  const dispatch = useDispatch();
  const childRef = useRef();
  const copyLotteryKeno = useSelector((state) => state.lottery.copyLotteryKeno);
  const selectLotteryKeno = useSelector(
    (state) => state.lottery.selectLotteryKeno
  );

  const [checklist, setChecklist] = useState();
  const [selectPrice, setSelectPrice] = useState(10000);
  const [levelSelect, setLevelSelect] = useState(10);

  useEffect(() => {
    if (!!data.subGame.length) {
      setChecklist(data.subGame);
      setSelectPrice(data.money);
      dispatch(addSelectPriceKeno(data.money));
    } else if (!data.listSelect.length) {
      dispatch(addSelectLotteryKeno(10));
      dispatch(addSelectPriceKeno(10000));
    } else {
      dispatch(addSelectLotteryKeno(data.listSelect.length));
      setLevelSelect(data.listSelect.length);
      dispatch(addSelectPriceKeno(data.money));
      setSelectPrice(data.money);
      childRef.current.handleShowList();
    }
  }, []);

  const isKeno = useMemo(() => {
    return gameType === "keno";
  }, [gameType]);

  const dataGameType = useMemo(() => {
    return LIST_TYPE_LOTTERY[lotteryType[gameType]];
  }, [gameType]);

  const arrNumberSelected = useMemo(() => {
    const arrNumber = get(dataGameType, "number", []).fill("");
    return arrNumber;
  }, [dataGameType]);

  const listInfoLevel = useMemo(() => {
    return get(dataGameType, "levels", []);
  }, [dataGameType]);

  const listInfoPrice = useMemo(() => {
    return get(dataGameType, "prices", []);
  }, [dataGameType]);

  const handleSelectLevel = (level) => {
    setLevelSelect(Number(level));
    dispatch(addSelectLotteryKeno(Number(level)));
  };

  const handleSelectPrice = (e) => {
    setSelectPrice(Number(e));
    dispatch(addSelectPriceKeno(Number(e)));
  };

  const handleCheckList = (e) => {
    setChecklist(e.target.value);
  };

  const handleCancelLastBoxKeno = () => {
    data.subGame = [];
    data.money = 0;
    handleClickButton({ index, data });
    window.closeModal();
  };
  const handleCloseModal = () => {
    dispatch(addSelectLotteryKeno(10));
    window.closeModal();
  };
  const handleCancelModal = () => {
    // childRef.current.handleCancelButton();
    data.listSelect = [];
    data.money = 0;
    handleClickButton({ index, data });
    window.closeModal();
  };
  const handleDoneSelect = () => {
    if (
      selectLotteryKeno < childRef.current.dataListSelect().length &&
      isKeno
    ) {
      toast.error("Bậc số không hợp lệ, vui lòng kiểm tra lại để tiếp tục");
    } else {
      childRef.current.hanldeSubmitContinues();
      window.closeModal();
    }
  };

  const handleSelectListNumber = (list) => {
    if (isBigSmall) {
      if (!checklist) {
        return toast.warning("Vui lòng chọn cách chơi");
      }
      data.money = selectPrice;
      data.subGame = [checklist];
      handleClickButton({ index, data });
      return window.closeModal();
    }
    data.listSelect = list;
    if (isKeno) {
      data.level = selectLotteryKeno;
      data.money = !list.length ? 0 : selectPrice;
    } else {
      data.money = list.length == data.level ? moneySelectLottery : 0;
    }
    handleClickButton({ index, data });
  };

  const handleCopy = () => {
    if (isKeno) {
      dispatch(addCopyLotteryKeno(selectLotteryKeno));
    }
    childRef.current.onCopy();
  };
  const handlePaste = () => {
    if (isKeno) {
      dispatch(addSelectLotteryKeno(copyLotteryKeno));
    }
    childRef.current.onPaste();
  };

  return (
    <div>
      {isKeno && isBigSmall ? (
        <LastModalKeno
          listInfoPrice={listInfoPrice}
          handleSelectPrice={handleSelectPrice}
          handleCheckList={handleCheckList}
          handleCancelLastBoxKeno={handleCancelLastBoxKeno}
          handleContinue={handleSelectListNumber}
          data={data}
        />
      ) : (
        <Fragment>
          {isKeno ? (
            <Fragment>
              <div className="flex">
                <div className="w-full mr-2">
                  <Select
                    options={listInfoLevel}
                    label={"Bậc số"}
                    onChangeSelect={handleSelectLevel}
                    defaultValue={levelSelect}
                  />
                </div>
                <div className="w-full">
                  <Select
                    options={listInfoPrice}
                    label={"Mệnh giá"}
                    onChangeSelect={handleSelectPrice}
                    defaultValue={selectPrice}
                  />
                </div>
              </div>
              <div className="w-full mt-2">
                <ListNumbers
                  ref={childRef}
                  numbers={arrNumberSelected}
                  max={levelSelect}
                  handleClickButton={handleSelectListNumber}
                  handleSelectLevel={handleSelectLevel}
                  isKeno={isKeno}
                  data={data}
                  index={index}
                />
              </div>
            </Fragment>
          ) : (
            <Fragment>
              <p>Chọn số</p>
              <div className="w-full mt-2">
                <ListNumbers
                  ref={childRef}
                  numbers={arrNumberSelected}
                  max={data.level}
                  handleClickButton={handleSelectListNumber}
                  index={index}
                  data={data}
                />
              </div>
            </Fragment>
          )}
          <div className="modal-action flex flex-col items-center">
            <div className="flex flex-col w-full">
              <div className="flex justify-between w-full">
                <Button onClick={() => childRef.current.handleRandNum()}>
                  Máy chọn
                </Button>
                <Button onClick={handleCopy}>Sao chép</Button>
                <Button onClick={handlePaste}>Dán</Button>
                <Button buttonColor={"btn-error"} onClick={handleCancelModal}>
                  Hủy
                </Button>
              </div>
              <Button onClick={handleDoneSelect} className="w-full mt-1">
                Tiếp theo
              </Button>
            </div>
          </div>
        </Fragment>
      )}
      <label
        className="absolute top-1 right-1 cursor-pointer"
        onClick={handleCloseModal}
      >
        <IconEva name="close-outline" width="20px" height="20px" />
      </label>
    </div>
  );
};

export default React.memo(BoxSelectNumber);
