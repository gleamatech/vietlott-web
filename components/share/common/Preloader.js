import React from "react";
import Lottie from "react-lottie";
import preloader from "public/images/lotties/preloader.json";

const Preloader = ({ size = 200, ...props }) => {
  return (
    <div className="absolute w-screen h-screen flex items-center justify-center z-50 bg-gray-500 bg-opacity-60">
      <Lottie
        height={size}
        width={size}
        options={{
          loop: true,
          autoplay: true,
          animationData: preloader,
        }}
        {...props}
      />
    </div>
  );
};

Preloader.propTypes = {};

export default Preloader;
