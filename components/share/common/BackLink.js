import Link from "next/link";
import React from "react";
import IconEva from "./IconEva";

const BackLink = ({
  backLink = "#",
  type,
  title,
  subTitle,
  isSendMoney,
  titleLink = "#",
}) => {
  return (
    <div
      className={`w-full bg-white flex ${
        isSendMoney ? "flex-col" : null
      }  justify-between sticky top-0 z-10`}
    >
      <div className="flex py-0.5 items-center">
        <Link href={backLink}>
          <div className="ml-1 p-0.5">
            <IconEva name="arrow-back-outline" height="20px" width="20px" />
          </div>
        </Link>
        <span className="pl-0.5 text-gray-900">
          {`${title ? `${title}` : `Kết quả quay số  ${type}`}`}
        </span>
      </div>

      <Link href={titleLink}>
        <div className="flex items-center">
          {subTitle ? (
            <span className="pr-0.5">
              <IconEva name="clock-outline" height="20px" width="20px" />
            </span>
          ) : null}

          <div className="flex my-auto">
            <div className="flex justify-center items-center mr-1">
              {`${subTitle ? `${subTitle}` : ""}`}
            </div>
          </div>
        </div>
      </Link>
      {isSendMoney ? (
        <div className="p-2 border-t-1">
          <p className="leading-6">
            Để nạp tiền vào tài khoản quý khách vui lòng chuyển tiền vào các tài
            khoản ngân hàng sau với nội dung chuyển tiền
          </p>
          <p className="text-red-500 pt-1 text-center">{`NAP <Số điện thoại>`}</p>
        </div>
      ) : null}
    </div>
  );
};

export default React.memo(BackLink);
