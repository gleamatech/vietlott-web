import React from "react";

const Button = ({
  children,
  isLoading,
  disabled,
  buttonColor = "btn-info",
  colorText = "text-white",
  className,
  ...props
}) => {
  return (
    <button
      className={`btn rounded border-0 normal-case text-lg ${colorText} ${buttonColor} ${
        isLoading ? "loading" : ""
      } ${className}`}
      disabled={disabled}
      {...props}
    >
      {children}
    </button>
  );
};

export default Button;
