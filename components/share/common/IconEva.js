import React, { useEffect } from "react";
import * as eva from "eva-icons";

const IconEva = ({ name, color, height, width }) => {
  useEffect(() => {
    eva.replace();
  }, []);

  return (
    <div>
      <i
        data-eva={name}
        data-eva-fill={color}
        data-eva-height={height}
        data-eva-width={width}
      ></i>
    </div>
  );
};

export default IconEva;
