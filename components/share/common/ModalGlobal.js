import React, { useState, useEffect } from "react";
import ModalGeneral from "components/share/common/ModalGeneral";

const ModalGlobal = () => {
  const [modals, setModals] = useState({});

  useEffect(() => {
    window.openModal = handleOpenModal;
    window.closeModal = closeModal;
  }, []);

  const handleOpenModal = (props) => {
    setModals({ ...props, isOpen: true });
  };

  const closeModal = () => {
    setModals({ isOpen: false });
  };

  return <>{modals ? <ModalGeneral {...modals} /> : null}</>;
};

export default ModalGlobal;
