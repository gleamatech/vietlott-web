import React from "react";
import Header from "components/share/common/Header";

const Container = ({ children, renderRightView, iconRightView }) => {
  return (
    <div className="bg-gray-100 relative">
      <Header rightView={renderRightView} iconRightView={iconRightView} />
      <div
        className="container mx-auto flex flex-col items-center pt-2 px-1.5"
        style={{ maxHeight: "95vh" }}
      >
        {children}
      </div>
    </div>
  );
};

export default React.memo(Container);
