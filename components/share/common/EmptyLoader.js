import React from "react";
import Lottie from "react-lottie";
import emptyloader from "public/images/lotties/empty.json";

const EmptyLoader = ({ size = 100, ...props }) => {
  return (
    <div className="flex justify-center">
      <Lottie
        height={size}
        width={size}
        options={{
          loop: true,
          autoplay: true,
          animationData: emptyloader,
        }}
        {...props}
      />
    </div>
  );
};

EmptyLoader.propTypes = {};

export default EmptyLoader;
