import PropTypes from "prop-types";
import React from "react";

const InputField = (props) => {
  const { field, type, label, placeholder, disabled, isVerifyCode, value } =
    props;
  const { name } = field;

  return (
    <>
      {label && (
        <label
          htmlFor={name}
          className="block text-gray-500 text-lg font-normal"
        >
          {label}
        </label>
      )}
      <input
        type={type}
        id={name}
        {...field}
        value={value}
        placeholder={placeholder}
        disabled={disabled}
        className={`appearance-none text-xl ${
          isVerifyCode ? "py-1" : ""
        } px-1 border border-gray-200 bg-gray-50 rounded w-full text-gray-800 focus:outline-none focus:border-blue-100`}
        {...props}
      />
    </>
  );
};

InputField.propTypes = {
  field: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,

  type: PropTypes.string,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  disabled: PropTypes.bool,
};

InputField.defaultProps = {
  type: "text",
  label: "",
  placeholder: "",
  disabled: false,
};

export default InputField;
