import { startCase } from "lodash";
import { useRouter } from "next/router";
import React, { Fragment, useMemo } from "react";
import ShowPDF from "./ContentModal/ShowPDF";
import IconEva from "./IconEva";

const Header = ({
  sizeIcon = "30px",
  rightView,
  toggleModal,
  className,
  isModal,
  iconRightView,
}) => {
  const router = useRouter();

  const renderText = useMemo(() => {
    let title = "keno";
    let info = "Cơ cấu";
    switch (router.pathname) {
      case "/keno":
        title = "keno";
        break;
      case "/power":
        title = "power";
        break;
      case "/mega":
        title = "mega";
        break;
      case "/together":
        title = "Quay lại";
        info = "Cơ cấu";
        break;
    }
    return { title, info };
  });

  const handleShowPdf = () => {
    return window.openModal({
      content: <ShowPDF />,
      isShowHeader: false,
    });
  };

  const handleActions = () => {
    if (isModal) {
      return window.closeModal();
    }
    if (toggleModal) {
      return toggleModal();
    }
    return router.back();
  };

  const renderRightView = useMemo(() => {
    if (rightView) {
      return rightView;
    }
    return (
      <Fragment>
        <div
          className="flex justify-center items-center text-2xl"
          onClick={handleShowPdf}
        >
          {renderText.info}
        </div>
        {iconRightView ? (
          iconRightView
        ) : (
          <div
            onClick={handleShowPdf}
            className=" flex justify-center items-center mr-1 ml-0.5"
          >
            <IconEva name="award-outline" width={sizeIcon} height={sizeIcon} />
          </div>
        )}
      </Fragment>
    );
  }, [rightView]);

  return (
    <div
      className={`w-full bg-white flex justify-between sticky top-0 h-20 ${className} `}
    >
      <div className="flex ">
        <div
          onClick={handleActions}
          className=" flex justify-center items-center mr-0.5 ml-1"
        >
          <IconEva
            name="arrow-back-outline"
            width={sizeIcon}
            height={sizeIcon}
          />
        </div>
        <div className={`flex justify-center items-center text-2xl `}>
          {startCase(renderText.title)}
        </div>
      </div>
      <div className="flex">{renderRightView}</div>
    </div>
  );
};

export default Header;
