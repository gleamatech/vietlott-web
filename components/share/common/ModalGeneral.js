import React, { useMemo } from "react";
import Header from "components/share/common/Header";

const ModalGeneral = ({
  isOpen,
  content,
  isShowHeader = true,
  rightView,
  txtCancel,
  txtConfirm,
  isShowActions,
  handleConfirm,
  handleCancel,
}) => {
  const styleWrapModal = useMemo(() => {
    return isOpen
      ? { opacity: 1, pointerEvents: "auto", visibility: "visible" }
      : {};
  }, [isOpen]);

  const renderTransition = useMemo(() => {
    return `${isOpen ? "-translate-y-1/2" : "translate-y-0"}`;
  }, [isOpen]);

  return (
    <div className="modal" style={styleWrapModal}>
      <div
        className={`modal-box bg-white fixed top-1/2 transform rounded-2xl max-h-full	${renderTransition}`}
      >
        {isShowHeader ? (
          <Header isModal={true} className="p-0" rightView={rightView} />
        ) : null}
        <div className="p-1">{content}</div>
        {isShowActions && (
          <div className="flex justify-end">
            <button
              onClick={handleCancel}
              class="mr-1 inline-flex justify-center py-1 px-1 border border-transparent shadow-sm text-sm font-medium rounded-md text-grey hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            >
              {txtCancel}
            </button>
            <button
              onClick={handleConfirm}
              class="inline-flex justify-center py-1 px-1 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            >
              {txtConfirm}
            </button>
          </div>
        )}
      </div>
    </div>
  );
};

export default ModalGeneral;
