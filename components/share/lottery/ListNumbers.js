import { cloneDeep } from "lodash";
import React, { forwardRef, useImperativeHandle, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import { addListCopy, addSelectLotteryKeno } from "store/lottery";

const ListNumbers = (
  {
    numbers,
    max = 1,
    handleClickButton,
    index,
    isKeno,
    data,
    handleSelectLevel,
  },
  ref
) => {
  const [listSelect, setListSelect] = useState([]);
  const dispatch = useDispatch();
  let selectLotteryKeno = useSelector(
    (state) => state.lottery.selectLotteryKeno
  );
  const { listCopy } = useSelector((state) => state.lottery);
  const handleSelectNumber = (number) => {
    let arrTemp = cloneDeep(listSelect);
    if (arrTemp.includes(number)) {
      if (isKeno) {
        arrTemp = arrTemp.filter((num) => num !== number);
        handleSelectLevel(arrTemp.length);
        dispatch(addSelectLotteryKeno(arrTemp.length));
        return setListSelect(arrTemp);
      } else {
        arrTemp = arrTemp.filter((num) => num !== number);
        return setListSelect(arrTemp);
      }
    }
    if (isKeno) {
      if (arrTemp.length < 10) {
        arrTemp.push(number);
        handleSelectLevel(arrTemp.length);
        dispatch(addSelectLotteryKeno(arrTemp.length));
        setListSelect(arrTemp, index);
      } else {
        toast.warning("Bạn đã chọn đủ số");
      }
    } else {
      if (arrTemp.length < max) {
        arrTemp.push(number);
        setListSelect(arrTemp, index);
      } else {
        toast.warning("Bạn đã chọn đủ số");
      }
    }
  };

  useImperativeHandle(ref, () => ({
    handleRandNum() {
      let arr;
      if (!!listSelect.length) {
        arr = cloneDeep([]);
      } else arr = cloneDeep(listSelect);
      while (arr.length < max) {
        let numRand = Math.floor(Math.random() * numbers.length) + 1;
        if (!arr.includes(numRand)) {
          arr.push(numRand);
        }
      }
      setListSelect(arr, index);
    },
    hanldeSubmitContinues() {
      if (listSelect.length == max || listSelect.length == selectLotteryKeno) {
        handleClickButton(listSelect);
        dispatch(addSelectLotteryKeno(10));
      } else {
        dispatch(addSelectLotteryKeno(10));
        handleClickButton(listSelect);
      }
    },
    // handleCancelButton() {
    //   setListSelect([]);
    //   handleClickButton([]);
    // },
    onCopy() {
      if (!!listSelect.length) {
        window.navigator.clipboard.writeText(listSelect);
        dispatch(addListCopy(listSelect));
      }
    },
    onPaste() {
      if (!!listCopy.length) {
        if (isKeno) {
          handleSelectLevel(listCopy.length);
        }
        setListSelect(listCopy);
      }
    },
    handleOnChangeInputTogether() {
      setListSelect([]);
    },
    handleShowList() {
      if (data.listSelect.length) {
        setListSelect(data.listSelect);
      }
    },
    dataListSelect() {
      return listSelect;
    },
  }));
  return (
    <div className={`grid grid-cols-${Math.ceil(numbers.length / 10)} gap-0.5`}>
      {numbers.map((num, index) => (
        <label
          key={index}
          className={`mx-auto rounded-full h-3 w-3 flex items-center justify-center border-1 border-red-500 ${
            listSelect.includes(index + 1) ? "bg-red-500" : ""
          }`}
          onClick={() => handleSelectNumber(index + 1)}
        >
          <span
            className={`text-small ${
              listSelect.includes(index + 1) ? "text-gray-50" : ""
            }`}
          >
            {index + 1}
          </span>
        </label>
      ))}
    </div>
  );
};

export default forwardRef(ListNumbers);
