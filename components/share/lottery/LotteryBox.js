import Button from "components/share/common/Button";
import moment from "moment";
import React, {
  Fragment,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from "react";
import { useSelector } from "react-redux";
import { formatDate } from "utils/functions";
import { get } from "lodash";

const LotteryType = ({
  data,
  isShowButton,
  isDetailLottery,
  handleClickButton,
}) => {
  const duration = useSelector((state) => state.lottery.kenoData.duration);
  const lottery = useSelector((state) => state.lottery);
  const { kenoData, megaData, powerData } = lottery;
  const [fmt, setFmt] = useState("mm:ss");

  const handleClick = useCallback(() => {
    handleClickButton(data);
  }, [data]);

  const renderCurrentData = useMemo(() => {
    let currentPeriod;
    let timeResult;
    switch (data.slug) {
      case "keno":
        currentPeriod = kenoData.current_ky;
        break;
      case "power":
        currentPeriod = get(powerData, "power.ky", "loading");
        timeResult = get(powerData, "next", "loading");
        break;
      case "mega":
        currentPeriod = get(megaData, "mega.ky", "loading");
        timeResult = get(megaData, "next", "loading");
        break;
    }

    return { currentPeriod, timeResult };
  }, [lottery]);
  useEffect(() => {
    if (duration > 3600) {
      setFmt("hh:mm:ss");
    } else {
      setFmt("mm:ss");
    }
  }, [duration]);

  const renderContentBoxRightKeno = useMemo(() => {
    return (
      <div className="flex flex-col text-center rounded-lg min-w-10 pr-1.5 pl-1.5 pt-1 pb-1 box-coutdown">
        <p className="text-sub-heading">Thời gian còn lại</p>
        <p className="sub-text-color">
          {moment
            .utc(moment.duration({ seconds: duration }).asMilliseconds())
            .format(fmt)}
        </p>
      </div>
    );
  }, [duration]);

  const renderContentBoxRight = useMemo(() => {
    if (data.slug == "together" || data.slug == "loto") {
      return (
        <React.Fragment>
          <p className="sub-text-color text-4xl mt-2">
            {data.slug == "together" ? "Bao 3 Miền" : "Lô Tô Đồng Nai"}
          </p>
          <p className="text-sub-heading">
            {data.slug == "together" ? "Mua chung dễ trúng" : "Đang cập nhật"}
          </p>
        </React.Fragment>
      );
    } else if (data.slug == "power") {
      return (
        <div className="flex flex-col text-center rounded-lg min-w-10 pr-1.5 pl-1.5 pt-1 pb-1 box-coutdown">
          <p className="text-sub-heading">Thời gian quay thưởng</p>
          <p className="sub-text-color">
            {formatDate(renderCurrentData.timeResult)}
          </p>
        </div>
      );
    } else if (data.slug == "mega") {
      return (
        <div className="flex flex-col text-center rounded-lg min-w-10 pr-1.5 pl-1.5 pt-1 pb-1 box-coutdown">
          <p className="text-sub-heading">Thời gian quay thưởng</p>
          <p className="sub-text-color">
            {formatDate(renderCurrentData.timeResult)}
          </p>
        </div>
      );
    }
  }, [renderCurrentData.timeResult]);

  return (
    <div className="flex justify-between items-center bg-white w-full p-1.5 rounded-lg h-64">
      <img
        className="mr-1"
        src={data.image}
        width="90px"
        height="90px"
        alt={data.name}
      />
      <div className="flex flex-col items-center min-w-18">
        {isDetailLottery ? (
          <p className="text-sub-heading">
            Kỳ quay{" "}
            <span className="text-sub-heading ">
              #{renderCurrentData.currentPeriod}
            </span>
          </p>
        ) : data.slug == "keno" ? (
          <React.Fragment>
            <p className="text-small mb-1">{data.time}</p>
          </React.Fragment>
        ) : (
          <React.Fragment>
            <p className="text-small">{data.date}</p>
            <p className="text-small mb-1">{data.time}</p>
          </React.Fragment>
        )}
        {data.slug == "keno"
          ? renderContentBoxRightKeno
          : renderContentBoxRight}

        {data.slug == "keno" && isShowButton ? (
          <Fragment>
            <div className="text-small mt-0.5">{data.date}</div>
            <div className="text-small mt-0.25">
              Giải thưởng lên đến <span>2 Tỷ đông</span>
            </div>
            <Button className="w-full mt-0.5" onClick={handleClick}>
              Mua Ngay
            </Button>
          </Fragment>
        ) : isShowButton && data.slug != "loto" ? (
          <Button className="w-full mt-1" onClick={handleClick}>
            Mua Ngay
          </Button>
        ) : null}
      </div>
    </div>
  );
};

export default React.memo(LotteryType);
