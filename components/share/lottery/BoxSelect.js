import Button from "components/share/common/Button";
import { get } from "lodash";
import React, { Fragment, useMemo, useState } from "react";
import { formatCurrency } from "utils/functions";
import IconEva from "../common/IconEva";

export const BoxSelect = ({ index, data, isBigSmall, handleOpenModal }) => {
  const title = useMemo(() => {
    const objTitle = {
      0: "A",
      1: "B",
      2: "C",
      3: "D",
      4: "E",
      5: "F",
    };
    return objTitle[index];
  }, [index]);

  const renderBodyBoxSelect = () => {
    if (data.subGame.length && isBigSmall) {
      return <Fragment>{get(data, "subGame[0]", "")}</Fragment>;
    } else if (data.listSelect.length) {
      return (
        <Fragment>
          <div className="mx-auto">
            <div className="flex w-full flex-wrap">
              {data.listSelect.map((num, index) => (
                <div
                  key={index}
                  className="rounded-full h-3 w-3 flex items-center justify-center border-1 border-red-500 mx-1 my-0.5"
                >
                  <span className="text-small">{num}</span>
                </div>
              ))}
            </div>
          </div>
        </Fragment>
      );
    } else {
      return (
        <Fragment>
          <IconEva name="archive-outline" width="30px" height="30px" />
          <p>Bạn chưa chọn số cho vé này</p>
        </Fragment>
      );
    }
  };

  return (
    <div className="w-full bg-white p-1.5 rounded-lg">
      <div className="flex justify-between pb-1.5 border-b">
        <p>Bảng chọn {title}</p>
        <p className="sub-text-color">{formatCurrency(data.money)}</p>
      </div>
      <div className="flex flex-col items-center pt-1.5 pb-1.5 ">
        {renderBodyBoxSelect()}
      </div>
      <Button
        onClick={handleOpenModal({ index, data })}
        className="w-full pt-0.5 pb-0.5 flex"
      >
        Chọn vé ngay
        <IconEva
          name="edit-outline"
          width="20px"
          height="20px"
          color="#FFFFFF"
        />
      </Button>
    </div>
  );
};

export default React.memo(BoxSelect);
