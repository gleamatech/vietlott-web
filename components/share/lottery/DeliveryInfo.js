import React from "react";

const DeliveryInfo = ({ arrInfo, onClickDelivery }) => {
  return (
    <div className="">
      {arrInfo.map((item, index) => (
        <div
          key={index}
          className="flex items-center mb-0.5 pl-1 cursor-pointer ml-0.5 p-0.5"
        >
          <input
            type="radio"
            id={`${item.display}`}
            name="delivery"
            className="form-checkbox rounded-full w-2 h-2"
            value={item.value}
            onClick={onClickDelivery}
            defaultChecked={index === 0}
          />
          <div className="flex flex-col ml-1 ">
            <label htmlFor={`${item.display}`} className="cursor-pointer">
              {item.display}
            </label>
          </div>
        </div>
      ))}
    </div>
  );
};

export default DeliveryInfo;
