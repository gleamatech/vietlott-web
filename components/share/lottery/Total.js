import { formatCurrency } from "@/utils/functions";
import React, { Fragment } from "react";
import Button from "../common/Button";
import ConfirmLottery from "../common/ContentModal/ConfirmLottery";
import Notification from "../common/ContentModal/Notification";

export const Total = ({
  price = 0,
  listTable,
  selectPeriod,
  gameType,
  handleBack,
  dashboardPage,
  valueTable,
  valueLottery,
  tabs,
  isRandom,
}) => {
  const renderRightViewModalConfirm = () => {
    return (
      <div className="flex justify-center items-center text-2xl mr-1">
        Thông tin giao hàng
      </div>
    );
  };
  const handleSubmitTotal = () => {
    let i = 0;
    if (price === 0 || isNaN(price)) {
      return window.openModal({
        content: (
          <Notification
            titleModal={"Xảy ra lỗi"}
            textModal={"Vui lòng kiểm tra lại vé"}
            titleSubmit={"OK"}
          />
        ),
        isShowHeader: false,
      });
    } else {
      while (i < listTable.length) {
        if (!!listTable[i].listSelect.length) {
          if (listTable[i].listSelect.length !== listTable[i].level) {
            return window.openModal({
              content: (
                <Notification
                  titleModal={"Xảy ra lỗi"}
                  textModal={"Vui lòng kiểm tra lại vé"}
                  titleSubmit={"OK"}
                />
              ),
              isShowHeader: false,
            });
          }
        }
        i++;
      }
      if (valueTable > 6) {
        return window.openModal({
          content: (
            <Notification
              titleModal={"Xảy ra lỗi"}
              textModal={"Tối đa được mua 6 bảng"}
              titleSubmit={"OK"}
            />
          ),
          isShowHeader: false,
        });
      } else if ((!valueTable || !valueLottery) && isRandom) {
        return window.openModal({
          content: (
            <Notification
              titleModal={"Xảy ra lỗi"}
              textModal={"Vui lòng điền đầy đủ thông tin vé TC"}
              titleSubmit={"OK"}
            />
          ),
          isShowHeader: false,
        });
      } else if (i == listTable.length || (valueTable && valueLottery)) {
        return window.openModal({
          content: (
            <ConfirmLottery
              listTable={listTable}
              selectPeriod={selectPeriod}
              gameType={gameType}
              total={price}
              tabs={tabs ? tabs[0].value : null}
              isRandom={isRandom}
              valueTable={valueTable}
              valueLottery={valueLottery}
            />
          ),
          rightView: renderRightViewModalConfirm(),
        });
      }
    }
  };
  return (
    <div className="container w-full fixed bg-fixed bottom-0 bg-gray-100 py-1 px-2">
      <div className="flex justify-between items-center mb-1 ">
        <p className="">Tổng tiền: </p>
        <span className="sub-text-color">{formatCurrency(price)}</span>
      </div>
      {gameType == "keno" || dashboardPage == true ? (
        <Fragment>
          <Button onClick={handleSubmitTotal} className=" w-full e">
            Tiếp tục
          </Button>
        </Fragment>
      ) : (
        <Fragment>
          <div className="flex justify-between">
            <Button onClick={handleBack} style={{ width: "47%" }}>
              Chọn lại
            </Button>
            <Button onClick={handleSubmitTotal} style={{ width: "47%" }}>
              Tiếp tục
            </Button>
          </div>
        </Fragment>
      )}
    </div>
  );
};

export default React.memo(Total);
