import React from "react";

export const Select = ({
  data,
  label,
  onChangeSelect,
  selectLotteryKeno,
  selectPriceKeno,
}) => {
  return (
    <React.Fragment>
      <span className="text-gray-700">{label}</span>
      <select
        className="form-select block w-full mt-1 text-xl rounded border-none bg-gray-50 appearance-none"
        onChange={onChangeSelect}
      >
        {data.map((item, index) => (
          <option
            value={item.value || item.WardCode || item.DistrictID}
            key={index}
            selected={
              (selectLotteryKeno || selectPriceKeno) == item.value
                ? true
                : false
            } 
          >
            {item.text || item.DistrictName || item.WardName}
          </option>
        ))}
      </select>
    </React.Fragment>
  );
};

export default Select;
