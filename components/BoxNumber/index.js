import React from "react";
import { formatCurrency, title } from "../../utils/functions";
import Numbers from "./Number";

const BoxNumber = ({
  index,
  item,
  nums,
  isResultKeno,
  isPower,
  isTitleName,
  borderColor,
  borderWidth,
  subGame = "",
  sameNumber,
  listOptions,
}) => {
  const listBigNumbers = (numbers) => {
    return numbers.filter((num) => num > 40);
  };

  const listSmallNumbers = (numbers) => {
    return numbers.filter((num) => num <= 40);
  };
  return (
    <>
      {(isTitleName && nums.length) || (isTitleName && subGame) ? (
        <>
          <p className="py-0.5">Bảng {title(index, subGame)}</p>
        </>
      ) : null}

      {item.win[index] > 0 && (
        <p className="pb-0.5">
          Đã trúng
          <span className="text-red-500">
            {` ${formatCurrency(item.win[index])}`}
          </span>
        </p>
      )}

      <div className="flex w-full">
        {isResultKeno ? (
          <div className="flex flex-col w-full">
            <div className="flex flex-col">
              <p className="pb-0.5">Nhỏ</p>
              <div className="flex flex-wrap">
                {listSmallNumbers(nums).map((num, index) => (
                  <Numbers
                    key={index}
                    num={num}
                    index={index}
                    borderColor={borderColor}
                    borderWidth={borderWidth}
                    sameNumber={sameNumber}
                    bgColor="bg-blue-color"
                  />
                ))}
              </div>
            </div>
            <div className="flex flex-col">
              <p className="py-0.5">Lớn</p>
              <div className="flex flex-wrap">
                {listBigNumbers(nums).map((num, index) => (
                  <Numbers
                    key={index}
                    num={num}
                    index={index}
                    borderColor={borderColor}
                    borderWidth={borderWidth}
                    sameNumber={sameNumber}
                    bgColor="bg-blue-color"
                  />
                ))}
              </div>
            </div>
            <div className="flex justify-center py-1 w-full">
              <div className="border-2 border-yellow-500 rounded-lg py-0.5 px-1 mr-0.5 ">
                {item.result.chan >= 10 ? (
                  <span>Chẵn {item.result.chan}</span>
                ) : (
                  <span>Lẻ {item.result.le}</span>
                )}
              </div>
              <div className="border-2 border-green-500 rounded-md py-0.5 px-1">
                {item.result.lon >= 10 ? (
                  <span>Lớn {item.result.lon}</span>
                ) : (
                  <span>Nhỏ {item.result.nho}</span>
                )}
              </div>
            </div>
          </div>
        ) : (
          <div className="flex flex-wrap">
            {nums
              .slice()
              .sort((a, b) => a - b)
              .map((num, index) => (
                <>
                  <Numbers
                    key={index}
                    num={num}
                    index={index}
                    isPower={isPower}
                    borderWidth={borderWidth}
                    borderColor={borderColor}
                    sameNumber={sameNumber}
                  />
                </>
              ))}
            {subGame.length > 0 && (
              <div className="text-bold p-1 border-2 rounded-md border-purple-800 font-medium">
                {listOptions.find((option) => option.value == subGame).display}
              </div>
            )}
          </div>
        )}
      </div>
    </>
  );
};

export default React.memo(BoxNumber);
