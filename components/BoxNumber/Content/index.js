import Button from "components/share/common/Button";
import IconEva from "components/share/common/IconEva";
import moment from "moment";
import Link from "next/link";
import React from "react";
import store from "store";
import { setLotteryHistory } from "store/lottery";
import { formatCurrency, totalLotteryWin } from "../../../utils/functions";
const Content = ({
  item,
  isKeno,
  isMega,
  isPower,
  isKenoTab,
  textColor = "text-yellow-500",
  fontWeight = "font-medium",
  children,
}) => {
  const onViewTicket = (variableTab, item) => () => {
    return store.dispatch(setLotteryHistory({ variableTab, item }));
  };

  const getBao = (listArray) => {
    let bao;
    for (const element of listArray) {
      element.nums.length ? (bao = element.nums.length) : 0;
    }
    return bao;
  };
  return (
    <div>
      <div className="w-full mb-1.5">
        <div className="container mx-auto bg-white p-1.5 flex flex-col sm:items-center rounded-xl">
          <div className="flex justify-between w-full">
            <div>
              <p className={`${textColor} ${fontWeight} text-2xl pb-0.4`}>
                {`${isKeno || isMega || isPower ? "Kỳ" : ""} #${item.ky}`}
              </p>

              <div className="flex mb-0.5 pb-0.5 w-full">
                {isKeno || isMega || isPower ? (
                  <span className="capitalize">
                    {moment(item.createdAt)
                      .locale("vi")
                      .format(`dddd D MMMM YYYY`)}
                  </span>
                ) : isKenoTab ? (
                  <div className="flex items-stretch">
                    <IconEva
                      name="calendar-outline"
                      width="20px"
                      height="20px"
                    />
                    <p className="capitalize leading-9">
                      {moment(item.createdAt).locale("vi").format(`LL`)}
                    </p>
                  </div>
                ) : (
                  <div className="flex items-stretch">
                    <IconEva
                      name="calendar-outline"
                      width="20px"
                      height="20px"
                    />
                    <p className="capitalize leading-9">
                      {moment(item.createdAt).format("DD/MM/YYYY")}
                    </p>
                  </div>
                )}
              </div>

              {/* check bao */}

              <div className="flex">
                {item.lotteryType === "mega"
                  ? item.lottery.length > 1 &&
                    (getBao(item.lottery) ? (
                      <>
                        <IconEva
                          name="archive-outline"
                          width="20px"
                          height="20px"
                        />
                        <span>{`Bao ${getBao(item.lottery)}`}</span>
                      </>
                    ) : (
                      <span></span>
                    ))
                  : ""}
              </div>
            </div>
            {isKeno || isMega || isPower ? null : item.result === null ? (
              <p>Chưa có kết quả</p>
            ) : item.isTC ? (
              <div>VÉ TC</div>
            ) : item.win.length ? (
              <div className="flex flex-col items-end">
                <p>{totalLotteryWin(item.win) === 0 ? "" : "Vé trúng"}</p>
                <p>
                  {totalLotteryWin(item.win) === 0 ? (
                    "Chúc bạn may mắn"
                  ) : (
                    <span className="text-red-500 font-normal text-3xl">
                      {formatCurrency(totalLotteryWin(item.win))}
                    </span>
                  )}
                </p>
              </div>
            ) : (
              <p>Chúc bạn may mắn</p>
            )}
          </div>
          {item.isTC === true ? (
            <div className="text-bold p-1 border-2 rounded-md border-red-400 text-red-400 text-center font-medium w-full">
              VÉ TC
            </div>
          ) : null}

          <div className="flex w-full">
            {item.room ? (
              <span className="capitalize">
                Loại vé: {item.room.gameType} - Bao {item.room.package}
              </span>
            ) : null}
          </div>
          <div className="w-full">
            {/* This is show list number here */}
            <div className="flex flex-col w-full pb-1">{children}</div>

            {isKeno || isMega || isPower ? null : (item.status !== "order" &&
                item.status !== "cancel") ||
              item.photos.length > 0 ? (
              <Link href="/history/view-lottery">
                <Button
                  className="w-full"
                  onClick={onViewTicket(item.lotteryType, item)}
                >
                  <p className="normal-case text-lg ">Xem vé</p>
                </Button>
              </Link>
            ) : item.result === null && item.status === "order" ? (
              <p className="text-center pt-1 border-t-1">
                Vé mua đang được xử lý
              </p>
            ) : (
              <p className="text-center">Mua vé không thành công</p>
            )}

            {/* {isMega && (
              <div className="flex justify-between pt-0.5">
                <p>JACKPOT</p>
                <p className="text-red-500 font-medium text-3xl">
                  {formatCurrency(item.jackpot)}
                </p>
              </div>
            )} */}

            {/* {isPower && (
              <div className="flex justify-end items-end flex-col pt-1">
                <div className="text-right">
                  <p>JACKPOT</p>
                  <p className="text-red-400 font-medium text-3xl">
                    {formatCurrency(item.jackpot)}
                  </p>
                </div>
                <div>
                  <p className="text-right">JACKPOT 2</p>
                  <p className="text-red-400 font-medium text-2xl">
                    {formatCurrency(item.jackpot2)}
                  </p>
                </div>
              </div>
            )} */}
          </div>
        </div>
      </div>
    </div>
  );
};

export default React.memo(Content);
