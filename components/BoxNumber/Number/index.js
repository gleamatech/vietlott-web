import React from "react";

const Numbers = ({
  borderColor = "border-blue-500",
  borderWidth = "border-2",
  bgColor = "bg-transparent",
  width = "w-3",
  height = "h-3",
  index,
  num,
  isPower,
  sameNumber = [],
}) => {
  return (
    <div
      key={index}
      className={`${num % 2 === 0 ? `${bgColor}` : null} ${
        isPower && index === 6 ? `bg-blue-200 border-blue-200` : null
      }  rounded-full ${height} ${width} ${borderWidth} ${
        sameNumber.includes(Number(num))
          ? "border-red-400 bg-white"
          : `${borderColor}`
      } mr-0.2 mb-0.5 flex items-center justify-center`}
    >
      <span className="text-xl font-normal">{num}</span>
    </div>
  );
};

export default React.memo(Numbers);
