import { LIST_TYPE_LOTTERY } from "@/constants/lottery";
import NavBar from "components/share/common/NavBar";
import Link from "next/link";
import React from "react";

const ResultScreen = () => {
  const { keno, power, mega } = LIST_TYPE_LOTTERY;
  const listTypeLottery = [keno, mega, power];

  return (
    <div className="w-full p-1.5">
      <NavBar />
      {listTypeLottery.map((item, index) => (
        <Link href={`result/${item.slug}`} key={index}>
          <div>
            <div className="flex justify-between items-center w-full py-3 px-1.5 bg-white rounded-lg mb-1.5">
              <img
                className="mr-1"
                src={item.image}
                width="90px"
                height="90px"
                alt={item.name}
              />
              <div className="flex flex-col items-center min-w-18">
                <p className="pb-0.5">Xem Kết Quả {item.name}</p>
                <p className="text-base pb-0.5">{item.time}</p>
                <p className="text-base">{item.date}</p>
              </div>
            </div>
          </div>
        </Link>
      ))}
    </div>
  );
};

export default React.memo(ResultScreen);
