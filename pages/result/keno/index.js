import React, { useEffect, useState } from "react";
import BoxNumber from "components/BoxNumber";
import Numbers from "components/BoxNumber/Number";
import moment from "moment";
import Content from "components/BoxNumber/Content";
import BackLink from "components/share/common/BackLink";
import BaseAPI from "config/BaseApi";
import { get } from "lodash";
import InfiniteScroll from "react-infinite-scroll-component";
import EmptyLoader from "components/share/common/EmptyLoader";

const PAGE_NUMBER = 1;

const KenoResult = () => {
  const [dataTable, setDataTable] = useState([]);
  const [page, setPage] = useState(PAGE_NUMBER);
  const [totalResult, setTotalResult] = useState(null);

  const fetchApi = async () => {
    let params = {
      limit: 20,
      page: page,
      sort: "-ky",
    };
    const data = await BaseAPI.get("keno", { params });
    const result = get(data, "data.docs", []).filter(
      (item) => item.result.length > 0
    );
    setTotalResult(data.data.docs.length);
    setDataTable([...dataTable, ...result]);
  };

  const listBigNumbers = (numbers) => {
    return numbers.filter((num) => num > 40);
  };
  const listSmallNumbers = (numbers) => {
    return numbers.filter((num) => num <= 40);
  };

  useEffect(() => {
    fetchApi();
  }, [page]);
  return (
    <div>
      <BackLink backLink="/result" type="keno" />
      <div className="w-full flex flex-col justify-center px-1.5 mt-2">
        <InfiniteScroll
          dataLength={dataTable.length}
          next={() => setPage(page + 1)}
          hasMore={totalResult > dataTable.length ? true : false}
          loader={<h4 className="text-center py-0.5">Đang tải thêm dữ liệu</h4>}
          endMessage={
            <h4 className="text-center py-0.5">
              {dataTable.length ? "Đã hiển thị tất cả kết quả" : ""}
            </h4>
          }
        >
          {dataTable.length ? (
            <React.Fragment>
              {dataTable.map((item, index) => (
                <div className="container mx-auto bg-white mb-3 p-1.5 flex flex-col sm:items-center rounded-xl">
                  <div className="flex justify-between w-full">
                    <div>
                      <p className={`text-2xl pb-0.4 font-medium`}>
                        Kỳ #{item.ky}
                      </p>
                      <span className="capitalize">
                        {moment(item.createdAt).locale("vi").format("dddd LL")}
                      </span>
                      <div className="flex flex-col w-full">
                        <div className="flex flex-col">
                          <p className="py-0.5">Lớn</p>
                          <div className="flex flex-wrap w-5/6">
                            {listBigNumbers(item.result).map((num, index) => (
                              <Numbers
                                key={index}
                                num={num}
                                index={index}
                                borderColor="border-red-500"
                                borderWidth="border-1"
                                bgColor="bg-blue-color"
                              />
                            ))}
                          </div>
                        </div>
                        <div className="flex flex-col">
                          <p className="pb-0.5">Nhỏ</p>
                          <div className="flex flex-wrap w-4/5">
                            {listSmallNumbers(item.result).map((num, index) => (
                              <Numbers
                                key={index}
                                num={num}
                                index={index}
                                borderColor="border-red-500"
                                borderWidth="border-1"
                                bgColor="bg-blue-color"
                              />
                            ))}
                          </div>
                        </div>
                        <div className="flex justify-center pt-1 w-full">
                          <div className="box-even-odd font-bold border-2 px-1 py-0.5 mr-0.5 ">
                            {item.chan >= 10 ? (
                              <span>Chẵn {item.chan}</span>
                            ) : (
                              <span>Lẻ {item.le}</span>
                            )}
                          </div>
                          <div className="box-big-small font-bold border-2 px-1 py-0.5">
                            {item.lon >= 10 ? (
                              <span>Lớn {item.lon}</span>
                            ) : (
                              <span>Nhỏ {item.nho}</span>
                            )}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </React.Fragment>
          ) : (
            <div className="flex flex-col items-center">
              <EmptyLoader />
              <p>Hiện Chưa Có Dữ Liệu</p>
            </div>
          )}
        </InfiniteScroll>
      </div>
    </div>
  );
};

export default React.memo(KenoResult);
