import BaseAPI from "config/BaseApi";
import React, { useEffect, useState } from "react";
import Numbers from "components/BoxNumber/Number";
import BoxNumber from "components/BoxNumber";
import Content from "components/BoxNumber/Content";
import BackLink from "components/share/common/BackLink";
import { get } from "lodash";
import InfiniteScroll from "react-infinite-scroll-component";
import moment from "moment";
import { formatCurrency } from "utils/functions";
import EmptyLoader from "components/share/common/EmptyLoader";
const PAGE_NUMBER = 1;

const PowerResult = () => {
  const [dataTable, setDataTable] = useState([]);
  const [page, setPage] = useState(PAGE_NUMBER);
  const [totalResult, setTotalResult] = useState(null);

  const fetchApi = async () => {
    let params = {
      limit: 20,
      page: page,
      sort: "-ky",
    };
    const data = await BaseAPI.get("power", { params });
    const result = get(data, "data.docs", []).filter(
      (item) => item.result.length > 0
    );
    setTotalResult(data.total);
    setDataTable([...dataTable, ...result]);
  };

  useEffect(() => {
    fetchApi();
  }, [page]);
  return (
    <div>
      <BackLink backLink="/result" type="power" />
      <div className="w-full flex flex-col justify-center px-1.5 mt-2">
        <InfiniteScroll
          dataLength={dataTable.length}
          next={() => setPage(page + 1)}
          hasMore={totalResult > dataTable.length ? true : false}
          loader={<h4 className="text-center py-0.5">Đang tải thêm dữ liệu</h4>}
          endMessage={
            <h4 className="text-center py-0.5">
              {dataTable.length ? "Đã hiển thị tất cả kết quả" : ""}
            </h4>
          }
        >
          {dataTable.length ? (
            <React.Fragment>
              {dataTable.map((item, index) => (
                <div className="container mx-auto bg-white mb-3 p-1.5 flex flex-col sm:items-center rounded-xl">
                  <div className="flex justify-between w-full">
                    <div className="w-full">
                      <p className="text-2xl pb-0.5 font-medium">
                        Kỳ #{item.ky}
                      </p>
                      <p className="capitalize pb-0.5">
                        {moment(item.createdAt).locale("vi").format("dddd LL")}
                      </p>
                      <div className="flex flex-wrap w-4/5">
                        {item.result.map((num, index) => (
                          <>
                            <Numbers
                              key={index}
                              index={index}
                              num={num}
                              borderColor="border-red-500"
                              borderWidth="border-1"
                              isPower
                            />
                          </>
                        ))}
                      </div>
                      <div className="flex justify-end items-end flex-col pt-0.5">
                        <div className="text-right">
                          <p>JACKPOT</p>
                          <p className="text-red-400 font-medium text-3xl">
                            {formatCurrency(item.jackpot)}
                          </p>
                        </div>
                        <div>
                          <p className="text-right">JACKPOT 2</p>
                          <p className="text-red-400 font-medium text-2xl">
                            {formatCurrency(item.jackpot2)}
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </React.Fragment>
          ) : (
            <div className="flex flex-col items-center">
              <EmptyLoader />
              <p>Hiện Chưa Có Dữ Liệu</p>
            </div>
          )}
        </InfiniteScroll>
      </div>
    </div>
  );
};

export default React.memo(PowerResult);
