import BoxNumber from "components/BoxNumber";
import Content from "components/BoxNumber/Content";
import EmptyLoader from "components/share/common/EmptyLoader";
import NavBar from "components/share/common/NavBar";
import BaseAPI from "config/BaseApi";
import { LIST_TYPE_LOTTERY } from "constants/lottery";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import { useSelector } from "react-redux";
import store from "store";
import { setLotteryHistory } from "store/lottery";
import { classNames } from "utils/functions";

const PAGE_NUMBER = 1;

const History = () => {
  let state = useSelector((state) => state.lottery.lotteryHistory);
  const listType = {
    ...LIST_TYPE_LOTTERY,
  };
  const [dataTable, setDataTable] = useState([]);
  const [page, setPage] = useState(PAGE_NUMBER);
  const [variableTab, setVariableTab] = useState(state.variableTab);
  const [totalResult, setTotalResult] = useState(null);

  const [tabs, setTabs] = useState([
    { title: "Keno", value: "keno" },
    { title: "Mega", value: "mega" },
    { title: "Power", value: "power" },
    { title: "3 Miền", value: "share" },
  ]);

  const handleClickButton = (item, index) => () => {
    try {
      fetchApi(item.value);
      setVariableTab(item.value);
      setPage(1);
      store.dispatch(setLotteryHistory({ variableTab: item.value }));
    } catch (error) {
      console.log(error);
    }
  };

  const fetchApi = async (variableTab) => {
    try {
      const data = await BaseAPI.get("lottery/filter", {
        params: {
          lotteryType: variableTab,
          sort: "-createdAt",
          limit: 20,
          page: page,
          isFromWeb: true
        },
      });
      setDataTable(data.data.docs);
      if (page === 1) {
        setDataTable(data.data.docs);
        setTotalResult(data.total);
      } else {
        setDataTable([...dataTable, ...data.data.docs]);
        setTotalResult(data.total);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const mergeArray = (listArray) => {
    let myArray = [];
    listArray.forEach((item, index) => myArray.push(...item));
    return myArray;
  };

  const findTheSameNumber = (array, childArr) => {
    return childArr.filter((element) => array.includes(element));
  };

  useEffect(() => {
    fetchApi(variableTab);
  }, [variableTab, page]);

  return (
    <div className="container mx-auto px-1.5">
      <NavBar />
      <nav className="flex sm:flex-row bg-white rounded mt-1.5">
        {tabs.map((item, index) => (
          <Link href="/history">
            <button
              key={index}
              onClick={handleClickButton(item, index)}
              className={classNames(
                `${
                  item.value === variableTab
                    ? "transition ease-in-out duration-700 w-1/4 py-1 px-1 block rounded hover:text-blue-500 focus:outline-none text-blue-500 border-4 font-medium border-blue-500"
                    : "text-gray-600 w-1/4 py-1 px-1 block hover:text-blue-500 focus:outline-none font-medium"
                } `
              )}
            >
              {item.title}
            </button>
          </Link>
        ))}
      </nav>
      <div className="w-full flex flex-col justify-center mt-1.5 mb-4.5">
        <InfiniteScroll
          dataLength={dataTable.length}
          next={() => setPage(page + 1)}
          hasMore={totalResult > dataTable.length ? true : false}
          loader={<h4 className="text-center py-0.5">Đang tải thêm dữ liệu</h4>}
          endMessage={
            <h4 className="text-center mb-1.5">
              {dataTable.length ? "Đã hiển thị tất cả kết quả" : ""}
            </h4>
          }
        >
          {dataTable.length ? (
            <React.Fragment>
              {dataTable.map((infoLottery, index) => (
                <Content
                  item={infoLottery}
                  isKenoTab={variableTab == "keno"}
                  key={index}
                >
                  {infoLottery.lottery.map(
                    (lottery, index) =>
                      lottery && (
                        <>
                          <BoxNumber
                            key={index}
                            index={index}
                            nums={lottery.nums}
                            item={infoLottery}
                            isTitleName
                            subGame={lottery.subGame ? lottery.subGame : ""}
                            listOptions={
                              lottery.subGame ? listType.keno.listOptions : []
                            }
                            sameNumber={
                              infoLottery.result !== null
                                ? findTheSameNumber(
                                    lottery.nums,
                                    infoLottery.result.result
                                  )
                                : []
                            }
                          />
                        </>
                      )
                  )}

                  {infoLottery.result && infoLottery.isTC === false && (
                    <div className="mt-1 border-t border-b pb-0.5 mb-0.5">
                      <p className="py-1">Kết quả xổ số</p>
                      <BoxNumber
                        key={index}
                        nums={infoLottery.result.result}
                        item={infoLottery}
                        borderColor="border-blue-500"
                        borderWidth="border-2"
                        isTitleName={false}
                        status={infoLottery.status}
                        isResultKeno={variableTab == "keno"}
                        sameNumber={
                          infoLottery.lottery.length
                            ? findTheSameNumber(
                                mergeArray(
                                  infoLottery.lottery.map(
                                    (num, index) => num.nums
                                  )
                                ),
                                infoLottery.result.result
                              )
                            : []
                        }
                      />
                    </div>
                  )}
                </Content>
              ))}
            </React.Fragment>
          ) : (
            <div className="flex flex-col items-center">
              <EmptyLoader />
              <p>Hiện Chưa Có Dữ Liệu</p>
            </div>
          )}
        </InfiniteScroll>
      </div>
    </div>
  );
};

export default React.memo(History);
