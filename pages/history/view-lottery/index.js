import BackLink from "components/share/common/BackLink";
import React from "react";
import ImageZoom from "react-medium-image-zoom";
import QRCode from "react-qr-code";
import { useSelector } from "react-redux";
import { formatCurrency } from "utils/functions";
const ViewLottery = () => {
  const initialValue = {
    customer: "",
    status: "",
    photos: [],
    shipType: "",
    room: "",
  };
  const state = useSelector((state) => state.lottery.lotteryHistory);
  const user = useSelector((state) => state.user.user);
  const { customer, status, photos, shipType, lotteryType, room } = state.item
    ? state.item
    : initialValue;
  let dataJoined, priceLottery, priceJoined;
  if (state.variableTab === "share") {
    dataJoined = room.join.filter(
      (item) => item.player.phoneNumber == user.phoneNumber
    );
    priceJoined = dataJoined.reduce((total, item) => total + item.price, 0);
    priceLottery = Math.ceil(room.pricePerNum * room.package);
  }
  const ticketId = state.item ? state.item._id : 0;
  const renderImage = (listImage) => {
    return (
      <div className="flex flex-col">
        {listImage.map((item, index) => (
          <div className="mb-0.5" key={index}>
            <ImageZoom
              key={index}
              image={{
                src: item.uri,
                alt: "Lottery",
                className: "img mb-1",
                style: { width: "100px", height: "180px" },
              }}
              zoomImage={{
                src: item.uri,
                alt: "Lottery",
              }}
              zoomMargin={5}
            />
          </div>
        ))}
      </div>
    );
  };

  return (
    <div className="min-w-full">
      <BackLink backLink="/history" title="Quay lại"></BackLink>
      <div className="bg-white p-1.5 mx-1 my-2 h-screen sm:h-auto">
        <p className="font-bold mb-0.5">Thông tin vé mua</p>
        {state.variableTab === "share" ? (
          dataJoined[0] ? (
            <div>
              <p className="mb-0.5">{dataJoined[0].player.phoneNumber} </p>{" "}
              <p className="mb-0.5">
                Đã tham gia:{" "}
                {`${formatCurrency(priceJoined)}/${formatCurrency(
                  priceLottery
                )}`}
              </p>
            </div>
          ) : null
        ) : (
          <p className="mb-0.5">Số điện thoại: {customer.phoneNumber} </p>
        )}

        <p className="mb-0.5">
          Trạng thái: {status === "complete" ? "Hoàn Tất" : ""}
        </p>
        <div className="mt-1.5">
          {shipType === "take" ? (
            <div>
              <p>Mã nhận vé tại quầy: </p>
              <QRCode value={ticketId} />
            </div>
          ) : null}

          {(shipType === "store" || lotteryType === "share") &&
          photos.length > 0 ? (
            <div>
              <p>Hình ảnh vé mua: </p>
              {renderImage(photos)}
            </div>
          ) : null}

          {shipType === "ship" && photos.length > 0 ? (
            <div>
              <p>Hình ảnh giao hàng: </p>
              {renderImage(photos)}
            </div>
          ) : null}

          {shipType === "take" && photos.length > 0 ? (
            <div className="mt-1">
              <p>Hình ảnh giao vé: </p>
              {renderImage(photos)}
            </div>
          ) : null}
        </div>
      </div>
    </div>
  );
};

export default React.memo(ViewLottery);
