import BackLink from "components/share/common/BackLink";
import Button from "components/share/common/Button";
import IconEva from "components/share/common/IconEva";
import BaseAPI from "config/BaseApi";
import { Field, Form, Formik } from "formik";
import { useRouter } from "next/router";
import React, { useMemo, useState } from "react";
import { toast } from "react-toastify";

const ChangePass = () => {
  const [isShowPass, setIsShowPass] = useState(false);
  const router = useRouter();
  const initialValues = {
    password: "",
    newPassword: "",
    confirmNewPassword: "",
  };
  const handleChangePassword = async ({ password, newPassword }) => {
    try {
      const response = await BaseAPI.post("/user/changePassword", {
        password,
        newPassword,
      });
      if (response.status) {
        toast.success(`Mật khẩu đã được cập nhật!`);
        router.push("/auth/login");
      }
    } catch (error) {
      console.log("error", error);
    }
  };
  const handleShowPass = () => {
    setIsShowPass(!isShowPass);
  };
  const renderIconShowPass = useMemo(() => {
    return (
      <div>
        <div
          className={!isShowPass ? "hidden" : "opacity-50 cursor-pointer"}
          onClick={handleShowPass}
        >
          <IconEva name="eye-outline" height="20" width="20" />
        </div>
        <div
          className={!isShowPass ? "opacity-50 cursor-pointer" : "hidden"}
          onClick={handleShowPass}
        >
          <IconEva name="eye-off-outline" height="20" width="20" />
        </div>
      </div>
    );
  }, [isShowPass]);

  return (
    <div>
      <BackLink backLink="/profile" title="Khôi phục mật khẩu" />
      <div className="w-full p-1">
        <Formik initialValues={initialValues} onSubmit={handleChangePassword}>
          {(formikProps) => {
            const { values } = formikProps;
            const { password, newPassword, confirmNewPassword } = values;
            return (
              <Form className="bg-white p-1 rounded-lg">
                <div className="flex flex-col mb-1">
                  <label className="block pt-0.5 text-gray-500 text-lg font-normal">
                    Mật khẩu hiện tại
                  </label>
                  <div className="relative">
                    <Field
                      name="password"
                      type={!isShowPass ? "password" : "text"}
                      placeholder="Nhập mật khẩu hiện tại"
                      className={`appearance-none text-xl px-1 border border-gray-200 bg-gray-50 rounded w-full text-gray-800 focus:outline-none focus:border-blue-100`}
                    />
                    <div className="absolute top-1/2 transform -translate-y-1/2 right-1">
                      {renderIconShowPass}
                    </div>
                  </div>
                </div>
                <div className="flex flex-col mb-1">
                  <label className="block pt-0.5 text-gray-500 text-lg font-normal">
                    Mật khẩu mới
                  </label>
                  <div className="relative">
                    <Field
                      name="newPassword"
                      type={!isShowPass ? "password" : "text"}
                      placeholder="Nhập mật khẩu mới"
                      className={`appearance-none text-xl px-1 border border-gray-200 bg-gray-50 rounded w-full text-gray-800 focus:outline-none focus:border-blue-100`}
                    />
                    <div className="absolute top-1/2 transform -translate-y-1/2 right-1">
                      {renderIconShowPass}
                    </div>
                  </div>
                </div>
                <div className="flex flex-col mb-1">
                  <label className="block pt-0.5 text-gray-500 text-lg font-normal">
                    Xác nhận mật khẩu mới
                  </label>
                  <div className="relative">
                    <Field
                      name="confirmNewPassword"
                      type={!isShowPass ? "password" : "text"}
                      placeholder="Xác nhận mật khẩu mới"
                      className={`appearance-none text-xl px-1 border border-gray-200 bg-gray-50 rounded w-full text-gray-800 focus:outline-none focus:border-blue-100`}
                    />
                    <div className="absolute top-1/2 transform -translate-y-1/2 right-1">
                      {renderIconShowPass}
                    </div>
                  </div>
                </div>
                <Button
                  type="submit"
                  disabled={
                    !password.length ||
                    !newPassword.length ||
                    !confirmNewPassword.length ||
                    newPassword !== confirmNewPassword
                  }
                  className={` ${
                    !password.length ||
                    !newPassword.length ||
                    !confirmNewPassword.length ||
                    newPassword !== confirmNewPassword
                      ? "bg-gray-400"
                      : "bg-blue-700"
                  } w-full `}
                >
                  Đổi mật khẩu
                </Button>
              </Form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
};

export default React.memo(ChangePass);
