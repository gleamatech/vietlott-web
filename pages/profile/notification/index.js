import React, { useState, useEffect } from "react";
import moment from "moment";
import BackLink from "../../../components/share/common/BackLink";
import Cookies from "js-cookie";
import BaseAPI from "config/BaseApi";
import { get } from "lodash";
import InfiniteScroll from "react-infinite-scroll-component";
import EmptyLoader from "components/share/common/EmptyLoader";
const PAGE_NUMBER = 1;

const Notification = () => {
  const [dataTable, setDataTable] = useState([]);
  const [page, setPage] = useState(PAGE_NUMBER);
  const [totalResult, setTotalResult] = useState(null);
  const token = Cookies.get("token");

  const fetchApi = async () => {
    let params = {
      page: page,
      limit: 20,
    };
    try {
      const data = await BaseAPI.get("/notification", { params });
      const result = get(data, "data.docs", []);
      setTotalResult(data.total);
      setDataTable([...dataTable, ...result]);
    } catch (error) {
      console.log("error", error);
    }
  };

  useEffect(() => {
    fetchApi();
  }, [page]);

  return (
    <div>
      <BackLink backLink="/profile" title="Thông báo" />
      <div className="container mx-auto px-1">
        <InfiniteScroll
          dataLength={dataTable.length}
          next={() => setPage(page + 1)}
          hasMore={totalResult > dataTable.length ? true : false}
          loader={<h4 className="text-center py-0.5">Đang tải thêm dữ liệu</h4>}
          endMessage={
            <h4 className="text-center py-0.5">
              {dataTable.length ? "Đã hiển thị tất cả kết quả" : ""}
            </h4>
          }
        >
          {dataTable.length ? (
            <React.Fragment>
              {dataTable.map((item, index) => (
                <div
                  className="w-full p-1.5 bg-white my-1 rounded-md"
                  key={index}
                >
                  <p className="font-medium pb-0.5">{item.title}</p>
                  <p className="pb-0.5">{item.message}</p>
                  <p className="text-base text-gray-400">
                    {moment(item.createdAt).locale("vi").format("LLL")}
                  </p>
                </div>
              ))}
            </React.Fragment>
          ) : (
            <div className="flex flex-col items-center">
              <EmptyLoader />
              <p>Hiện Chưa Có Dữ Liệu</p>
            </div>
          )}
        </InfiniteScroll>
      </div>
    </div>
  );
};

export default React.memo(Notification);
