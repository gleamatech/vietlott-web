import Button from "components/share/common/Button";
import WithdrawConfirmPassword from "components/share/common/ContentModal/WithdrawConfirmPassword";
import InputField from "components/share/common/Form/InputField";
import { FastField, Form, Formik } from "formik";
import React, { Fragment } from "react";
import { useSelector } from "react-redux";
import { formatCurrency } from "utils/functions";
import * as Yup from "yup";
import BackLink from "../../../components/share/common/BackLink";
import Notification from "components/share/common/ContentModal/Notification";

const Withdraw = () => {
  const initialValues = {
    bankName: "",
    accountNumber: "",
    name: "",
    money: "",
  };

  const user = useSelector((state) => state.user.user);
  const MoneySchema = Yup.object().shape({
    money: Yup.number()
      .min(0, "Số tiền rút không hợp lệ!")
      .max(user.balance, "Số tiền vượt quá số dư tài khoản!"),
  });
  const handleShowModalConfirmPass = ({
    bankName,
    accountNumber,
    name,
    money,
  }) => {
    return window.openModal({
      content: (
        <WithdrawConfirmPassword
          withdrawType={bankName}
          withdrawTo={accountNumber}
          info={name}
          amount={parseFloat(money)}
        />
      ),
      isShowHeader: false,
    });
  };

  const handleClick = () => {
    return window.openModal({
      content: (
        <Notification
          titleModal={"Không khả dụng"}
          titleSubmit={"OK"}
        />
      ),
      isShowHeader: false,
    });
  };
  return (
    <div>
      <BackLink
        backLink="/profile"
        title="Rút tiền"
        subTitle="Lịch sử rút tiền"
        titleLink="/profile/withdraw/history"
      />
      <div className="w-full p-1">
        <div className="mb-1">Dịch vụ tiện ích (đang cập nhật)</div>
        <div onClick={handleClick} className="rounded bg-white flex flex-col mb-1 p-1">
          Thanh toán tiền điện
        </div>
        <div onClick={handleClick} className="rounded bg-white flex flex-col mb-1 p-1">
          Thanh toán tiền nước
        </div>
        <Formik
          initialValues={initialValues}
          validationSchema={MoneySchema}
          onSubmit={handleShowModalConfirmPass}
        >
          {(formikProps) => {
            const { values, errors } = formikProps;
            const { bankName, accountNumber, name, money } = values;
            return (
              <Form>
                <div className="flex flex-col mb-1">
                  <FastField
                    name="bankName"
                    component={InputField}
                    type="text"
                    label="Ngân hàng/ Ví điện tử"
                    placeholder="Nhập tên ngân hàng/ Ví điện tử"
                  />
                </div>
                <div className="flex flex-col mb-1">
                  <FastField
                    name="accountNumber"
                    component={InputField}
                    type="text"
                    label="Số tài khoản"
                    placeholder="Nhập số tài khoản"
                  />
                </div>
                <div className="flex flex-col mb-1">
                  <FastField
                    name="name"
                    component={InputField}
                    type="text"
                    label="Tên người thụ hưởng"
                    placeholder="Nhập tên người thụ hưởng"
                  />
                </div>
                <div className="flex flex-col mb-1">
                  <FastField
                    name="money"
                    component={InputField}
                    type="Number"
                    label="Số tiền rút"
                    placeholder="0"
                  />
                  {errors.money ? (
                    <div className="text-red-500 text-lg pt-0.5">
                      {errors.money}
                    </div>
                  ) : null}
                  <label
                    htmlFor="money"
                    className="block pt-0.5 text-gray-500 text-lg font-normal"
                  >
                    Số dư hiện tại: <span>{formatCurrency(user.balance)}</span>
                  </label>
                </div>
                {
                  <Fragment>
                    <Button
                      type="submit"
                      className=" w-full"
                      disabled={
                        !bankName.length ||
                        !accountNumber.length ||
                        !name.length ||
                        !money
                          ? true
                          : false
                      }
                    >
                      Rút tiền
                    </Button>
                  </Fragment>
                }
              </Form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
};

export default React.memo(Withdraw);
