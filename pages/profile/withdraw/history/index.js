import BackLink from "components/share/common/BackLink";
import BaseAPI from "config/BaseApi";
import Cookies from "js-cookie";
import { get } from "lodash";
import moment from "moment";
import { useRouter } from "next/router";
import React, { Fragment, useEffect, useState } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import { formatCurrency } from "utils/functions";
import EmptyLoader from "components/share/common/EmptyLoader";
const PAGE_NUMBER = 1;

const WithdrawHistory = () => {
  const [dataTable, setDataTable] = useState([]);
  const [page, setPage] = useState(PAGE_NUMBER);
  const [totalResult, setTotalResult] = useState(null);
  const router = useRouter();
  const fetchApi = async () => {
    let params = {
      page: page,
      limit: 20,
    };
    try {
      const data = await BaseAPI.get("withdraw", { params });
      const result = get(data, "data.docs", []);
      setTotalResult(data.total);
      setDataTable([...dataTable, ...result]);
    } catch (error) {
      console.log(error.message);
    }
  };
  useEffect(() => {
    fetchApi();
  }, [page]);
  return (
    <div>
      <BackLink backLink="/profile/withdraw" title="Lịch sử rút tiền" />
      <div className="container mx-auto px-1">
        <InfiniteScroll
          dataLength={dataTable.length}
          next={() => setPage(page + 1)}
          hasMore={totalResult > dataTable.length ? true : false}
          loader={<h4 className="text-center py-0.5">Đang tải thêm dữ liệu</h4>}
          endMessage={
            <h4 className="text-center py-0.5">
              {dataTable.length ? "Đã hiển thị tất cả kết quả" : ""}
            </h4>
          }
        >
          {dataTable.length ? (
            <Fragment>
              {dataTable.map((item, index) => (
                <div
                  className="w-full p-1.5 bg-white my-1 rounded-md"
                  key={index}
                >
                  <p className="pb-0.5">
                    <span>Ngân hàng / Ví: </span>
                    <span>{item.withdrawType}</span>
                  </p>
                  <p className="pb-0.5">
                    <span>Số tài khoản: </span>
                    {item.withdrawTo}
                  </p>
                  <p className="pb-0.5">
                    <span>Số tiền: </span>
                    <span className="sub-text-color">
                      {formatCurrency(item.amount)}
                    </span>
                  </p>
                  <p className="pb-0.5">
                    <span>Trạng thái: </span>
                    <span className="text-green-500">
                      {item.status == "done" ? "Hoàn tất" : "Đang xử lý"}
                    </span>
                  </p>
                  <p className="pb-0.5">
                    <span>Ngày rút: </span>
                    {moment(item.createdAt).locale("vi").format("LL")}
                  </p>
                </div>
              ))}
            </Fragment>
          ) : (
            <div className="flex flex-col items-center">
              <EmptyLoader />
              <p>Hiện Chưa Có Dữ Liệu</p>
            </div>
          )}
        </InfiniteScroll>
      </div>
    </div>
  );
};

export default WithdrawHistory;
