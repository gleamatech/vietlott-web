import Button from "components/share/common/Button";
import IconEva from "components/share/common/IconEva";
import { ArchiveIcon } from "@heroicons/react/outline";
import NavBar from "components/share/common/NavBar";
import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { logOut } from "store/user";
import { formatCurrency } from "utils/functions";

const ProfileScreen = () => {
  const dispatch = useDispatch();
  const router = useRouter();
  const user = useSelector((state) => state.user.user);
  const handleLogOut = () => {
    dispatch(logOut());
    router.push("/auth/login");
  };

  return (
    <div className="w-full relative flex flex-col pt-1 mb-1 px-1.5">
      <div className="w-full flex justify-end mb-3">
        <div className="absolute top-0 left-0 right-0 -z-1">
          <img
            className="mx-auto w-full h-56 object-cover"
            src="/images/background/top_cover.jpg"
            alt="bg color"
          />
        </div>
        <div className="flex justify-center items-center p-0.4 rounded-full bg-info">
          <Link href="/profile/notification">
            <span>
              <IconEva
                name="bell-outline"
                color="#fff"
                height="15px"
                width="15px"
              />
            </span>
          </Link>
        </div>
      </div>
      <div className="f-full">
        <div className="bg-white rounded-xl p-1.5">
          <div className="">
            <p className="font-normal text-3xl">{user.name}</p>
            <p className="text-gray-400">Tài khoản:</p>
            <p className="text-red-500">{formatCurrency(user.balance)}</p>
          </div>
          <div className="flex justify-around mt-1.5">
            <Link href="/profile/send-money">
              <Button className="w-1/2 py-0.5 rounded mr-1">
                <IconEva
                  name="credit-card-outline"
                  color="#fff"
                  height="15px"
                  width="15px"
                />
                <p className="break-normal ml-1">Nạp tiền </p>
              </Button>
            </Link>
            <Link href="/profile/withdraw">
              <Button className="w-1/2 py-0.5 rounded">
                <IconEva
                  name="swap-outline"
                  color="#fff"
                  height="15px"
                  width="15px"
                />
                <p className="ml-1">Rút tiền</p>
              </Button>
            </Link>
          </div>
        </div>
      </div>
      <div className="flex my-1.5 ">
        <div className="flex justify-center items-center mr-1 rounded-lg bg-info p-0.5 text-white w-4 h-4">
          <IconEva
            name="archive-outline"
            width="30px"
            height="30px"
            color="#fff"
          />
        </div>
        <div>
          <p>Tài khoản</p> <p className="text-gray-500">Thông tin tài khoản</p>
        </div>
      </div>
      <div className="bg-white rounded-xl p-1.5">
        <div className="flex flex-col">
          <div className="flex justify-between py-1 border-b-1">
            <p>Tên</p>
            <p>{user.name}</p>
          </div>
          <div className="flex justify-between py-1 border-b-1">
            <p>Số điện thoại</p>
            <p>{user.phoneNumber}</p>
          </div>
          <Link href="/profile/change-pass">
            <div className="flex justify-between py-1 border-b-1">
              <p>Đổi mật khẩu</p>
            </div>
          </Link>
          <Link href="/profile/transaction-history">
            <div className="flex justify-between py-1 border-b-1">
              <p>Lịch sử giao dịch</p>
            </div>
          </Link>

          <button
            onClick={handleLogOut}
            className="btn-red focus:outline-none text-white py-1 rounded font-medium"
          >
            Đăng xuất
          </button>
        </div>
      </div>
      <div className="flex my-1.5 ">
        <div className="flex justify-center items-center mr-1 rounded-lg bg-info p-0.5 text-white w-4 h-4">
          <IconEva
            name="email-outline"
            color="#fff"
            height="20px"
            width="20px"
          />
        </div>
        <div>
          <p>Hỗ trợ & Phản hồi</p>{" "}
          <p className="text-gray-500">Liên hệ với chúng tôi</p>
        </div>
      </div>
      <div className="bg-white rounded-xl p-1.5 mb-5">
        <div className="flex flex-col">
          <div className="flex justify-between py-1 border-b-1">
            <p>Câu hỏi thường gặp & Liên hệ</p>
          </div>
          <div className="flex justify-between py-1 ">
            <a href="tel:1800-789-998">
              <p>Hotline: 1800 789 998</p>
            </a>
          </div>
        </div>
      </div>
      <NavBar />
    </div>
  );
};

export default React.memo(ProfileScreen);
