import EmptyLoader from "components/share/common/EmptyLoader";
import BaseAPI from "config/BaseApi";
import { get } from "lodash";
import moment from "moment";
import React, { Fragment, useEffect, useState } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import BackLink from "../../../components/share/common/BackLink";
import { formatCurrency } from "../../../utils/functions";
const PAGE_NUMBER = 1;

const TransactionHistory = () => {
  const [dataTable, setDataTable] = useState([]);
  const [page, setPage] = useState(PAGE_NUMBER);
  const [totalResult, setTotalResult] = useState(null);
  const fetchApi = async () => {
    let params = {
      page: page,
      limit: 20,
    };
    try {
      const data = await BaseAPI.get(`/transaction`, { params });
      const result = get(data, "data.docs", []).sort(function (a, b) {
        return new Date(b.createdAt) - new Date(a.createdAt);
      });
      setTotalResult(data.total);
      setDataTable([...dataTable, ...result]);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchApi();
  }, [page]);
  return (
    <div>
      <BackLink backLink="/profile" title="Lịch sử giao dịch" />
      <div className="container mx-auto px-1">
        <InfiniteScroll
          dataLength={dataTable.length}
          next={() => setPage(page + 1)}
          hasMore={totalResult > dataTable.length ? true : false}
          loader={<h4 className="text-center py-0.5">Đang tải thêm dữ liệu</h4>}
          endMessage={
            <h4 className="text-center py-0.5">
              {dataTable.length ? "Đã hiển thị tất cả kết quả" : ""}
            </h4>
          }
        >
          {dataTable.length ? (
            <Fragment>
              {dataTable.map((item, index) => (
                <Fragment key={index}>
                  {item.amount === 0 ? null : (
                    <div
                      className="w-full p-1.5 bg-white my-1 rounded-md"
                      key={index}
                    >
                      <p>
                        <span>Biến động số dư </span>
                        <p className="sub-text-color">
                          {item.amount > 0
                            ? `+${formatCurrency(item.amount)}`
                            : formatCurrency(item.amount)}
                        </p>
                      </p>
                      <p>
                        <span>Số dư tài khoản </span>
                        <p className="sub-text-color">
                          {formatCurrency(item.totalAtMoment)}
                        </p>
                      </p>
                      <p>
                        <span>Ghi chú: </span>
                        <p>{item.reason}</p>
                      </p>
                      <p className="text-base text-gray-400 capitalize">
                        {moment(item.createdAt).locale("vi").format("LLLL")}
                      </p>
                    </div>
                  )}
                </Fragment>
              ))}
            </Fragment>
          ) : (
            <div className="flex flex-col items-center">
              <EmptyLoader />
              <p>Hiện Chưa Có Dữ Liệu</p>
            </div>
          )}
        </InfiniteScroll>
      </div>
    </div>
  );
};

export default React.memo(TransactionHistory);
