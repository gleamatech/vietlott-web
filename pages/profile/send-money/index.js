import React, { useState, useEffect } from "react";
import BackLink from "../../../components/share/common/BackLink";
import Cookies from "js-cookie";
const SendMoney = () => {
  const [dataTable, setDataTable] = useState([
    {
      title: "Techcombank Hồ Chí Minh",
      content: "19034830608012",
      name: "Nguyễn Đăng Khánh",
    },
    {
      title: "BIDV Hồ Chí Minh",
      content: "31010002296752",
      name: "Nguyễn Đăng Khánh",
    },
    {
      title: "Sacombank Thủ Đức",
      content: "060211144842",
      name: "Nguyễn Đăng Khánh",
    },
    {
      title: "ACB Hồ Chí Minh",
      content: "8101697",
      name: "Nguyễn Đăng Khánh",
    },
    {
      title: "Vietinbank Hồ Chí Minh",
      content: "108872520956",
      name: "Nguyễn Đăng Khánh",
    },
    {
      title: "Ví điện tử",
      content: "108872520956",
      wallet: [{ momo: "0909392725" }, { viettelPay: "0909392725" }],
      name: "Nguyễn Đăng Khánh",
    },
  ]);

  return (
    <div>
      <BackLink backLink="/profile" title="Nạp tiền" isSendMoney />
      <div className="container mx-auto">
        {dataTable.map((item, index) => (
          <div className="w-full p-1.5 bg-white my-1" key={index}>
            <p className="pb-0.5">{item.title}</p>
            {!item.wallet && (
              <p className="pb-0.5">
                <span>Số tài khoản: </span>
                <span className="text-red-500">{item.content}</span>
              </p>
            )}

            {item.wallet && (
              <p>
                Momo:
                <span className="text-red-500"> {item.wallet[0].momo}</span>
              </p>
            )}
            {item.wallet && (
              <p>
                Viettel Pay:
                <span className="text-red-500">
                  {" "}
                  {item.wallet[1].viettelPay}
                </span>
              </p>
            )}
            <p className="">
              <span>Chủ tài khoản: </span>
              {item.name}
            </p>
          </div>
        ))}
      </div>
    </div>
  );
};

export default React.memo(SendMoney);
