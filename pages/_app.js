import Router, { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { Provider } from "react-redux";
import { toast } from "react-toastify";
import { PersistGate } from "redux-persist/integration/react";
import { get } from "lodash";
import { parseCookies } from "utils/parseCookies.js";
import SocketServices from "../config/socket-client";
import kenoServices from "../config/kenoServices";
import "react-toastify/dist/ReactToastify.css";
import "tailwindcss/tailwind.css";
import store, { persistor } from "../store";
import "../styles/globals.scss";
import Head from "next/head";
import Preloader from "components/share/common/Preloader";
import ModalGlobal from "components/share/common/ModalGlobal";

toast.configure({ autoClose: 1500 });

function MyApp({ Component, pageProps, token }) {
  const router = useRouter();
  const [isShowLoader, setIsShowLoader] = useState(false);

  Router.onRouteChangeStart = () => {
    setIsShowLoader(true);
  };
  Router.onRouteChangeComplete = () => {
    setIsShowLoader(false);
  };
  Router.onRouteChangeError = () => {
    setIsShowLoader(false);
  };

  useEffect(() => {
    if (!token) {
      switch (router.pathname) {
        case "/auth/register":
          router.push("/auth/register");
          break;
        case "/auth/restore-pass":
          router.push("/auth/restore-pass");
          break;
        default:
          router.push("/auth/login");
          break;
      }
    }
  }, [router.pathname]);

  useEffect(() => {
    if (!token) return;
    SocketServices.initSocket(token);
    kenoServices.init();
  }, [token]);

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Head>
          <title>VietlottClub - Cơ Hội Để Tốt Hơn</title>
          <meta
            name="viewport"
            content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"
          />
        </Head>
        <div className={`${isShowLoader ? "overflow-hidden" : ""}`}>
          {isShowLoader ? <Preloader /> : null}
          <ModalGlobal />
          <Component {...pageProps} />
        </div>
      </PersistGate>
    </Provider>
  );
}

MyApp.getInitialProps = async ({ Component, ctx }) => {
  const cookie = parseCookies(ctx.req);
  const pageProps = Component.getInitialProps
    ? await Component.getInitialProps(ctx)
    : {};
  return { pageProps, token: get(cookie, "token", null) };
};

export default MyApp;
