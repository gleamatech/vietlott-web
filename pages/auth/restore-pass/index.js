import InputField from "components/share/common/Form/InputField";
import BaseAPI from "config/BaseApi";
import { FastField, Form, Formik, Field } from "formik";
import React, { useMemo, useState } from "react";
import { toast } from "react-toastify";
import BackLink from "../../../components/share/common/BackLink";
import firebase from "config/firebaseConfig";
import "firebase/auth";
import "firebase/analytics";
import Button from "components/share/common/Button";
import IconEva from "components/share/common/IconEva";
import { useRouter } from "next/router";

const RestorePass = () => {
  const [isSend, setIsSend] = useState(false);
  const [isShowPass, setIsShowPass] = useState(false);
  const router = useRouter();
  const initialValues = {
    phoneNumber: "",
    password: "",
    passwordConfirm: "",
    verifyCode: "",
  };
  const handleRestorePass = async ({ phoneNumber, password, verifyCode }) => {
    try {
      confirmationResult
        .confirm(verifyCode)
        .then(async (result) => {
          const newToken = await result.user.getIdToken();
          await BaseAPI.post("/user/reset", {
            resetToken: newToken,
            newPassword: password,
          })
            .then((success) => {
              toast.success("Mật khẩu đã được khôi phục");
              router.push("/auth/login");
            })
            .catch((error) => {
              toast.info(
                "Vui lòng kiểm tra lại thông tin hoặc tài khoản đã tồn tại"
              );
            });
        })
        .catch((error) => {
          toast.info("Mã xác nhận không chính xác, vui lòng thử lại!");
        });
    } catch (err) {
      toast.info("Chưa có mã xác thực");
    }
  };

  const onSignInSubmit = (phoneNumber) => () => {
    const phoneValid = `+84${phoneNumber.slice(1)}`;
    const appVerifier = new firebase.auth.RecaptchaVerifier(
      "recaptcha-container",
      {
        size: "invisible",
      }
    );
    firebase
      .auth()
      .signInWithPhoneNumber(phoneValid, appVerifier)
      .then((confirmationResult) => {
        window.confirmationResult = confirmationResult;
        toast.info("Tin nhắn đã gửi , vui lòng kiểm tra điện thoại!");
      })
      .catch((error) => {
        toast.info("Xảy ra lỗi, vui lòng kiểm tra lại số điện thoại !");
        console.log("error", error);
      });
    setIsSend(true);
  };
  const handleShowPass = () => {
    setIsShowPass(!isShowPass);
  };
  const renderIconShowPass = useMemo(() => {
    return (
      <div>
        <div
          className={!isShowPass ? "hidden" : "opacity-50 cursor-pointer"}
          onClick={handleShowPass}
        >
          <IconEva name="eye-outline" height="20" width="20" />
        </div>
        <div
          className={!isShowPass ? "opacity-50 cursor-pointer" : "hidden"}
          onClick={handleShowPass}
        >
          <IconEva name="eye-off-outline" height="20" width="20" />
        </div>
      </div>
    );
  }, [isShowPass]);
  return (
    <div>
      <BackLink backLink="/auth/login" title="Khôi phục mật khẩu" />
      <div className="w-full fixed p-1 bg-white mt-1 rounded-2xl">
        <Formik initialValues={initialValues} onSubmit={handleRestorePass}>
          {(formikProps) => {
            const { values } = formikProps;
            const { phoneNumber, password, passwordConfirm, verifyCode } =
              values;
            return (
              <Form>
                <div className="flex flex-col mb-1">
                  <FastField
                    name="phoneNumber"
                    component={InputField}
                    type="text"
                    label="Số điện thoại"
                    placeholder="Nhập số điện thoại của bạn"
                  />
                </div>
                <div className="flex flex-col mb-1">
                  <label className="block pt-0.5 text-gray-500 text-lg font-normal">
                    Mã xác thực
                  </label>
                  <div className="relative">
                    <FastField
                      name="verifyCode"
                      component={InputField}
                      type="text"
                      placeholder="Gửi và nhập mã xác nhận"
                      isVerifyCode
                    />
                    <div className="absolute top-1/2 transform -translate-y-1/2 right-1">
                      <Button
                        id="recaptcha-container"
                        type="button"
                        buttonColor="btn-primary"
                        disabled={!phoneNumber.length || isSend}
                        onClick={onSignInSubmit(phoneNumber)}
                        className={`${
                          !phoneNumber.length || isSend
                            ? "bg-gray-400"
                            : "bg-blue-700"
                        }`}
                      >
                        Gửi mã
                      </Button>
                    </div>
                  </div>
                </div>
                <div className="flex flex-col mb-1">
                  <label className="block pt-0.5 text-gray-500 text-lg font-normal">
                    Mật khẩu mới
                  </label>
                  <div className="relative">
                    <Field
                      name="password"
                      type={!isShowPass ? "password" : "text"}
                      placeholder="Nhập mật khẩu mới"
                      className={`appearance-none text-xl px-1 border border-gray-200 bg-gray-50 rounded w-full text-gray-800 focus:outline-none focus:border-blue-100`}
                    />
                    <div className="absolute top-1/2 transform -translate-y-1/2 right-1">
                      {renderIconShowPass}
                    </div>
                  </div>
                </div>
                <div className="flex flex-col mb-1">
                  <label className="block pt-0.5 text-gray-500 text-lg font-normal">
                    Xác nhận mật khẩu mới
                  </label>
                  <div className="relative">
                    <Field
                      name="passwordConfirm"
                      type={!isShowPass ? "password" : "text"}
                      placeholder="Xác nhận mật khẩu mới"
                      className={`appearance-none text-xl px-1 border border-gray-200 bg-gray-50 rounded w-full text-gray-800 focus:outline-none focus:border-blue-100`}
                    />
                    <div className="absolute top-1/2 transform -translate-y-1/2 right-1">
                      {renderIconShowPass}
                    </div>
                  </div>
                </div>
                <Button
                  type="submit"
                  disabled={
                    !password.length ||
                    !phoneNumber.length ||
                    !passwordConfirm.length ||
                    passwordConfirm != password
                  }
                  buttonColor="btn-primary"
                  className={` ${
                    !password.length ||
                    !phoneNumber.length ||
                    !passwordConfirm.length ||
                    passwordConfirm != password
                      ? "bg-gray-400"
                      : "bg-blue-700"
                  } w-full`}
                >
                  Khôi phục mật khẩu
                </Button>
              </Form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
};

export default React.memo(RestorePass);
