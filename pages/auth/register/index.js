import Button from "components/share/common/Button";
import ShowPDF from "components/share/common/ContentModal/ShowPDF";
import InputField from "components/share/common/Form/InputField";
import IconEva from "components/share/common/IconEva";
import BaseAPI from "config/BaseApi";
import firebase from "config/firebaseConfig";
import "firebase/analytics";
import "firebase/auth";
import { FastField, Field, Form, Formik, useField } from "formik";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useMemo, useState } from "react";
import { toast } from "react-toastify";
import Calendar from "react-calendar";
import moment from "moment";
import "react-calendar/dist/Calendar.css";

const Register = () => {
  const initialValues = {
    phoneNumber: "",
    password: "",
    passwordConfirm: "",
    name: "",
    verifyCode: "",
  };
  const router = useRouter();
  const [isShowPass, setIsShowPass] = useState(false);
  const [dateSelect, setDateSelect] = useState(new Date());
  const [dateOfBirth, setDateOfBirth] = useState("");

  const handleRegister = async ({
    phoneNumber,
    password,
    name,
    passwordConfirm,
    verifyCode,
  }) => {
    try {
      const code = verifyCode;
      window.confirmationResult
        .confirm(code)
        .then(async (result) => {
          await BaseAPI.post("/user/signup", {
            phoneNumber,
            name,
            password,
            passwordConfirm,
            dateOfBirth: new Date(dateSelect),
          })
            .then((success) => {
              toast.success("Đăng kí thành công");
              router.push("/auth/login");
            })
            .catch((error) => {
              toast.info(
                "Vui lòng kiểm tra lại thông tin hoặc tài khoản đã tồn tại"
              );
            });
        })
        .catch((error) => {
          toast.info("Mã xác nhận không chính xác, vui lòng thử lại!");
        });
    } catch (err) {
      toast.info("Mã xác nhận không chính xác, vui lòng thử lại!");
    }
  };

  const onSignInSubmit = (phoneNumber) => () => {
    const phoneValid = `+84${phoneNumber.slice(1)}`;
    const appVerifier = new firebase.auth.RecaptchaVerifier(
      "recaptcha-container",
      {
        size: "invisible",
      }
    );
    firebase
      .auth()
      .signInWithPhoneNumber(phoneValid, appVerifier)
      .then((confirmationResult) => {
        window.confirmationResult = confirmationResult;
        toast.info("Tin nhắn đã gửi , vui lòng kiểm tra điện thoại!");
      })
      .catch((error) => {
        toast.info("Xảy ra lỗi, vui lòng kiểm tra lại số điện thoại !");
      });
  };
  const handleShowPDF = () => {
    return window.openModal({
      content: <ShowPDF />,
      isShowHeader: false,
    });
  };
  const handleShowPass = () => {
    setIsShowPass(!isShowPass);
  };
  const renderIconShowPass = useMemo(() => {
    return (
      <div>
        <div
          className={!isShowPass ? "hidden" : "opacity-50 cursor-pointer"}
          onClick={handleShowPass}
        >
          <IconEva name="eye-outline" height="20" width="20" />
        </div>
        <div
          className={!isShowPass ? "opacity-50 cursor-pointer" : "hidden"}
          onClick={handleShowPass}
        >
          <IconEva name="eye-off-outline" height="20" width="20" />
        </div>
      </div>
    );
  }, [isShowPass]);

  const handleChooseDate = () => {
    console.log({ dateSelect }, "function");
    const dateFormat = moment(dateSelect).format("L");
    setDateOfBirth(dateFormat);
    window.closeModal();
  };

  const onChangeDate = (date) => {
    // const dateChoose = new Date(date);
    setDateSelect(date);
  };

  const renderCalendar = useMemo(() => {
    return <Calendar onChange={onChangeDate} defaultValue={dateSelect} />;
  }, [dateSelect, dateOfBirth]);

  const onFocus = () => {
    window.openModal({
      content: renderCalendar,
      isShowHeader: false,
      isShowActions: true,
      // txtCancel: "Huỷ",
      txtConfirm: "Chọn",
      // handleCancel: () => window.closeModal(),
      handleConfirm: handleChooseDate,
    });
  };
  return (
    <div className="w-full fixed bottom-0 left-0 right-0 bg-white p-1 rounded-2xl">
      <p className="text-center p-0.5 text-4xl">Đăng Ký</p>
      <Formik initialValues={initialValues} onSubmit={handleRegister}>
        {(formikProps) => {
          const { values } = formikProps;
          const { phoneNumber, password, passwordConfirm, name, verifyCode } =
            values;
          return (
            <Form>
              <div className="flex flex-col mb-1">
                <FastField
                  name="phoneNumber"
                  component={InputField}
                  type="text"
                  label="Số điện thoại"
                  placeholder="Nhập số điện thoại"
                />
                <label
                  htmlFor="numberPhone"
                  className="block pt-0.5 text-gray-500 text-lg font-normal"
                >
                  Nhập số điện thoại nhận mã xác nhận
                </label>
              </div>
              <div className="flex flex-col mb-1">
                <label className="block pt-0.5 text-gray-500 text-lg font-normal">
                  Mật khẩu
                </label>
                <div className="relative">
                  <Field
                    name="password"
                    type={!isShowPass ? "password" : "text"}
                    placeholder="Nhập mật khẩu"
                    className={`appearance-none text-xl px-1 border border-gray-200 bg-gray-50 rounded w-full text-gray-800 focus:outline-none focus:border-blue-100`}
                  />
                  <div className="absolute top-1/2 transform -translate-y-1/2 right-1">
                    {renderIconShowPass}
                  </div>
                </div>
                <label
                  htmlFor="password"
                  className="block pt-0.5 text-gray-500 text-lg font-normal"
                >
                  Tối thiểu 6 ký tự
                </label>
              </div>
              <div className="flex flex-col mb-1">
                <label className="block pt-0.5 text-gray-500 text-lg font-normal">
                  Xác nhận mật khẩu
                </label>
                <div className="relative">
                  <Field
                    name="passwordConfirm"
                    type={!isShowPass ? "password" : "text"}
                    placeholder="Nhập lại mật khẩu"
                    className={`appearance-none text-xl px-1 border border-gray-200 bg-gray-50 rounded w-full text-gray-800 focus:outline-none focus:border-blue-100`}
                  />
                  <div className="absolute top-1/2 transform -translate-y-1/2 right-1">
                    {renderIconShowPass}
                  </div>
                </div>
              </div>
              <div className="flex flex-col mb-1">
                <FastField
                  name="name"
                  component={InputField}
                  type="text"
                  label="Họ tên"
                  placeholder="Nhập tên định danh"
                />
                <label
                  htmlFor="password"
                  className="block pt-0.5 text-gray-500 text-lg font-normal"
                >
                  Tên được ghi trên CCCD/CMND
                </label>
              </div>
              <div className="flex flex-col mb-1">
                <InputField
                  field="dateOfBirth"
                  type="text"
                  label="Ngày tháng năm sinh"
                  placeholder="Nhập ngày tháng năm sinh"
                  onFocus={onFocus}
                  value={moment(dateSelect).format("L")}
                />
                <label
                  htmlFor="password"
                  className="block pt-0.5 text-gray-500 text-lg font-normal"
                >
                  Được ghi trên CCCD/CMND
                </label>
              </div>
              <div className="flex flex-col mb-1">
                <label className="block pt-0.5 text-gray-500 text-lg font-normal">
                  Mã xác thực
                </label>
                <div className="relative">
                  <FastField
                    name="verifyCode"
                    component={InputField}
                    type="text"
                    placeholder="Nhập tên định danh"
                    isVerifyCode
                  />
                  <div className="absolute top-1/2 transform -translate-y-1/2 right-1">
                    <div id="recaptcha-container"></div>
                    <Button
                      type="button"
                      disabled={!phoneNumber.length}
                      buttonColor="btn-primary"
                      onClick={onSignInSubmit(phoneNumber)}
                      className={`${
                        !phoneNumber.length ? "bg-gray-400" : "bg-blue-700"
                      }`}
                    >
                      Gửi
                    </Button>
                  </div>
                </div>
              </div>
              <div className="flex justify-between pt-1 pb-1.5">
                <p className="text-blue-700 text-xl" onClick={handleShowPDF}>
                  Khi đăng ký bạn đã đồng ý với các điều khoản của chúng tôi.
                </p>
              </div>
              <Button
                type="submit"
                disabled={
                  !password.length ||
                  !phoneNumber.length ||
                  !passwordConfirm.length ||
                  !name.length ||
                  !verifyCode.length ||
                  !dateOfBirth.length
                }
                buttonColor="btn-primary"
                className={` ${
                  !password.length ||
                  !phoneNumber.length ||
                  !passwordConfirm.length ||
                  !name.length ||
                  !verifyCode.length ||
                  !dateOfBirth.length
                    ? "bg-gray-400"
                    : "bg-blue-700"
                } w-full`}
              >
                Đăng ký
              </Button>
              <Link href="/auth/login">
                <div className="flex justify-between py-1">
                  <p>Bạn đã có tài khoản ?</p>
                  <p className="text-blue-700">Đăng nhập ngay</p>
                </div>
              </Link>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};

export default React.memo(Register);
