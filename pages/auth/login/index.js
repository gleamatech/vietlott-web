import Button from "components/share/common/Button";
import InputField from "components/share/common/Form/InputField";
import IconEva from "components/share/common/IconEva";
import BaseAPI from "config/BaseApi";
import { FastField, Field, Form, Formik } from "formik";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useMemo, useState } from "react";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";
import { setUser } from "store/user";
import * as Yup from "yup";

const Login = () => {
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false);
  const [isShowPass, setIsShowPass] = useState(false);
  const router = useRouter();
  const isVietnamesePhoneNumber =
    /([\+84|84|0]+(3|5|7|8|9|1[2|6|8|9]))+([0-9]{8})\b/;

  const initialValues = {
    phoneNumber: "",
    password: "",
  };

  const LogInSchema = Yup.object().shape({
    phoneNumber: Yup.string().matches(
      isVietnamesePhoneNumber,
      "Vui lòng nhập số điện thoại Việt Nam"
    ),
  });

  const handleSubmit = async ({ phoneNumber, password }) => {
    try {
      setIsLoading(true);
      const response = await BaseAPI.post("user/login", {
        phoneNumber,
        password,
      });
      if (response.status) {
        dispatch(setUser(response));
        toast.success("Đăng nhập thành công");
        router.push("/");
        setIsLoading(false);
      }
    } catch (error) {
      toast.error("Nhập thông tin sai");
      setIsLoading(false);
    }
  };
  const handleShowPass = () => {
    setIsShowPass(!isShowPass);
  };
  const renderIconShowPass = useMemo(() => {
    return (
      <div>
        <div
          className={!isShowPass ? "hidden" : "opacity-50 cursor-pointer"}
          onClick={handleShowPass}
        >
          <IconEva name="eye-outline" height="20" width="20" />
        </div>
        <div
          className={!isShowPass ? "opacity-50 cursor-pointer" : "hidden"}
          onClick={handleShowPass}
        >
          <IconEva name="eye-off-outline" height="20" width="20" />
        </div>
      </div>
    );
  }, [isShowPass]);
  return (
    <div className="">
      <Link href="/">
        <img
          src="/images/logo/logo_web.png"
          className="ml-16 mt-1.5 w-1/2 h-7 sm:w-1/6"
          alt="Logo"
        />
      </Link>
      <div className="absolute top-0 bottom-0 left-0 right-0 h-screen w-full -z-1">
        <img
          src="/images/background/login.jpg"
          alt="bg-img"
          className="h-screen w-full object-cover"
        />
      </div>
      <div className="w-full fixed bottom-0 left-0 right-0 bg-white p-1 rounded-2xl">
        <p className="text-center p-0.5 text-4xl">Đăng nhập</p>
        <Formik
          initialValues={initialValues}
          onSubmit={handleSubmit}
          validationSchema={LogInSchema}
        >
          {(formikProps) => {
            const { values, errors } = formikProps;
            const { phoneNumber, password } = values;
            return (
              <Form>
                <div className="flex flex-col mb-1">
                  <FastField
                    name="phoneNumber"
                    component={InputField}
                    type="text"
                    label="Số điện thoại"
                    placeholder="Nhập số điện thoại"
                  />
                  {errors.phoneNumber ? (
                    <div className="text-blue-500 text-lg pt-0.5">
                      {errors.phoneNumber}
                    </div>
                  ) : null}
                </div>
                <div className="flex flex-col mb-1">
                  <label className="block pt-0.5 text-gray-500 text-lg font-normal">
                    Mật khẩu
                  </label>
                  <div className="relative">
                    <Field
                      name="password"
                      type={!isShowPass ? "password" : "text"}
                      placeholder="Nhập mật khẩu"
                      className={`appearance-none text-xl px-1 border border-gray-200 bg-gray-50 rounded w-full text-gray-800 focus:outline-none focus:border-blue-100`}
                    />
                    <div className="absolute top-1/2 transform -translate-y-1/2 right-1">
                      {renderIconShowPass}
                    </div>
                  </div>
                </div>
                <div className="flex justify-between pt-1 pb-1.5">
                  <p>Quên mật khẩu ?</p>
                  <Link href="/auth/restore-pass">
                    <p className="text-blue-700">Khôi phục mật khẩu</p>
                  </Link>
                </div>
                <Button
                  type="submit"
                  className={`w-full`}
                  isLoading={isLoading}
                  disabled={
                    !password.length || !phoneNumber.length || isLoading
                  }
                >
                  Đăng nhập
                </Button>
              </Form>
            );
          }}
        </Formik>
        <Link href="/auth/register">
          <div className="flex justify-center py-1">
            <p>
              Bạn chưa có tài khoản ? 
              <span className="ml-1 text-blue-700">Đăng ký tại đây</span>
            </p>
          </div>
        </Link>
      </div>
    </div>
  );
};

export default React.memo(Login);
