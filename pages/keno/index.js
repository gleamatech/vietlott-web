import Container from "components/share/common/Container";
import BoxSelectNumber from "components/share/common/ContentModal/BoxSelectNumber";
import Select from "components/share/common/Select";
import BoxSelect from "components/share/lottery/BoxSelect";
import LotteryBox from "components/share/lottery/LotteryBox";
import Total from "components/share/lottery/Total";
import { LIST_TYPE_LOTTERY, LOTTERY_DATA } from "constants/lottery";
import { cloneDeep } from "lodash";
import React, { useMemo, useState } from "react";

const dataDefaultKeno = {
  ...LOTTERY_DATA,
  money: 0,
};

const Keno = () => {
  const [listTable, setListTable] = useState([
    cloneDeep(dataDefaultKeno),
    cloneDeep(dataDefaultKeno),
    cloneDeep(dataDefaultKeno),
  ]);
  const dataKeno = LIST_TYPE_LOTTERY.keno;
  const [selectPeriod, setSelectPeriod] = useState(1);

  const totalMoney = useMemo(() => {
    const total = listTable.reduce((total, item) => total + item.money, 0);
    return total * selectPeriod;
  }, [listTable, selectPeriod]);

  const handleClickButton = ({ data, index }) => {
    const arrTemp = cloneDeep(listTable);
    arrTemp[index] = data;
    setListTable(arrTemp);
  };

  const onChangeSelect = (value) => {
    setSelectPeriod(value);
  };

  const handleOpenModal =
    ({ index }) =>
    () => {
      const dataModal = listTable[index];
      window.openModal({
        content: (
          <BoxSelectNumber
            gameType={dataKeno.slug}
            index={index}
            data={dataModal}
            isBigSmall={index === 2}
            length={dataKeno.number.length}
            handleClickButton={handleClickButton}
          />
        ),
        isShowHeader: false,
      });
    };

  const renderListBoxSelect = useMemo(() => {
    return listTable.map((box, index) => (
      <div key={index} className="mb-1.5">
        <BoxSelect
          key={index}
          index={index}
          handleOpenModal={handleOpenModal}
          data={box}
          isBigSmall={index === 2}
        />
      </div>
    ));
  }, [listTable]);

  return (
    <Container>
      <div className="w-full">
        <LotteryBox isDetailLottery data={dataKeno} isKeno />
        <div className="flex justify-center flex-col w-full mt-2">
          <Select
            label={"Số kỳ mua"}
            defaultValue={selectPeriod}
            options={dataKeno.period}
            onChangeSelect={onChangeSelect}
          />
        </div>
      </div>

      <div
        className="w-full mt-2 rounded overflow-y-auto mb-40 hidden-scrollbar"
        style={{ maxHeight: "70vh" }}
      >
        {renderListBoxSelect}
      </div>
      <Total
        price={totalMoney}
        listTable={listTable}
        selectPeriod={selectPeriod}
        gameType="keno"
      />
    </Container>
  );
};

export default React.memo(Keno);
