import { LIST_TYPE_LOTTERY } from "@/constants/lottery";
import NavBar from "components/share/common/NavBar";
import LotteryBox from "components/share/lottery/LotteryBox";
import BaseAPI from "config/BaseApi";
import lottie from "lottie-web";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useEffect, useRef, useState } from "react";
import { useDispatch } from "react-redux";
import Ticker from "react-ticker";
import { setDataMega, setDataPower } from "store/lottery";
const HomeScreen = () => {
  const router = useRouter();
  const { TOGETHER, KENO, POWER, MEGA, LOTO } = LIST_TYPE_LOTTERY;
  const listTypeLottery = [
    TOGETHER,
    LIST_TYPE_LOTTERY.keno,
    LIST_TYPE_LOTTERY.mega,
    LIST_TYPE_LOTTERY.power,
    LOTO,
  ];
  const blockAnimation = useRef(null);
  const [contentMarquee, setContentMarquee] = useState(
    "Công Ty TNHH Phát Thương Đông Dương [VietlottClub.com].Trân Trọng Kính Chào Quý Khách."
  );

  const handleClickButton = (item) => {
    if (item.slug != "loto") router.push(`${item.slug}`);
  };
  const dispatch = useDispatch();
  const fetchPrices = async () => {
    const res = await BaseAPI.get("/lottery/prices");
    if (res.status) {
      dispatch(setDataMega(res.data.mega));
      dispatch(setDataPower(res.data.power));
    }
  };
  useEffect(() => {
    fetchPrices();
    lottie.loadAnimation({
      container: blockAnimation.current,
      renderer: "svg",
      loop: true,
      autoplay: true,
      animationData: require("../public/images/lotties/logo.json"),
    });
    let interval = setInterval(() => {
      getAds();
    }, 30000);
    return () => {
      clearInterval(interval);
    };
  }, []);

  const getAds = async () => {
    try {
      const response = await BaseAPI.get("ads/ads");
      if (response.status) {
        setContentMarquee(response.data.doc[0].content);
      }
    } catch (error) {}
  };

  const showContentMarque = () => {
    return (
      <>
        <div style={{ whiteSpace: "nowrap" }}>{contentMarquee}</div>
      </>
    );
  };
  return (
    <div className="h-screen bg-gray-color">
      <NavBar />
      <Link href="/" className="w-full">
        <img
          src="images/logo/logo_web.png"
          className="ml-16 w-1/2 h-7 sm:w-1/6"
          alt=""
        />
      </Link>
      <Ticker offset="run-in" speed={10} mode="await">
        {showContentMarque}
      </Ticker>
      <div className="logo flex justify-center mt-2 -mb-3 sm:-mb-5 z-0 ">
        <div className="w-6/12 h-52 sm:h-auto" ref={blockAnimation}></div>
      </div>
      <div className="container mx-auto pb-7.5 flex flex-col items-center px-1.5">
        {listTypeLottery.map((item, index) => (
          <div key={index} className="w-full mt-1.5 z-10">
            <LotteryBox
              isShowButton
              handleClickButton={handleClickButton}
              data={item}
            />
          </div>
        ))}
      </div>
    </div>
  );
};

export default React.memo(HomeScreen);
