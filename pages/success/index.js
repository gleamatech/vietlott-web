import Button from "components/share/common/Button";
import Header from "components/share/common/Header";
import Lottie from "lottie-web";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { Fragment, useEffect, useRef } from "react";
const SuccessScreen = () => {
  const blockAnimation = useRef(null);
  useEffect(() => {
    Lottie.loadAnimation({
      container: blockAnimation.current,
      renderer: "svg",
      loop: true,
      autoplay: true,
      animationData: require("../../public/images/lotties/lottery-success.json"),
    });
  }, []);
  const router = useRouter();
  const handleBackLink = () => {
    router.back();
  };
  return (
    <Fragment>
      <div className="mt-72 ">
        <div className="sm:h-96 h-80" ref={blockAnimation}></div>
      </div>
      <div className="text-center text-xl sm:text-4xl">Chọn số thành công</div>
      <div className="text-center text-lg sm:text-2xl mt-2 sm:mt-2.5">
        Xin vui lòng chờ thông báo
      </div>
      <div className="text-center text-lg sm:text-2xl mt-1.5 sm:mt-2">
        Vé mua thành công khi nhận được hình ảnh vé!
      </div>
      <div className="logo flex justify-center mt-1.5 sm:mt-2">
        <Link href="/">
          <Button className="btn btn-primary mx-0.5 w-36 sm:w-40">
            Trang chủ
          </Button>
        </Link>

        <Button
          className="btn btn-primary mx-0.5 w-36 sm:w-40"
          onClick={handleBackLink}
        >
          Mua tiếp
        </Button>
      </div>
    </Fragment>
  );
};

export default React.memo(SuccessScreen);
