import OptionPeriod from "components/ScreenPage/MegaPowerScreen/OptionPeriod";
import Container from "components/share/common/Container";
import BoxSelectNumber from "components/share/common/ContentModal/BoxSelectNumber";
import ContentAlert from "components/share/common/ContentModal/Notification";
import Select from "components/share/common/Select";
import BoxSelect from "components/share/lottery/BoxSelect";
import LotteryBox from "components/share/lottery/LotteryBox";
import Total from "components/share/lottery/Total";
import { LIST_TYPE_LOTTERY, LOTTERY_DATA } from "constants/lottery";
import { cloneDeep } from "lodash";
import React, { Fragment, useMemo, useState } from "react";
import RuleRandom from "components/ScreenPage/MegaPowerScreen/RuleRandom";

const dataDefaultPower = {
  ...LOTTERY_DATA,
  money: 0,
};
const power = () => {
  const [listTable, setListTable] = useState([
    cloneDeep(dataDefaultPower),
    cloneDeep(dataDefaultPower),
    cloneDeep(dataDefaultPower),
    cloneDeep(dataDefaultPower),
    cloneDeep(dataDefaultPower),
  ]);
  const dataPower = LIST_TYPE_LOTTERY.power;
  const [valueSelectLottery, setValueSelectLottery] = useState(6);
  const [selectPeriod, setSelectPeriod] = useState(1);
  const [buyLottey, setBuyLottey] = useState(false);
  const [buyRand, setBuyRand] = useState(false);
  const [dashboardPage, setDashboardPage] = useState(true);
  const [valueTable, setvalueTable] = useState();
  const [valueLottery, setvalueLottery] = useState();
  const [tabs, setTabs] = useState([
    { title: " Kỳ trùng bộ số", value: "same", current: true },
    { title: " Kỳ khác bộ số", value: "diff", current: false },
  ]);

  const handleClickButton = (data, index) => {
    const arrTemp = cloneDeep(listTable);
    arrTemp[index] = data;
    setListTable(arrTemp);
  };

  const handleSelectLottery = (value) => {
    setValueSelectLottery(Number(value));
  };

  const moneySelectLottery = useMemo(() => {
    const data = dataPower.lottery.find(
      (option) => option.value == valueSelectLottery
    );
    return data.money;
  }, [valueSelectLottery]);

  const handleSelectPeriod = (value) => {
    setSelectPeriod(Number(value));
  };

  const handleOpenModal =
    ({ index }) =>
    () => {
      const dataModal = listTable[index];
      window.openModal({
        content: (
          <BoxSelectNumber
            gameType={dataPower.slug}
            index={index}
            data={dataModal}
            length={dataPower.number.length}
            handleClickButton={handleClickButton}
            moneySelectLottery={moneySelectLottery}
          />
        ),
        isShowHeader: false,
      });
    };

  const handleBuyLot = () => {
    setBuyLottey(true);
    setDashboardPage(false);
  };

  const handleShowModalRand = () => {
    window.openModal({
      content: (
        <ContentAlert
          titleModal={"Xác nhận"}
          textModal={
            "Vé được thiết bị của vietlot chọn ngẫu nhiên. Không cung cấp dịch vụ tự dò. Vé được lưu trong phần lịch sử hình ảnh"
          }
          titleSubmit={"Đồng ý"}
          titleCancel={"Quay lại"}
          handleOnClickSubmit={handleSubmitRand}
        />
      ),
      isShowHeader: false,
    });
  };

  const handleBackButton = () => {
    setListTable([
      cloneDeep(dataDefaultPower),
      cloneDeep(dataDefaultPower),
      cloneDeep(dataDefaultPower),
      cloneDeep(dataDefaultPower),
      cloneDeep(dataDefaultPower),
    ]);
    setvalueTable(0);
    setvalueLottery(0);
    setBuyLottey(false);
    setBuyRand(false);
    setDashboardPage(true);
  };

  const handleTablePlay = (e) => {
    const { value } = e.target;
    const numberTable = Number(value);
    if (numberTable > 6) {
      return toast.warning("Tối đa 6 bảng");
    }
    setvalueTable(numberTable);
  };

  const handleValueBuy = (e) => {
    const { value } = e.target;
    const valueBuy = Number(value);
    setvalueLottery(valueBuy);
  };

  useMemo(() => {
    listTable.map((item) => {
      if (item.listSelect.length == valueSelectLottery) {
        item.money = moneySelectLottery;
      } else {
        item.money = 0;
      }
      item.level = valueSelectLottery;
      item.period = selectPeriod;
    });
    return listTable;
  }, [valueSelectLottery, selectPeriod]);

  const totalMoney = useMemo(() => {
    let total;
    // handle money when buy random
    if (valueTable && valueLottery) {
      total = moneySelectLottery * valueTable * valueLottery;
    } else if (valueTable) {
      total = moneySelectLottery * valueTable;
    } else if (valueLottery) {
      total = moneySelectLottery * valueLottery;
    }
    //hand money when buy normal
    else {
      total = listTable.reduce((total, item) => total + item.money, 0);
    }
    return total * selectPeriod;
  }, [listTable, selectPeriod, valueSelectLottery, valueTable, valueLottery]);

  const handleClickTab = (item, index) => () => {
    for (let i in tabs) {
      if (tabs[i].current === true) {
        tabs[i].current = false;
        break;
      }
    }
    setTabs([
      ...tabs.slice(0, index),
      { ...tabs[index], current: !item.current },
      ...tabs.slice(index + 1),
    ]);
  };

  const renderBody = useMemo(() => {
    switch (true) {
      case buyLottey:
        return (
          <Fragment>
            <div
              className="w-full mt-2 rounded overflow-y-auto mb-40 hidden-scrollbar"
              style={{ maxHeight: "70vh", height: "70vh" }}
            >
              {listTable.map((box, index) => (
                <div key={index} className="mb-1.5">
                  <BoxSelect
                    key={index}
                    index={index}
                    handleOpenModal={handleOpenModal}
                    data={box}
                  />
                </div>
              ))}
            </div>
          </Fragment>
        );
      case buyRand:
        return (
          <Fragment>
            <div
              className="w-full mt-2.5 overflow-y-auto mb-40 hidden-scrollbar"
              style={{ maxHeight: "70vh", height: "70vh" }}
            >
              <label className="label">
                <span className=" text-lg">Số bảng chơi</span>
              </label>
              <input
                type="number"
                id="tablePlay"
                placeholder="Nhập số bảng bạn muốn chơi"
                className="input input-bordered w-full mb-1.5 text-xl bg-white"
                onChange={handleTablePlay}
              />
              <label className="label">
                <span className=" text-lg">Số lượng mua</span>
              </label>
              <input
                type="number"
                id="valueBuy"
                placeholder="Nhập số lượng vé bạn muốn mua"
                className="input input-bordered w-full mb-1.5 text-xl bg-white"
                onChange={handleValueBuy}
              />
            </div>
          </Fragment>
        );
      default:
        return (
          <Fragment>
            <div
              className=" w-full flex flex-col justify-between container mt-2 overflow-y-auto mb-40 hidden-scrollbar"
              style={{ maxHeight: "70vh", height: "70vh" }}
            >
              <div
                className=" border-red-400 border-1 flex flex-col justify-center items-center uppercase text-red-600 rounded-lg font-bold"
                onClick={handleBuyLot}
                style={{ height: "47%" }}
              >
                Mua vé chọn số
              </div>
              <label
                onClick={handleShowModalRand}
                className="relative border-red-400 border-1 flex flex-col justify-center items-center uppercase text-red-600 rounded-lg bg-cover bg-center font-bold"
                style={{
                  backgroundImage: "url(/images/logo/buyRanLottery.jpg)",
                  height: "47%",
                }}
              >
                Mua vé tự chọn
                <div className="absolute top-1 right-1">
                  <RuleRandom />
                </div>
              </label>
            </div>
          </Fragment>
        );
    }
  }, [buyLottey, buyRand, listTable, valueSelectLottery, selectPeriod]);

  const handleSubmitRand = () => {
    setBuyRand(true);
    setDashboardPage(false);
    window.closeModal();
  };

  return (
    <Container>
      <div className="w-full">
        <LotteryBox isDetailLottery data={dataPower} />
        {buyRand == true ? (
          <OptionPeriod tabs={tabs} handleClickTab={handleClickTab} />
        ) : (
          <></>
        )}

        <div className="w-full mt-2 grid grid-cols-2 gap-1">
          <div className="flex justify-center flex-col w-full">
            <Select
              label={"Số kỳ mua"}
              defaultValue={selectPeriod}
              options={dataPower.period}
              onChangeSelect={handleSelectPeriod}
            />
          </div>
          <div className="flex justify-center flex-col w-full">
            <Select
              options={dataPower.lottery}
              label={"Bao"}
              defaultValue={valueSelectLottery}
              onChangeSelect={handleSelectLottery}
            />
          </div>
        </div>
      </div>
      {renderBody}
      <Total
        price={totalMoney}
        listTable={listTable}
        selectPeriod={selectPeriod}
        gameType="power"
        handleBack={handleBackButton}
        dashboardPage={dashboardPage}
        valueLottery={valueLottery}
        valueTable={valueTable}
        tabs={tabs.filter((item) => item.current == true)}
        isRandom={buyRand}
      />
    </Container>
  );
};

export default React.memo(power);
