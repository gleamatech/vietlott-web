import BoxSelectTogether from "components/ScreenPage/TogetherScreen/BoxSelectTogether";
import ListTab from "components/ScreenPage/TogetherScreen/ListTab";
import Button from "components/share/common/Button";
import Container from "components/share/common/Container";
import TogetherCreateRoom from "components/share/common/ContentModal/TogetherCreateRoom";
import { LIST_TYPE_LOTTERY, lotteryType } from "constants/lottery";
import React, { useMemo, useRef, useState } from "react";
import { Fragment } from "react";
import { useSelector } from "react-redux";

const Together = () => {
  const dataMega = LIST_TYPE_LOTTERY.mega;
  const dataPower = LIST_TYPE_LOTTERY.power;
  const lotteryMega = dataMega.lottery.filter((item) => item.value != 6);
  const lotteryPower = dataPower.lottery.filter((item) => item.value != 6);
  const [tabActive, setTabActive] = useState(lotteryType.mega);
  const [tabOption, setTabOption] = useState("5");
  const roomShare = useSelector((state) => state.lottery.roomShare);

  const selectTabOption = (option) => () => {
    setTabOption(option);
  };
  const myRef = useRef();
  const dataRoom = useMemo(() => {
    return roomShare[tabActive];
  }, [tabActive, roomShare]);

  const listType = useMemo(() => {
    return Object.keys(dataRoom).map((type, index) => (
      <div
        key={index}
        onClick={selectTabOption(type)}
        className={`text-gray-600 text-center ease-in-out duration-300 bg-white rounded-lg badge badge-ghost text-xl w-full py-1 px-1 my-1 mx-0.5 focus:outline-none font-medium ${
          tabOption === type
            ? "text-blue-500 border-4 font-medium border-blue-500"
            : ""
        }`}
      >
        {`Bao ${type}`}
      </div>
    ));
  }, [tabOption, tabActive]);

  const listRoom = useMemo(() => {
    return dataRoom[tabOption].map((room, index) => (
      <BoxSelectTogether key={index} item={room} />
    ));
  }, [tabOption, tabActive, dataRoom]);

  const renderIconRightViewModal = () => {
    return (
      <Fragment>
        <div className="flex justify-center items-center mx-1">
          <Button onClick={handleShowModalCreateRoom} className="">
            Tạo vé mới
          </Button>
        </div>
      </Fragment>
    );
  };
  const renderRightView = () => {
    return (
      <Button
        onClick={() => myRef.current.onCreateNewRoom()}
        className="normal-case text-lg m-0.5"
      >
        Tiếp tục
      </Button>
    );
  };
  const handleClickTab = (tab) => () => {
    setTabActive(tab.slug);
  };
  const handleShowModalCreateRoom = () => {
    return window.openModal({
      content: (
        <TogetherCreateRoom
          lotteryMega={lotteryMega}
          lotteryPower={lotteryPower}
          ref={myRef}
        />
      ),
      rightView: renderRightView(),
    });
  };

  return (
    <Container iconRightView={renderIconRightViewModal()}>
      <div className="w-full">
        <ListTab tabActive={tabActive} handleClickTab={handleClickTab} />
        <div className="flex overflow-x-scroll hidden-scrollbar">
          {listType}
        </div>
        {listRoom}
      </div>
    </Container>
  );
};

export default React.memo(Together);
