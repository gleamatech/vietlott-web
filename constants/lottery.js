export const KENO = "keno";
export const MAX_3D = "3d";
export const MAX_4D = "4d";
export const POWER = "power";
export const MEGA = "mega";
export const TOGETHER = "together";
export const LOTO = "loto";

export const lotteryType = {
  keno: "keno",
  power: "power",
  mega: "mega",
};

// options plus keno
export const BIG = "lon";
export const EQUAL_BIG = "hoa-lonnho";
export const SMALL = "nho";
export const EVEN = "chan";
export const EVEN_11 = "chan-11-12";
export const ODD_11 = "le-11-12";
export const EQUAL_EVEN = "hoa-chanle";
export const ODD = "le";
export const BIG_SMALL = "BIG_SMALL";
export const ODD_EVEN = "ODD_EVEN";

export const LOTTERY_DATA = {
  currentPeriod: 0,
  listSelect: [],
  listSelectedGroup: [], // use in buy lottery group
  isShowAlert: false,
  type: "",
  period: 1,
  money: 10000,
  level: 10,
  subGame: [],
  subGameName: {},
};

export const megaPrice = [
  { text: "Bao 5", value: 5, money: 400000 },
  { text: "Bao 6", value: 6, money: 10000 },
  { text: "Bao 7", value: 7, money: 70000 },
  { text: "Bao 8", value: 8, money: 280000 },
  { text: "Bao 9", value: 9, money: 840000 },
  { text: "Bao 10", value: 10, money: 2100000 },
  { text: "Bao 11", value: 11, money: 4620000 },
  { text: "Bao 12", value: 12, money: 9240000 },
  { text: "Bao 13", value: 13, money: 17160000 },
  { text: "Bao 14", value: 14, money: 30030000 },
  { text: "Bao 15", value: 15, money: 50050000 },
  { text: "Bao 18", value: 18, money: 185640000 },
];

export const powerPrice = [
  { text: "Bao 5", value: 5, money: 500000 },
  { text: "Bao 6", value: 6, money: 10000 },
  { text: "Bao 7", value: 7, money: 70000 },
  { text: "Bao 8", value: 8, money: 280000 },
  { text: "Bao 9", value: 9, money: 840000 },
  { text: "Bao 10", value: 10, money: 2100000 },
  { text: "Bao 11", value: 11, money: 4620000 },
  { text: "Bao 12", value: 12, money: 9240000 },
  { text: "Bao 13", value: 13, money: 17160000 },
  { text: "Bao 14", value: 14, money: 30030000 },
  { text: "Bao 15", value: 15, money: 50050000 },
  { text: "Bao 18", value: 18, money: 185640000 },
];

export const LIST_TYPE_LOTTERY = {
  keno: {
    image: "/images/logo/keno_logo.png",
    name: "Keno",
    slug: "keno",
    number: new Array(80),
    date: "Từ 6:00 đến 22:00 hằng ngày",
    time: "Mỗi 10 phút",
    period: [
      { text: "1 Kỳ", value: 1 },
      { text: "2 Kỳ", value: 2 },
      { text: "3 Kỳ", value: 3 },
      { text: "4 Kỳ", value: 4 },
      { text: "5 Kỳ", value: 5 },
      { text: "10 Kỳ", value: 10 },
      { text: "15 Kỳ", value: 15 },
      { text: "20 Kỳ", value: 20 },
      { text: "30 Kỳ", value: 30 },
    ],
    levels: [
      { text: "1", value: 1 },
      { text: "2", value: 2 },
      { text: "3", value: 3 },
      { text: "4", value: 4 },
      { text: "5", value: 5 },
      { text: "6", value: 6 },
      { text: "7", value: 7 },
      { text: "8", value: 8 },
      { text: "9", value: 9 },
      { text: "10", value: 10 },
    ],
    prices: [
      { text: "10.000", value: 10000 },
      { text: "20.000", value: 20000 },
      { text: "30.000", value: 30000 },
      { text: "40.000", value: 40000 },
      { text: "50.000", value: 50000 },
      { text: "100.000", value: 100000 },
      { text: "150.000", value: 150000 },
      { text: "200.000", value: 200000 },
      { text: "500.000", value: 500000 },
    ],
    listOptions: [
      {
        name: "Lớn: có 11 số trở lên từ 41 đến 80",
        display: "Lớn",
        value: BIG,
        type: BIG_SMALL,
      },

      {
        name: "Hoà Lớn-Nhỏ: có 10 số từ 01 đến 40 và 10 số từ 41-80",
        display: "Hoà Lớn-Nhỏ",
        value: EQUAL_BIG,
        type: BIG_SMALL,
      },
      {
        name: "Nhỏ: có 11 số trở lên từ 01 đến 40",
        display: "Nhỏ",
        value: SMALL,
        type: BIG_SMALL,
      },
      {
        name: "Chẵn: có 13 số trở lên là số chẵn",
        display: "Chẵn",
        value: EVEN,
        type: ODD_EVEN,
      },
      {
        name: "Chẵn 11-12: có 11 hoặc 12 số chẵn",
        display: "Chẵn 11-12",
        value: EVEN_11,
        type: ODD_EVEN,
      },
      {
        name: "Hoà Chẵn-Lẻ: có 10 số chẵn và 10 số lẻ",
        display: "Hoà Chẵn-Lẻ",
        value: EQUAL_EVEN,
        type: ODD_EVEN,
      },
      {
        name: "Lẻ 11-12: có 11 hoặc 12 số lẻ",
        display: "Lẻ 11-12",
        value: ODD_11,
        type: ODD_EVEN,
      },
      {
        name: "Lẻ: có 13 số trở lên là số lẻ",
        display: "Lẻ",
        value: ODD,
        type: ODD_EVEN,
      },
    ],
  },
  power: {
    image: "/images/logo/power655_logo.png",
    name: "Power 6/55",
    number: new Array(55),
    slug: "power",
    date: "Thứ 3 - Thứ 5 - Thứ 7",
    time: "Vào lúc 18:00",
    period: [
      { text: "1 Kỳ", value: 1 },
      { text: "2 Kỳ", value: 2 },
      { text: "3 Kỳ", value: 3 },
      { text: "4 Kỳ", value: 4 },
      { text: "5 Kỳ", value: 5 },
      { text: "6 Kỳ", value: 6 },
    ],
    lottery: powerPrice,
  },
  mega: {
    image: "/images/logo/mega645_logo.png",
    name: "Mega 6/45",
    number: new Array(45),
    slug: "mega",
    date: "Thứ 4 - Thứ 6 - Chủ Nhật",
    time: "Vào lúc 18:00",
    lottery: megaPrice,
    period: [
      { text: "1 Kỳ", value: 1 },
      { text: "2 Kỳ", value: 2 },
      { text: "3 Kỳ", value: 3 },
      { text: "4 Kỳ", value: 4 },
      { text: "5 Kỳ", value: 5 },
      { text: "6 Kỳ", value: 6 },
    ],
  },
  TOGETHER: {
    image: "/images/logo/together.jpg",
    name: "Bao 3 Miền",
    type: ["mega", "power"],
    slug: "together",
  },
  LOTO: {
    image: "/images/logo/lotodongnai.jpg",
    name: "Lô Tô Đồng Nai",
    type: [],
    slug: "loto",
  },
};
