import Cookies from "cookie";

export const parseCookies = (req) => {
  return Cookies.parse(req ? req.headers.cookie || "" : document.cookie);
};
