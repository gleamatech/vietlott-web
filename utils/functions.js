import moment from "moment";

export const formatCurrency = (money) => {
  const num = parseInt(money);
  return `${(num || 0).toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")}đ`;
};

export const classNames = (...classes) => {
  return classes.filter(Boolean).join(" ");
};

export const title = (index, subGame) => {
  const objTitle = {
    0: "A",
    1: "B",
    2: `${subGame ? "Trò Chơi Phụ" : "C"}`,
    3: "D",
    4: "E",
    5: "F",
  };
  return objTitle[index];
};

export const formatDate = (date) => moment(date).locale("vi").format("L");

export const totalLotteryWin = (listNumber) => {
  return listNumber.reduce((a, b) => a + b, 0);
};
