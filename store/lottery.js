import { createSlice } from "@reduxjs/toolkit";

export const lotteryModule = createSlice({
  name: "lottery",
  initialState: {
    user: {},
    listCopy: [],
    roomShare: {},
    selectLotteryKeno: {},
    selectPriceKeno: {},
    kenoData: {},
    megaData: {},
    powerData: {},
    lotteryHistory: { variableTab: "keno" },
    copyLotteryKeno: {},
  },
  reducers: {
    addListCopy: (state, action) => {
      state.listCopy = action.payload;
    },
    addSelectLotteryKeno: (state, action) => {
      state.selectLotteryKeno = action.payload;
    },
    addCopyLotteryKeno: (state, action) => {
      state.copyLotteryKeno = action.payload;
    },
    addSelectPriceKeno: (state, action) => {
      state.selectPriceKeno = action.payload;
    },
    addLottery: (state, action) => {
      state.infoLottery.push(action.payload);
    },
    setRooms: (state, action) => {
      state.roomShare = action.payload;
    },
    setDataKeno: (state, action) => {
      state.kenoData = action.payload;
    },
    setDataMega: (state, action) => {
      state.megaData = action.payload;
    },
    setDataPower: (state, action) => {
      state.powerData = action.payload;
    },
    setLotteryHistory: (state, action) => {
      state.lotteryHistory = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const {
  addListCopy,
  addLottery,
  setRooms,
  addSelectLotteryKeno,
  addSelectPriceKeno,
  setDataKeno,
  setDataMega,
  setDataPower,
  setLotteryHistory,
  addCopyLotteryKeno,
} = lotteryModule.actions;

export default lotteryModule.reducer;
