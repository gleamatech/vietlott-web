import { createSlice } from "@reduxjs/toolkit";
import Cookies from "js-cookie";

export const userModule = createSlice({
  name: "user",
  initialState: {
    user: {},
  },
  reducers: {
    setUser: (state, action) => {
      const { token, user } = action.payload;
      state.user = user;
      if (token) {
        Cookies.set("token", token);
      }
    },
    logOut: () => {
      Cookies.remove("token");
    },
  },
});

// Action creators are generated for each case reducer function
export const { setUser, logOut } = userModule.actions;

export default userModule.reducer;
