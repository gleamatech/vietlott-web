(function() {
var exports = {};
exports.id = 490;
exports.ids = [490];
exports.modules = {

/***/ 2268:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }



const Button = (_ref) => {
  let {
    children,
    isLoading,
    disabled,
    buttonColor = "btn-info",
    colorText = "text-white",
    className
  } = _ref,
      props = _objectWithoutProperties(_ref, ["children", "isLoading", "disabled", "buttonColor", "colorText", "className"]);

  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", _objectSpread(_objectSpread({
    className: `btn rounded border-0 normal-case text-lg ${colorText} ${buttonColor} ${isLoading ? "loading" : ""} ${className}`,
    disabled: disabled
  }, props), {}, {
    children: children
  }));
};

/* harmony default export */ __webpack_exports__["Z"] = (Button);

/***/ }),

/***/ 9566:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var components_share_common_Header__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5162);





const Container = ({
  children,
  renderRightView,
  iconRightView
}) => {
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    className: "bg-gray-100 relative",
    children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(components_share_common_Header__WEBPACK_IMPORTED_MODULE_2__/* .default */ .Z, {
      rightView: renderRightView,
      iconRightView: iconRightView
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
      className: "container mx-auto flex flex-col items-center pt-2 px-1.5",
      style: {
        maxHeight: "95vh"
      },
      children: children
    })]
  });
};

/* harmony default export */ __webpack_exports__["Z"] = (/*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default().memo(Container));

/***/ }),

/***/ 125:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": function() { return /* binding */ ContentModal_BoxSelectNumber; }
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(5282);
// EXTERNAL MODULE: ./components/share/common/Button.js
var Button = __webpack_require__(2268);
// EXTERNAL MODULE: ./components/share/common/Select.js
var Select = __webpack_require__(7598);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(9297);
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);
// EXTERNAL MODULE: external "react-redux"
var external_react_redux_ = __webpack_require__(79);
// EXTERNAL MODULE: ./constants/lottery.js
var lottery = __webpack_require__(1377);
;// CONCATENATED MODULE: ./components/ScreenPage/KenoScreen/LastModalKeno.js









const LastModalKeno = ({
  listInfoPrice,
  handleSelectPrice,
  handleCheckList,
  handleCancelLastBoxKeno,
  handleContinue,
  data
}) => {
  const selectPriceKeno = (0,external_react_redux_.useSelector)(state => state.lottery.selectPriceKeno);
  const listOptions = (0,external_react_.useMemo)(() => {
    return lottery/* LIST_TYPE_LOTTERY.keno.listOptions */.G7.keno.listOptions;
  }, []);
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(external_react_.Fragment, {
    children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
      className: "flex",
      children: /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "w-full",
        children: /*#__PURE__*/jsx_runtime_.jsx(Select/* default */.Z, {
          options: listInfoPrice,
          label: "Mệnh giá",
          onChangeSelect: handleSelectPrice,
          defaultValue: selectPriceKeno
        })
      })
    }), /*#__PURE__*/jsx_runtime_.jsx("div", {
      className: "w-full mt-1",
      children: listOptions.map((item, index) => /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: "flex items-center border-2 mb-0.5 pl-1 cursor-pointer",
        children: [/*#__PURE__*/jsx_runtime_.jsx("input", {
          type: "radio",
          id: `${item.name}`,
          name: "big small",
          value: item.value,
          className: "form-checkbox rounded-none border border-red-600 text-red-500 w-2 h-2",
          onClick: handleCheckList,
          defaultChecked: item.value == data.subGame
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: "flex flex-col ml-1 ",
          children: [/*#__PURE__*/jsx_runtime_.jsx("label", {
            htmlFor: `${item.name}`,
            className: "cursor-pointer",
            children: item.display
          }), /*#__PURE__*/jsx_runtime_.jsx("label", {
            htmlFor: `${item.name}`,
            className: "cursor-pointer",
            children: item.name
          })]
        })]
      }, index))
    }), /*#__PURE__*/jsx_runtime_.jsx("div", {
      className: "modal-action flex flex-col items-center w-full",
      children: /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "flex flex-col w-full",
        children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: "flex justify-between w-full max-w-full",
          children: [/*#__PURE__*/jsx_runtime_.jsx(Button/* default */.Z, {
            className: "btn bg-red-500 ",
            style: {
              width: "48%"
            },
            onClick: handleCancelLastBoxKeno,
            buttonColor: "btn-error",
            children: "H\u1EE7y"
          }), /*#__PURE__*/jsx_runtime_.jsx(Button/* default */.Z, {
            style: {
              width: "48%"
            },
            onClick: handleContinue,
            children: "Ti\u1EBFp theo"
          })]
        })
      })
    })]
  });
};

/* harmony default export */ var KenoScreen_LastModalKeno = (LastModalKeno);
// EXTERNAL MODULE: external "react-toastify"
var external_react_toastify_ = __webpack_require__(2034);
// EXTERNAL MODULE: ./store/lottery.js
var store_lottery = __webpack_require__(2360);
// EXTERNAL MODULE: external "lodash"
var external_lodash_ = __webpack_require__(3804);
;// CONCATENATED MODULE: ./components/share/lottery/ListNumbers.js







const ListNumbers = ({
  numbers,
  max = 1,
  handleClickButton,
  index,
  isKeno,
  data,
  handleSelectLevel
}, ref) => {
  const {
    0: listSelect,
    1: setListSelect
  } = (0,external_react_.useState)([]);
  const dispatch = (0,external_react_redux_.useDispatch)();
  let selectLotteryKeno = (0,external_react_redux_.useSelector)(state => state.lottery.selectLotteryKeno);
  const {
    listCopy
  } = (0,external_react_redux_.useSelector)(state => state.lottery);

  const handleSelectNumber = number => {
    let arrTemp = (0,external_lodash_.cloneDeep)(listSelect);

    if (arrTemp.includes(number)) {
      if (isKeno) {
        arrTemp = arrTemp.filter(num => num !== number);
        handleSelectLevel(arrTemp.length);
        dispatch((0,store_lottery/* addSelectLotteryKeno */.$L)(arrTemp.length));
        return setListSelect(arrTemp);
      } else {
        arrTemp = arrTemp.filter(num => num !== number);
        return setListSelect(arrTemp);
      }
    }

    if (isKeno) {
      if (arrTemp.length < 10) {
        arrTemp.push(number);
        handleSelectLevel(arrTemp.length);
        dispatch((0,store_lottery/* addSelectLotteryKeno */.$L)(arrTemp.length));
        setListSelect(arrTemp, index);
      } else {
        external_react_toastify_.toast.warning("Bạn đã chọn đủ số");
      }
    } else {
      if (arrTemp.length < max) {
        arrTemp.push(number);
        setListSelect(arrTemp, index);
      } else {
        external_react_toastify_.toast.warning("Bạn đã chọn đủ số");
      }
    }
  };

  (0,external_react_.useImperativeHandle)(ref, () => ({
    handleRandNum() {
      let arr;

      if (!!listSelect.length) {
        arr = (0,external_lodash_.cloneDeep)([]);
      } else arr = (0,external_lodash_.cloneDeep)(listSelect);

      while (arr.length < max) {
        let numRand = Math.floor(Math.random() * numbers.length) + 1;

        if (!arr.includes(numRand)) {
          arr.push(numRand);
        }
      }

      setListSelect(arr, index);
    },

    hanldeSubmitContinues() {
      if (listSelect.length == max || listSelect.length == selectLotteryKeno) {
        handleClickButton(listSelect);
        dispatch((0,store_lottery/* addSelectLotteryKeno */.$L)(10));
      } else {
        dispatch((0,store_lottery/* addSelectLotteryKeno */.$L)(10));
        handleClickButton(listSelect);
      }
    },

    // handleCancelButton() {
    //   setListSelect([]);
    //   handleClickButton([]);
    // },
    onCopy() {
      if (!!listSelect.length) {
        window.navigator.clipboard.writeText(listSelect);
        dispatch((0,store_lottery/* addListCopy */.Vg)(listSelect));
      }
    },

    onPaste() {
      if (!!listCopy.length) {
        if (isKeno) {
          handleSelectLevel(listCopy.length);
        }

        setListSelect(listCopy);
      }
    },

    handleOnChangeInputTogether() {
      setListSelect([]);
    },

    handleShowList() {
      if (data.listSelect.length) {
        setListSelect(data.listSelect);
      }
    },

    dataListSelect() {
      return listSelect;
    }

  }));
  return /*#__PURE__*/jsx_runtime_.jsx("div", {
    className: `grid grid-cols-${Math.ceil(numbers.length / 10)} gap-0.5`,
    children: numbers.map((num, index) => /*#__PURE__*/jsx_runtime_.jsx("label", {
      className: `mx-auto rounded-full h-3 w-3 flex items-center justify-center border-1 border-red-500 ${listSelect.includes(index + 1) ? "bg-red-500" : ""}`,
      onClick: () => handleSelectNumber(index + 1),
      children: /*#__PURE__*/jsx_runtime_.jsx("span", {
        className: `text-small ${listSelect.includes(index + 1) ? "text-gray-50" : ""}`,
        children: index + 1
      })
    }, index))
  });
};

/* harmony default export */ var lottery_ListNumbers = (/*#__PURE__*/(0,external_react_.forwardRef)(ListNumbers));
// EXTERNAL MODULE: ./components/share/common/IconEva.js
var IconEva = __webpack_require__(8656);
;// CONCATENATED MODULE: ./components/share/common/ContentModal/BoxSelectNumber.js














const BoxSelectNumber = ({
  index,
  data,
  isBigSmall = false,
  handleClickButton,
  gameType,
  moneySelectLottery
}) => {
  const dispatch = (0,external_react_redux_.useDispatch)();
  const childRef = (0,external_react_.useRef)();
  const copyLotteryKeno = (0,external_react_redux_.useSelector)(state => state.lottery.copyLotteryKeno);
  const selectLotteryKeno = (0,external_react_redux_.useSelector)(state => state.lottery.selectLotteryKeno);
  const {
    0: checklist,
    1: setChecklist
  } = (0,external_react_.useState)();
  const {
    0: selectPrice,
    1: setSelectPrice
  } = (0,external_react_.useState)(10000);
  const {
    0: levelSelect,
    1: setLevelSelect
  } = (0,external_react_.useState)(10);
  (0,external_react_.useEffect)(() => {
    if (!!data.subGame.length) {
      setChecklist(data.subGame);
      setSelectPrice(data.money);
      dispatch((0,store_lottery/* addSelectPriceKeno */.$2)(data.money));
    } else if (!data.listSelect.length) {
      dispatch((0,store_lottery/* addSelectLotteryKeno */.$L)(10));
      dispatch((0,store_lottery/* addSelectPriceKeno */.$2)(10000));
    } else {
      dispatch((0,store_lottery/* addSelectLotteryKeno */.$L)(data.listSelect.length));
      setLevelSelect(data.listSelect.length);
      dispatch((0,store_lottery/* addSelectPriceKeno */.$2)(data.money));
      setSelectPrice(data.money);
      childRef.current.handleShowList();
    }
  }, []);
  const isKeno = (0,external_react_.useMemo)(() => {
    return gameType === "keno";
  }, [gameType]);
  const dataGameType = (0,external_react_.useMemo)(() => {
    return lottery/* LIST_TYPE_LOTTERY */.G7[lottery/* lotteryType */.wO[gameType]];
  }, [gameType]);
  const arrNumberSelected = (0,external_react_.useMemo)(() => {
    const arrNumber = (0,external_lodash_.get)(dataGameType, "number", []).fill("");
    return arrNumber;
  }, [dataGameType]);
  const listInfoLevel = (0,external_react_.useMemo)(() => {
    return (0,external_lodash_.get)(dataGameType, "levels", []);
  }, [dataGameType]);
  const listInfoPrice = (0,external_react_.useMemo)(() => {
    return (0,external_lodash_.get)(dataGameType, "prices", []);
  }, [dataGameType]);

  const handleSelectLevel = level => {
    setLevelSelect(Number(level));
    dispatch((0,store_lottery/* addSelectLotteryKeno */.$L)(Number(level)));
  };

  const handleSelectPrice = e => {
    setSelectPrice(Number(e));
    dispatch((0,store_lottery/* addSelectPriceKeno */.$2)(Number(e)));
  };

  const handleCheckList = e => {
    setChecklist(e.target.value);
  };

  const handleCancelLastBoxKeno = () => {
    data.subGame = [];
    data.money = 0;
    handleClickButton({
      index,
      data
    });
    window.closeModal();
  };

  const handleCloseModal = () => {
    dispatch((0,store_lottery/* addSelectLotteryKeno */.$L)(10));
    window.closeModal();
  };

  const handleCancelModal = () => {
    // childRef.current.handleCancelButton();
    data.listSelect = [];
    data.money = 0;
    handleClickButton({
      index,
      data
    });
    window.closeModal();
  };

  const handleDoneSelect = () => {
    if (selectLotteryKeno < childRef.current.dataListSelect().length && isKeno) {
      external_react_toastify_.toast.error("Bậc số không hợp lệ, vui lòng kiểm tra lại để tiếp tục");
    } else {
      childRef.current.hanldeSubmitContinues();
      window.closeModal();
    }
  };

  const handleSelectListNumber = list => {
    if (isBigSmall) {
      if (!checklist) {
        return external_react_toastify_.toast.warning("Vui lòng chọn cách chơi");
      }

      data.money = selectPrice;
      data.subGame = [checklist];
      handleClickButton({
        index,
        data
      });
      return window.closeModal();
    }

    data.listSelect = list;

    if (isKeno) {
      data.level = selectLotteryKeno;
      data.money = !list.length ? 0 : selectPrice;
    } else {
      data.money = list.length == data.level ? moneySelectLottery : 0;
    }

    handleClickButton({
      index,
      data
    });
  };

  const handleCopy = () => {
    if (isKeno) {
      dispatch((0,store_lottery/* addCopyLotteryKeno */.Nf)(selectLotteryKeno));
    }

    childRef.current.onCopy();
  };

  const handlePaste = () => {
    if (isKeno) {
      dispatch((0,store_lottery/* addSelectLotteryKeno */.$L)(copyLotteryKeno));
    }

    childRef.current.onPaste();
  };

  return /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
    children: [isKeno && isBigSmall ? /*#__PURE__*/jsx_runtime_.jsx(KenoScreen_LastModalKeno, {
      listInfoPrice: listInfoPrice,
      handleSelectPrice: handleSelectPrice,
      handleCheckList: handleCheckList,
      handleCancelLastBoxKeno: handleCancelLastBoxKeno,
      handleContinue: handleSelectListNumber,
      data: data
    }) : /*#__PURE__*/(0,jsx_runtime_.jsxs)(external_react_.Fragment, {
      children: [isKeno ? /*#__PURE__*/(0,jsx_runtime_.jsxs)(external_react_.Fragment, {
        children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: "flex",
          children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
            className: "w-full mr-2",
            children: /*#__PURE__*/jsx_runtime_.jsx(Select/* default */.Z, {
              options: listInfoLevel,
              label: "Bậc số",
              onChangeSelect: handleSelectLevel,
              defaultValue: levelSelect
            })
          }), /*#__PURE__*/jsx_runtime_.jsx("div", {
            className: "w-full",
            children: /*#__PURE__*/jsx_runtime_.jsx(Select/* default */.Z, {
              options: listInfoPrice,
              label: "Mệnh giá",
              onChangeSelect: handleSelectPrice,
              defaultValue: selectPrice
            })
          })]
        }), /*#__PURE__*/jsx_runtime_.jsx("div", {
          className: "w-full mt-2",
          children: /*#__PURE__*/jsx_runtime_.jsx(lottery_ListNumbers, {
            ref: childRef,
            numbers: arrNumberSelected,
            max: levelSelect,
            handleClickButton: handleSelectListNumber,
            handleSelectLevel: handleSelectLevel,
            isKeno: isKeno,
            data: data,
            index: index
          })
        })]
      }) : /*#__PURE__*/(0,jsx_runtime_.jsxs)(external_react_.Fragment, {
        children: [/*#__PURE__*/jsx_runtime_.jsx("p", {
          children: "Ch\u1ECDn s\u1ED1"
        }), /*#__PURE__*/jsx_runtime_.jsx("div", {
          className: "w-full mt-2",
          children: /*#__PURE__*/jsx_runtime_.jsx(lottery_ListNumbers, {
            ref: childRef,
            numbers: arrNumberSelected,
            max: data.level,
            handleClickButton: handleSelectListNumber,
            index: index,
            data: data
          })
        })]
      }), /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "modal-action flex flex-col items-center",
        children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: "flex flex-col w-full",
          children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
            className: "flex justify-between w-full",
            children: [/*#__PURE__*/jsx_runtime_.jsx(Button/* default */.Z, {
              onClick: () => childRef.current.handleRandNum(),
              children: "M\xE1y ch\u1ECDn"
            }), /*#__PURE__*/jsx_runtime_.jsx(Button/* default */.Z, {
              onClick: handleCopy,
              children: "Sao ch\xE9p"
            }), /*#__PURE__*/jsx_runtime_.jsx(Button/* default */.Z, {
              onClick: handlePaste,
              children: "D\xE1n"
            }), /*#__PURE__*/jsx_runtime_.jsx(Button/* default */.Z, {
              buttonColor: "btn-error",
              onClick: handleCancelModal,
              children: "H\u1EE7y"
            })]
          }), /*#__PURE__*/jsx_runtime_.jsx(Button/* default */.Z, {
            onClick: handleDoneSelect,
            className: "w-full mt-1",
            children: "Ti\u1EBFp theo"
          })]
        })
      })]
    }), /*#__PURE__*/jsx_runtime_.jsx("label", {
      className: "absolute top-1 right-1 cursor-pointer",
      onClick: handleCloseModal,
      children: /*#__PURE__*/jsx_runtime_.jsx(IconEva/* default */.Z, {
        name: "close-outline",
        width: "20px",
        height: "20px"
      })
    })]
  });
};

/* harmony default export */ var ContentModal_BoxSelectNumber = (/*#__PURE__*/external_react_default().memo(BoxSelectNumber));

/***/ }),

/***/ 3477:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);




const Notification = ({
  titleModal,
  textModal,
  titleSubmit,
  titleCancel,
  handleOnClickSubmit
}) => {
  const handleCloseModal = () => {
    window.closeModal();
  };

  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react__WEBPACK_IMPORTED_MODULE_1__.Fragment, {
    children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h1", {
      className: "text-black text-2xl",
      children: titleModal
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
      className: "mt-1",
      children: textModal
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
      className: "modal-action",
      children: titleCancel ? /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react__WEBPACK_IMPORTED_MODULE_1__.Fragment, {
        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("label", {
          onClick: handleCloseModal,
          className: "mr-1",
          children: titleCancel
        }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("label", {
          className: "mr-1",
          onClick: handleOnClickSubmit,
          children: titleSubmit
        })]
      }) : /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react__WEBPACK_IMPORTED_MODULE_1__.Fragment, {
        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("label", {
          onClick: handleCloseModal,
          className: "mr-1",
          children: titleSubmit
        })
      })
    })]
  });
};

/* harmony default export */ __webpack_exports__["Z"] = (/*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default().memo(Notification));

/***/ }),

/***/ 5413:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6731);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _IconEva__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(8656);






const ShowPDF = ({
  urlPDF
}) => {
  const handleCloseModal = () => {
    window.closeModal();
  };

  const router = (0,next_router__WEBPACK_IMPORTED_MODULE_1__.useRouter)();
  const renderLinkPdf = (0,react__WEBPACK_IMPORTED_MODULE_2__.useMemo)(() => {
    if (urlPDF) return {
      linkPdf: urlPDF
    };
    let linkPdf, src;

    switch (router.pathname) {
      case "/keno":
        linkPdf = "139.180.219.58/the-le-keno.pdf";
        break;

      case "/power":
        linkPdf = "139.180.219.58/the-le-power-655.pdf";
        break;

      case "/mega":
        linkPdf = "139.180.219.58/mega-645.pdf";
        break;

      case "/together":
        linkPdf = "139.180.219.58/3mien.pdf";
        break;

      case "/auth/register":
        linkPdf = "139.180.219.58/dieu_khoan_su_dung.pdf";
        break;
    }

    return {
      linkPdf
    };
  });
  const renderPdf = (0,react__WEBPACK_IMPORTED_MODULE_2__.useMemo)(() => {
    return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("object", {
      className: "w-full h-full",
      data: renderLinkPdf.linkPdf,
      type: "application/pdf",
      children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("iframe", {
        className: "w-full h-full",
        src: `https://docs.google.com/viewer?url=${renderLinkPdf.linkPdf}&embedded=true`
      })
    });
  }, [router, urlPDF]);
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    style: {
      height: "60vh"
    },
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
      style: {
        height: "90%",
        marginTop: "5%"
      },
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        className: "text-2xl mb-1 font-medium",
        children: "C\u01A1 c\u1EA5u v\xE0 \u0111i\u1EC1u kho\u1EA3n"
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        className: "w-full h-full rounded-xl",
        children: renderPdf
      })]
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("label", {
      className: "absolute top-1 right-1 cursor-pointer",
      onClick: handleCloseModal,
      children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_IconEva__WEBPACK_IMPORTED_MODULE_3__/* .default */ .Z, {
        name: "close-outline",
        width: "20px",
        height: "20px"
      })
    })]
  });
};

/* harmony default export */ __webpack_exports__["Z"] = (/*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default().memo(ShowPDF));

/***/ }),

/***/ 5162:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3804);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6731);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _ContentModal_ShowPDF__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(5413);
/* harmony import */ var _IconEva__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(8656);








const Header = ({
  sizeIcon = "30px",
  rightView,
  toggleModal,
  className,
  isModal,
  iconRightView
}) => {
  const router = (0,next_router__WEBPACK_IMPORTED_MODULE_2__.useRouter)();
  const renderText = (0,react__WEBPACK_IMPORTED_MODULE_3__.useMemo)(() => {
    let title = "keno";
    let info = "Cơ cấu";

    switch (router.pathname) {
      case "/keno":
        title = "keno";
        break;

      case "/power":
        title = "power";
        break;

      case "/mega":
        title = "mega";
        break;

      case "/together":
        title = "Quay lại";
        info = "Cơ cấu";
        break;
    }

    return {
      title,
      info
    };
  });

  const handleShowPdf = () => {
    return window.openModal({
      content: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_ContentModal_ShowPDF__WEBPACK_IMPORTED_MODULE_4__/* .default */ .Z, {}),
      isShowHeader: false
    });
  };

  const handleActions = () => {
    if (isModal) {
      return window.closeModal();
    }

    if (toggleModal) {
      return toggleModal();
    }

    return router.back();
  };

  const renderRightView = (0,react__WEBPACK_IMPORTED_MODULE_3__.useMemo)(() => {
    if (rightView) {
      return rightView;
    }

    return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react__WEBPACK_IMPORTED_MODULE_3__.Fragment, {
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        className: "flex justify-center items-center text-2xl",
        onClick: handleShowPdf,
        children: renderText.info
      }), iconRightView ? iconRightView : /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        onClick: handleShowPdf,
        className: " flex justify-center items-center mr-1 ml-0.5",
        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_IconEva__WEBPACK_IMPORTED_MODULE_5__/* .default */ .Z, {
          name: "award-outline",
          width: sizeIcon,
          height: sizeIcon
        })
      })]
    });
  }, [rightView]);
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    className: `w-full bg-white flex justify-between sticky top-0 h-20 ${className} `,
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
      className: "flex ",
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        onClick: handleActions,
        className: " flex justify-center items-center mr-0.5 ml-1",
        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_IconEva__WEBPACK_IMPORTED_MODULE_5__/* .default */ .Z, {
          name: "arrow-back-outline",
          width: sizeIcon,
          height: sizeIcon
        })
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        className: `flex justify-center items-center text-2xl `,
        children: (0,lodash__WEBPACK_IMPORTED_MODULE_1__.startCase)(renderText.title)
      })]
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
      className: "flex",
      children: renderRightView
    })]
  });
};

/* harmony default export */ __webpack_exports__["Z"] = (Header);

/***/ }),

/***/ 8656:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var eva_icons__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1765);
/* harmony import */ var eva_icons__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(eva_icons__WEBPACK_IMPORTED_MODULE_2__);




const IconEva = ({
  name,
  color,
  height,
  width
}) => {
  (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(() => {
    eva_icons__WEBPACK_IMPORTED_MODULE_2__.replace();
  }, []);
  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
    children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("i", {
      "data-eva": name,
      "data-eva-fill": color,
      "data-eva-height": height,
      "data-eva-width": width
    })
  });
};

/* harmony default export */ __webpack_exports__["Z"] = (IconEva);

/***/ }),

/***/ 7598:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);



function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }



const Select = (_ref) => {
  let {
    options,
    defaultValue,
    label,
    onChangeSelect,
    isGetAllData
  } = _ref,
      props = _objectWithoutProperties(_ref, ["options", "defaultValue", "label", "onChangeSelect", "isGetAllData"]);

  const {
    0: value,
    1: setValue
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)("");
  (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(() => {
    if (isGetAllData) {
      const data = options.find(option => option.value == defaultValue.value);
      return setValue(data.value);
    }

    return setValue(defaultValue);
  }, [defaultValue]);

  const onChange = () => e => {
    const {
      value
    } = e.target;
    setValue(value);

    if (isGetAllData) {
      const data = options.find(option => option.value == value);
      return onChangeSelect(data);
    }

    onChangeSelect(value);
  };

  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    children: [label ? /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
      className: "text-gray-700 text-lg",
      children: label
    }) : null, /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("select", {
      value: value,
      className: "form-select block w-full text-xl rounded border-none bg-gray-50 appearance-none",
      onChange: onChange(),
      children: options.map((item, index) => /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("option", {
        value: item.value,
        children: item.text
      }, index))
    })]
  });
};

/* harmony default export */ __webpack_exports__["Z"] = (Select);

/***/ }),

/***/ 9199:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export BoxSelect */
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var components_share_common_Button__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2268);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3804);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var utils_functions__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(6187);
/* harmony import */ var _common_IconEva__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(8656);







const BoxSelect = ({
  index,
  data,
  isBigSmall,
  handleOpenModal
}) => {
  const title = (0,react__WEBPACK_IMPORTED_MODULE_3__.useMemo)(() => {
    const objTitle = {
      0: "A",
      1: "B",
      2: "C",
      3: "D",
      4: "E",
      5: "F"
    };
    return objTitle[index];
  }, [index]);

  const renderBodyBoxSelect = () => {
    if (data.subGame.length && isBigSmall) {
      return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react__WEBPACK_IMPORTED_MODULE_3__.Fragment, {
        children: (0,lodash__WEBPACK_IMPORTED_MODULE_2__.get)(data, "subGame[0]", "")
      });
    } else if (data.listSelect.length) {
      return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react__WEBPACK_IMPORTED_MODULE_3__.Fragment, {
        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
          className: "mx-auto",
          children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            className: "flex w-full flex-wrap",
            children: data.listSelect.map((num, index) => /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
              className: "rounded-full h-3 w-3 flex items-center justify-center border-1 border-red-500 mx-1 my-0.5",
              children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                className: "text-small",
                children: num
              })
            }, index))
          })
        })
      });
    } else {
      return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react__WEBPACK_IMPORTED_MODULE_3__.Fragment, {
        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_common_IconEva__WEBPACK_IMPORTED_MODULE_5__/* .default */ .Z, {
          name: "archive-outline",
          width: "30px",
          height: "30px"
        }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
          children: "B\u1EA1n ch\u01B0a ch\u1ECDn s\u1ED1 cho v\xE9 n\xE0y"
        })]
      });
    }
  };

  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    className: "w-full bg-white p-1.5 rounded-lg",
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
      className: "flex justify-between pb-1.5 border-b",
      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("p", {
        children: ["B\u1EA3ng ch\u1ECDn ", title]
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
        className: "sub-text-color",
        children: (0,utils_functions__WEBPACK_IMPORTED_MODULE_4__/* .formatCurrency */ .xG)(data.money)
      })]
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
      className: "flex flex-col items-center pt-1.5 pb-1.5 ",
      children: renderBodyBoxSelect()
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(components_share_common_Button__WEBPACK_IMPORTED_MODULE_1__/* .default */ .Z, {
      onClick: handleOpenModal({
        index,
        data
      }),
      className: "w-full pt-0.5 pb-0.5 flex",
      children: ["Ch\u1ECDn v\xE9 ngay", /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_common_IconEva__WEBPACK_IMPORTED_MODULE_5__/* .default */ .Z, {
        name: "edit-outline",
        width: "20px",
        height: "20px",
        color: "#FFFFFF"
      })]
    })]
  });
};
/* harmony default export */ __webpack_exports__["Z"] = (/*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default().memo(BoxSelect));

/***/ }),

/***/ 3155:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var components_share_common_Button__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2268);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(2470);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(79);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var utils_functions__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(6187);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(3804);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_6__);









const LotteryType = ({
  data,
  isShowButton,
  isDetailLottery,
  handleClickButton
}) => {
  const duration = (0,react_redux__WEBPACK_IMPORTED_MODULE_4__.useSelector)(state => state.lottery.kenoData.duration);
  const lottery = (0,react_redux__WEBPACK_IMPORTED_MODULE_4__.useSelector)(state => state.lottery);
  const {
    kenoData,
    megaData,
    powerData
  } = lottery;
  const {
    0: fmt,
    1: setFmt
  } = (0,react__WEBPACK_IMPORTED_MODULE_3__.useState)("mm:ss");
  const handleClick = (0,react__WEBPACK_IMPORTED_MODULE_3__.useCallback)(() => {
    handleClickButton(data);
  }, [data]);
  const renderCurrentData = (0,react__WEBPACK_IMPORTED_MODULE_3__.useMemo)(() => {
    let currentPeriod;
    let timeResult;

    switch (data.slug) {
      case "keno":
        currentPeriod = kenoData.current_ky;
        break;

      case "power":
        currentPeriod = (0,lodash__WEBPACK_IMPORTED_MODULE_6__.get)(powerData, "power.ky", "loading");
        timeResult = (0,lodash__WEBPACK_IMPORTED_MODULE_6__.get)(powerData, "next", "loading");
        break;

      case "mega":
        currentPeriod = (0,lodash__WEBPACK_IMPORTED_MODULE_6__.get)(megaData, "mega.ky", "loading");
        timeResult = (0,lodash__WEBPACK_IMPORTED_MODULE_6__.get)(megaData, "next", "loading");
        break;
    }

    return {
      currentPeriod,
      timeResult
    };
  }, [lottery]);
  (0,react__WEBPACK_IMPORTED_MODULE_3__.useEffect)(() => {
    if (duration > 3600) {
      setFmt("hh:mm:ss");
    } else {
      setFmt("mm:ss");
    }
  }, [duration]);
  const renderContentBoxRightKeno = (0,react__WEBPACK_IMPORTED_MODULE_3__.useMemo)(() => {
    return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
      className: "flex flex-col text-center rounded-lg min-w-10 pr-1.5 pl-1.5 pt-1 pb-1 box-coutdown",
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
        className: "text-sub-heading",
        children: "Th\u1EDDi gian c\xF2n l\u1EA1i"
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
        className: "sub-text-color",
        children: moment__WEBPACK_IMPORTED_MODULE_2___default().utc(moment__WEBPACK_IMPORTED_MODULE_2___default().duration({
          seconds: duration
        }).asMilliseconds()).format(fmt)
      })]
    });
  }, [duration]);
  const renderContentBoxRight = (0,react__WEBPACK_IMPORTED_MODULE_3__.useMemo)(() => {
    if (data.slug == "together" || data.slug == "loto") {
      return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)((react__WEBPACK_IMPORTED_MODULE_3___default().Fragment), {
        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
          className: "sub-text-color text-4xl mt-2",
          children: data.slug == "together" ? "Bao 3 Miền" : "Lô Tô Đồng Nai"
        }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
          className: "text-sub-heading",
          children: data.slug == "together" ? "Mua chung dễ trúng" : "Đang cập nhật"
        })]
      });
    } else if (data.slug == "power") {
      return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
        className: "flex flex-col text-center rounded-lg min-w-10 pr-1.5 pl-1.5 pt-1 pb-1 box-coutdown",
        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
          className: "text-sub-heading",
          children: "Th\u1EDDi gian quay th\u01B0\u1EDFng"
        }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
          className: "sub-text-color",
          children: (0,utils_functions__WEBPACK_IMPORTED_MODULE_5__/* .formatDate */ .p6)(renderCurrentData.timeResult)
        })]
      });
    } else if (data.slug == "mega") {
      return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
        className: "flex flex-col text-center rounded-lg min-w-10 pr-1.5 pl-1.5 pt-1 pb-1 box-coutdown",
        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
          className: "text-sub-heading",
          children: "Th\u1EDDi gian quay th\u01B0\u1EDFng"
        }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
          className: "sub-text-color",
          children: (0,utils_functions__WEBPACK_IMPORTED_MODULE_5__/* .formatDate */ .p6)(renderCurrentData.timeResult)
        })]
      });
    }
  }, [renderCurrentData.timeResult]);
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    className: "flex justify-between items-center bg-white w-full p-1.5 rounded-lg h-64",
    children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
      className: "mr-1",
      src: data.image,
      width: "90px",
      height: "90px",
      alt: data.name
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
      className: "flex flex-col items-center min-w-18",
      children: [isDetailLottery ? /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("p", {
        className: "text-sub-heading",
        children: ["K\u1EF3 quay", " ", /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
          className: "text-sub-heading ",
          children: ["#", renderCurrentData.currentPeriod]
        })]
      }) : data.slug == "keno" ? /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((react__WEBPACK_IMPORTED_MODULE_3___default().Fragment), {
        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
          className: "text-small mb-1",
          children: data.time
        })
      }) : /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)((react__WEBPACK_IMPORTED_MODULE_3___default().Fragment), {
        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
          className: "text-small",
          children: data.date
        }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
          className: "text-small mb-1",
          children: data.time
        })]
      }), data.slug == "keno" ? renderContentBoxRightKeno : renderContentBoxRight, data.slug == "keno" && isShowButton ? /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react__WEBPACK_IMPORTED_MODULE_3__.Fragment, {
        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
          className: "text-small mt-0.5",
          children: data.date
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
          className: "text-small mt-0.25",
          children: ["Gi\u1EA3i th\u01B0\u1EDFng l\xEAn \u0111\u1EBFn ", /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
            children: "2 T\u1EF7 \u0111\xF4ng"
          })]
        }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(components_share_common_Button__WEBPACK_IMPORTED_MODULE_1__/* .default */ .Z, {
          className: "w-full mt-0.5",
          onClick: handleClick,
          children: "Mua Ngay"
        })]
      }) : isShowButton && data.slug != "loto" ? /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(components_share_common_Button__WEBPACK_IMPORTED_MODULE_1__/* .default */ .Z, {
        className: "w-full mt-1",
        onClick: handleClick,
        children: "Mua Ngay"
      }) : null]
    })]
  });
};

/* harmony default export */ __webpack_exports__["Z"] = (/*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_3___default().memo(LotteryType));

/***/ }),

/***/ 4039:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": function() { return /* binding */ lottery_Total; }
});

// UNUSED EXPORTS: Total

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(5282);
// EXTERNAL MODULE: ./utils/functions.js
var functions = __webpack_require__(6187);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(9297);
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);
// EXTERNAL MODULE: ./components/share/common/Button.js
var Button = __webpack_require__(2268);
;// CONCATENATED MODULE: ./components/share/lottery/DeliveryInfo.js




const DeliveryInfo = ({
  arrInfo,
  onClickDelivery
}) => {
  return /*#__PURE__*/jsx_runtime_.jsx("div", {
    className: "",
    children: arrInfo.map((item, index) => /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: "flex items-center mb-0.5 pl-1 cursor-pointer ml-0.5 p-0.5",
      children: [/*#__PURE__*/jsx_runtime_.jsx("input", {
        type: "radio",
        id: `${item.display}`,
        name: "delivery",
        className: "form-checkbox rounded-full w-2 h-2",
        value: item.value,
        onClick: onClickDelivery,
        defaultChecked: index === 0
      }), /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "flex flex-col ml-1 ",
        children: /*#__PURE__*/jsx_runtime_.jsx("label", {
          htmlFor: `${item.display}`,
          className: "cursor-pointer",
          children: item.display
        })
      })]
    }, index))
  });
};

/* harmony default export */ var lottery_DeliveryInfo = (DeliveryInfo);
// EXTERNAL MODULE: ./config/BaseApi.js + 1 modules
var BaseApi = __webpack_require__(9900);
;// CONCATENATED MODULE: ./constants/infoDelivery.js
const dist = [{
  DistrictID: 1442,
  ProvinceID: 202,
  DistrictName: "Quận 1",
  Code: "0201",
  Type: 1,
  SupportType: 3
}, {
  DistrictID: 1443,
  ProvinceID: 202,
  DistrictName: "Quận 2",
  Code: "0202",
  Type: 1,
  SupportType: 3
}, {
  DistrictID: 1444,
  ProvinceID: 202,
  DistrictName: "Quận 3",
  Code: "0203",
  Type: 1,
  SupportType: 3
}, {
  DistrictID: 1446,
  ProvinceID: 202,
  DistrictName: "Quận 4",
  Code: "0204",
  Type: 1,
  SupportType: 3
}, {
  DistrictID: 1447,
  ProvinceID: 202,
  DistrictName: "Quận 5",
  Code: "0205",
  Type: 1,
  SupportType: 3
}, {
  DistrictID: 1448,
  ProvinceID: 202,
  DistrictName: "Quận 6",
  Code: "0206",
  Type: 1,
  SupportType: 3
}, {
  DistrictID: 1449,
  ProvinceID: 202,
  DistrictName: "Quận 7",
  Code: "0207",
  Type: 1,
  SupportType: 3
}, {
  DistrictID: 1450,
  ProvinceID: 202,
  DistrictName: "Quận 8",
  Code: "0208",
  Type: 1,
  SupportType: 3
}, {
  DistrictID: 1451,
  ProvinceID: 202,
  DistrictName: "Quận 9",
  Code: "0209",
  Type: 2,
  SupportType: 3
}, {
  DistrictID: 1452,
  ProvinceID: 202,
  DistrictName: "Quận 10",
  Code: "0210",
  Type: 1,
  SupportType: 3
}, {
  DistrictID: 1453,
  ProvinceID: 202,
  DistrictName: "Quận 11",
  Code: "0211",
  Type: 1,
  SupportType: 3
}, {
  DistrictID: 1454,
  ProvinceID: 202,
  DistrictName: "Quận 12",
  Code: "0212",
  Type: 2,
  SupportType: 3
}, {
  DistrictID: 1455,
  ProvinceID: 202,
  DistrictName: "Quận Tân Bình",
  Code: "0214",
  Type: 1,
  SupportType: 3
}, {
  DistrictID: 1456,
  ProvinceID: 202,
  DistrictName: "Quận Tân Phú",
  Code: "0215",
  Type: 1,
  SupportType: 3
}, {
  DistrictID: 1457,
  ProvinceID: 202,
  DistrictName: "Quận Phú Nhuận",
  Code: "0217",
  Type: 1,
  SupportType: 3
}, {
  DistrictID: 1458,
  ProvinceID: 202,
  DistrictName: "Quận Bình Tân",
  Code: "0219",
  Type: 2,
  SupportType: 3
}, {
  DistrictID: 1459,
  ProvinceID: 202,
  DistrictName: "Huyện Hóc Môn",
  Code: "0222",
  Type: 3,
  SupportType: 3
}, {
  DistrictID: 1460,
  ProvinceID: 202,
  DistrictName: "Huyện Củ Chi",
  Code: "0221",
  Type: 3,
  SupportType: 3
}, {
  DistrictID: 1461,
  ProvinceID: 202,
  DistrictName: "Quận Gò Vấp",
  Code: "0213",
  Type: 1,
  SupportType: 3
}, {
  DistrictID: 1462,
  ProvinceID: 202,
  DistrictName: "Quận Bình Thạnh",
  Code: "0216",
  Type: 1,
  SupportType: 3
}, {
  DistrictID: 1463,
  ProvinceID: 202,
  DistrictName: "Quận Thủ Đức",
  Code: "0218",
  Type: 2,
  SupportType: 3
}, {
  DistrictID: 1533,
  ProvinceID: 202,
  DistrictName: "Huyện Bình Chánh",
  Code: "0220",
  Type: 3,
  SupportType: 3
}, {
  DistrictID: 1534,
  ProvinceID: 202,
  DistrictName: "Huyện Nhà Bè",
  Code: "0223",
  Type: 3,
  SupportType: 3
}, {
  DistrictID: 2090,
  ProvinceID: 202,
  DistrictName: "Huyện Cần Giờ",
  Code: "0224",
  Type: 3,
  SupportType: 3
}];
const ward = [{
  WardCode: "20110",
  DistrictID: 1442,
  WardName: "Phường Tân Định"
}, {
  WardCode: "20106",
  DistrictID: 1442,
  WardName: "Phường Đa Kao"
}, {
  WardCode: "20101",
  DistrictID: 1442,
  WardName: "Phường Bến Nghé"
}, {
  WardCode: "20102",
  DistrictID: 1442,
  WardName: "Phường Bến Thành"
}, {
  WardCode: "20108",
  DistrictID: 1442,
  WardName: "Phường Nguyễn Thái Bình"
}, {
  WardCode: "20109",
  DistrictID: 1442,
  WardName: "Phường Phạm Ngũ Lão"
}, {
  WardCode: "20104",
  DistrictID: 1442,
  WardName: "Phường Cầu Ông Lãnh"
}, {
  WardCode: "20105",
  DistrictID: 1442,
  WardName: "Phường Cô Giang"
}, {
  WardCode: "20107",
  DistrictID: 1442,
  WardName: "Phường Nguyễn Cư Trinh"
}, {
  WardCode: "20103",
  DistrictID: 1442,
  WardName: "Phường Cầu Kho"
}, {
  WardCode: "20210",
  DistrictID: 1443,
  WardName: "Phường Thảo Điền"
}, {
  WardCode: "20203",
  DistrictID: 1443,
  WardName: "Phường An Phú"
}, {
  WardCode: "20204",
  DistrictID: 1443,
  WardName: "Phường Bình An"
}, {
  WardCode: "20206",
  DistrictID: 1443,
  WardName: "Phường Bình Trưng Đông"
}, {
  WardCode: "20207",
  DistrictID: 1443,
  WardName: "Phường Bình Trưng Tây"
}, {
  WardCode: "20205",
  DistrictID: 1443,
  WardName: "Phường Bình Khánh"
}, {
  WardCode: "20201",
  DistrictID: 1443,
  WardName: "Phường An Khánh"
}, {
  WardCode: "20208",
  DistrictID: 1443,
  WardName: "Phường Cát Lái"
}, {
  WardCode: "20209",
  DistrictID: 1443,
  WardName: "Phường Thạnh Mỹ Lợi"
}, {
  WardCode: "20202",
  DistrictID: 1443,
  WardName: "Phường An Lợi Đông"
}, {
  WardCode: "20211",
  DistrictID: 1443,
  WardName: "Phường Thủ Thiêm"
}, {
  WardCode: "20308",
  DistrictID: 1444,
  WardName: "Phường 8"
}, {
  WardCode: "20307",
  DistrictID: 1444,
  WardName: "Phường 7"
}, {
  WardCode: "20314",
  DistrictID: 1444,
  WardName: "Phường 14"
}, {
  WardCode: "20312",
  DistrictID: 1444,
  WardName: "Phường 12"
}, {
  WardCode: "20311",
  DistrictID: 1444,
  WardName: "Phường 11"
}, {
  WardCode: "20313",
  DistrictID: 1444,
  WardName: "Phường 13"
}, {
  WardCode: "20306",
  DistrictID: 1444,
  WardName: "Phường 6"
}, {
  WardCode: "20309",
  DistrictID: 1444,
  WardName: "Phường 9"
}, {
  WardCode: "20310",
  DistrictID: 1444,
  WardName: "Phường 10"
}, {
  WardCode: "20304",
  DistrictID: 1444,
  WardName: "Phường 4"
}, {
  WardCode: "20305",
  DistrictID: 1444,
  WardName: "Phường 5"
}, {
  WardCode: "20303",
  DistrictID: 1444,
  WardName: "Phường 3"
}, {
  WardCode: "20302",
  DistrictID: 1444,
  WardName: "Phường 2"
}, {
  WardCode: "20301",
  DistrictID: 1444,
  WardName: "Phường 1"
}, {
  WardCode: "20410",
  DistrictID: 1446,
  WardName: "Phường 12"
}, {
  WardCode: "20411",
  DistrictID: 1446,
  WardName: "Phường 13"
}, {
  WardCode: "20408",
  DistrictID: 1446,
  WardName: "Phường 9"
}, {
  WardCode: "20406",
  DistrictID: 1446,
  WardName: "Phường 6"
}, {
  WardCode: "20407",
  DistrictID: 1446,
  WardName: "Phường 8"
}, {
  WardCode: "20409",
  DistrictID: 1446,
  WardName: "Phường 10"
}, {
  WardCode: "20405",
  DistrictID: 1446,
  WardName: "Phường 5"
}, {
  WardCode: "20415",
  DistrictID: 1446,
  WardName: "Phường 18"
}, {
  WardCode: "20412",
  DistrictID: 1446,
  WardName: "Phường 14"
}, {
  WardCode: "20404",
  DistrictID: 1446,
  WardName: "Phường 4"
}, {
  WardCode: "20403",
  DistrictID: 1446,
  WardName: "Phường 3"
}, {
  WardCode: "20414",
  DistrictID: 1446,
  WardName: "Phường 16"
}, {
  WardCode: "20402",
  DistrictID: 1446,
  WardName: "Phường 2"
}, {
  WardCode: "20413",
  DistrictID: 1446,
  WardName: "Phường 15"
}, {
  WardCode: "20401",
  DistrictID: 1446,
  WardName: "Phường 1"
}, {
  WardCode: "20504",
  DistrictID: 1447,
  WardName: "Phường 4"
}, {
  WardCode: "20509",
  DistrictID: 1447,
  WardName: "Phường 9"
}, {
  WardCode: "20503",
  DistrictID: 1447,
  WardName: "Phường 3"
}, {
  WardCode: "20512",
  DistrictID: 1447,
  WardName: "Phường 12"
}, {
  WardCode: "20502",
  DistrictID: 1447,
  WardName: "Phường 2"
}, {
  WardCode: "20508",
  DistrictID: 1447,
  WardName: "Phường 8"
}, {
  WardCode: "20515",
  DistrictID: 1447,
  WardName: "Phường 15"
}, {
  WardCode: "20507",
  DistrictID: 1447,
  WardName: "Phường 7"
}, {
  WardCode: "20501",
  DistrictID: 1447,
  WardName: "Phường 1"
}, {
  WardCode: "20511",
  DistrictID: 1447,
  WardName: "Phường 11"
}, {
  WardCode: "20514",
  DistrictID: 1447,
  WardName: "Phường 14"
}, {
  WardCode: "20505",
  DistrictID: 1447,
  WardName: "Phường 5"
}, {
  WardCode: "20506",
  DistrictID: 1447,
  WardName: "Phường 6"
}, {
  WardCode: "20510",
  DistrictID: 1447,
  WardName: "Phường 10"
}, {
  WardCode: "20513",
  DistrictID: 1447,
  WardName: "Phường 13"
}, {
  WardCode: "20614",
  DistrictID: 1448,
  WardName: "Phường 14"
}, {
  WardCode: "20613",
  DistrictID: 1448,
  WardName: "Phường 13"
}, {
  WardCode: "20609",
  DistrictID: 1448,
  WardName: "Phường 9"
}, {
  WardCode: "20606",
  DistrictID: 1448,
  WardName: "Phường 6"
}, {
  WardCode: "20612",
  DistrictID: 1448,
  WardName: "Phường 12"
}, {
  WardCode: "20605",
  DistrictID: 1448,
  WardName: "Phường 5"
}, {
  WardCode: "20611",
  DistrictID: 1448,
  WardName: "Phường 11"
}, {
  WardCode: "20602",
  DistrictID: 1448,
  WardName: "Phường 2"
}, {
  WardCode: "20601",
  DistrictID: 1448,
  WardName: "Phường 1"
}, {
  WardCode: "20604",
  DistrictID: 1448,
  WardName: "Phường 4"
}, {
  WardCode: "20608",
  DistrictID: 1448,
  WardName: "Phường 8"
}, {
  WardCode: "20603",
  DistrictID: 1448,
  WardName: "Phường 3"
}, {
  WardCode: "20607",
  DistrictID: 1448,
  WardName: "Phường 7"
}, {
  WardCode: "20610",
  DistrictID: 1448,
  WardName: "Phường 10"
}, {
  WardCode: "20709",
  DistrictID: 1449,
  WardName: "Phường Tân Thuận Đông"
}, {
  WardCode: "20710",
  DistrictID: 1449,
  WardName: "Phường Tân Thuận Tây"
}, {
  WardCode: "20705",
  DistrictID: 1449,
  WardName: "Phường Tân Kiểng"
}, {
  WardCode: "20704",
  DistrictID: 1449,
  WardName: "Phường Tân Hưng"
}, {
  WardCode: "20701",
  DistrictID: 1449,
  WardName: "Phường Bình Thuận"
}, {
  WardCode: "20708",
  DistrictID: 1449,
  WardName: "Phường Tân Quy"
}, {
  WardCode: "20703",
  DistrictID: 1449,
  WardName: "Phường Phú Thuận"
}, {
  WardCode: "20707",
  DistrictID: 1449,
  WardName: "Phường Tân Phú"
}, {
  WardCode: "20706",
  DistrictID: 1449,
  WardName: "Phường Tân Phong"
}, {
  WardCode: "20702",
  DistrictID: 1449,
  WardName: "Phường Phú Mỹ"
}, {
  WardCode: "20808",
  DistrictID: 1450,
  WardName: "Phường 8"
}, {
  WardCode: "20802",
  DistrictID: 1450,
  WardName: "Phường 2"
}, {
  WardCode: "20801",
  DistrictID: 1450,
  WardName: "Phường 1"
}, {
  WardCode: "20803",
  DistrictID: 1450,
  WardName: "Phường 3"
}, {
  WardCode: "20811",
  DistrictID: 1450,
  WardName: "Phường 11"
}, {
  WardCode: "20809",
  DistrictID: 1450,
  WardName: "Phường 9"
}, {
  WardCode: "20810",
  DistrictID: 1450,
  WardName: "Phường 10"
}, {
  WardCode: "20804",
  DistrictID: 1450,
  WardName: "Phường 4"
}, {
  WardCode: "20813",
  DistrictID: 1450,
  WardName: "Phường 13"
}, {
  WardCode: "20812",
  DistrictID: 1450,
  WardName: "Phường 12"
}, {
  WardCode: "20805",
  DistrictID: 1450,
  WardName: "Phường 5"
}, {
  WardCode: "20814",
  DistrictID: 1450,
  WardName: "Phường 14"
}, {
  WardCode: "20806",
  DistrictID: 1450,
  WardName: "Phường 6"
}, {
  WardCode: "20815",
  DistrictID: 1450,
  WardName: "Phường 15"
}, {
  WardCode: "20816",
  DistrictID: 1450,
  WardName: "Phường 16"
}, {
  WardCode: "20807",
  DistrictID: 1450,
  WardName: "Phường 7"
}, {
  WardCode: "20902",
  DistrictID: 1451,
  WardName: "Phường Long Bình"
}, {
  WardCode: "20904",
  DistrictID: 1451,
  WardName: "Phường Long Thạnh Mỹ"
}, {
  WardCode: "20910",
  DistrictID: 1451,
  WardName: "Phường Tân Phú"
}, {
  WardCode: "20901",
  DistrictID: 1451,
  WardName: "Phường Hiệp Phú"
}, {
  WardCode: "20911",
  DistrictID: 1451,
  WardName: "Phường Tăng Nhơn Phú A"
}, {
  WardCode: "20912",
  DistrictID: 1451,
  WardName: "Phường Tăng Nhơn Phú B"
}, {
  WardCode: "20909",
  DistrictID: 1451,
  WardName: "Phường Phước Long B"
}, {
  WardCode: "20908",
  DistrictID: 1451,
  WardName: "Phường Phước Long A"
}, {
  WardCode: "20913",
  DistrictID: 1451,
  WardName: "Phường Trường Thạnh"
}, {
  WardCode: "20903",
  DistrictID: 1451,
  WardName: "Phường Long Phước"
}, {
  WardCode: "20905",
  DistrictID: 1451,
  WardName: "Phường Long Trường"
}, {
  WardCode: "20907",
  DistrictID: 1451,
  WardName: "Phường Phước Bình"
}, {
  WardCode: "20906",
  DistrictID: 1451,
  WardName: "Phường Phú Hữu"
}, {
  WardCode: "21015",
  DistrictID: 1452,
  WardName: "Phường 15"
}, {
  WardCode: "21013",
  DistrictID: 1452,
  WardName: "Phường 13"
}, {
  WardCode: "21014",
  DistrictID: 1452,
  WardName: "Phường 14"
}, {
  WardCode: "21012",
  DistrictID: 1452,
  WardName: "Phường 12"
}, {
  WardCode: "21011",
  DistrictID: 1452,
  WardName: "Phường 11"
}, {
  WardCode: "21010",
  DistrictID: 1452,
  WardName: "Phường 10"
}, {
  WardCode: "21009",
  DistrictID: 1452,
  WardName: "Phường 9"
}, {
  WardCode: "21001",
  DistrictID: 1452,
  WardName: "Phường 1"
}, {
  WardCode: "21008",
  DistrictID: 1452,
  WardName: "Phường 8"
}, {
  WardCode: "21002",
  DistrictID: 1452,
  WardName: "Phường 2"
}, {
  WardCode: "21004",
  DistrictID: 1452,
  WardName: "Phường 4"
}, {
  WardCode: "21007",
  DistrictID: 1452,
  WardName: "Phường 7"
}, {
  WardCode: "21005",
  DistrictID: 1452,
  WardName: "Phường 5"
}, {
  WardCode: "21006",
  DistrictID: 1452,
  WardName: "Phường 6"
}, {
  WardCode: "21003",
  DistrictID: 1452,
  WardName: "Phường 3"
}, {
  WardCode: "21115",
  DistrictID: 1453,
  WardName: "Phường 15"
}, {
  WardCode: "21105",
  DistrictID: 1453,
  WardName: "Phường 5"
}, {
  WardCode: "21114",
  DistrictID: 1453,
  WardName: "Phường 14"
}, {
  WardCode: "21111",
  DistrictID: 1453,
  WardName: "Phường 11"
}, {
  WardCode: "21103",
  DistrictID: 1453,
  WardName: "Phường 3"
}, {
  WardCode: "21110",
  DistrictID: 1453,
  WardName: "Phường 10"
}, {
  WardCode: "21113",
  DistrictID: 1453,
  WardName: "Phường 13"
}, {
  WardCode: "21108",
  DistrictID: 1453,
  WardName: "Phường 8"
}, {
  WardCode: "21109",
  DistrictID: 1453,
  WardName: "Phường 9"
}, {
  WardCode: "21112",
  DistrictID: 1453,
  WardName: "Phường 12"
}, {
  WardCode: "21107",
  DistrictID: 1453,
  WardName: "Phường 7"
}, {
  WardCode: "21106",
  DistrictID: 1453,
  WardName: "Phường 6"
}, {
  WardCode: "21104",
  DistrictID: 1453,
  WardName: "Phường 4"
}, {
  WardCode: "21101",
  DistrictID: 1453,
  WardName: "Phường 1"
}, {
  WardCode: "21102",
  DistrictID: 1453,
  WardName: "Phường 2"
}, {
  WardCode: "21116",
  DistrictID: 1453,
  WardName: "Phường 16"
}, {
  WardCode: "21209",
  DistrictID: 1454,
  WardName: "Phường Thạnh Xuân"
}, {
  WardCode: "21208",
  DistrictID: 1454,
  WardName: "Phường Thạnh Lộc"
}, {
  WardCode: "21203",
  DistrictID: 1454,
  WardName: "Phường Hiệp Thành"
}, {
  WardCode: "21210",
  DistrictID: 1454,
  WardName: "Phường Thới An"
}, {
  WardCode: "21204",
  DistrictID: 1454,
  WardName: "Phường Tân Chánh Hiệp"
}, {
  WardCode: "21201",
  DistrictID: 1454,
  WardName: "Phường An Phú Đông"
}, {
  WardCode: "21206",
  DistrictID: 1454,
  WardName: "Phường Tân Thới Hiệp"
}, {
  WardCode: "21211",
  DistrictID: 1454,
  WardName: "Phường Trung Mỹ Tây"
}, {
  WardCode: "21205",
  DistrictID: 1454,
  WardName: "Phường Tân Hưng Thuận"
}, {
  WardCode: "21202",
  DistrictID: 1454,
  WardName: "Phường Đông Hưng Thuận"
}, {
  WardCode: "21207",
  DistrictID: 1454,
  WardName: "Phường Tân Thới Nhất"
}, {
  WardCode: "21402",
  DistrictID: 1455,
  WardName: "Phường 2"
}, {
  WardCode: "21404",
  DistrictID: 1455,
  WardName: "Phường 4"
}, {
  WardCode: "21412",
  DistrictID: 1455,
  WardName: "Phường 12"
}, {
  WardCode: "21413",
  DistrictID: 1455,
  WardName: "Phường 13"
}, {
  WardCode: "21401",
  DistrictID: 1455,
  WardName: "Phường 1"
}, {
  WardCode: "21403",
  DistrictID: 1455,
  WardName: "Phường 3"
}, {
  WardCode: "21411",
  DistrictID: 1455,
  WardName: "Phường 11"
}, {
  WardCode: "21407",
  DistrictID: 1455,
  WardName: "Phường 7"
}, {
  WardCode: "21405",
  DistrictID: 1455,
  WardName: "Phường 5"
}, {
  WardCode: "21410",
  DistrictID: 1455,
  WardName: "Phường 10"
}, {
  WardCode: "21406",
  DistrictID: 1455,
  WardName: "Phường 6"
}, {
  WardCode: "21408",
  DistrictID: 1455,
  WardName: "Phường 8"
}, {
  WardCode: "21409",
  DistrictID: 1455,
  WardName: "Phường 9"
}, {
  WardCode: "21414",
  DistrictID: 1455,
  WardName: "Phường 14"
}, {
  WardCode: "21415",
  DistrictID: 1455,
  WardName: "Phường 15"
}, {
  WardCode: "21508",
  DistrictID: 1456,
  WardName: "Phường Tân Sơn Nhì"
}, {
  WardCode: "21511",
  DistrictID: 1456,
  WardName: "Phường Tây Thạnh"
}, {
  WardCode: "21506",
  DistrictID: 1456,
  WardName: "Phường Sơn Kỳ"
}, {
  WardCode: "21507",
  DistrictID: 1456,
  WardName: "Phường Tân Quý"
}, {
  WardCode: "21509",
  DistrictID: 1456,
  WardName: "Phường Tân Thành"
}, {
  WardCode: "21504",
  DistrictID: 1456,
  WardName: "Phường Phú Thọ Hòa"
}, {
  WardCode: "21503",
  DistrictID: 1456,
  WardName: "Phường Phú Thạnh"
}, {
  WardCode: "21505",
  DistrictID: 1456,
  WardName: "Phường Phú Trung"
}, {
  WardCode: "21502",
  DistrictID: 1456,
  WardName: "Phường Hòa Thạnh"
}, {
  WardCode: "21501",
  DistrictID: 1456,
  WardName: "Phường Hiệp Tân"
}, {
  WardCode: "21510",
  DistrictID: 1456,
  WardName: "Phường Tân Thới Hòa"
}, {
  WardCode: "21704",
  DistrictID: 1457,
  WardName: "Phường 4"
}, {
  WardCode: "21705",
  DistrictID: 1457,
  WardName: "Phường 5"
}, {
  WardCode: "21708",
  DistrictID: 1457,
  WardName: "Phường 9"
}, {
  WardCode: "21706",
  DistrictID: 1457,
  WardName: "Phường 7"
}, {
  WardCode: "21703",
  DistrictID: 1457,
  WardName: "Phường 3"
}, {
  WardCode: "21701",
  DistrictID: 1457,
  WardName: "Phường 1"
}, {
  WardCode: "21702",
  DistrictID: 1457,
  WardName: "Phường 2"
}, {
  WardCode: "21707",
  DistrictID: 1457,
  WardName: "Phường 8"
}, {
  WardCode: "21714",
  DistrictID: 1457,
  WardName: "Phường 15"
}, {
  WardCode: "21709",
  DistrictID: 1457,
  WardName: "Phường 10"
}, {
  WardCode: "21710",
  DistrictID: 1457,
  WardName: "Phường 11"
}, {
  WardCode: "21715",
  DistrictID: 1457,
  WardName: "Phường 17"
}, {
  WardCode: "21713",
  DistrictID: 1457,
  WardName: "Phường 14"
}, {
  WardCode: "21711",
  DistrictID: 1457,
  WardName: "Phường 12"
}, {
  WardCode: "21712",
  DistrictID: 1457,
  WardName: "Phường 13"
}, {
  WardCode: "21903",
  DistrictID: 1458,
  WardName: "Phường Bình Hưng Hòa"
}, {
  WardCode: "21904",
  DistrictID: 1458,
  WardName: "Phường Bình Hưng Hoà A"
}, {
  WardCode: "21905",
  DistrictID: 1458,
  WardName: "Phường Bình Hưng Hoà B"
}, {
  WardCode: "21906",
  DistrictID: 1458,
  WardName: "Phường Bình Trị Đông"
}, {
  WardCode: "21907",
  DistrictID: 1458,
  WardName: "Phường Bình Trị Đông A"
}, {
  WardCode: "21908",
  DistrictID: 1458,
  WardName: "Phường Bình Trị Đông B"
}, {
  WardCode: "21909",
  DistrictID: 1458,
  WardName: "Phường Tân Tạo"
}, {
  WardCode: "21910",
  DistrictID: 1458,
  WardName: "Phường Tân Tạo A"
}, {
  WardCode: "21901",
  DistrictID: 1458,
  WardName: "Phường  An Lạc"
}, {
  WardCode: "21902",
  DistrictID: 1458,
  WardName: "Phường An Lạc A"
}, {
  WardCode: "22201",
  DistrictID: 1459,
  WardName: "Thị trấn Hóc Môn"
}, {
  WardCode: "22205",
  DistrictID: 1459,
  WardName: "Xã Tân Hiệp"
}, {
  WardCode: "22204",
  DistrictID: 1459,
  WardName: "Xã Nhị Bình"
}, {
  WardCode: "22203",
  DistrictID: 1459,
  WardName: "Xã Đông Thạnh"
}, {
  WardCode: "22206",
  DistrictID: 1459,
  WardName: "Xã Tân Thới Nhì"
}, {
  WardCode: "22208",
  DistrictID: 1459,
  WardName: "Xã Thới Tam Thôn"
}, {
  WardCode: "22211",
  DistrictID: 1459,
  WardName: "Xã Xuân Thới Sơn"
}, {
  WardCode: "22207",
  DistrictID: 1459,
  WardName: "Xã Tân Xuân"
}, {
  WardCode: "22210",
  DistrictID: 1459,
  WardName: "Xã Xuân Thới Đông"
}, {
  WardCode: "22209",
  DistrictID: 1459,
  WardName: "Xã Trung Chánh"
}, {
  WardCode: "22212",
  DistrictID: 1459,
  WardName: "Xã Xuân Thới Thượng"
}, {
  WardCode: "22202",
  DistrictID: 1459,
  WardName: "Xã Bà Điểm"
}, {
  WardCode: "22101",
  DistrictID: 1460,
  WardName: "Thị trấn Củ Chi"
}, {
  WardCode: "22109",
  DistrictID: 1460,
  WardName: "Xã Phú Mỹ Hưng"
}, {
  WardCode: "22103",
  DistrictID: 1460,
  WardName: "Xã An Phú"
}, {
  WardCode: "22121",
  DistrictID: 1460,
  WardName: "Xã Trung Lập Thượng"
}, {
  WardCode: "22102",
  DistrictID: 1460,
  WardName: "Xã An Nhơn Tây"
}, {
  WardCode: "22106",
  DistrictID: 1460,
  WardName: "Xã Nhuận Đức"
}, {
  WardCode: "22107",
  DistrictID: 1460,
  WardName: "Xã Phạm Văn Cội"
}, {
  WardCode: "22108",
  DistrictID: 1460,
  WardName: "Xã Phú Hòa Đông"
}, {
  WardCode: "22120",
  DistrictID: 1460,
  WardName: "Xã Trung Lập Hạ"
}, {
  WardCode: "22119",
  DistrictID: 1460,
  WardName: "Xã Trung An"
}, {
  WardCode: "22111",
  DistrictID: 1460,
  WardName: "Xã Phước Thạnh"
}, {
  WardCode: "22110",
  DistrictID: 1460,
  WardName: "Xã Phước Hiệp"
}, {
  WardCode: "22113",
  DistrictID: 1460,
  WardName: "Xã Tân An Hội"
}, {
  WardCode: "22112",
  DistrictID: 1460,
  WardName: "Xã Phước Vĩnh An"
}, {
  WardCode: "22118",
  DistrictID: 1460,
  WardName: "Xã Thái Mỹ"
}, {
  WardCode: "22116",
  DistrictID: 1460,
  WardName: "Xã Tân Thạnh Tây"
}, {
  WardCode: "22105",
  DistrictID: 1460,
  WardName: "Xã Hòa Phú"
}, {
  WardCode: "22115",
  DistrictID: 1460,
  WardName: "Xã Tân Thạnh Đông"
}, {
  WardCode: "22104",
  DistrictID: 1460,
  WardName: "Xã Bình Mỹ"
}, {
  WardCode: "22114",
  DistrictID: 1460,
  WardName: "Xã Tân Phú Trung"
}, {
  WardCode: "22117",
  DistrictID: 1460,
  WardName: "Xã Tân Thông Hội"
}, {
  WardCode: "21311",
  DistrictID: 1461,
  WardName: "Phường 15"
}, {
  WardCode: "21309",
  DistrictID: 1461,
  WardName: "Phường 13"
}, {
  WardCode: "21313",
  DistrictID: 1461,
  WardName: "Phường 17"
}, {
  WardCode: "21314",
  DistrictID: 1461,
  WardName: "Phường 6"
}, {
  WardCode: "21312",
  DistrictID: 1461,
  WardName: "Phường 16"
}, {
  WardCode: "21308",
  DistrictID: 1461,
  WardName: "Phường 12"
}, {
  WardCode: "21310",
  DistrictID: 1461,
  WardName: "Phường 14"
}, {
  WardCode: "21306",
  DistrictID: 1461,
  WardName: "Phường 10"
}, {
  WardCode: "21304",
  DistrictID: 1461,
  WardName: "Phường 5"
}, {
  WardCode: "21305",
  DistrictID: 1461,
  WardName: "Phường 7"
}, {
  WardCode: "21303",
  DistrictID: 1461,
  WardName: "Phường 4"
}, {
  WardCode: "21301",
  DistrictID: 1461,
  WardName: "Phường 1"
}, {
  WardCode: "21316",
  DistrictID: 1461,
  WardName: "Phường 9"
}, {
  WardCode: "21315",
  DistrictID: 1461,
  WardName: "Phường 8"
}, {
  WardCode: "21307",
  DistrictID: 1461,
  WardName: "Phường 11"
}, {
  WardCode: "21302",
  DistrictID: 1461,
  WardName: "Phường 3"
}, {
  WardCode: "21609",
  DistrictID: 1462,
  WardName: "Phường 13"
}, {
  WardCode: "21607",
  DistrictID: 1462,
  WardName: "Phường 11"
}, {
  WardCode: "21619",
  DistrictID: 1462,
  WardName: "Phường 27"
}, {
  WardCode: "21618",
  DistrictID: 1462,
  WardName: "Phường 26"
}, {
  WardCode: "21608",
  DistrictID: 1462,
  WardName: "Phường 12"
}, {
  WardCode: "21617",
  DistrictID: 1462,
  WardName: "Phường 25"
}, {
  WardCode: "21604",
  DistrictID: 1462,
  WardName: "Phường 5"
}, {
  WardCode: "21606",
  DistrictID: 1462,
  WardName: "Phường 7"
}, {
  WardCode: "21616",
  DistrictID: 1462,
  WardName: "Phường 24"
}, {
  WardCode: "21605",
  DistrictID: 1462,
  WardName: "Phường 6"
}, {
  WardCode: "21610",
  DistrictID: 1462,
  WardName: "Phường 14"
}, {
  WardCode: "21611",
  DistrictID: 1462,
  WardName: "Phường 15"
}, {
  WardCode: "21602",
  DistrictID: 1462,
  WardName: "Phường 2"
}, {
  WardCode: "21601",
  DistrictID: 1462,
  WardName: "Phường 1"
}, {
  WardCode: "21603",
  DistrictID: 1462,
  WardName: "Phường 3"
}, {
  WardCode: "21612",
  DistrictID: 1462,
  WardName: "Phường 17"
}, {
  WardCode: "21614",
  DistrictID: 1462,
  WardName: "Phường 21"
}, {
  WardCode: "21615",
  DistrictID: 1462,
  WardName: "Phường 22"
}, {
  WardCode: "21613",
  DistrictID: 1462,
  WardName: "Phường 19"
}, {
  WardCode: "21620",
  DistrictID: 1462,
  WardName: "Phường 28"
}, {
  WardCode: "21809",
  DistrictID: 1463,
  WardName: "Phường Linh Xuân"
}, {
  WardCode: "21801",
  DistrictID: 1463,
  WardName: "Phường Bình Chiểu"
}, {
  WardCode: "21808",
  DistrictID: 1463,
  WardName: "Phường Linh Trung"
}, {
  WardCode: "21810",
  DistrictID: 1463,
  WardName: "Phường Tam Bình"
}, {
  WardCode: "21811",
  DistrictID: 1463,
  WardName: "Phường Tam Phú"
}, {
  WardCode: "21804",
  DistrictID: 1463,
  WardName: "Phường Hiệp Bình Phước"
}, {
  WardCode: "21803",
  DistrictID: 1463,
  WardName: "Phường Hiệp Bình Chánh"
}, {
  WardCode: "21805",
  DistrictID: 1463,
  WardName: "Phường Linh Chiểu"
}, {
  WardCode: "21807",
  DistrictID: 1463,
  WardName: "Phường Linh Tây"
}, {
  WardCode: "21806",
  DistrictID: 1463,
  WardName: "Phường Linh Đông"
}, {
  WardCode: "21802",
  DistrictID: 1463,
  WardName: "Phường Bình Thọ"
}, {
  WardCode: "21812",
  DistrictID: 1463,
  WardName: "Phường Trường Thọ"
}, {
  WardCode: "22001",
  DistrictID: 1533,
  WardName: "Thị trấn Tân Túc"
}, {
  WardCode: "22009",
  DistrictID: 1533,
  WardName: "Xã Phạm Văn Hai"
}, {
  WardCode: "22015",
  DistrictID: 1533,
  WardName: "Xã Vĩnh Lộc A"
}, {
  WardCode: "22016",
  DistrictID: 1533,
  WardName: "Xã Vĩnh Lộc B"
}, {
  WardCode: "22005",
  DistrictID: 1533,
  WardName: "Xã Bình Lợi"
}, {
  WardCode: "22008",
  DistrictID: 1533,
  WardName: "Xã Lê Minh Xuân"
}, {
  WardCode: "22013",
  DistrictID: 1533,
  WardName: "Xã Tân Nhựt"
}, {
  WardCode: "22012",
  DistrictID: 1533,
  WardName: "Xã Tân Kiên"
}, {
  WardCode: "22004",
  DistrictID: 1533,
  WardName: "Xã Bình Hưng"
}, {
  WardCode: "22010",
  DistrictID: 1533,
  WardName: "Xã Phong Phú"
}, {
  WardCode: "22002",
  DistrictID: 1533,
  WardName: "Xã An Phú Tây"
}, {
  WardCode: "22007",
  DistrictID: 1533,
  WardName: "Xã Hưng Long"
}, {
  WardCode: "22006",
  DistrictID: 1533,
  WardName: "Xã Đa Phước"
}, {
  WardCode: "22014",
  DistrictID: 1533,
  WardName: "Xã Tân Quý Tây"
}, {
  WardCode: "22003",
  DistrictID: 1533,
  WardName: "Xã Bình Chánh"
}, {
  WardCode: "22011",
  DistrictID: 1533,
  WardName: "Xã Quy Đức"
}, {
  WardCode: "22301",
  DistrictID: 1534,
  WardName: "Thị trấn Nhà Bè"
}, {
  WardCode: "22306",
  DistrictID: 1534,
  WardName: "Xã Phước Kiển"
}, {
  WardCode: "22307",
  DistrictID: 1534,
  WardName: "Xã Phước Lộc"
}, {
  WardCode: "22304",
  DistrictID: 1534,
  WardName: "Xã Nhơn Đức"
}, {
  WardCode: "22305",
  DistrictID: 1534,
  WardName: "Xã Phú Xuân"
}, {
  WardCode: "22303",
  DistrictID: 1534,
  WardName: "Xã Long Thới"
}, {
  WardCode: "22302",
  DistrictID: 1534,
  WardName: "Xã Hiệp Phước"
}, {
  WardCode: "22401",
  DistrictID: 2090,
  WardName: "Thị trấn Cần Thạnh"
}, {
  WardCode: "22403",
  DistrictID: 2090,
  WardName: "Xã Bình Khánh"
}, {
  WardCode: "22406",
  DistrictID: 2090,
  WardName: "Xã Tam Thôn Hiệp"
}, {
  WardCode: "22402",
  DistrictID: 2090,
  WardName: "Xã An Thới Đông"
}, {
  WardCode: "22407",
  DistrictID: 2090,
  WardName: "Xã Thạnh An"
}, {
  WardCode: "22404",
  DistrictID: 2090,
  WardName: "Xã Long Hòa"
}, {
  WardCode: "22405",
  DistrictID: 2090,
  WardName: "Xã Lý Nhơn"
}];
// EXTERNAL MODULE: external "lodash"
var external_lodash_ = __webpack_require__(3804);
// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(6731);
// EXTERNAL MODULE: external "react-redux"
var external_react_redux_ = __webpack_require__(79);
// EXTERNAL MODULE: external "react-toastify"
var external_react_toastify_ = __webpack_require__(2034);
// EXTERNAL MODULE: ./store/user.js
var store_user = __webpack_require__(5175);
// EXTERNAL MODULE: ./components/share/common/Select.js
var Select = __webpack_require__(7598);
;// CONCATENATED MODULE: ./components/share/common/ContentModal/ConfirmLottery.js















const ConfirmLottery = ({
  listTable,
  selectPeriod,
  total,
  gameType,
  tabs,
  isRandom,
  valueTable,
  valueLottery
}) => {
  const dataProvince = [{
    text: "TP. Hồ Chí Minh",
    value: 1
  }, {
    text: "Khu vực khác",
    value: 2
  }];
  const arrDelivery = ["store", "ship", "take"];
  const router = (0,router_.useRouter)();
  const dispatch = (0,external_react_redux_.useDispatch)();
  const user = (0,external_react_redux_.useSelector)(state => state.user.user);
  const lottery = (0,external_react_redux_.useSelector)(state => state.lottery);
  const {
    kenoData,
    megaData,
    powerData
  } = lottery;
  const {
    0: area,
    1: setArea
  } = (0,external_react_.useState)({
    text: "TP. Hồ Chí Minh",
    value: 1
  });
  const {
    0: dataDist,
    1: setDataDist
  } = (0,external_react_.useState)([]);
  const {
    0: distId,
    1: setDistId
  } = (0,external_react_.useState)(null);
  const {
    0: dataWard,
    1: setDataWard
  } = (0,external_react_.useState)([]);
  const {
    0: wardId,
    1: setWardId
  } = (0,external_react_.useState)(null);
  const {
    0: address,
    1: setAddress
  } = (0,external_react_.useState)("");
  const {
    0: showAddressForm,
    1: setShowAddressForm
  } = (0,external_react_.useState)(false);
  const {
    0: disableButton,
    1: setDisableButton
  } = (0,external_react_.useState)(true);
  const {
    0: valueDelivery,
    1: setValueDelivery
  } = (0,external_react_.useState)(0);
  const {
    0: loading,
    1: setLoading
  } = (0,external_react_.useState)(false);
  const {
    0: shipType,
    1: setShipType
  } = (0,external_react_.useState)(null);
  const {
    0: feeShip,
    1: setFeeShip
  } = (0,external_react_.useState)([{
    name: "Phí giao hàng",
    price: 18500
  }]);
  const {
    0: delivery,
    1: setDelivery
  } = (0,external_react_.useState)({});
  const isKeno = (0,external_react_.useMemo)(() => {
    return gameType === "keno";
  }, [gameType]);
  const listService = (0,external_react_.useMemo)(() => {
    if (isKeno) {
      return [{
        display: "Lưu tại cửa hàng",
        value: 1
      }];
    }

    return [{
      display: "Lưu tại cửa hàng",
      value: 1
    }, {
      display: "Giao hàng",
      value: 2
    }, {
      display: "Đến nhận tại cửa hàng",
      value: 3
    }];
  }, [gameType]);
  const getCurrentPeriod = (0,external_react_.useMemo)(() => {
    switch (gameType) {
      case "keno":
        return (0,external_lodash_.get)(kenoData, "current_ky", "");

      case "power":
        return (0,external_lodash_.get)(powerData, "power.ky", "");

      case "mega":
        return (0,external_lodash_.get)(megaData, "mega.ky", "");
    }
  }, [lottery]);

  const handleDelivery = e => {
    setValueDelivery(e.target.value);
    const valueShip = e.target.value;
    setShipType(arrDelivery[valueShip - 1]);

    if (valueShip == 2) {
      setFeeShip([{
        name: "Phí giao hàng",
        price: 18500
      }]);
      setShowAddressForm(true);
    } else {
      setDelivery({});
      setFeeShip([]);
      setShowAddressForm(false);
    }
  };

  const handleInputInfo = e => {
    const addressInfo = e.target.value;
    let nameDistrict = "",
        nameWard = "";
    setAddress(addressInfo);

    if (area.value === 2) {
      setFeeShip([{
        name: "Phí giao hàng",
        price: 28000
      }]);
    } else {
      nameDistrict = dist.find(item => item.DistrictID === distId).DistrictName;
      nameWard = ward.find(item => item.WardCode == wardId).WardName;
      setFeeShip([{
        name: "Phí giao hàng",
        price: 18500
      }]);
    }

    if (e.target.value == "") {
      setDisableButton(true);
    } else {
      setDelivery({
        area: area.value === 2,
        dist: distId,
        ward: wardId,
        address: `${address},${nameWard},${nameDistrict}`
      });
      setDisableButton(false);
    }
  };

  const onChangeArea = event => {
    setArea(dataProvince[event - 1]);

    if (event === "1") {
      setFeeShip([{
        name: "Phí giao hàng",
        price: 18500
      }]);
      setDataDist(dist);
      setAddress("");
      setArea({
        text: "TP. Hồ Chí Minh",
        value: 1
      });
      setDistId(1442);
      setWardId(20110);
      setDataDist(dist);
      setDataWard(ward.filter(item => item.DistrictID === 1442));
    } else if (event === "2") {
      setArea({
        text: "Khu vực khác",
        value: 2
      });
      setDataDist([]);
      setDataWard([]);
      setAddress("");
      setDistId(null);
      setWardId(null);
      setFeeShip([{
        name: "Phí giao hàng",
        price: 28000
      }]);
      setDelivery({
        area: true,
        dist: null,
        ward: null,
        address: ""
      });
    }
  };

  const onChangeDistrict = event => {
    const districtId = Number(event);
    const firstWardId = ward.filter(item => item.DistrictID === districtId)[0].WardCode;
    const nameDistrict = dist.find(item => item.DistrictID === districtId).DistrictName;
    const nameWard = ward.find(item => item.WardCode == firstWardId).WardName;
    setDelivery({
      area: area == 2,
      dist: districtId,
      ward: Number(firstWardId),
      address: `${address},${nameWard},${nameDistrict}`
    });
    setDistId(districtId);
    setDataWard(ward.filter(item => item.DistrictID === districtId));
  };

  const onChangeWard = event => {
    const wardID = Number(event);
    const nameWard = ward.find(item => item.WardCode == wardID).WardName;
    const districtId = ward.find(item => item.WardCode == wardID).DistrictID;
    const nameDistrict = dist.find(item => item.DistrictID === districtId).DistrictName;
    setDelivery({
      area: area.value == 2,
      dist: distId,
      ward: wardID,
      address: `${address},${nameWard}, ${nameDistrict}`
    });
    setWardId(wardID);
  };

  const updateInfoUser = async () => {
    const response = await BaseApi/* default.post */.Z.post("/user/me");

    if (response.status) {
      dispatch((0,store_user/* setUser */.av)(response));
    }
  };

  const handleBuyLottery = async () => {
    setLoading(true);
    const arrLottery = listTable.map(item => ({
      price: item.money,
      subGame: (0,external_lodash_.get)(item, "subGame[0]", ""),
      numOfNums: (0,external_lodash_.get)(item, "listSelect", []).length,
      nums: (0,external_lodash_.get)(item, "listSelect", []),
      lon: false,
      nho: false,
      chan: false,
      le: false,
      plus: false
    }));

    try {
      const response = await BaseApi/* default.post */.Z.post("/lottery", {
        address: `${address}`,
        shipType,
        fee: feeShip,
        delivery,
        isFromWeb: true,
        isPrint: false,
        isUseBalance: true,
        ky: getCurrentPeriod,
        lottery: arrLottery,
        lotteryType: gameType,
        name: user.name,
        phoneNumber: user.phoneNumber,
        nums: Number(selectPeriod),
        price: total,
        isTC: isRandom,
        numsTicket: valueLottery,
        numsTable: valueTable,
        isSame: tabs === "same"
      });
      setLoading(false);

      if (response.status) {
        external_react_toastify_.toast.success("Mua vé thành công");
        router.push("/success");
        window.closeModal();
        updateInfoUser();
      } else external_react_toastify_.toast.error("Lỗi mua vé");
    } catch (error) {
      setLoading(false);
      external_react_toastify_.toast.error("Lỗi hệ thống");
    }
  };

  (0,external_react_.useEffect)(() => {
    //auto check delivery
    setValueDelivery(1);
    setShipType(arrDelivery[0]);
    setArea({
      text: "TP. Hồ Chí Minh",
      value: 1
    });
    setDistId(1442);
    setWardId(20110);
    dist.map(item => (item.value = item.DistrictID, item.text = item.DistrictName));
    ward.map(item => (item.value = item.WardCode, item.text = item.WardName));
    setDataDist(dist);
    setDataWard(ward.filter(item => item.DistrictID === 1442));
  }, []);
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(external_react_.Fragment, {
    children: [/*#__PURE__*/jsx_runtime_.jsx(lottery_DeliveryInfo, {
      arrInfo: listService,
      onClickDelivery: handleDelivery
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: showAddressForm ? "" : "invisible",
      children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "mb-1",
        children: /*#__PURE__*/jsx_runtime_.jsx(Select/* default */.Z, {
          options: dataProvince,
          label: "Khu vực",
          onChangeSelect: onChangeArea
        })
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: `${area.value === 2 ? "flex flex-col-reverse mb-1" : ""}`,
        children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: `${area.value === 2 ? "invisible" : ""}`,
          children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
            className: "mb-1",
            children: /*#__PURE__*/jsx_runtime_.jsx(Select/* default */.Z, {
              options: dataDist,
              label: "Quận",
              onChangeSelect: onChangeDistrict
            })
          }), /*#__PURE__*/jsx_runtime_.jsx("div", {
            className: "mb-1",
            children: /*#__PURE__*/jsx_runtime_.jsx(Select/* default */.Z, {
              options: dataWard,
              label: "Phường",
              onChangeSelect: onChangeWard
            })
          })]
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: "mb-1",
          children: [/*#__PURE__*/jsx_runtime_.jsx("span", {
            className: "text-gray-700",
            children: "\u0110\u1ECBa ch\u1EC9 chi ti\u1EBFt"
          }), /*#__PURE__*/jsx_runtime_.jsx("input", {
            className: "form-select block w-full mt-1 text-xl rounded border-none bg-gray-50 appearance-none input input-primary focus:border",
            style: {
              height: "27px"
            },
            value: address,
            onChange: handleInputInfo,
            placeholder: "Nh\u1EADp \u0111\u1ECBa ch\u1EC9 giao h\xE0ng"
          })]
        })]
      })]
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: "mt-2",
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: "flex justify-between",
        children: [/*#__PURE__*/jsx_runtime_.jsx("p", {
          children: "V\xE9 mua"
        }), /*#__PURE__*/jsx_runtime_.jsx("span", {
          className: "text-red-600",
          children: (0,functions/* formatCurrency */.xG)(total)
        })]
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: "flex justify-between",
        children: [/*#__PURE__*/jsx_runtime_.jsx("p", {
          children: "Ph\xED giao h\xE0ng"
        }), /*#__PURE__*/jsx_runtime_.jsx("span", {
          className: " text-red-600",
          children: valueDelivery == 2 ? (0,functions/* formatCurrency */.xG)(feeShip[0].price) : 0
        })]
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: "flex justify-between",
        children: [/*#__PURE__*/jsx_runtime_.jsx("p", {
          children: "Th\xE0nh ti\u1EC1n"
        }), /*#__PURE__*/jsx_runtime_.jsx("span", {
          className: " text-red-600",
          children: valueDelivery == 2 ? (0,functions/* formatCurrency */.xG)(total + feeShip[0].price) : (0,functions/* formatCurrency */.xG)(total)
        })]
      })]
    }), /*#__PURE__*/jsx_runtime_.jsx("div", {
      className: "modal-action",
      children: /*#__PURE__*/jsx_runtime_.jsx(external_react_.Fragment, {
        children: /*#__PURE__*/jsx_runtime_.jsx(Button/* default */.Z, {
          className: "w-full",
          onClick: handleBuyLottery,
          disabled: disableButton && showAddressForm || loading,
          children: "Ho\xE0n t\u1EA5t mua h\xE0ng"
        })
      })
    })]
  });
};

/* harmony default export */ var ContentModal_ConfirmLottery = (/*#__PURE__*/external_react_default().memo(ConfirmLottery));
// EXTERNAL MODULE: ./components/share/common/ContentModal/Notification.js
var Notification = __webpack_require__(3477);
;// CONCATENATED MODULE: ./components/share/lottery/Total.js







const Total = ({
  price = 0,
  listTable,
  selectPeriod,
  gameType,
  handleBack,
  dashboardPage,
  valueTable,
  valueLottery,
  tabs,
  isRandom
}) => {
  const renderRightViewModalConfirm = () => {
    return /*#__PURE__*/jsx_runtime_.jsx("div", {
      className: "flex justify-center items-center text-2xl mr-1",
      children: "Th\xF4ng tin giao h\xE0ng"
    });
  };

  const handleSubmitTotal = () => {
    let i = 0;

    if (price === 0 || isNaN(price)) {
      return window.openModal({
        content: /*#__PURE__*/jsx_runtime_.jsx(Notification/* default */.Z, {
          titleModal: "Xảy ra lỗi",
          textModal: "Vui lòng kiểm tra lại vé",
          titleSubmit: "OK"
        }),
        isShowHeader: false
      });
    } else {
      while (i < listTable.length) {
        if (!!listTable[i].listSelect.length) {
          if (listTable[i].listSelect.length !== listTable[i].level) {
            return window.openModal({
              content: /*#__PURE__*/jsx_runtime_.jsx(Notification/* default */.Z, {
                titleModal: "Xảy ra lỗi",
                textModal: "Vui lòng kiểm tra lại vé",
                titleSubmit: "OK"
              }),
              isShowHeader: false
            });
          }
        }

        i++;
      }

      if (valueTable > 6) {
        return window.openModal({
          content: /*#__PURE__*/jsx_runtime_.jsx(Notification/* default */.Z, {
            titleModal: "Xảy ra lỗi",
            textModal: "Tối đa được mua 6 bảng",
            titleSubmit: "OK"
          }),
          isShowHeader: false
        });
      } else if ((!valueTable || !valueLottery) && isRandom) {
        return window.openModal({
          content: /*#__PURE__*/jsx_runtime_.jsx(Notification/* default */.Z, {
            titleModal: "Xảy ra lỗi",
            textModal: "Vui lòng điền đầy đủ thông tin vé TC",
            titleSubmit: "OK"
          }),
          isShowHeader: false
        });
      } else if (i == listTable.length || valueTable && valueLottery) {
        return window.openModal({
          content: /*#__PURE__*/jsx_runtime_.jsx(ContentModal_ConfirmLottery, {
            listTable: listTable,
            selectPeriod: selectPeriod,
            gameType: gameType,
            total: price,
            tabs: tabs ? tabs[0].value : null,
            isRandom: isRandom,
            valueTable: valueTable,
            valueLottery: valueLottery
          }),
          rightView: renderRightViewModalConfirm()
        });
      }
    }
  };

  return /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
    className: "container w-full fixed bg-fixed bottom-0 bg-gray-100 py-1 px-2",
    children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: "flex justify-between items-center mb-1 ",
      children: [/*#__PURE__*/jsx_runtime_.jsx("p", {
        className: "",
        children: "T\u1ED5ng ti\u1EC1n: "
      }), /*#__PURE__*/jsx_runtime_.jsx("span", {
        className: "sub-text-color",
        children: (0,functions/* formatCurrency */.xG)(price)
      })]
    }), gameType == "keno" || dashboardPage == true ? /*#__PURE__*/jsx_runtime_.jsx(external_react_.Fragment, {
      children: /*#__PURE__*/jsx_runtime_.jsx(Button/* default */.Z, {
        onClick: handleSubmitTotal,
        className: " w-full e",
        children: "Ti\u1EBFp t\u1EE5c"
      })
    }) : /*#__PURE__*/jsx_runtime_.jsx(external_react_.Fragment, {
      children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: "flex justify-between",
        children: [/*#__PURE__*/jsx_runtime_.jsx(Button/* default */.Z, {
          onClick: handleBack,
          style: {
            width: "47%"
          },
          children: "Ch\u1ECDn l\u1EA1i"
        }), /*#__PURE__*/jsx_runtime_.jsx(Button/* default */.Z, {
          onClick: handleSubmitTotal,
          style: {
            width: "47%"
          },
          children: "Ti\u1EBFp t\u1EE5c"
        })]
      })
    })]
  });
};
/* harmony default export */ var lottery_Total = (/*#__PURE__*/external_react_default().memo(Total));

/***/ }),

/***/ 9900:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": function() { return /* binding */ BaseApi; }
});

;// CONCATENATED MODULE: external "axios"
var external_axios_namespaceObject = require("axios");;
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_namespaceObject);
// EXTERNAL MODULE: external "js-cookie"
var external_js_cookie_ = __webpack_require__(6155);
var external_js_cookie_default = /*#__PURE__*/__webpack_require__.n(external_js_cookie_);
// EXTERNAL MODULE: external "store"
var external_store_ = __webpack_require__(7405);
var external_store_default = /*#__PURE__*/__webpack_require__.n(external_store_);
// EXTERNAL MODULE: ./store/user.js
var user = __webpack_require__(5175);
// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(6731);
var router_default = /*#__PURE__*/__webpack_require__.n(router_);
// EXTERNAL MODULE: external "lodash"
var external_lodash_ = __webpack_require__(3804);
;// CONCATENATED MODULE: ./config/BaseApi.js






const BaseAPI = external_axios_default().create({
  baseURL: "https://apis.kenoclub.net",
  timeout: 12000
});
BaseAPI.interceptors.response.use(config => {
  const token = external_js_cookie_default().get("token");
  const authDevice = "d04f817cf80fb5cad2eb936fa2aba25ae6d1a77dab4e5bf4662099072cd9f6d1";
  if (!!token) config.headers.Authorization = `Bearer ${token}`;
  config.headers.authdevice = authDevice;
  const {
    status
  } = config.data;

  if (status) {
    return config.data;
  }

  return config;
}, error => {
  if ((0,external_lodash_.get)(error, "response.status", "") === 401) {
    external_store_default().dispatch((0,user/* logOut */.ni)());
    router_default().push("/auth/login");
  }

  Promise.reject((0,external_lodash_.get)(error, "response.data", {}));
});
BaseAPI.interceptors.request.use(config => {
  const token = external_js_cookie_default().get("token");
  const authDevice = "d04f817cf80fb5cad2eb936fa2aba25ae6d1a77dab4e5bf4662099072cd9f6d1";
  if (!!token) config.headers.Authorization = `Bearer ${token}`;
  config.headers.authdevice = authDevice;
  return config;
}, err => Promise.reject(err));
/* harmony default export */ var BaseApi = (BaseAPI);

/***/ }),

/***/ 1377:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "wO": function() { return /* binding */ lotteryType; },
/* harmony export */   "ZW": function() { return /* binding */ LOTTERY_DATA; },
/* harmony export */   "Po": function() { return /* binding */ megaPrice; },
/* harmony export */   "bD": function() { return /* binding */ powerPrice; },
/* harmony export */   "G7": function() { return /* binding */ LIST_TYPE_LOTTERY; }
/* harmony export */ });
/* unused harmony exports KENO, MAX_3D, MAX_4D, POWER, MEGA, TOGETHER, LOTO, BIG, EQUAL_BIG, SMALL, EVEN, EVEN_11, ODD_11, EQUAL_EVEN, ODD, BIG_SMALL, ODD_EVEN */
const KENO = "keno";
const MAX_3D = "3d";
const MAX_4D = "4d";
const POWER = "power";
const MEGA = "mega";
const TOGETHER = "together";
const LOTO = "loto";
const lotteryType = {
  keno: "keno",
  power: "power",
  mega: "mega"
}; // options plus keno

const BIG = "lon";
const EQUAL_BIG = "hoa-lonnho";
const SMALL = "nho";
const EVEN = "chan";
const EVEN_11 = "chan-11-12";
const ODD_11 = "le-11-12";
const EQUAL_EVEN = "hoa-chanle";
const ODD = "le";
const BIG_SMALL = "BIG_SMALL";
const ODD_EVEN = "ODD_EVEN";
const LOTTERY_DATA = {
  currentPeriod: 0,
  listSelect: [],
  listSelectedGroup: [],
  // use in buy lottery group
  isShowAlert: false,
  type: "",
  period: 1,
  money: 10000,
  level: 10,
  subGame: [],
  subGameName: {}
};
const megaPrice = [{
  text: "Bao 5",
  value: 5,
  money: 400000
}, {
  text: "Bao 6",
  value: 6,
  money: 10000
}, {
  text: "Bao 7",
  value: 7,
  money: 70000
}, {
  text: "Bao 8",
  value: 8,
  money: 280000
}, {
  text: "Bao 9",
  value: 9,
  money: 840000
}, {
  text: "Bao 10",
  value: 10,
  money: 2100000
}, {
  text: "Bao 11",
  value: 11,
  money: 4620000
}, {
  text: "Bao 12",
  value: 12,
  money: 9240000
}, {
  text: "Bao 13",
  value: 13,
  money: 17160000
}, {
  text: "Bao 14",
  value: 14,
  money: 30030000
}, {
  text: "Bao 15",
  value: 15,
  money: 50050000
}, {
  text: "Bao 18",
  value: 18,
  money: 185640000
}];
const powerPrice = [{
  text: "Bao 5",
  value: 5,
  money: 500000
}, {
  text: "Bao 6",
  value: 6,
  money: 10000
}, {
  text: "Bao 7",
  value: 7,
  money: 70000
}, {
  text: "Bao 8",
  value: 8,
  money: 280000
}, {
  text: "Bao 9",
  value: 9,
  money: 840000
}, {
  text: "Bao 10",
  value: 10,
  money: 2100000
}, {
  text: "Bao 11",
  value: 11,
  money: 4620000
}, {
  text: "Bao 12",
  value: 12,
  money: 9240000
}, {
  text: "Bao 13",
  value: 13,
  money: 17160000
}, {
  text: "Bao 14",
  value: 14,
  money: 30030000
}, {
  text: "Bao 15",
  value: 15,
  money: 50050000
}, {
  text: "Bao 18",
  value: 18,
  money: 185640000
}];
const LIST_TYPE_LOTTERY = {
  keno: {
    image: "/images/logo/keno_logo.png",
    name: "Keno",
    slug: "keno",
    number: new Array(80),
    date: "Từ 6:00 đến 22:00 hằng ngày",
    time: "Mỗi 10 phút",
    period: [{
      text: "1 Kỳ",
      value: 1
    }, {
      text: "2 Kỳ",
      value: 2
    }, {
      text: "3 Kỳ",
      value: 3
    }, {
      text: "4 Kỳ",
      value: 4
    }, {
      text: "5 Kỳ",
      value: 5
    }, {
      text: "10 Kỳ",
      value: 10
    }, {
      text: "15 Kỳ",
      value: 15
    }, {
      text: "20 Kỳ",
      value: 20
    }, {
      text: "30 Kỳ",
      value: 30
    }],
    levels: [{
      text: "1",
      value: 1
    }, {
      text: "2",
      value: 2
    }, {
      text: "3",
      value: 3
    }, {
      text: "4",
      value: 4
    }, {
      text: "5",
      value: 5
    }, {
      text: "6",
      value: 6
    }, {
      text: "7",
      value: 7
    }, {
      text: "8",
      value: 8
    }, {
      text: "9",
      value: 9
    }, {
      text: "10",
      value: 10
    }],
    prices: [{
      text: "10.000",
      value: 10000
    }, {
      text: "20.000",
      value: 20000
    }, {
      text: "30.000",
      value: 30000
    }, {
      text: "40.000",
      value: 40000
    }, {
      text: "50.000",
      value: 50000
    }, {
      text: "100.000",
      value: 100000
    }, {
      text: "150.000",
      value: 150000
    }, {
      text: "200.000",
      value: 200000
    }, {
      text: "500.000",
      value: 500000
    }],
    listOptions: [{
      name: "Lớn: có 11 số trở lên từ 41 đến 80",
      display: "Lớn",
      value: BIG,
      type: BIG_SMALL
    }, {
      name: "Hoà Lớn-Nhỏ: có 10 số từ 01 đến 40 và 10 số từ 41-80",
      display: "Hoà Lớn-Nhỏ",
      value: EQUAL_BIG,
      type: BIG_SMALL
    }, {
      name: "Nhỏ: có 11 số trở lên từ 01 đến 40",
      display: "Nhỏ",
      value: SMALL,
      type: BIG_SMALL
    }, {
      name: "Chẵn: có 13 số trở lên là số chẵn",
      display: "Chẵn",
      value: EVEN,
      type: ODD_EVEN
    }, {
      name: "Chẵn 11-12: có 11 hoặc 12 số chẵn",
      display: "Chẵn 11-12",
      value: EVEN_11,
      type: ODD_EVEN
    }, {
      name: "Hoà Chẵn-Lẻ: có 10 số chẵn và 10 số lẻ",
      display: "Hoà Chẵn-Lẻ",
      value: EQUAL_EVEN,
      type: ODD_EVEN
    }, {
      name: "Lẻ 11-12: có 11 hoặc 12 số lẻ",
      display: "Lẻ 11-12",
      value: ODD_11,
      type: ODD_EVEN
    }, {
      name: "Lẻ: có 13 số trở lên là số lẻ",
      display: "Lẻ",
      value: ODD,
      type: ODD_EVEN
    }]
  },
  power: {
    image: "/images/logo/power655_logo.png",
    name: "Power 6/55",
    number: new Array(55),
    slug: "power",
    date: "Thứ 3 - Thứ 5 - Thứ 7",
    time: "Vào lúc 18:00",
    period: [{
      text: "1 Kỳ",
      value: 1
    }, {
      text: "2 Kỳ",
      value: 2
    }, {
      text: "3 Kỳ",
      value: 3
    }, {
      text: "4 Kỳ",
      value: 4
    }, {
      text: "5 Kỳ",
      value: 5
    }, {
      text: "6 Kỳ",
      value: 6
    }],
    lottery: powerPrice
  },
  mega: {
    image: "/images/logo/mega645_logo.png",
    name: "Mega 6/45",
    number: new Array(45),
    slug: "mega",
    date: "Thứ 4 - Thứ 6 - Chủ Nhật",
    time: "Vào lúc 18:00",
    lottery: megaPrice,
    period: [{
      text: "1 Kỳ",
      value: 1
    }, {
      text: "2 Kỳ",
      value: 2
    }, {
      text: "3 Kỳ",
      value: 3
    }, {
      text: "4 Kỳ",
      value: 4
    }, {
      text: "5 Kỳ",
      value: 5
    }, {
      text: "6 Kỳ",
      value: 6
    }]
  },
  TOGETHER: {
    image: "/images/logo/together.jpg",
    name: "Bao 3 Miền",
    type: ["mega", "power"],
    slug: "together"
  },
  LOTO: {
    image: "/images/logo/lotodongnai.jpg",
    name: "Lô Tô Đồng Nai",
    type: [],
    slug: "loto"
  }
};

/***/ }),

/***/ 7772:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var components_share_common_Container__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(9566);
/* harmony import */ var components_share_common_ContentModal_BoxSelectNumber__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(125);
/* harmony import */ var components_share_common_Select__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(7598);
/* harmony import */ var components_share_lottery_BoxSelect__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(9199);
/* harmony import */ var components_share_lottery_LotteryBox__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(3155);
/* harmony import */ var components_share_lottery_Total__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(4039);
/* harmony import */ var constants_lottery__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(1377);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(3804);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_9__);



function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }











const dataDefaultKeno = _objectSpread(_objectSpread({}, constants_lottery__WEBPACK_IMPORTED_MODULE_7__/* .LOTTERY_DATA */ .ZW), {}, {
  money: 0
});

const Keno = () => {
  const {
    0: listTable,
    1: setListTable
  } = (0,react__WEBPACK_IMPORTED_MODULE_9__.useState)([(0,lodash__WEBPACK_IMPORTED_MODULE_8__.cloneDeep)(dataDefaultKeno), (0,lodash__WEBPACK_IMPORTED_MODULE_8__.cloneDeep)(dataDefaultKeno), (0,lodash__WEBPACK_IMPORTED_MODULE_8__.cloneDeep)(dataDefaultKeno)]);
  const dataKeno = constants_lottery__WEBPACK_IMPORTED_MODULE_7__/* .LIST_TYPE_LOTTERY.keno */ .G7.keno;
  const {
    0: selectPeriod,
    1: setSelectPeriod
  } = (0,react__WEBPACK_IMPORTED_MODULE_9__.useState)(1);
  const totalMoney = (0,react__WEBPACK_IMPORTED_MODULE_9__.useMemo)(() => {
    const total = listTable.reduce((total, item) => total + item.money, 0);
    return total * selectPeriod;
  }, [listTable, selectPeriod]);

  const handleClickButton = ({
    data,
    index
  }) => {
    const arrTemp = (0,lodash__WEBPACK_IMPORTED_MODULE_8__.cloneDeep)(listTable);
    arrTemp[index] = data;
    setListTable(arrTemp);
  };

  const onChangeSelect = value => {
    setSelectPeriod(value);
  };

  const handleOpenModal = ({
    index
  }) => () => {
    const dataModal = listTable[index];
    window.openModal({
      content: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(components_share_common_ContentModal_BoxSelectNumber__WEBPACK_IMPORTED_MODULE_2__/* .default */ .Z, {
        gameType: dataKeno.slug,
        index: index,
        data: dataModal,
        isBigSmall: index === 2,
        length: dataKeno.number.length,
        handleClickButton: handleClickButton
      }),
      isShowHeader: false
    });
  };

  const renderListBoxSelect = (0,react__WEBPACK_IMPORTED_MODULE_9__.useMemo)(() => {
    return listTable.map((box, index) => /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
      className: "mb-1.5",
      children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(components_share_lottery_BoxSelect__WEBPACK_IMPORTED_MODULE_4__/* .default */ .Z, {
        index: index,
        handleOpenModal: handleOpenModal,
        data: box,
        isBigSmall: index === 2
      }, index)
    }, index));
  }, [listTable]);
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(components_share_common_Container__WEBPACK_IMPORTED_MODULE_1__/* .default */ .Z, {
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
      className: "w-full",
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(components_share_lottery_LotteryBox__WEBPACK_IMPORTED_MODULE_5__/* .default */ .Z, {
        isDetailLottery: true,
        data: dataKeno,
        isKeno: true
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        className: "flex justify-center flex-col w-full mt-2",
        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(components_share_common_Select__WEBPACK_IMPORTED_MODULE_3__/* .default */ .Z, {
          label: "Số kỳ mua",
          defaultValue: selectPeriod,
          options: dataKeno.period,
          onChangeSelect: onChangeSelect
        })
      })]
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
      className: "w-full mt-2 rounded overflow-y-auto mb-40 hidden-scrollbar",
      style: {
        maxHeight: "70vh"
      },
      children: renderListBoxSelect
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(components_share_lottery_Total__WEBPACK_IMPORTED_MODULE_6__/* .default */ .Z, {
      price: totalMoney,
      listTable: listTable,
      selectPeriod: selectPeriod,
      gameType: "keno"
    })]
  });
};

/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_9___default().memo(Keno));

/***/ }),

/***/ 2360:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Vg": function() { return /* binding */ addListCopy; },
/* harmony export */   "CF": function() { return /* binding */ setRooms; },
/* harmony export */   "$L": function() { return /* binding */ addSelectLotteryKeno; },
/* harmony export */   "$2": function() { return /* binding */ addSelectPriceKeno; },
/* harmony export */   "HL": function() { return /* binding */ setDataKeno; },
/* harmony export */   "yS": function() { return /* binding */ setDataMega; },
/* harmony export */   "pG": function() { return /* binding */ setDataPower; },
/* harmony export */   "CC": function() { return /* binding */ setLotteryHistory; },
/* harmony export */   "Nf": function() { return /* binding */ addCopyLotteryKeno; }
/* harmony export */ });
/* unused harmony exports lotteryModule, addLottery */
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6139);
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__);

const lotteryModule = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createSlice)({
  name: "lottery",
  initialState: {
    user: {},
    listCopy: [],
    roomShare: {},
    selectLotteryKeno: {},
    selectPriceKeno: {},
    kenoData: {},
    megaData: {},
    powerData: {},
    lotteryHistory: {
      variableTab: "keno"
    },
    copyLotteryKeno: {}
  },
  reducers: {
    addListCopy: (state, action) => {
      state.listCopy = action.payload;
    },
    addSelectLotteryKeno: (state, action) => {
      state.selectLotteryKeno = action.payload;
    },
    addCopyLotteryKeno: (state, action) => {
      state.copyLotteryKeno = action.payload;
    },
    addSelectPriceKeno: (state, action) => {
      state.selectPriceKeno = action.payload;
    },
    addLottery: (state, action) => {
      state.infoLottery.push(action.payload);
    },
    setRooms: (state, action) => {
      state.roomShare = action.payload;
    },
    setDataKeno: (state, action) => {
      state.kenoData = action.payload;
    },
    setDataMega: (state, action) => {
      state.megaData = action.payload;
    },
    setDataPower: (state, action) => {
      state.powerData = action.payload;
    },
    setLotteryHistory: (state, action) => {
      state.lotteryHistory = action.payload;
    }
  }
}); // Action creators are generated for each case reducer function

const {
  addListCopy,
  addLottery,
  setRooms,
  addSelectLotteryKeno,
  addSelectPriceKeno,
  setDataKeno,
  setDataMega,
  setDataPower,
  setLotteryHistory,
  addCopyLotteryKeno
} = lotteryModule.actions;
/* harmony default export */ __webpack_exports__["ZP"] = (lotteryModule.reducer);

/***/ }),

/***/ 5175:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "av": function() { return /* binding */ setUser; },
/* harmony export */   "ni": function() { return /* binding */ logOut; }
/* harmony export */ });
/* unused harmony export userModule */
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6139);
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6155);
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(js_cookie__WEBPACK_IMPORTED_MODULE_1__);


const userModule = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createSlice)({
  name: "user",
  initialState: {
    user: {}
  },
  reducers: {
    setUser: (state, action) => {
      const {
        token,
        user
      } = action.payload;
      state.user = user;

      if (token) {
        js_cookie__WEBPACK_IMPORTED_MODULE_1___default().set("token", token);
      }
    },
    logOut: () => {
      js_cookie__WEBPACK_IMPORTED_MODULE_1___default().remove("token");
    }
  }
}); // Action creators are generated for each case reducer function

const {
  setUser,
  logOut
} = userModule.actions;
/* harmony default export */ __webpack_exports__["ZP"] = (userModule.reducer);

/***/ }),

/***/ 6187:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "xG": function() { return /* binding */ formatCurrency; },
/* harmony export */   "AK": function() { return /* binding */ classNames; },
/* harmony export */   "TN": function() { return /* binding */ title; },
/* harmony export */   "p6": function() { return /* binding */ formatDate; },
/* harmony export */   "pw": function() { return /* binding */ totalLotteryWin; }
/* harmony export */ });
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2470);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_0__);

const formatCurrency = money => {
  const num = parseInt(money);
  return `${(num || 0).toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")}đ`;
};
const classNames = (...classes) => {
  return classes.filter(Boolean).join(" ");
};
const title = (index, subGame) => {
  const objTitle = {
    0: "A",
    1: "B",
    2: `${subGame ? "Trò Chơi Phụ" : "C"}`,
    3: "D",
    4: "E",
    5: "F"
  };
  return objTitle[index];
};
const formatDate = date => moment__WEBPACK_IMPORTED_MODULE_0___default()(date).locale("vi").format("L");
const totalLotteryWin = listNumber => {
  return listNumber.reduce((a, b) => a + b, 0);
};

/***/ }),

/***/ 6139:
/***/ (function(module) {

"use strict";
module.exports = require("@reduxjs/toolkit");;

/***/ }),

/***/ 1765:
/***/ (function(module) {

"use strict";
module.exports = require("eva-icons");;

/***/ }),

/***/ 6155:
/***/ (function(module) {

"use strict";
module.exports = require("js-cookie");;

/***/ }),

/***/ 3804:
/***/ (function(module) {

"use strict";
module.exports = require("lodash");;

/***/ }),

/***/ 2470:
/***/ (function(module) {

"use strict";
module.exports = require("moment");;

/***/ }),

/***/ 6731:
/***/ (function(module) {

"use strict";
module.exports = require("next/router");;

/***/ }),

/***/ 9297:
/***/ (function(module) {

"use strict";
module.exports = require("react");;

/***/ }),

/***/ 79:
/***/ (function(module) {

"use strict";
module.exports = require("react-redux");;

/***/ }),

/***/ 2034:
/***/ (function(module) {

"use strict";
module.exports = require("react-toastify");;

/***/ }),

/***/ 5282:
/***/ (function(module) {

"use strict";
module.exports = require("react/jsx-runtime");;

/***/ }),

/***/ 7405:
/***/ (function(module) {

"use strict";
module.exports = require("store");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = function(moduleId) { return __webpack_require__(__webpack_require__.s = moduleId); }
var __webpack_exports__ = (__webpack_exec__(7772));
module.exports = __webpack_exports__;

})();