(function() {
var exports = {};
exports.id = 888;
exports.ids = [888];
exports.modules = {

/***/ 5413:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6731);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _IconEva__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(8656);






const ShowPDF = ({
  urlPDF
}) => {
  const handleCloseModal = () => {
    window.closeModal();
  };

  const router = (0,next_router__WEBPACK_IMPORTED_MODULE_1__.useRouter)();
  const renderLinkPdf = (0,react__WEBPACK_IMPORTED_MODULE_2__.useMemo)(() => {
    if (urlPDF) return {
      linkPdf: urlPDF
    };
    let linkPdf, src;

    switch (router.pathname) {
      case "/keno":
        linkPdf = "139.180.219.58/the-le-keno.pdf";
        break;

      case "/power":
        linkPdf = "139.180.219.58/the-le-power-655.pdf";
        break;

      case "/mega":
        linkPdf = "139.180.219.58/mega-645.pdf";
        break;

      case "/together":
        linkPdf = "139.180.219.58/3mien.pdf";
        break;

      case "/auth/register":
        linkPdf = "139.180.219.58/dieu_khoan_su_dung.pdf";
        break;
    }

    return {
      linkPdf
    };
  });
  const renderPdf = (0,react__WEBPACK_IMPORTED_MODULE_2__.useMemo)(() => {
    return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("object", {
      className: "w-full h-full",
      data: renderLinkPdf.linkPdf,
      type: "application/pdf",
      children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("iframe", {
        className: "w-full h-full",
        src: `https://docs.google.com/viewer?url=${renderLinkPdf.linkPdf}&embedded=true`
      })
    });
  }, [router, urlPDF]);
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    style: {
      height: "60vh"
    },
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
      style: {
        height: "90%",
        marginTop: "5%"
      },
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        className: "text-2xl mb-1 font-medium",
        children: "C\u01A1 c\u1EA5u v\xE0 \u0111i\u1EC1u kho\u1EA3n"
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        className: "w-full h-full rounded-xl",
        children: renderPdf
      })]
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("label", {
      className: "absolute top-1 right-1 cursor-pointer",
      onClick: handleCloseModal,
      children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_IconEva__WEBPACK_IMPORTED_MODULE_3__/* .default */ .Z, {
        name: "close-outline",
        width: "20px",
        height: "20px"
      })
    })]
  });
};

/* harmony default export */ __webpack_exports__["Z"] = (/*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default().memo(ShowPDF));

/***/ }),

/***/ 5162:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3804);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6731);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _ContentModal_ShowPDF__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(5413);
/* harmony import */ var _IconEva__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(8656);








const Header = ({
  sizeIcon = "30px",
  rightView,
  toggleModal,
  className,
  isModal,
  iconRightView
}) => {
  const router = (0,next_router__WEBPACK_IMPORTED_MODULE_2__.useRouter)();
  const renderText = (0,react__WEBPACK_IMPORTED_MODULE_3__.useMemo)(() => {
    let title = "keno";
    let info = "Cơ cấu";

    switch (router.pathname) {
      case "/keno":
        title = "keno";
        break;

      case "/power":
        title = "power";
        break;

      case "/mega":
        title = "mega";
        break;

      case "/together":
        title = "Quay lại";
        info = "Cơ cấu";
        break;
    }

    return {
      title,
      info
    };
  });

  const handleShowPdf = () => {
    return window.openModal({
      content: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_ContentModal_ShowPDF__WEBPACK_IMPORTED_MODULE_4__/* .default */ .Z, {}),
      isShowHeader: false
    });
  };

  const handleActions = () => {
    if (isModal) {
      return window.closeModal();
    }

    if (toggleModal) {
      return toggleModal();
    }

    return router.back();
  };

  const renderRightView = (0,react__WEBPACK_IMPORTED_MODULE_3__.useMemo)(() => {
    if (rightView) {
      return rightView;
    }

    return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react__WEBPACK_IMPORTED_MODULE_3__.Fragment, {
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        className: "flex justify-center items-center text-2xl",
        onClick: handleShowPdf,
        children: renderText.info
      }), iconRightView ? iconRightView : /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        onClick: handleShowPdf,
        className: " flex justify-center items-center mr-1 ml-0.5",
        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_IconEva__WEBPACK_IMPORTED_MODULE_5__/* .default */ .Z, {
          name: "award-outline",
          width: sizeIcon,
          height: sizeIcon
        })
      })]
    });
  }, [rightView]);
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    className: `w-full bg-white flex justify-between sticky top-0 h-20 ${className} `,
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
      className: "flex ",
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        onClick: handleActions,
        className: " flex justify-center items-center mr-0.5 ml-1",
        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_IconEva__WEBPACK_IMPORTED_MODULE_5__/* .default */ .Z, {
          name: "arrow-back-outline",
          width: sizeIcon,
          height: sizeIcon
        })
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        className: `flex justify-center items-center text-2xl `,
        children: (0,lodash__WEBPACK_IMPORTED_MODULE_1__.startCase)(renderText.title)
      })]
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
      className: "flex",
      children: renderRightView
    })]
  });
};

/* harmony default export */ __webpack_exports__["Z"] = (Header);

/***/ }),

/***/ 8656:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var eva_icons__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1765);
/* harmony import */ var eva_icons__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(eva_icons__WEBPACK_IMPORTED_MODULE_2__);




const IconEva = ({
  name,
  color,
  height,
  width
}) => {
  (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(() => {
    eva_icons__WEBPACK_IMPORTED_MODULE_2__.replace();
  }, []);
  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
    children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("i", {
      "data-eva": name,
      "data-eva-fill": color,
      "data-eva-height": height,
      "data-eva-width": width
    })
  });
};

/* harmony default export */ __webpack_exports__["Z"] = (IconEva);

/***/ }),

/***/ 9900:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": function() { return /* binding */ BaseApi; }
});

;// CONCATENATED MODULE: external "axios"
var external_axios_namespaceObject = require("axios");;
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_namespaceObject);
// EXTERNAL MODULE: external "js-cookie"
var external_js_cookie_ = __webpack_require__(6155);
var external_js_cookie_default = /*#__PURE__*/__webpack_require__.n(external_js_cookie_);
// EXTERNAL MODULE: external "store"
var external_store_ = __webpack_require__(7405);
var external_store_default = /*#__PURE__*/__webpack_require__.n(external_store_);
// EXTERNAL MODULE: ./store/user.js
var user = __webpack_require__(5175);
// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(6731);
var router_default = /*#__PURE__*/__webpack_require__.n(router_);
// EXTERNAL MODULE: external "lodash"
var external_lodash_ = __webpack_require__(3804);
;// CONCATENATED MODULE: ./config/BaseApi.js






const BaseAPI = external_axios_default().create({
  baseURL: "https://apis.kenoclub.net",
  timeout: 12000
});
BaseAPI.interceptors.response.use(config => {
  const token = external_js_cookie_default().get("token");
  const authDevice = "d04f817cf80fb5cad2eb936fa2aba25ae6d1a77dab4e5bf4662099072cd9f6d1";
  if (!!token) config.headers.Authorization = `Bearer ${token}`;
  config.headers.authdevice = authDevice;
  const {
    status
  } = config.data;

  if (status) {
    return config.data;
  }

  return config;
}, error => {
  if ((0,external_lodash_.get)(error, "response.status", "") === 401) {
    external_store_default().dispatch((0,user/* logOut */.ni)());
    router_default().push("/auth/login");
  }

  Promise.reject((0,external_lodash_.get)(error, "response.data", {}));
});
BaseAPI.interceptors.request.use(config => {
  const token = external_js_cookie_default().get("token");
  const authDevice = "d04f817cf80fb5cad2eb936fa2aba25ae6d1a77dab4e5bf4662099072cd9f6d1";
  if (!!token) config.headers.Authorization = `Bearer ${token}`;
  config.headers.authdevice = authDevice;
  return config;
}, err => Promise.reject(err));
/* harmony default export */ var BaseApi = (BaseAPI);

/***/ }),

/***/ 8089:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": function() { return /* binding */ socket_client; }
});

;// CONCATENATED MODULE: external "socket.io-client"
var external_socket_io_client_namespaceObject = require("socket.io-client");;
var external_socket_io_client_default = /*#__PURE__*/__webpack_require__.n(external_socket_io_client_namespaceObject);
// EXTERNAL MODULE: ./store/index.js + 3 modules
var store = __webpack_require__(4279);
// EXTERNAL MODULE: ./store/lottery.js
var lottery = __webpack_require__(2360);
// EXTERNAL MODULE: ./store/user.js
var user = __webpack_require__(5175);
// EXTERNAL MODULE: ./config/BaseApi.js + 1 modules
var BaseApi = __webpack_require__(9900);
;// CONCATENATED MODULE: ./config/socket-client.js
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }







class SocketServices {
  constructor() {
    _defineProperty(this, "socket", void 0);
  }

  static initSocket(token) {
    const sk = external_socket_io_client_default()(`${"https://socket.kenoclub.net"}`, {
      query: {
        token: `${token}`
      }
    });

    const updateInfoUser = async () => {
      const response = await BaseApi/* default.post */.Z.post("/user/me");

      if (response.status) {
        store/* default.dispatch */.Z.dispatch((0,user/* setUser */.av)(response));
      }
    };

    sk.on("connect", () => {
      console.log(`WS Chat Connected`);
    });
    sk.on("GET_ALL_ROOM", msg => {
      store/* default.dispatch */.Z.dispatch((0,lottery/* setRooms */.CF)(msg));
      updateInfoUser();
    });
    sk.on("JOIN_ROOM", msg => {
      sk.emit("GET_ALL_ROOM");
    });
    sk.on("CLOSE_ROOM", msg => {
      sk.emit("GET_ALL_ROOM");
    });
    sk.on("NEW_ROOM", msg => {
      sk.emit("GET_ALL_ROOM");
    });
    sk.on("LEAVE_ROOM", msg => {
      sk.emit("GET_ALL_ROOM");
    });
    sk.on("JOIN_ROOM_SUCCESS", msg => {
      sk.emit("GET_ALL_ROOM");
    });
    sk.on("ERROR", msg => {
      const data = JSON.parse(msg);
      console.log("error", data.msg); //   Alert.alert("Lỗi", data.msg);
    });
    sk.on("disconnect", () => {
      console.log("disconnect socket");
    });
    this.socket = sk;
  }

  static getSocketInstance() {
    return this.socket;
  }

}

/* harmony default export */ var socket_client = (SocketServices);

/***/ }),

/***/ 6734:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": function() { return /* binding */ _app; }
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(5282);
// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(6731);
var router_default = /*#__PURE__*/__webpack_require__.n(router_);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(9297);
// EXTERNAL MODULE: external "react-redux"
var external_react_redux_ = __webpack_require__(79);
// EXTERNAL MODULE: external "react-toastify"
var external_react_toastify_ = __webpack_require__(2034);
;// CONCATENATED MODULE: external "redux-persist/integration/react"
var react_namespaceObject = require("redux-persist/integration/react");;
// EXTERNAL MODULE: external "lodash"
var external_lodash_ = __webpack_require__(3804);
;// CONCATENATED MODULE: external "cookie"
var external_cookie_namespaceObject = require("cookie");;
var external_cookie_default = /*#__PURE__*/__webpack_require__.n(external_cookie_namespaceObject);
;// CONCATENATED MODULE: ./utils/parseCookies.js

const parseCookies = req => {
  return external_cookie_default().parse(req ? req.headers.cookie || "" : document.cookie);
};
// EXTERNAL MODULE: ./config/socket-client.js + 1 modules
var socket_client = __webpack_require__(8089);
// EXTERNAL MODULE: external "store"
var external_store_ = __webpack_require__(7405);
var external_store_default = /*#__PURE__*/__webpack_require__.n(external_store_);
// EXTERNAL MODULE: ./store/lottery.js
var lottery = __webpack_require__(2360);
// EXTERNAL MODULE: ./config/BaseApi.js + 1 modules
var BaseApi = __webpack_require__(9900);
// EXTERNAL MODULE: external "moment"
var external_moment_ = __webpack_require__(2470);
var external_moment_default = /*#__PURE__*/__webpack_require__.n(external_moment_);
;// CONCATENATED MODULE: ./config/kenoServices.js
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }






class KenoServices {
  //
  constructor() {
    this.result = null;
    this.duration = null;
    this.timer = null;
  }

  static async init() {
    const response = await BaseApi/* default.get */.Z.get("keno/current");
    const {
      status,
      data
    } = response;

    if (status) {
      clearInterval(this.timer);
      this.result = data;
      const nowDate = external_moment_default()(data.result_date);
      nowDate.seconds(0);
      nowDate.milliseconds(0);
      const d = nowDate.diff(external_moment_default()(), "seconds");
      this.duration = d;
      data.duration = d;
      external_store_default().dispatch((0,lottery/* setDataKeno */.HL)(data));
      this.intervalUpdate();
    }
  }

  static async intervalUpdate() {
    this.timer = setTimeout(() => {
      if (this.duration > 60) {
        this.duration = this.duration - 1;
        external_store_default().dispatch((0,lottery/* setDataKeno */.HL)(_objectSpread(_objectSpread({}, this.result), {}, {
          duration: this.duration
        })));
        this.intervalUpdate();
      } else {
        if (this.duration > 0) {
          this.init();
        }
      }
    }, 1000);
  }

}

/* harmony default export */ var kenoServices = (KenoServices);
// EXTERNAL MODULE: ./node_modules/react-toastify/dist/ReactToastify.css
var ReactToastify = __webpack_require__(8819);
// EXTERNAL MODULE: ./store/index.js + 3 modules
var store = __webpack_require__(4279);
;// CONCATENATED MODULE: external "next/head"
var head_namespaceObject = require("next/head");;
var head_default = /*#__PURE__*/__webpack_require__.n(head_namespaceObject);
// EXTERNAL MODULE: external "react-lottie"
var external_react_lottie_ = __webpack_require__(9102);
var external_react_lottie_default = /*#__PURE__*/__webpack_require__.n(external_react_lottie_);
;// CONCATENATED MODULE: ./public/images/lotties/preloader.json
var preloader_namespaceObject = JSON.parse('{"v":"5.5.7","meta":{"g":"LottieFiles AE 0.1.20","a":"","k":"","d":"","tc":""},"fr":29.9700012207031,"ip":0,"op":120.0000048877,"w":200,"h":100,"nm":"spot-random","ddd":0,"assets":[],"layers":[{"ddd":0,"ind":1,"ty":4,"nm":"3","sr":1,"ks":{"o":{"a":1,"k":[{"i":{"x":[0.833],"y":[0.833]},"o":{"x":[0.167],"y":[0.167]},"t":60,"s":[100]},{"t":70.0000028511585,"s":[0]}],"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":1,"k":[{"i":{"x":0.196,"y":1},"o":{"x":0.167,"y":0.167},"t":60,"s":[124.586,61.587,0],"to":[2.5,-1.708,0],"ti":[-11.167,1.958,0]},{"t":70.0000028511585,"s":[139.586,51.337,0]}],"ix":2},"a":{"a":0,"k":[2.479,2.479,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,-1.231],[1.231,0],[0,1.23],[-1.23,0]],"o":[[0,1.23],[-1.23,0],[0,-1.231],[1.231,0]],"v":[[2.229,0.001],[-0.001,2.229],[-2.229,0.001],[-0.001,-2.229]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[1,0.905999995213,0.231000010173,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"bm":0,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[2.479,2.478],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 1","np":2,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false}],"ip":60.0000024438501,"op":120.0000048877,"st":0,"bm":0},{"ddd":0,"ind":2,"ty":4,"nm":"2","sr":1,"ks":{"o":{"a":1,"k":[{"i":{"x":[0.833],"y":[0.833]},"o":{"x":[0.167],"y":[0.167]},"t":60,"s":[100]},{"t":70.0000028511585,"s":[0]}],"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":1,"k":[{"i":{"x":0.209,"y":1},"o":{"x":0.167,"y":0.167},"t":60,"s":[73.614,51.155,0],"to":[-2.083,-1.292,0],"ti":[7.583,2.542,0]},{"t":70.0000028511585,"s":[61.114,43.405,0]}],"ix":2},"a":{"a":0,"k":[3.107,3.107,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0.276,-0.478],[0,0],[0.478,0.276],[0,0],[-0.276,0.478],[0,0],[-0.479,-0.276],[0,0]],"o":[[0,0],[-0.276,0.478],[0,0],[-0.478,-0.276],[0,0],[0.276,-0.478],[0,0],[0.478,0.276]],"v":[[2.581,0.04],[1.326,2.215],[-0.04,2.581],[-2.215,1.326],[-2.581,-0.04],[-1.326,-2.214],[0.04,-2.581],[2.215,-1.326]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0.344999994016,0.779999976065,0.779999976065,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"bm":0,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[3.107,3.107],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 1","np":2,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false}],"ip":60.0000024438501,"op":120.0000048877,"st":0,"bm":0},{"ddd":0,"ind":3,"ty":4,"nm":"1","sr":1,"ks":{"o":{"a":1,"k":[{"i":{"x":[0.833],"y":[0.833]},"o":{"x":[0.167],"y":[0.167]},"t":60,"s":[100]},{"t":70.0000028511585,"s":[0]}],"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":1,"k":[{"i":{"x":0.183,"y":1},"o":{"x":0.167,"y":0.167},"t":60,"s":[71.139,61.578,0],"to":[-3.125,-0.417,0],"ti":[9.125,-0.833,0]},{"t":70.0000028511585,"s":[52.389,59.078,0]}],"ix":2},"a":{"a":0,"k":[3.062,3.138,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0.733,0.235],[0,0],[-0.571,0.516],[0,0],[-0.162,-0.753],[0,0]],"o":[[0,0],[-0.733,-0.236],[0,0],[0.571,-0.517],[0,0],[0.162,0.753]],"v":[[1.367,2.654],[-1.878,1.609],[-2.242,-0.085],[0.285,-2.372],[1.934,-1.84],[2.65,1.491]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0.243000000598,0.352999997606,1,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"bm":0,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[3.062,3.138],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 1","np":2,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false}],"ip":60.0000024438501,"op":120.0000048877,"st":0,"bm":0},{"ddd":0,"ind":4,"ty":4,"nm":"4 yellow Outlines","sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":1,"k":[{"i":{"x":[0.558],"y":[1]},"o":{"x":[0.167],"y":[0.167]},"t":60,"s":[45]},{"t":100.000004073084,"s":[0]}],"ix":10},"p":{"a":1,"k":[{"i":{"x":0,"y":1},"o":{"x":0.167,"y":0.167},"t":60,"s":[84.521,75.128,0],"to":[-0.125,-4.183,0],"ti":[3.989,1.318,0]},{"t":88.0000035843135,"s":[80.021,64.279,0]}],"ix":2},"a":{"a":0,"k":[12.684,12.684,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[2.27,-0.826],[0,0],[0.826,2.269],[0,0],[-2.27,0.826],[0,0],[-0.826,-2.27],[0,0]],"o":[[0,0],[-2.269,0.826],[0,0],[-0.826,-2.271],[0,0],[2.269,-0.826],[0,0],[0.826,2.269]],"v":[[8.994,7.607],[-2.001,11.607],[-7.606,8.995],[-11.608,-1.999],[-8.993,-7.605],[2.001,-11.607],[7.606,-8.993],[11.608,2.001]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[1,0.905999995213,0.231000010173,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"bm":0,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[12.684,12.683],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 1","np":2,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false}],"ip":60.0000024438501,"op":120.0000048877,"st":0,"bm":0},{"ddd":0,"ind":5,"ty":4,"nm":"matte","parent":8,"td":1,"sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[20.979,41.708,0],"ix":2},"a":{"a":0,"k":[20.979,41.708,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,-11.449],[11.449,0],[0,11.448],[-11.448,0]],"o":[[0,11.448],[-11.448,0],[0,-11.449],[11.449,0]],"v":[[20.729,0],[0,20.729],[-20.729,0],[0,-20.729]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0.243000000598,0.352999997606,1,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"bm":0,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[20.979,20.979],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 1","np":2,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false}],"ip":60.0000024438501,"op":120.0000048877,"st":0,"bm":0},{"ddd":0,"ind":6,"ty":4,"nm":"4 blue shadow","parent":4,"tt":1,"sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[24.036,14.836,0],"ix":2},"a":{"a":0,"k":[19.972,12.425,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[-20.617,-1.642],[0,0],[0.826,2.27],[0,0],[-2.269,0.826],[0,0]],"o":[[8.658,0.69],[-17.084,2.114],[0,0],[-0.826,-2.269],[0,0],[0,0]],"v":[[21.619,-1.914],[10.712,18.455],[-14.895,8.734],[-18.896,-2.26],[-16.283,-7.865],[-4.291,-12.176]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0.161000001197,0.13300000359,0.666999966491,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"bm":0,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[19.971,12.426],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 1","np":2,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false}],"ip":60.0000024438501,"op":120.0000048877,"st":0,"bm":0},{"ddd":0,"ind":7,"ty":4,"nm":"4 blue face","parent":8,"sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":1,"k":[{"i":{"x":0,"y":1},"o":{"x":0,"y":0},"t":60,"s":[16.561,13.983,0],"to":[1.652,0.472,0],"ti":[-1.652,-0.472,0]},{"t":100.000004073084,"s":[26.471,16.818,0]}],"ix":2},"a":{"a":0,"k":[7.889,5.543,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[-0.653,-0.157],[-2.465,0.033],[0.029,0.655],[0.672,-0.01],[2.249,0.542],[0.147,-0.646]],"o":[[2.396,0.577],[0.626,-0.009],[-0.03,-0.643],[-2.364,0.031],[-0.609,-0.147],[-0.141,0.617]],"v":[[-3.792,0.8],[3.542,1.628],[4.742,0.427],[3.542,-0.772],[-3.154,-1.514],[-4.63,-0.676]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0.161000001197,0.13300000359,0.666999966491,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"bm":0,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[7.265,9.175],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 1","np":2,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,-1.142],[1.142,0],[0,1.143],[-1.142,0]],"o":[[0,1.143],[-1.142,0],[0,-1.142],[1.142,0]],"v":[[2.069,-0.001],[0,2.068],[-2.069,-0.001],[0,-2.068]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0.161000001197,0.13300000359,0.666999966491,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"bm":0,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[13.459,3.483],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 2","np":2,"cix":2,"bm":0,"ix":2,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,-1.143],[1.142,0],[0,1.143],[-1.143,0]],"o":[[0,1.143],[-1.143,0],[0,-1.143],[1.142,0]],"v":[[2.068,0.001],[0.001,2.068],[-2.068,0.001],[0.001,-2.068]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0.161000001197,0.13300000359,0.666999966491,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"bm":0,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[2.319,2.319],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 3","np":2,"cix":2,"bm":0,"ix":3,"mn":"ADBE Vector Group","hd":false}],"ip":60.0000024438501,"op":120.0000048877,"st":0,"bm":0},{"ddd":0,"ind":8,"ty":4,"nm":"4 blue","sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":1,"k":[{"i":{"x":[0.498],"y":[1]},"o":{"x":[0.012],"y":[0]},"t":60,"s":[-30]},{"t":100.000004073084,"s":[0]}],"ix":10},"p":{"a":1,"k":[{"i":{"x":0,"y":1},"o":{"x":0.032,"y":0.026},"t":60,"s":[97.339,66.271,0],"to":[0,-3.884,0],"ti":[0,3.884,0]},{"t":80.0000032584668,"s":[97.339,42.967,0]}],"ix":2},"a":{"a":0,"k":[20.979,20.979,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,-11.449],[11.449,0],[0,11.448],[-11.448,0]],"o":[[0,11.448],[-11.448,0],[0,-11.449],[11.449,0]],"v":[[20.729,0],[0,20.729],[-20.729,0],[0,-20.729]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0.243000000598,0.352999997606,1,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"bm":0,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[20.979,20.979],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 1","np":2,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false}],"ip":60.0000024438501,"op":120.0000048877,"st":0,"bm":0},{"ddd":0,"ind":9,"ty":4,"nm":"4 mint","sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":1,"k":[{"i":{"x":[0.152],"y":[1]},"o":{"x":[0.167],"y":[0.167]},"t":60,"s":[-45]},{"t":100.000004073084,"s":[0]}],"ix":10},"p":{"a":1,"k":[{"i":{"x":0.667,"y":1},"o":{"x":0.167,"y":0.167},"t":60,"s":[110.053,75.761,0],"to":[2.792,-4.436,0],"ti":[-3.917,2.686,0]},{"t":88.0000035843135,"s":[120.803,64.897,0]}],"ix":2},"a":{"a":0,"k":[11.64,11.948,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[1.802,0.58],[0,0],[-1.405,1.271],[0,0],[-0.398,-1.851],[0,0]],"o":[[0,0],[-1.803,-0.581],[0,0],[1.405,-1.271],[0,0],[0.399,1.851]],"v":[[7.833,11.118],[-9.09,5.668],[-9.986,1.502],[3.195,-10.427],[7.251,-9.121],[10.992,8.259]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0.344999994016,0.779999976065,0.779999976065,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"bm":0,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[11.64,11.948],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 1","np":2,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false}],"ip":60.0000024438501,"op":120.0000048877,"st":0,"bm":0},{"ddd":0,"ind":10,"ty":4,"nm":"4 shadow","sr":1,"ks":{"o":{"a":1,"k":[{"i":{"x":[0.246],"y":[1]},"o":{"x":[0.167],"y":[0.167]},"t":60,"s":[20]},{"t":75.0000030548126,"s":[10]}],"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[100,86.889,0],"ix":2},"a":{"a":0,"k":[27.788,3.473,0],"ix":1},"s":{"a":1,"k":[{"i":{"x":[0.64,0.64,0.667],"y":[1,1,1]},"o":{"x":[0.167,0.167,0.167],"y":[0.167,0.167,0]},"t":60,"s":[100,100,100]},{"t":75.0000030548126,"s":[80,80,100]}],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,-1.918],[15.347,0],[0,1.918],[-15.347,0]],"o":[[0,1.918],[-15.347,0],[0,-1.918],[15.347,0]],"v":[[27.788,0],[0,3.474],[-27.788,0],[0,-3.473]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0.097999999102,0.122000002394,0.156999999402,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"bm":0,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[27.788,3.473],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 1","np":2,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false}],"ip":60.0000024438501,"op":120.0000048877,"st":0,"bm":0},{"ddd":0,"ind":11,"ty":4,"nm":"3 mint Outlines","sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":1,"k":[{"i":{"x":[0.246],"y":[1]},"o":{"x":[0.167],"y":[0.167]},"t":40,"s":[-90]},{"i":{"x":[0.833],"y":[0.833]},"o":{"x":[0.786],"y":[0]},"t":50,"s":[0]},{"t":60.0000024438501,"s":[60]}],"ix":10},"p":{"s":true,"x":{"a":1,"k":[{"i":{"x":[0.667],"y":[1]},"o":{"x":[0.167],"y":[0.167]},"t":40,"s":[99.996]},{"t":50.0000020365418,"s":[103.996]}],"ix":3},"y":{"a":1,"k":[{"i":{"x":[0.18],"y":[1]},"o":{"x":[0.167],"y":[0.167]},"t":40,"s":[71.221]},{"i":{"x":[0.833],"y":[0.833]},"o":{"x":[0.733],"y":[0]},"t":50,"s":[42.596]},{"t":60.0000024438501,"s":[72.346]}],"ix":4}},"a":{"a":0,"k":[16.292,17.406,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[2.852,-1.161],[0,0],[-0.421,3.051],[0,0],[-2.431,-1.889],[0,0]],"o":[[0,0],[-2.853,1.16],[0,0],[0.42,-3.05],[0,0],[2.432,1.889]],"v":[[12.663,6.711],[-10.15,15.996],[-15.62,11.744],[-12.255,-12.655],[-5.837,-15.267],[13.609,-0.152]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0.344999994016,0.779999976065,0.779999976065,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"bm":0,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[16.292,17.406],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 1","np":2,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false}],"ip":40.0000016292334,"op":60.0000024438501,"st":-20.0000008146167,"bm":0},{"ddd":0,"ind":12,"ty":4,"nm":"2 yellow Outlines","sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":1,"k":[{"i":{"x":[0.62],"y":[1]},"o":{"x":[0.167],"y":[0.167]},"t":20,"s":[-60]},{"i":{"x":[0.833],"y":[0.833]},"o":{"x":[0.413],"y":[0]},"t":30,"s":[0]},{"t":40.0000016292334,"s":[90]}],"ix":10},"p":{"s":true,"x":{"a":0,"k":100.899,"ix":3},"y":{"a":1,"k":[{"i":{"x":[0.325],"y":[1]},"o":{"x":[0.167],"y":[0.167]},"t":20,"s":[69.201]},{"i":{"x":[0.833],"y":[0.833]},"o":{"x":[0.671],"y":[0]},"t":30,"s":[42.565]},{"t":40.0000016292334,"s":[70.826]}],"ix":4}},"a":{"a":0,"k":[18.2,18.201,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[4.151,1.511],[0,0],[-1.512,4.153],[0,0],[-4.151,-1.512],[0,0],[1.51,-4.151],[0,0]],"o":[[0,0],[-4.152,-1.51],[0,0],[1.511,-4.152],[0,0],[4.152,1.511],[0,0],[-1.511,4.152]],"v":[[1.425,16.439],[-11.657,11.678],[-16.438,1.423],[-11.677,-11.658],[-1.424,-16.438],[11.658,-11.678],[16.44,-1.424],[11.678,11.658]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[1,0.905999995213,0.231000010173,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"bm":0,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[18.2,18.201],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 1","np":2,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false}],"ip":20.0000008146167,"op":40.0000016292334,"st":-10.0000004073083,"bm":0},{"ddd":0,"ind":13,"ty":4,"nm":"1 blue Outlines","sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"s":true,"x":{"a":0,"k":99.968,"ix":3},"y":{"a":1,"k":[{"i":{"x":[0.329],"y":[1]},"o":{"x":[0.167],"y":[0.167]},"t":0,"s":[87]},{"i":{"x":[0.833],"y":[0.833]},"o":{"x":[0.663],"y":[0]},"t":10,"s":[58.527]},{"t":20.0000008146167,"s":[87]}],"ix":4}},"a":{"a":0,"k":[17.122,33.995,0],"ix":1},"s":{"a":1,"k":[{"i":{"x":[0.833,0.833,0.833],"y":[0.833,0.833,0.833]},"o":{"x":[0.167,0.167,0.167],"y":[0.167,0.167,0.167]},"t":18,"s":[100,100,100]},{"t":20.0000008146167,"s":[100,90,100]}],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,-9.318],[9.318,0],[0,9.318],[-9.318,0]],"o":[[0,9.318],[-9.318,0],[0,-9.318],[9.318,0]],"v":[[16.872,0.001],[-0.001,16.872],[-16.872,0.001],[-0.001,-16.872]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0.243000000598,0.352999997606,1,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"bm":0,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[17.123,17.123],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 1","np":2,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false}],"ip":0,"op":20.0000008146167,"st":0,"bm":0},{"ddd":0,"ind":14,"ty":4,"nm":"shadows Outlines","sr":1,"ks":{"o":{"a":1,"k":[{"i":{"x":[0.667],"y":[1]},"o":{"x":[0.167],"y":[0.167]},"t":0,"s":[20]},{"i":{"x":[0.833],"y":[0.833]},"o":{"x":[0.333],"y":[0]},"t":10,"s":[10]},{"i":{"x":[0.667],"y":[1]},"o":{"x":[0.167],"y":[0.167]},"t":20,"s":[20]},{"i":{"x":[0.833],"y":[0.833]},"o":{"x":[0.333],"y":[0]},"t":30,"s":[10]},{"i":{"x":[0.667],"y":[1]},"o":{"x":[0.167],"y":[0.167]},"t":40,"s":[20]},{"i":{"x":[0.833],"y":[0.833]},"o":{"x":[0.333],"y":[0]},"t":50,"s":[10]},{"t":60.0000024438501,"s":[20]}],"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[99.969,86.889,0],"ix":2},"a":{"a":0,"k":[15.716,3.473,0],"ix":1},"s":{"a":1,"k":[{"i":{"x":[0.64,0.64,0.667],"y":[1,1,1]},"o":{"x":[0.167,0.167,0.167],"y":[0.167,0.167,-1.852]},"t":0,"s":[100,100,100]},{"i":{"x":[0.833,0.833,0.833],"y":[0.833,0.833,-5.019]},"o":{"x":[0.406,0.406,0.333],"y":[0,0,0]},"t":10,"s":[80,80,100]},{"i":{"x":[0.64,0.64,0.667],"y":[1,1,1]},"o":{"x":[0.167,0.167,0.167],"y":[0.167,0.167,-4.167]},"t":20,"s":[100,100,100]},{"i":{"x":[0.833,0.833,0.833],"y":[0.833,0.833,1]},"o":{"x":[0.406,0.406,0.333],"y":[0,0,0]},"t":30,"s":[80,80,100]},{"i":{"x":[0.64,0.64,0.667],"y":[1,1,1]},"o":{"x":[0.167,0.167,0.167],"y":[0.167,0.167,0]},"t":40,"s":[100,100,100]},{"i":{"x":[0.833,0.833,0.833],"y":[0.833,0.833,1]},"o":{"x":[0.406,0.406,0.333],"y":[0,0,0]},"t":50,"s":[80,80,100]},{"t":60.0000024438501,"s":[100,100,100]}],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,-1.918],[8.68,0],[0,1.918],[-8.68,0]],"o":[[0,1.918],[-8.68,0],[0,-1.918],[8.68,0]],"v":[[15.716,0],[0,3.474],[-15.717,0],[0,-3.473]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0.097999999102,0.122000002394,0.156999999402,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"bm":0,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[15.716,3.473],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 1","np":2,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false}],"ip":0,"op":60.0000024438501,"st":0,"bm":0}],"markers":[]}');
;// CONCATENATED MODULE: ./components/share/common/Preloader.js


function Preloader_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function Preloader_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { Preloader_ownKeys(Object(source), true).forEach(function (key) { Preloader_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { Preloader_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function Preloader_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }





const Preloader = (_ref) => {
  let {
    size = 200
  } = _ref,
      props = _objectWithoutProperties(_ref, ["size"]);

  return /*#__PURE__*/jsx_runtime_.jsx("div", {
    className: "absolute w-screen h-screen flex items-center justify-center z-50 bg-gray-500 bg-opacity-60",
    children: /*#__PURE__*/jsx_runtime_.jsx((external_react_lottie_default()), Preloader_objectSpread({
      height: size,
      width: size,
      options: {
        loop: true,
        autoplay: true,
        animationData: preloader_namespaceObject
      }
    }, props))
  });
};

/* harmony default export */ var common_Preloader = (Preloader);
// EXTERNAL MODULE: ./components/share/common/Header.js
var Header = __webpack_require__(5162);
;// CONCATENATED MODULE: ./components/share/common/ModalGeneral.js





const ModalGeneral = ({
  isOpen,
  content,
  isShowHeader = true,
  rightView,
  txtCancel,
  txtConfirm,
  isShowActions,
  handleConfirm,
  handleCancel
}) => {
  const styleWrapModal = (0,external_react_.useMemo)(() => {
    return isOpen ? {
      opacity: 1,
      pointerEvents: "auto",
      visibility: "visible"
    } : {};
  }, [isOpen]);
  const renderTransition = (0,external_react_.useMemo)(() => {
    return `${isOpen ? "-translate-y-1/2" : "translate-y-0"}`;
  }, [isOpen]);
  return /*#__PURE__*/jsx_runtime_.jsx("div", {
    className: "modal",
    style: styleWrapModal,
    children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: `modal-box bg-white fixed top-1/2 transform rounded-2xl max-h-full	${renderTransition}`,
      children: [isShowHeader ? /*#__PURE__*/jsx_runtime_.jsx(Header/* default */.Z, {
        isModal: true,
        className: "p-0",
        rightView: rightView
      }) : null, /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "p-1",
        children: content
      }), isShowActions && /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: "flex justify-end",
        children: [/*#__PURE__*/jsx_runtime_.jsx("button", {
          onClick: handleCancel,
          class: "mr-1 inline-flex justify-center py-1 px-1 border border-transparent shadow-sm text-sm font-medium rounded-md text-grey hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",
          children: txtCancel
        }), /*#__PURE__*/jsx_runtime_.jsx("button", {
          onClick: handleConfirm,
          class: "inline-flex justify-center py-1 px-1 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",
          children: txtConfirm
        })]
      })]
    })
  });
};

/* harmony default export */ var common_ModalGeneral = (ModalGeneral);
;// CONCATENATED MODULE: ./components/share/common/ModalGlobal.js



function ModalGlobal_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function ModalGlobal_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ModalGlobal_ownKeys(Object(source), true).forEach(function (key) { ModalGlobal_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ModalGlobal_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function ModalGlobal_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }




const ModalGlobal = () => {
  const {
    0: modals,
    1: setModals
  } = (0,external_react_.useState)({});
  (0,external_react_.useEffect)(() => {
    window.openModal = handleOpenModal;
    window.closeModal = closeModal;
  }, []);

  const handleOpenModal = props => {
    setModals(ModalGlobal_objectSpread(ModalGlobal_objectSpread({}, props), {}, {
      isOpen: true
    }));
  };

  const closeModal = () => {
    setModals({
      isOpen: false
    });
  };

  return /*#__PURE__*/jsx_runtime_.jsx(jsx_runtime_.Fragment, {
    children: modals ? /*#__PURE__*/jsx_runtime_.jsx(common_ModalGeneral, ModalGlobal_objectSpread({}, modals)) : null
  });
};

/* harmony default export */ var common_ModalGlobal = (ModalGlobal);
;// CONCATENATED MODULE: ./pages/_app.js



function _app_ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _app_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { _app_ownKeys(Object(source), true).forEach(function (key) { _app_defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { _app_ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _app_defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

















external_react_toastify_.toast.configure({
  autoClose: 1500
});

function MyApp({
  Component,
  pageProps,
  token
}) {
  const router = (0,router_.useRouter)();
  const {
    0: isShowLoader,
    1: setIsShowLoader
  } = (0,external_react_.useState)(false);

  (router_default()).onRouteChangeStart = () => {
    setIsShowLoader(true);
  };

  (router_default()).onRouteChangeComplete = () => {
    setIsShowLoader(false);
  };

  (router_default()).onRouteChangeError = () => {
    setIsShowLoader(false);
  };

  (0,external_react_.useEffect)(() => {
    if (!token) {
      switch (router.pathname) {
        case "/auth/register":
          router.push("/auth/register");
          break;

        case "/auth/restore-pass":
          router.push("/auth/restore-pass");
          break;

        default:
          router.push("/auth/login");
          break;
      }
    }
  }, [router.pathname]);
  (0,external_react_.useEffect)(() => {
    if (!token) return;
    socket_client/* default.initSocket */.Z.initSocket(token);
    kenoServices.init();
  }, [token]);
  return /*#__PURE__*/jsx_runtime_.jsx(external_react_redux_.Provider, {
    store: store/* default */.Z,
    children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(react_namespaceObject.PersistGate, {
      loading: null,
      persistor: store/* persistor */.D,
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)((head_default()), {
        children: [/*#__PURE__*/jsx_runtime_.jsx("title", {
          children: "VietlottClub - C\u01A1 H\u1ED9i \u0110\u1EC3 T\u1ED1t H\u01A1n"
        }), /*#__PURE__*/jsx_runtime_.jsx("meta", {
          name: "viewport",
          content: "width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"
        })]
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: `${isShowLoader ? "overflow-hidden" : ""}`,
        children: [isShowLoader ? /*#__PURE__*/jsx_runtime_.jsx(common_Preloader, {}) : null, /*#__PURE__*/jsx_runtime_.jsx(common_ModalGlobal, {}), /*#__PURE__*/jsx_runtime_.jsx(Component, _app_objectSpread({}, pageProps))]
      })]
    })
  });
}

MyApp.getInitialProps = async ({
  Component,
  ctx
}) => {
  const cookie = parseCookies(ctx.req);
  const pageProps = Component.getInitialProps ? await Component.getInitialProps(ctx) : {};
  return {
    pageProps,
    token: (0,external_lodash_.get)(cookie, "token", null)
  };
};

/* harmony default export */ var _app = (MyApp);

/***/ }),

/***/ 4279:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": function() { return /* binding */ store_0; },
  "D": function() { return /* binding */ persistor; }
});

// EXTERNAL MODULE: external "@reduxjs/toolkit"
var toolkit_ = __webpack_require__(6139);
;// CONCATENATED MODULE: external "redux"
var external_redux_namespaceObject = require("redux");;
;// CONCATENATED MODULE: external "redux-persist"
var external_redux_persist_namespaceObject = require("redux-persist");;
;// CONCATENATED MODULE: external "redux-persist/lib/storage"
var storage_namespaceObject = require("redux-persist/lib/storage");;
var storage_default = /*#__PURE__*/__webpack_require__.n(storage_namespaceObject);
// EXTERNAL MODULE: ./store/lottery.js
var lottery = __webpack_require__(2360);
// EXTERNAL MODULE: ./store/user.js
var user = __webpack_require__(5175);
;// CONCATENATED MODULE: ./store/index.js



 // defaults to localStorage for web



const persistConfig = {
  key: "store",
  storage: (storage_default())
};
const reducer = (0,external_redux_namespaceObject.combineReducers)({
  lottery: lottery/* default */.ZP,
  user: user/* default */.ZP
});
const persistedReducer = (0,external_redux_persist_namespaceObject.persistReducer)(persistConfig, reducer);
const store = (0,toolkit_.configureStore)({
  reducer: persistedReducer
});
const persistor = (0,external_redux_persist_namespaceObject.persistStore)(store);
/* harmony default export */ var store_0 = (store);

/***/ }),

/***/ 2360:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Vg": function() { return /* binding */ addListCopy; },
/* harmony export */   "CF": function() { return /* binding */ setRooms; },
/* harmony export */   "$L": function() { return /* binding */ addSelectLotteryKeno; },
/* harmony export */   "$2": function() { return /* binding */ addSelectPriceKeno; },
/* harmony export */   "HL": function() { return /* binding */ setDataKeno; },
/* harmony export */   "yS": function() { return /* binding */ setDataMega; },
/* harmony export */   "pG": function() { return /* binding */ setDataPower; },
/* harmony export */   "CC": function() { return /* binding */ setLotteryHistory; },
/* harmony export */   "Nf": function() { return /* binding */ addCopyLotteryKeno; }
/* harmony export */ });
/* unused harmony exports lotteryModule, addLottery */
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6139);
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__);

const lotteryModule = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createSlice)({
  name: "lottery",
  initialState: {
    user: {},
    listCopy: [],
    roomShare: {},
    selectLotteryKeno: {},
    selectPriceKeno: {},
    kenoData: {},
    megaData: {},
    powerData: {},
    lotteryHistory: {
      variableTab: "keno"
    },
    copyLotteryKeno: {}
  },
  reducers: {
    addListCopy: (state, action) => {
      state.listCopy = action.payload;
    },
    addSelectLotteryKeno: (state, action) => {
      state.selectLotteryKeno = action.payload;
    },
    addCopyLotteryKeno: (state, action) => {
      state.copyLotteryKeno = action.payload;
    },
    addSelectPriceKeno: (state, action) => {
      state.selectPriceKeno = action.payload;
    },
    addLottery: (state, action) => {
      state.infoLottery.push(action.payload);
    },
    setRooms: (state, action) => {
      state.roomShare = action.payload;
    },
    setDataKeno: (state, action) => {
      state.kenoData = action.payload;
    },
    setDataMega: (state, action) => {
      state.megaData = action.payload;
    },
    setDataPower: (state, action) => {
      state.powerData = action.payload;
    },
    setLotteryHistory: (state, action) => {
      state.lotteryHistory = action.payload;
    }
  }
}); // Action creators are generated for each case reducer function

const {
  addListCopy,
  addLottery,
  setRooms,
  addSelectLotteryKeno,
  addSelectPriceKeno,
  setDataKeno,
  setDataMega,
  setDataPower,
  setLotteryHistory,
  addCopyLotteryKeno
} = lotteryModule.actions;
/* harmony default export */ __webpack_exports__["ZP"] = (lotteryModule.reducer);

/***/ }),

/***/ 5175:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "av": function() { return /* binding */ setUser; },
/* harmony export */   "ni": function() { return /* binding */ logOut; }
/* harmony export */ });
/* unused harmony export userModule */
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6139);
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6155);
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(js_cookie__WEBPACK_IMPORTED_MODULE_1__);


const userModule = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createSlice)({
  name: "user",
  initialState: {
    user: {}
  },
  reducers: {
    setUser: (state, action) => {
      const {
        token,
        user
      } = action.payload;
      state.user = user;

      if (token) {
        js_cookie__WEBPACK_IMPORTED_MODULE_1___default().set("token", token);
      }
    },
    logOut: () => {
      js_cookie__WEBPACK_IMPORTED_MODULE_1___default().remove("token");
    }
  }
}); // Action creators are generated for each case reducer function

const {
  setUser,
  logOut
} = userModule.actions;
/* harmony default export */ __webpack_exports__["ZP"] = (userModule.reducer);

/***/ }),

/***/ 8819:
/***/ (function() {



/***/ }),

/***/ 6139:
/***/ (function(module) {

"use strict";
module.exports = require("@reduxjs/toolkit");;

/***/ }),

/***/ 1765:
/***/ (function(module) {

"use strict";
module.exports = require("eva-icons");;

/***/ }),

/***/ 6155:
/***/ (function(module) {

"use strict";
module.exports = require("js-cookie");;

/***/ }),

/***/ 3804:
/***/ (function(module) {

"use strict";
module.exports = require("lodash");;

/***/ }),

/***/ 2470:
/***/ (function(module) {

"use strict";
module.exports = require("moment");;

/***/ }),

/***/ 6731:
/***/ (function(module) {

"use strict";
module.exports = require("next/router");;

/***/ }),

/***/ 9297:
/***/ (function(module) {

"use strict";
module.exports = require("react");;

/***/ }),

/***/ 9102:
/***/ (function(module) {

"use strict";
module.exports = require("react-lottie");;

/***/ }),

/***/ 79:
/***/ (function(module) {

"use strict";
module.exports = require("react-redux");;

/***/ }),

/***/ 2034:
/***/ (function(module) {

"use strict";
module.exports = require("react-toastify");;

/***/ }),

/***/ 5282:
/***/ (function(module) {

"use strict";
module.exports = require("react/jsx-runtime");;

/***/ }),

/***/ 7405:
/***/ (function(module) {

"use strict";
module.exports = require("store");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = function(moduleId) { return __webpack_require__(__webpack_require__.s = moduleId); }
var __webpack_exports__ = (__webpack_exec__(6734));
module.exports = __webpack_exports__;

})();