(function() {
var exports = {};
exports.id = 445;
exports.ids = [445];
exports.modules = {

/***/ 2268:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }



const Button = (_ref) => {
  let {
    children,
    isLoading,
    disabled,
    buttonColor = "btn-info",
    colorText = "text-white",
    className
  } = _ref,
      props = _objectWithoutProperties(_ref, ["children", "isLoading", "disabled", "buttonColor", "colorText", "className"]);

  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", _objectSpread(_objectSpread({
    className: `btn rounded border-0 normal-case text-lg ${colorText} ${buttonColor} ${isLoading ? "loading" : ""} ${className}`,
    disabled: disabled
  }, props), {}, {
    children: children
  }));
};

/* harmony default export */ __webpack_exports__["Z"] = (Button);

/***/ }),

/***/ 9566:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var components_share_common_Header__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5162);





const Container = ({
  children,
  renderRightView,
  iconRightView
}) => {
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    className: "bg-gray-100 relative",
    children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(components_share_common_Header__WEBPACK_IMPORTED_MODULE_2__/* .default */ .Z, {
      rightView: renderRightView,
      iconRightView: iconRightView
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
      className: "container mx-auto flex flex-col items-center pt-2 px-1.5",
      style: {
        maxHeight: "95vh"
      },
      children: children
    })]
  });
};

/* harmony default export */ __webpack_exports__["Z"] = (/*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default().memo(Container));

/***/ }),

/***/ 5413:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6731);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _IconEva__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(8656);






const ShowPDF = ({
  urlPDF
}) => {
  const handleCloseModal = () => {
    window.closeModal();
  };

  const router = (0,next_router__WEBPACK_IMPORTED_MODULE_1__.useRouter)();
  const renderLinkPdf = (0,react__WEBPACK_IMPORTED_MODULE_2__.useMemo)(() => {
    if (urlPDF) return {
      linkPdf: urlPDF
    };
    let linkPdf, src;

    switch (router.pathname) {
      case "/keno":
        linkPdf = "139.180.219.58/the-le-keno.pdf";
        break;

      case "/power":
        linkPdf = "139.180.219.58/the-le-power-655.pdf";
        break;

      case "/mega":
        linkPdf = "139.180.219.58/mega-645.pdf";
        break;

      case "/together":
        linkPdf = "139.180.219.58/3mien.pdf";
        break;

      case "/auth/register":
        linkPdf = "139.180.219.58/dieu_khoan_su_dung.pdf";
        break;
    }

    return {
      linkPdf
    };
  });
  const renderPdf = (0,react__WEBPACK_IMPORTED_MODULE_2__.useMemo)(() => {
    return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("object", {
      className: "w-full h-full",
      data: renderLinkPdf.linkPdf,
      type: "application/pdf",
      children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("iframe", {
        className: "w-full h-full",
        src: `https://docs.google.com/viewer?url=${renderLinkPdf.linkPdf}&embedded=true`
      })
    });
  }, [router, urlPDF]);
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    style: {
      height: "60vh"
    },
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
      style: {
        height: "90%",
        marginTop: "5%"
      },
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        className: "text-2xl mb-1 font-medium",
        children: "C\u01A1 c\u1EA5u v\xE0 \u0111i\u1EC1u kho\u1EA3n"
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        className: "w-full h-full rounded-xl",
        children: renderPdf
      })]
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("label", {
      className: "absolute top-1 right-1 cursor-pointer",
      onClick: handleCloseModal,
      children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_IconEva__WEBPACK_IMPORTED_MODULE_3__/* .default */ .Z, {
        name: "close-outline",
        width: "20px",
        height: "20px"
      })
    })]
  });
};

/* harmony default export */ __webpack_exports__["Z"] = (/*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_2___default().memo(ShowPDF));

/***/ }),

/***/ 5162:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(3804);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6731);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _ContentModal_ShowPDF__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(5413);
/* harmony import */ var _IconEva__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(8656);








const Header = ({
  sizeIcon = "30px",
  rightView,
  toggleModal,
  className,
  isModal,
  iconRightView
}) => {
  const router = (0,next_router__WEBPACK_IMPORTED_MODULE_2__.useRouter)();
  const renderText = (0,react__WEBPACK_IMPORTED_MODULE_3__.useMemo)(() => {
    let title = "keno";
    let info = "Cơ cấu";

    switch (router.pathname) {
      case "/keno":
        title = "keno";
        break;

      case "/power":
        title = "power";
        break;

      case "/mega":
        title = "mega";
        break;

      case "/together":
        title = "Quay lại";
        info = "Cơ cấu";
        break;
    }

    return {
      title,
      info
    };
  });

  const handleShowPdf = () => {
    return window.openModal({
      content: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_ContentModal_ShowPDF__WEBPACK_IMPORTED_MODULE_4__/* .default */ .Z, {}),
      isShowHeader: false
    });
  };

  const handleActions = () => {
    if (isModal) {
      return window.closeModal();
    }

    if (toggleModal) {
      return toggleModal();
    }

    return router.back();
  };

  const renderRightView = (0,react__WEBPACK_IMPORTED_MODULE_3__.useMemo)(() => {
    if (rightView) {
      return rightView;
    }

    return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react__WEBPACK_IMPORTED_MODULE_3__.Fragment, {
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        className: "flex justify-center items-center text-2xl",
        onClick: handleShowPdf,
        children: renderText.info
      }), iconRightView ? iconRightView : /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        onClick: handleShowPdf,
        className: " flex justify-center items-center mr-1 ml-0.5",
        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_IconEva__WEBPACK_IMPORTED_MODULE_5__/* .default */ .Z, {
          name: "award-outline",
          width: sizeIcon,
          height: sizeIcon
        })
      })]
    });
  }, [rightView]);
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    className: `w-full bg-white flex justify-between sticky top-0 h-20 ${className} `,
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
      className: "flex ",
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        onClick: handleActions,
        className: " flex justify-center items-center mr-0.5 ml-1",
        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_IconEva__WEBPACK_IMPORTED_MODULE_5__/* .default */ .Z, {
          name: "arrow-back-outline",
          width: sizeIcon,
          height: sizeIcon
        })
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        className: `flex justify-center items-center text-2xl `,
        children: (0,lodash__WEBPACK_IMPORTED_MODULE_1__.startCase)(renderText.title)
      })]
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
      className: "flex",
      children: renderRightView
    })]
  });
};

/* harmony default export */ __webpack_exports__["Z"] = (Header);

/***/ }),

/***/ 8656:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var eva_icons__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1765);
/* harmony import */ var eva_icons__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(eva_icons__WEBPACK_IMPORTED_MODULE_2__);




const IconEva = ({
  name,
  color,
  height,
  width
}) => {
  (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(() => {
    eva_icons__WEBPACK_IMPORTED_MODULE_2__.replace();
  }, []);
  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
    children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("i", {
      "data-eva": name,
      "data-eva-fill": color,
      "data-eva-height": height,
      "data-eva-width": width
    })
  });
};

/* harmony default export */ __webpack_exports__["Z"] = (IconEva);

/***/ }),

/***/ 7598:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);



function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }



const Select = (_ref) => {
  let {
    options,
    defaultValue,
    label,
    onChangeSelect,
    isGetAllData
  } = _ref,
      props = _objectWithoutProperties(_ref, ["options", "defaultValue", "label", "onChangeSelect", "isGetAllData"]);

  const {
    0: value,
    1: setValue
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)("");
  (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(() => {
    if (isGetAllData) {
      const data = options.find(option => option.value == defaultValue.value);
      return setValue(data.value);
    }

    return setValue(defaultValue);
  }, [defaultValue]);

  const onChange = () => e => {
    const {
      value
    } = e.target;
    setValue(value);

    if (isGetAllData) {
      const data = options.find(option => option.value == value);
      return onChangeSelect(data);
    }

    onChangeSelect(value);
  };

  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    children: [label ? /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
      className: "text-gray-700 text-lg",
      children: label
    }) : null, /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("select", {
      value: value,
      className: "form-select block w-full text-xl rounded border-none bg-gray-50 appearance-none",
      onChange: onChange(),
      children: options.map((item, index) => /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("option", {
        value: item.value,
        children: item.text
      }, index))
    })]
  });
};

/* harmony default export */ __webpack_exports__["Z"] = (Select);

/***/ }),

/***/ 9900:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": function() { return /* binding */ BaseApi; }
});

;// CONCATENATED MODULE: external "axios"
var external_axios_namespaceObject = require("axios");;
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_namespaceObject);
// EXTERNAL MODULE: external "js-cookie"
var external_js_cookie_ = __webpack_require__(6155);
var external_js_cookie_default = /*#__PURE__*/__webpack_require__.n(external_js_cookie_);
// EXTERNAL MODULE: external "store"
var external_store_ = __webpack_require__(7405);
var external_store_default = /*#__PURE__*/__webpack_require__.n(external_store_);
// EXTERNAL MODULE: ./store/user.js
var user = __webpack_require__(5175);
// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(6731);
var router_default = /*#__PURE__*/__webpack_require__.n(router_);
// EXTERNAL MODULE: external "lodash"
var external_lodash_ = __webpack_require__(3804);
;// CONCATENATED MODULE: ./config/BaseApi.js






const BaseAPI = external_axios_default().create({
  baseURL: "https://apis.kenoclub.net",
  timeout: 12000
});
BaseAPI.interceptors.response.use(config => {
  const token = external_js_cookie_default().get("token");
  const authDevice = "d04f817cf80fb5cad2eb936fa2aba25ae6d1a77dab4e5bf4662099072cd9f6d1";
  if (!!token) config.headers.Authorization = `Bearer ${token}`;
  config.headers.authdevice = authDevice;
  const {
    status
  } = config.data;

  if (status) {
    return config.data;
  }

  return config;
}, error => {
  if ((0,external_lodash_.get)(error, "response.status", "") === 401) {
    external_store_default().dispatch((0,user/* logOut */.ni)());
    router_default().push("/auth/login");
  }

  Promise.reject((0,external_lodash_.get)(error, "response.data", {}));
});
BaseAPI.interceptors.request.use(config => {
  const token = external_js_cookie_default().get("token");
  const authDevice = "d04f817cf80fb5cad2eb936fa2aba25ae6d1a77dab4e5bf4662099072cd9f6d1";
  if (!!token) config.headers.Authorization = `Bearer ${token}`;
  config.headers.authdevice = authDevice;
  return config;
}, err => Promise.reject(err));
/* harmony default export */ var BaseApi = (BaseAPI);

/***/ }),

/***/ 8089:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": function() { return /* binding */ socket_client; }
});

;// CONCATENATED MODULE: external "socket.io-client"
var external_socket_io_client_namespaceObject = require("socket.io-client");;
var external_socket_io_client_default = /*#__PURE__*/__webpack_require__.n(external_socket_io_client_namespaceObject);
// EXTERNAL MODULE: ./store/index.js + 3 modules
var store = __webpack_require__(4279);
// EXTERNAL MODULE: ./store/lottery.js
var lottery = __webpack_require__(2360);
// EXTERNAL MODULE: ./store/user.js
var user = __webpack_require__(5175);
// EXTERNAL MODULE: ./config/BaseApi.js + 1 modules
var BaseApi = __webpack_require__(9900);
;// CONCATENATED MODULE: ./config/socket-client.js
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }







class SocketServices {
  constructor() {
    _defineProperty(this, "socket", void 0);
  }

  static initSocket(token) {
    const sk = external_socket_io_client_default()(`${"https://socket.kenoclub.net"}`, {
      query: {
        token: `${token}`
      }
    });

    const updateInfoUser = async () => {
      const response = await BaseApi/* default.post */.Z.post("/user/me");

      if (response.status) {
        store/* default.dispatch */.Z.dispatch((0,user/* setUser */.av)(response));
      }
    };

    sk.on("connect", () => {
      console.log(`WS Chat Connected`);
    });
    sk.on("GET_ALL_ROOM", msg => {
      store/* default.dispatch */.Z.dispatch((0,lottery/* setRooms */.CF)(msg));
      updateInfoUser();
    });
    sk.on("JOIN_ROOM", msg => {
      sk.emit("GET_ALL_ROOM");
    });
    sk.on("CLOSE_ROOM", msg => {
      sk.emit("GET_ALL_ROOM");
    });
    sk.on("NEW_ROOM", msg => {
      sk.emit("GET_ALL_ROOM");
    });
    sk.on("LEAVE_ROOM", msg => {
      sk.emit("GET_ALL_ROOM");
    });
    sk.on("JOIN_ROOM_SUCCESS", msg => {
      sk.emit("GET_ALL_ROOM");
    });
    sk.on("ERROR", msg => {
      const data = JSON.parse(msg);
      console.log("error", data.msg); //   Alert.alert("Lỗi", data.msg);
    });
    sk.on("disconnect", () => {
      console.log("disconnect socket");
    });
    this.socket = sk;
  }

  static getSocketInstance() {
    return this.socket;
  }

}

/* harmony default export */ var socket_client = (SocketServices);

/***/ }),

/***/ 1377:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "wO": function() { return /* binding */ lotteryType; },
/* harmony export */   "ZW": function() { return /* binding */ LOTTERY_DATA; },
/* harmony export */   "Po": function() { return /* binding */ megaPrice; },
/* harmony export */   "bD": function() { return /* binding */ powerPrice; },
/* harmony export */   "G7": function() { return /* binding */ LIST_TYPE_LOTTERY; }
/* harmony export */ });
/* unused harmony exports KENO, MAX_3D, MAX_4D, POWER, MEGA, TOGETHER, LOTO, BIG, EQUAL_BIG, SMALL, EVEN, EVEN_11, ODD_11, EQUAL_EVEN, ODD, BIG_SMALL, ODD_EVEN */
const KENO = "keno";
const MAX_3D = "3d";
const MAX_4D = "4d";
const POWER = "power";
const MEGA = "mega";
const TOGETHER = "together";
const LOTO = "loto";
const lotteryType = {
  keno: "keno",
  power: "power",
  mega: "mega"
}; // options plus keno

const BIG = "lon";
const EQUAL_BIG = "hoa-lonnho";
const SMALL = "nho";
const EVEN = "chan";
const EVEN_11 = "chan-11-12";
const ODD_11 = "le-11-12";
const EQUAL_EVEN = "hoa-chanle";
const ODD = "le";
const BIG_SMALL = "BIG_SMALL";
const ODD_EVEN = "ODD_EVEN";
const LOTTERY_DATA = {
  currentPeriod: 0,
  listSelect: [],
  listSelectedGroup: [],
  // use in buy lottery group
  isShowAlert: false,
  type: "",
  period: 1,
  money: 10000,
  level: 10,
  subGame: [],
  subGameName: {}
};
const megaPrice = [{
  text: "Bao 5",
  value: 5,
  money: 400000
}, {
  text: "Bao 6",
  value: 6,
  money: 10000
}, {
  text: "Bao 7",
  value: 7,
  money: 70000
}, {
  text: "Bao 8",
  value: 8,
  money: 280000
}, {
  text: "Bao 9",
  value: 9,
  money: 840000
}, {
  text: "Bao 10",
  value: 10,
  money: 2100000
}, {
  text: "Bao 11",
  value: 11,
  money: 4620000
}, {
  text: "Bao 12",
  value: 12,
  money: 9240000
}, {
  text: "Bao 13",
  value: 13,
  money: 17160000
}, {
  text: "Bao 14",
  value: 14,
  money: 30030000
}, {
  text: "Bao 15",
  value: 15,
  money: 50050000
}, {
  text: "Bao 18",
  value: 18,
  money: 185640000
}];
const powerPrice = [{
  text: "Bao 5",
  value: 5,
  money: 500000
}, {
  text: "Bao 6",
  value: 6,
  money: 10000
}, {
  text: "Bao 7",
  value: 7,
  money: 70000
}, {
  text: "Bao 8",
  value: 8,
  money: 280000
}, {
  text: "Bao 9",
  value: 9,
  money: 840000
}, {
  text: "Bao 10",
  value: 10,
  money: 2100000
}, {
  text: "Bao 11",
  value: 11,
  money: 4620000
}, {
  text: "Bao 12",
  value: 12,
  money: 9240000
}, {
  text: "Bao 13",
  value: 13,
  money: 17160000
}, {
  text: "Bao 14",
  value: 14,
  money: 30030000
}, {
  text: "Bao 15",
  value: 15,
  money: 50050000
}, {
  text: "Bao 18",
  value: 18,
  money: 185640000
}];
const LIST_TYPE_LOTTERY = {
  keno: {
    image: "/images/logo/keno_logo.png",
    name: "Keno",
    slug: "keno",
    number: new Array(80),
    date: "Từ 6:00 đến 22:00 hằng ngày",
    time: "Mỗi 10 phút",
    period: [{
      text: "1 Kỳ",
      value: 1
    }, {
      text: "2 Kỳ",
      value: 2
    }, {
      text: "3 Kỳ",
      value: 3
    }, {
      text: "4 Kỳ",
      value: 4
    }, {
      text: "5 Kỳ",
      value: 5
    }, {
      text: "10 Kỳ",
      value: 10
    }, {
      text: "15 Kỳ",
      value: 15
    }, {
      text: "20 Kỳ",
      value: 20
    }, {
      text: "30 Kỳ",
      value: 30
    }],
    levels: [{
      text: "1",
      value: 1
    }, {
      text: "2",
      value: 2
    }, {
      text: "3",
      value: 3
    }, {
      text: "4",
      value: 4
    }, {
      text: "5",
      value: 5
    }, {
      text: "6",
      value: 6
    }, {
      text: "7",
      value: 7
    }, {
      text: "8",
      value: 8
    }, {
      text: "9",
      value: 9
    }, {
      text: "10",
      value: 10
    }],
    prices: [{
      text: "10.000",
      value: 10000
    }, {
      text: "20.000",
      value: 20000
    }, {
      text: "30.000",
      value: 30000
    }, {
      text: "40.000",
      value: 40000
    }, {
      text: "50.000",
      value: 50000
    }, {
      text: "100.000",
      value: 100000
    }, {
      text: "150.000",
      value: 150000
    }, {
      text: "200.000",
      value: 200000
    }, {
      text: "500.000",
      value: 500000
    }],
    listOptions: [{
      name: "Lớn: có 11 số trở lên từ 41 đến 80",
      display: "Lớn",
      value: BIG,
      type: BIG_SMALL
    }, {
      name: "Hoà Lớn-Nhỏ: có 10 số từ 01 đến 40 và 10 số từ 41-80",
      display: "Hoà Lớn-Nhỏ",
      value: EQUAL_BIG,
      type: BIG_SMALL
    }, {
      name: "Nhỏ: có 11 số trở lên từ 01 đến 40",
      display: "Nhỏ",
      value: SMALL,
      type: BIG_SMALL
    }, {
      name: "Chẵn: có 13 số trở lên là số chẵn",
      display: "Chẵn",
      value: EVEN,
      type: ODD_EVEN
    }, {
      name: "Chẵn 11-12: có 11 hoặc 12 số chẵn",
      display: "Chẵn 11-12",
      value: EVEN_11,
      type: ODD_EVEN
    }, {
      name: "Hoà Chẵn-Lẻ: có 10 số chẵn và 10 số lẻ",
      display: "Hoà Chẵn-Lẻ",
      value: EQUAL_EVEN,
      type: ODD_EVEN
    }, {
      name: "Lẻ 11-12: có 11 hoặc 12 số lẻ",
      display: "Lẻ 11-12",
      value: ODD_11,
      type: ODD_EVEN
    }, {
      name: "Lẻ: có 13 số trở lên là số lẻ",
      display: "Lẻ",
      value: ODD,
      type: ODD_EVEN
    }]
  },
  power: {
    image: "/images/logo/power655_logo.png",
    name: "Power 6/55",
    number: new Array(55),
    slug: "power",
    date: "Thứ 3 - Thứ 5 - Thứ 7",
    time: "Vào lúc 18:00",
    period: [{
      text: "1 Kỳ",
      value: 1
    }, {
      text: "2 Kỳ",
      value: 2
    }, {
      text: "3 Kỳ",
      value: 3
    }, {
      text: "4 Kỳ",
      value: 4
    }, {
      text: "5 Kỳ",
      value: 5
    }, {
      text: "6 Kỳ",
      value: 6
    }],
    lottery: powerPrice
  },
  mega: {
    image: "/images/logo/mega645_logo.png",
    name: "Mega 6/45",
    number: new Array(45),
    slug: "mega",
    date: "Thứ 4 - Thứ 6 - Chủ Nhật",
    time: "Vào lúc 18:00",
    lottery: megaPrice,
    period: [{
      text: "1 Kỳ",
      value: 1
    }, {
      text: "2 Kỳ",
      value: 2
    }, {
      text: "3 Kỳ",
      value: 3
    }, {
      text: "4 Kỳ",
      value: 4
    }, {
      text: "5 Kỳ",
      value: 5
    }, {
      text: "6 Kỳ",
      value: 6
    }]
  },
  TOGETHER: {
    image: "/images/logo/together.jpg",
    name: "Bao 3 Miền",
    type: ["mega", "power"],
    slug: "together"
  },
  LOTO: {
    image: "/images/logo/lotodongnai.jpg",
    name: "Lô Tô Đồng Nai",
    type: [],
    slug: "loto"
  }
};

/***/ }),

/***/ 7206:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": function() { return /* binding */ together; }
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(5282);
// EXTERNAL MODULE: ./components/share/common/Button.js
var Button = __webpack_require__(2268);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(9297);
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);
// EXTERNAL MODULE: external "react-toastify"
var external_react_toastify_ = __webpack_require__(2034);
;// CONCATENATED MODULE: ./components/ScreenPage/TogetherScreen/TableSelect.js




const TableSelect = ({
  numberList,
  maxSelect,
  numberSelect = [],
  handleUpdateList
}) => {
  const {
    0: listSelected,
    1: setListSelected
  } = (0,external_react_.useState)(numberSelect);

  const handleSelectNumber = number => () => {
    const isSelected = listSelected.includes(number);

    if (isSelected) {
      const arrTemp = listSelected.filter(x => x !== number);
      handleUpdateList(arrTemp);
      return setListSelected(arrTemp);
    }

    if (listSelected.length === maxSelect) {
      return external_react_toastify_.toast.warning("Bạn đã chọn đủ số");
    }

    handleUpdateList([...listSelected, number]);
    setListSelected([...listSelected, number]);
  };

  return /*#__PURE__*/jsx_runtime_.jsx("div", {
    className: `grid grid-cols-${Math.ceil(numberList.length / 10)} gap-0.5 overflow-y-auto hidden-scrollbar overflow-hidden mb-1`,
    style: {
      maxHeight: "30vh",
      height: "30vh"
    },
    children: numberList.fill("").map((num, index) => /*#__PURE__*/jsx_runtime_.jsx("label", {
      className: `mx-auto rounded-full h-3 w-3 flex items-center justify-center border-1 border-red-500 ${listSelected.includes(index + 1) ? "bg-red-500" : ""}`,
      onClick: handleSelectNumber(index + 1),
      children: /*#__PURE__*/jsx_runtime_.jsx("span", {
        className: `text-small ${listSelected.includes(index + 1) ? "text-gray-50" : ""}`,
        children: index + 1
      })
    }, index))
  });
};

/* harmony default export */ var TogetherScreen_TableSelect = (TableSelect);
// EXTERNAL MODULE: ./config/socket-client.js + 1 modules
var socket_client = __webpack_require__(8089);
// EXTERNAL MODULE: external "lodash"
var external_lodash_ = __webpack_require__(3804);
;// CONCATENATED MODULE: external "react-number-format"
var external_react_number_format_namespaceObject = require("react-number-format");;
var external_react_number_format_default = /*#__PURE__*/__webpack_require__.n(external_react_number_format_namespaceObject);
// EXTERNAL MODULE: ./utils/functions.js
var functions = __webpack_require__(6187);
;// CONCATENATED MODULE: ./components/share/common/ContentModal/TogetherJoinRoom.js










const TogetherJoinRoom = ({
  item
}, ref) => {
  const {
    0: listNumberSelect,
    1: setListNumberSelect
  } = (0,external_react_.useState)([]);
  const {
    0: price,
    1: setPrice
  } = (0,external_react_.useState)("");
  const isMega = item.gameType === "mega";
  const joinNums = item.join.filter(it => !it.lowCost).map(it => it.nums);
  const uniqNums = [...new Set((0,external_lodash_.flatten)(joinNums))].sort((a, b) => a - b);
  const packagePrice = (0,external_react_.useMemo)(() => {
    return item.price;
  });
  const totalJoin = (0,external_react_.useMemo)(() => {
    return item.join.reduce((total, current) => {
      return total + parseFloat(current.price);
    }, 0);
  });
  const priceRemain = (0,external_react_.useMemo)(() => {
    return packagePrice - totalJoin;
  });
  const pricePerNum = (0,external_react_.useMemo)(() => {
    const type = item.package;
    return Math.floor(packagePrice / type);
  });
  const isLow = (0,external_react_.useMemo)(() => {
    return parseFloat(price) < parseFloat(pricePerNum);
  }, [price]);
  const maxSelect = (0,external_react_.useMemo)(() => {
    let option = Math.floor(parseFloat(price) / parseFloat(pricePerNum));
    return option = option === 0 ? 1 : option;
  }, [price, pricePerNum]);
  const renderNotes = (0,external_react_.useMemo)(() => {
    if (price && price <= priceRemain) {
      return /*#__PURE__*/jsx_runtime_.jsx("p", {
        className: "text-center text-lg text-yellow-500 mt-1",
        children: isLow ? `Bạn được chọn tối đa ${maxSelect} số, 
Số của bạn sẽ không được hiển thị do thấp hơn mức tối thiểu` : `Bạn được chọn tối đa ${maxSelect} số`
      });
    } else if (price > priceRemain) {
      return /*#__PURE__*/jsx_runtime_.jsx("p", {
        className: "text-center text-lg text-yellow-500 mt-1",
        children: "B\u1EA1n \u0111\xE3 nh\u1EADp nhi\u1EC1u h\u01A1n s\u1ED1 ti\u1EC1n v\xE9"
      });
    } else {
      return null;
    }
  }, [price]);

  const handleInput = () => e => {
    setPrice(e.floatValue);
  };

  const handleSelectNumber = list => {
    setListNumberSelect(list);
  };

  const renderListNumber = (0,external_react_.useMemo)(() => {
    const numberList = isMega ? new Array(45) : new Array(55);
    return /*#__PURE__*/jsx_runtime_.jsx(TogetherScreen_TableSelect, {
      numberSelect: listNumberSelect,
      numberList: numberList,
      maxSelect: maxSelect,
      handleUpdateList: handleSelectNumber
    });
  }, [price]);

  const renderListSelected = (list = []) => {
    return /*#__PURE__*/jsx_runtime_.jsx("div", {
      className: "mx-auto",
      children: /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "flex w-full flex-wrap",
        children: list.map((num, index) => /*#__PURE__*/jsx_runtime_.jsx("div", {
          className: "rounded-full h-3 w-3 flex items-center justify-center border-1 border-red-500 mx-1 my-0.5",
          children: /*#__PURE__*/jsx_runtime_.jsx("span", {
            className: "text-small",
            children: num
          })
        }, index))
      })
    });
  };

  (0,external_react_.useImperativeHandle)(ref, () => ({
    async onCreateNewRoom() {
      if (listNumberSelect.length === maxSelect && price && price <= priceRemain) {
        const res = Math.abs(parseFloat(price) - priceRemain);

        if (parseFloat(price) < 10000) {
          return external_react_toastify_.toast.warning("Vui lòng tham gia tối thiểu 10.000đ!");
        }

        if (res <= 5000 && res > 0) {
          return external_react_toastify_.toast.warning("Vui lòng tham gia với mệnh giá khác!");
        }

        const socketInstance = socket_client/* default.getSocketInstance */.Z.getSocketInstance();
        const joinObject = {
          roomId: item._id,
          price: price,
          nums: listNumberSelect
        };
        socketInstance.emit("JOIN_ROOM", JSON.stringify(joinObject));
        window.closeModal();
        external_react_toastify_.toast.success("Tham gia thành công, chúc quý khách may mắn");
      } else {
        return external_react_toastify_.toast.warning("Vui lòng kiểm tra lại dữ liệu");
      }
    }

  }));
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(external_react_.Fragment, {
    children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: "flex justify-between w-full mt-1 bg-gray-100 pb-1.5 pt-1.5 rounded-md",
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: "pl-2 my-auto",
        children: ["T\u1ED5ng s\u1ED1:", /*#__PURE__*/jsx_runtime_.jsx("span", {
          className: "sub-text-colors ",
          children: (0,functions/* formatCurrency */.xG)(totalJoin)
        })]
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: "my-auto pr-2",
        children: ["C\xF2n l\u1EA1i:", /*#__PURE__*/jsx_runtime_.jsx("span", {
          className: "sub-text-colors",
          children: (0,functions/* formatCurrency */.xG)(priceRemain)
        })]
      })]
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: "flex justify-between flex-col w-full mt-1 bg-gray-100 pb-1.5 pt-1.5 rounded-md",
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: "flex justify-between w-full",
        children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
          className: "pl-2 my-auto",
          children: "C\xE1c s\u1ED1 \u0111\xE3 ch\u1ECDn"
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: "my-auto pr-2",
          children: ["Bao ", item.package]
        })]
      }), renderListSelected(uniqNums)]
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: "w-full flex flex-col  mt-1 rounded-md",
      children: [/*#__PURE__*/jsx_runtime_.jsx("label", {
        className: "text-base mb-0.5",
        children: "Nh\u1EADp s\u1ED1 ti\u1EC1n b\u1EA1n mu\u1ED1n g\xF3p"
      }), /*#__PURE__*/jsx_runtime_.jsx((external_react_number_format_default()), {
        displayType: "input",
        onValueChange: handleInput(),
        value: price,
        className: "bg-gray-100 pl-0.5 h-3 input text-lg",
        placeholder: `Mỗi số có trị giá ${(0,functions/* formatCurrency */.xG)(pricePerNum)}`,
        thousandSeparator: true
      })]
    }), renderNotes, /*#__PURE__*/jsx_runtime_.jsx("div", {
      className: `w-full flex flex-col  mt-1 rounded-md ${price && price <= priceRemain ? "" : "invisible"}`,
      children: renderListNumber
    })]
  });
};

/* harmony default export */ var ContentModal_TogetherJoinRoom = (/*#__PURE__*/(0,external_react_.forwardRef)(TogetherJoinRoom));
// EXTERNAL MODULE: external "react-redux"
var external_react_redux_ = __webpack_require__(79);
;// CONCATENATED MODULE: ./components/ScreenPage/TogetherScreen/BoxSelectTogether.js










const BoxSelectTogether = ({
  item
}) => {
  const user = (0,external_react_redux_.useSelector)(state => state.user.user);

  const sumPriceFunc = (total = 0, current) => {
    return total + parseFloat(current.price);
  };

  const joinInfo = item.join.find(it => {
    return it.player._id === (0,external_lodash_.get)(user, "_id", "");
  });
  const sumJoin = item.join.slice().filter(it => it.player._id === (0,external_lodash_.get)(user, "_id")).reduce((total = 0, current) => {
    return total + parseFloat(current.price);
  }, 0);
  const totalJoin = item.join.reduce(sumPriceFunc, 0);
  const joinNums = item.join.filter(it => !it.lowCost).map(it => it.nums);
  const joinLowNums = item.join.filter(it => it.lowCost).map(it => it.nums);
  const uniqNums = [...new Set((0,external_lodash_.flatten)(joinNums))].sort((a, b) => a - b);
  const remain = item.price - totalJoin;

  const renderListNumber = (list = []) => {
    return /*#__PURE__*/jsx_runtime_.jsx("div", {
      className: "mx-auto",
      children: /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "flex w-full flex-wrap",
        children: list.map((num, index) => /*#__PURE__*/jsx_runtime_.jsx("div", {
          className: "rounded-full h-3 w-3 flex items-center justify-center border-1 border-red-500 mx-1 my-0.5",
          children: /*#__PURE__*/jsx_runtime_.jsx("span", {
            className: "text-small",
            children: num
          })
        }, index))
      })
    });
  };

  const myRef = (0,external_react_.useRef)();

  const renderRightViewModal = () => {
    return /*#__PURE__*/jsx_runtime_.jsx(Button/* default */.Z, {
      onClick: () => myRef.current.onCreateNewRoom(),
      className: "normal-case text-lg m-0.5",
      children: "Tham gia"
    });
  };

  const handleShowModalJoin = () => {
    return window.openModal({
      content: /*#__PURE__*/jsx_runtime_.jsx(ContentModal_TogetherJoinRoom, {
        item: item,
        renderRightViewModal: renderRightViewModal,
        ref: myRef
      }),
      rightView: renderRightViewModal()
    });
  };

  return /*#__PURE__*/jsx_runtime_.jsx("div", {
    children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: "w-full bg-white p-1.5 rounded-lg mt-1.5",
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: "pb-1.5 border-b",
        children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: "flex justify-between pb-0.5",
          children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
            children: "T\u1ED5ng s\u1ED1:"
          }), /*#__PURE__*/jsx_runtime_.jsx("div", {
            className: "sub-text-color",
            children: (0,functions/* formatCurrency */.xG)(item.price)
          })]
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: "flex justify-between pb-0.5",
          children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
            children: "Ho\xE0n thi\u1EC7n:"
          }), /*#__PURE__*/jsx_runtime_.jsx("div", {
            className: "sub-text-color",
            children: (0,functions/* formatCurrency */.xG)(totalJoin)
          })]
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: "flex justify-between",
          children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
            children: "C\xF2n l\u1EA1i:"
          }), /*#__PURE__*/jsx_runtime_.jsx("div", {
            className: "sub-text-color",
            children: (0,functions/* formatCurrency */.xG)(remain)
          })]
        })]
      }), joinInfo && /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: "flex justify-between align-center pb-1.5 pt-1.5 border-b",
        children: [/*#__PURE__*/jsx_runtime_.jsx("span", {
          className: "text-blue-500",
          children: (0,external_lodash_.get)(joinInfo, "player.phoneNumber", "")
        }), " ", /*#__PURE__*/(0,jsx_runtime_.jsxs)("p", {
          children: ["\u0110\xE3 tham gia:", " ", /*#__PURE__*/jsx_runtime_.jsx("span", {
            className: "sub-text-color",
            children: (0,functions/* formatCurrency */.xG)(sumJoin)
          })]
        })]
      }), /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "flex justify-between p-0.5",
        children: /*#__PURE__*/jsx_runtime_.jsx("p", {
          children: "C\xE1c s\u1ED1 \u0111\xE3 ch\u1ECDn"
        })
      }), /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "flex justify-between pb-0.5 border-b",
        children: renderListNumber(uniqNums)
      }), /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "flex justify-between p-0.5",
        children: /*#__PURE__*/jsx_runtime_.jsx("p", {
          children: "D\xE3y s\u1ED1 ng\u1EABu nhi\xEAn"
        })
      }), /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "flex justify-between pb-0.5 border-b",
        children: renderListNumber(item.preNums)
      }), /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "flex justify-between py-1 border-b",
        children: /*#__PURE__*/jsx_runtime_.jsx(Button/* default */.Z, {
          className: "w-full",
          onClick: handleShowModalJoin,
          children: "Tham gia"
        })
      })]
    })
  });
};

/* harmony default export */ var TogetherScreen_BoxSelectTogether = (/*#__PURE__*/external_react_default().memo(BoxSelectTogether));
// EXTERNAL MODULE: ./constants/lottery.js
var lottery = __webpack_require__(1377);
;// CONCATENATED MODULE: ./components/ScreenPage/TogetherScreen/ListTab.js




const ListTab = ({
  handleClickTab,
  tabActive
}) => {
  const tabs = [lottery/* LIST_TYPE_LOTTERY.mega */.G7.mega, lottery/* LIST_TYPE_LOTTERY.power */.G7.power];
  return /*#__PURE__*/jsx_runtime_.jsx("div", {
    className: "flex justify-between items-center bg-white rounded-lg w-full h-32",
    children: tabs.map((item, index) => /*#__PURE__*/jsx_runtime_.jsx("div", {
      className: `w-1/2 h-full py-1.5 transition ease-in-out duration-300${tabActive === item.slug ? "text-blue-500 border-4 font-medium border-blue-500" : ""}`,
      onClick: handleClickTab(item),
      children: /*#__PURE__*/jsx_runtime_.jsx("img", {
        className: "object-scale-down h-full m-auto ",
        src: item.image,
        alt: item.name
      })
    }, index))
  });
};

/* harmony default export */ var TogetherScreen_ListTab = (ListTab);
// EXTERNAL MODULE: ./components/share/common/Container.js
var Container = __webpack_require__(9566);
// EXTERNAL MODULE: ./config/BaseApi.js + 1 modules
var BaseApi = __webpack_require__(9900);
// EXTERNAL MODULE: ./components/share/common/Select.js
var Select = __webpack_require__(7598);
;// CONCATENATED MODULE: ./components/share/common/ContentModal/TogetherCreateRoom.js
















const TogetherCreateRoom = ({
  lotteryMega
}, ref) => {
  const user = (0,external_react_redux_.useSelector)(state => state.user.user);
  const {
    0: tabActive,
    1: setTabActive
  } = (0,external_react_.useState)(lottery/* lotteryType.mega */.wO.mega);
  const {
    0: selectValue,
    1: setSelectValue
  } = (0,external_react_.useState)(lotteryMega[0].value);
  const {
    0: price,
    1: setPrice
  } = (0,external_react_.useState)("");
  const {
    0: listNumberSelect,
    1: setListNumberSelect
  } = (0,external_react_.useState)([]);
  const selectList = (0,external_react_.useMemo)(() => {
    const arrTemp = tabActive === lottery/* lotteryType.mega */.wO.mega ? lottery/* megaPrice */.Po : lottery/* powerPrice */.bD;
    const listOptions = arrTemp.filter(item => item.value !== 6);
    return listOptions;
  });
  const packagePrice = (0,external_react_.useMemo)(() => {
    const type = parseInt(selectValue);
    const selectedOption = selectList.find(item => item.value === type);
    setPrice("");
    return selectedOption.money;
  }, [selectValue]);
  const pricePerNum = (0,external_react_.useMemo)(() => {
    const type = parseInt(selectValue);
    return Math.floor(packagePrice / type);
  }, [selectValue]);
  const isLow = (0,external_react_.useMemo)(() => {
    return parseFloat(price) < parseFloat(pricePerNum);
  }, [selectValue, price]);
  const maxSelect = (0,external_react_.useMemo)(() => {
    let option = Math.floor(parseFloat(price) / parseFloat(pricePerNum));
    return option = option === 0 ? 1 : option;
  }, [price, pricePerNum]);
  const renderNotes = (0,external_react_.useMemo)(() => {
    if (price && price <= packagePrice) {
      return /*#__PURE__*/jsx_runtime_.jsx("p", {
        className: "text-center text-lg text-yellow-500 mt-1",
        children: isLow ? `Bạn được chọn tối đa ${maxSelect} số, 
Số của bạn sẽ không được hiển thị do thấp hơn mức tối thiểu` : `Bạn được chọn tối đa ${maxSelect} số`
      });
    } else if (price > packagePrice) {
      return /*#__PURE__*/jsx_runtime_.jsx("p", {
        className: "text-center text-lg text-yellow-500 mt-1",
        children: "B\u1EA1n \u0111\xE3 nh\u1EADp nhi\u1EC1u h\u01A1n s\u1ED1 ti\u1EC1n v\xE9"
      });
    } else {
      return null;
    }
  }, [price]);

  const handleSelectNumber = list => {
    setListNumberSelect(list);
  };

  const renderListNumber = (0,external_react_.useMemo)(() => {
    const numberList = tabActive === lottery/* lotteryType.mega */.wO.mega ? new Array(45) : new Array(55);
    return /*#__PURE__*/jsx_runtime_.jsx(TogetherScreen_TableSelect, {
      numberSelect: listNumberSelect,
      numberList: numberList,
      maxSelect: maxSelect,
      handleUpdateList: handleSelectNumber
    });
  }, [price]);

  const handleOnChangeSelect = e => {
    setSelectValue(e);
  };

  const handleClickTab = tab => () => {
    setTabActive(tab.slug);
  };

  const handleInput = () => e => {
    setPrice(e.floatValue);
  };

  (0,external_react_.useImperativeHandle)(ref, () => ({
    async onCreateNewRoom() {
      if (listNumberSelect.length === maxSelect && price && price <= packagePrice) {
        const socketInstance = socket_client/* default.getSocketInstance */.Z.getSocketInstance();
        const createObject = {
          gameType: tabActive,
          package: parseInt(selectValue),
          createdBy: user._id
        };
        const response = await BaseApi/* default.post */.Z.post("/room", createObject);
        const roomId = (0,external_lodash_.get)(response, "data.doc._id");

        if (response.status && roomId) {
          const joinObject = {
            roomId: roomId,
            price,
            nums: listNumberSelect
          };
          socketInstance.emit("JOIN_ROOM", JSON.stringify(joinObject));
          window.closeModal();
          external_react_toastify_.toast.success("Tham gia thành công, chúc quý khách may mắn");
        }
      } else {
        return external_react_toastify_.toast.warning("Vui lòng kiểm tra dữ liệu");
      }
    }

  }));
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)(external_react_.Fragment, {
    children: [/*#__PURE__*/jsx_runtime_.jsx(TogetherScreen_ListTab, {
      tabActive: tabActive,
      handleClickTab: handleClickTab
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: "w-full flex flex-col  mt-1 rounded-md",
      children: [/*#__PURE__*/jsx_runtime_.jsx("label", {
        className: "text-base",
        children: "Bao"
      }), /*#__PURE__*/jsx_runtime_.jsx(Select/* default */.Z, {
        options: selectList,
        onChangeSelect: handleOnChangeSelect
      })]
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: "w-full flex flex-col  mt-1 rounded-md",
      children: [/*#__PURE__*/jsx_runtime_.jsx("label", {
        className: "text-base mb-0.5",
        children: "Nh\u1EADp s\u1ED1 ti\u1EC1n b\u1EA1n mu\u1ED1n g\u1ED9p"
      }), /*#__PURE__*/jsx_runtime_.jsx((external_react_number_format_default()), {
        displayType: "input",
        onValueChange: handleInput(),
        value: price,
        className: "bg-gray-100 pl-0.5 h-3 input text-lg",
        placeholder: `Mỗi số có trị giá ${(0,functions/* formatCurrency */.xG)(pricePerNum)}`,
        thousandSeparator: true
      })]
    }), renderNotes, /*#__PURE__*/jsx_runtime_.jsx("div", {
      className: `w-full flex flex-col  mt-1 rounded-md ${price && price <= packagePrice ? "" : "invisible"}`,
      children: renderListNumber
    })]
  });
};

/* harmony default export */ var ContentModal_TogetherCreateRoom = (/*#__PURE__*/(0,external_react_.forwardRef)(TogetherCreateRoom));
;// CONCATENATED MODULE: ./pages/together/index.js












const Together = () => {
  const dataMega = lottery/* LIST_TYPE_LOTTERY.mega */.G7.mega;
  const dataPower = lottery/* LIST_TYPE_LOTTERY.power */.G7.power;
  const lotteryMega = dataMega.lottery.filter(item => item.value != 6);
  const lotteryPower = dataPower.lottery.filter(item => item.value != 6);
  const {
    0: tabActive,
    1: setTabActive
  } = (0,external_react_.useState)(lottery/* lotteryType.mega */.wO.mega);
  const {
    0: tabOption,
    1: setTabOption
  } = (0,external_react_.useState)("5");
  const roomShare = (0,external_react_redux_.useSelector)(state => state.lottery.roomShare);

  const selectTabOption = option => () => {
    setTabOption(option);
  };

  const myRef = (0,external_react_.useRef)();
  const dataRoom = (0,external_react_.useMemo)(() => {
    return roomShare[tabActive];
  }, [tabActive, roomShare]);
  const listType = (0,external_react_.useMemo)(() => {
    return Object.keys(dataRoom).map((type, index) => /*#__PURE__*/jsx_runtime_.jsx("div", {
      onClick: selectTabOption(type),
      className: `text-gray-600 text-center ease-in-out duration-300 bg-white rounded-lg badge badge-ghost text-xl w-full py-1 px-1 my-1 mx-0.5 focus:outline-none font-medium ${tabOption === type ? "text-blue-500 border-4 font-medium border-blue-500" : ""}`,
      children: `Bao ${type}`
    }, index));
  }, [tabOption, tabActive]);
  const listRoom = (0,external_react_.useMemo)(() => {
    return dataRoom[tabOption].map((room, index) => /*#__PURE__*/jsx_runtime_.jsx(TogetherScreen_BoxSelectTogether, {
      item: room
    }, index));
  }, [tabOption, tabActive, dataRoom]);

  const renderIconRightViewModal = () => {
    return /*#__PURE__*/jsx_runtime_.jsx(external_react_.Fragment, {
      children: /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "flex justify-center items-center mx-1",
        children: /*#__PURE__*/jsx_runtime_.jsx(Button/* default */.Z, {
          onClick: handleShowModalCreateRoom,
          className: "",
          children: "T\u1EA1o v\xE9 m\u1EDBi"
        })
      })
    });
  };

  const renderRightView = () => {
    return /*#__PURE__*/jsx_runtime_.jsx(Button/* default */.Z, {
      onClick: () => myRef.current.onCreateNewRoom(),
      className: "normal-case text-lg m-0.5",
      children: "Ti\u1EBFp t\u1EE5c"
    });
  };

  const handleClickTab = tab => () => {
    setTabActive(tab.slug);
  };

  const handleShowModalCreateRoom = () => {
    return window.openModal({
      content: /*#__PURE__*/jsx_runtime_.jsx(ContentModal_TogetherCreateRoom, {
        lotteryMega: lotteryMega,
        lotteryPower: lotteryPower,
        ref: myRef
      }),
      rightView: renderRightView()
    });
  };

  return /*#__PURE__*/jsx_runtime_.jsx(Container/* default */.Z, {
    iconRightView: renderIconRightViewModal(),
    children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: "w-full",
      children: [/*#__PURE__*/jsx_runtime_.jsx(TogetherScreen_ListTab, {
        tabActive: tabActive,
        handleClickTab: handleClickTab
      }), /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "flex overflow-x-scroll hidden-scrollbar",
        children: listType
      }), listRoom]
    })
  });
};

/* harmony default export */ var together = (/*#__PURE__*/external_react_default().memo(Together));

/***/ }),

/***/ 4279:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": function() { return /* binding */ store_0; },
  "D": function() { return /* binding */ persistor; }
});

// EXTERNAL MODULE: external "@reduxjs/toolkit"
var toolkit_ = __webpack_require__(6139);
;// CONCATENATED MODULE: external "redux"
var external_redux_namespaceObject = require("redux");;
;// CONCATENATED MODULE: external "redux-persist"
var external_redux_persist_namespaceObject = require("redux-persist");;
;// CONCATENATED MODULE: external "redux-persist/lib/storage"
var storage_namespaceObject = require("redux-persist/lib/storage");;
var storage_default = /*#__PURE__*/__webpack_require__.n(storage_namespaceObject);
// EXTERNAL MODULE: ./store/lottery.js
var lottery = __webpack_require__(2360);
// EXTERNAL MODULE: ./store/user.js
var user = __webpack_require__(5175);
;// CONCATENATED MODULE: ./store/index.js



 // defaults to localStorage for web



const persistConfig = {
  key: "store",
  storage: (storage_default())
};
const reducer = (0,external_redux_namespaceObject.combineReducers)({
  lottery: lottery/* default */.ZP,
  user: user/* default */.ZP
});
const persistedReducer = (0,external_redux_persist_namespaceObject.persistReducer)(persistConfig, reducer);
const store = (0,toolkit_.configureStore)({
  reducer: persistedReducer
});
const persistor = (0,external_redux_persist_namespaceObject.persistStore)(store);
/* harmony default export */ var store_0 = (store);

/***/ }),

/***/ 2360:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Vg": function() { return /* binding */ addListCopy; },
/* harmony export */   "CF": function() { return /* binding */ setRooms; },
/* harmony export */   "$L": function() { return /* binding */ addSelectLotteryKeno; },
/* harmony export */   "$2": function() { return /* binding */ addSelectPriceKeno; },
/* harmony export */   "HL": function() { return /* binding */ setDataKeno; },
/* harmony export */   "yS": function() { return /* binding */ setDataMega; },
/* harmony export */   "pG": function() { return /* binding */ setDataPower; },
/* harmony export */   "CC": function() { return /* binding */ setLotteryHistory; },
/* harmony export */   "Nf": function() { return /* binding */ addCopyLotteryKeno; }
/* harmony export */ });
/* unused harmony exports lotteryModule, addLottery */
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6139);
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__);

const lotteryModule = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createSlice)({
  name: "lottery",
  initialState: {
    user: {},
    listCopy: [],
    roomShare: {},
    selectLotteryKeno: {},
    selectPriceKeno: {},
    kenoData: {},
    megaData: {},
    powerData: {},
    lotteryHistory: {
      variableTab: "keno"
    },
    copyLotteryKeno: {}
  },
  reducers: {
    addListCopy: (state, action) => {
      state.listCopy = action.payload;
    },
    addSelectLotteryKeno: (state, action) => {
      state.selectLotteryKeno = action.payload;
    },
    addCopyLotteryKeno: (state, action) => {
      state.copyLotteryKeno = action.payload;
    },
    addSelectPriceKeno: (state, action) => {
      state.selectPriceKeno = action.payload;
    },
    addLottery: (state, action) => {
      state.infoLottery.push(action.payload);
    },
    setRooms: (state, action) => {
      state.roomShare = action.payload;
    },
    setDataKeno: (state, action) => {
      state.kenoData = action.payload;
    },
    setDataMega: (state, action) => {
      state.megaData = action.payload;
    },
    setDataPower: (state, action) => {
      state.powerData = action.payload;
    },
    setLotteryHistory: (state, action) => {
      state.lotteryHistory = action.payload;
    }
  }
}); // Action creators are generated for each case reducer function

const {
  addListCopy,
  addLottery,
  setRooms,
  addSelectLotteryKeno,
  addSelectPriceKeno,
  setDataKeno,
  setDataMega,
  setDataPower,
  setLotteryHistory,
  addCopyLotteryKeno
} = lotteryModule.actions;
/* harmony default export */ __webpack_exports__["ZP"] = (lotteryModule.reducer);

/***/ }),

/***/ 5175:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "av": function() { return /* binding */ setUser; },
/* harmony export */   "ni": function() { return /* binding */ logOut; }
/* harmony export */ });
/* unused harmony export userModule */
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6139);
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6155);
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(js_cookie__WEBPACK_IMPORTED_MODULE_1__);


const userModule = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createSlice)({
  name: "user",
  initialState: {
    user: {}
  },
  reducers: {
    setUser: (state, action) => {
      const {
        token,
        user
      } = action.payload;
      state.user = user;

      if (token) {
        js_cookie__WEBPACK_IMPORTED_MODULE_1___default().set("token", token);
      }
    },
    logOut: () => {
      js_cookie__WEBPACK_IMPORTED_MODULE_1___default().remove("token");
    }
  }
}); // Action creators are generated for each case reducer function

const {
  setUser,
  logOut
} = userModule.actions;
/* harmony default export */ __webpack_exports__["ZP"] = (userModule.reducer);

/***/ }),

/***/ 6187:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "xG": function() { return /* binding */ formatCurrency; },
/* harmony export */   "AK": function() { return /* binding */ classNames; },
/* harmony export */   "TN": function() { return /* binding */ title; },
/* harmony export */   "p6": function() { return /* binding */ formatDate; },
/* harmony export */   "pw": function() { return /* binding */ totalLotteryWin; }
/* harmony export */ });
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2470);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_0__);

const formatCurrency = money => {
  const num = parseInt(money);
  return `${(num || 0).toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")}đ`;
};
const classNames = (...classes) => {
  return classes.filter(Boolean).join(" ");
};
const title = (index, subGame) => {
  const objTitle = {
    0: "A",
    1: "B",
    2: `${subGame ? "Trò Chơi Phụ" : "C"}`,
    3: "D",
    4: "E",
    5: "F"
  };
  return objTitle[index];
};
const formatDate = date => moment__WEBPACK_IMPORTED_MODULE_0___default()(date).locale("vi").format("L");
const totalLotteryWin = listNumber => {
  return listNumber.reduce((a, b) => a + b, 0);
};

/***/ }),

/***/ 6139:
/***/ (function(module) {

"use strict";
module.exports = require("@reduxjs/toolkit");;

/***/ }),

/***/ 1765:
/***/ (function(module) {

"use strict";
module.exports = require("eva-icons");;

/***/ }),

/***/ 6155:
/***/ (function(module) {

"use strict";
module.exports = require("js-cookie");;

/***/ }),

/***/ 3804:
/***/ (function(module) {

"use strict";
module.exports = require("lodash");;

/***/ }),

/***/ 2470:
/***/ (function(module) {

"use strict";
module.exports = require("moment");;

/***/ }),

/***/ 6731:
/***/ (function(module) {

"use strict";
module.exports = require("next/router");;

/***/ }),

/***/ 9297:
/***/ (function(module) {

"use strict";
module.exports = require("react");;

/***/ }),

/***/ 79:
/***/ (function(module) {

"use strict";
module.exports = require("react-redux");;

/***/ }),

/***/ 2034:
/***/ (function(module) {

"use strict";
module.exports = require("react-toastify");;

/***/ }),

/***/ 5282:
/***/ (function(module) {

"use strict";
module.exports = require("react/jsx-runtime");;

/***/ }),

/***/ 7405:
/***/ (function(module) {

"use strict";
module.exports = require("store");;

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = function(moduleId) { return __webpack_require__(__webpack_require__.s = moduleId); }
var __webpack_exports__ = (__webpack_exec__(7206));
module.exports = __webpack_exports__;

})();