(function() {
var exports = {};
exports.id = 276;
exports.ids = [276];
exports.modules = {

/***/ 5404:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var components_share_common_Button__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(2268);
/* harmony import */ var components_share_common_IconEva__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(8656);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(2470);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(1664);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var store__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(7405);
/* harmony import */ var store__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(store__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var store_lottery__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(2360);
/* harmony import */ var _utils_functions__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(6187);












const Content = ({
  item,
  isKeno,
  isMega,
  isPower,
  isKenoTab,
  textColor = "text-yellow-500",
  fontWeight = "font-medium",
  children
}) => {
  const onViewTicket = (variableTab, item) => () => {
    return store__WEBPACK_IMPORTED_MODULE_6___default().dispatch((0,store_lottery__WEBPACK_IMPORTED_MODULE_7__/* .setLotteryHistory */ .CC)({
      variableTab,
      item
    }));
  };

  const getBao = listArray => {
    let bao;

    for (const element of listArray) {
      element.nums.length ? bao = element.nums.length : 0;
    }

    return bao;
  };

  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
    children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
      className: "w-full mb-1.5",
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
        className: "container mx-auto bg-white p-1.5 flex flex-col sm:items-center rounded-xl",
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
          className: "flex justify-between w-full",
          children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
            children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
              className: `${textColor} ${fontWeight} text-2xl pb-0.4`,
              children: `${isKeno || isMega || isPower ? "Kỳ" : ""} #${item.ky}`
            }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
              className: "flex mb-0.5 pb-0.5 w-full",
              children: isKeno || isMega || isPower ? /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                className: "capitalize",
                children: moment__WEBPACK_IMPORTED_MODULE_3___default()(item.createdAt).locale("vi").format(`dddd D MMMM YYYY`)
              }) : isKenoTab ? /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                className: "flex items-stretch",
                children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(components_share_common_IconEva__WEBPACK_IMPORTED_MODULE_2__/* .default */ .Z, {
                  name: "calendar-outline",
                  width: "20px",
                  height: "20px"
                }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                  className: "capitalize leading-9",
                  children: moment__WEBPACK_IMPORTED_MODULE_3___default()(item.createdAt).locale("vi").format(`LL`)
                })]
              }) : /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                className: "flex items-stretch",
                children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(components_share_common_IconEva__WEBPACK_IMPORTED_MODULE_2__/* .default */ .Z, {
                  name: "calendar-outline",
                  width: "20px",
                  height: "20px"
                }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                  className: "capitalize leading-9",
                  children: moment__WEBPACK_IMPORTED_MODULE_3___default()(item.createdAt).format("DD/MM/YYYY")
                })]
              })
            }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
              className: "flex",
              children: item.lotteryType === "mega" ? item.lottery.length > 1 && (getBao(item.lottery) ? /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
                children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(components_share_common_IconEva__WEBPACK_IMPORTED_MODULE_2__/* .default */ .Z, {
                  name: "archive-outline",
                  width: "20px",
                  height: "20px"
                }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                  children: `Bao ${getBao(item.lottery)}`
                })]
              }) : /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {})) : ""
            })]
          }), isKeno || isMega || isPower ? null : item.result === null ? /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
            children: "Ch\u01B0a c\xF3 k\u1EBFt qu\u1EA3"
          }) : item.isTC ? /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            children: "V\xC9 TC"
          }) : item.win.length ? /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
            className: "flex flex-col items-end",
            children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
              children: (0,_utils_functions__WEBPACK_IMPORTED_MODULE_8__/* .totalLotteryWin */ .pw)(item.win) === 0 ? "" : "Vé trúng"
            }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
              children: (0,_utils_functions__WEBPACK_IMPORTED_MODULE_8__/* .totalLotteryWin */ .pw)(item.win) === 0 ? "Chúc bạn may mắn" : /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
                className: "text-red-500 font-normal text-3xl",
                children: (0,_utils_functions__WEBPACK_IMPORTED_MODULE_8__/* .formatCurrency */ .xG)((0,_utils_functions__WEBPACK_IMPORTED_MODULE_8__/* .totalLotteryWin */ .pw)(item.win))
              })
            })]
          }) : /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
            children: "Ch\xFAc b\u1EA1n may m\u1EAFn"
          })]
        }), item.isTC === true ? /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
          className: "text-bold p-1 border-2 rounded-md border-red-400 text-red-400 text-center font-medium w-full",
          children: "V\xC9 TC"
        }) : null, /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
          className: "flex w-full",
          children: item.room ? /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
            className: "capitalize",
            children: ["Lo\u1EA1i v\xE9: ", item.room.gameType, " - Bao ", item.room.package]
          }) : null
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
          className: "w-full",
          children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            className: "flex flex-col w-full pb-1",
            children: children
          }), isKeno || isMega || isPower ? null : item.status !== "order" && item.status !== "cancel" || item.photos.length > 0 ? /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_link__WEBPACK_IMPORTED_MODULE_4__.default, {
            href: "/history/view-lottery",
            children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(components_share_common_Button__WEBPACK_IMPORTED_MODULE_1__/* .default */ .Z, {
              className: "w-full",
              onClick: onViewTicket(item.lotteryType, item),
              children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                className: "normal-case text-lg ",
                children: "Xem v\xE9"
              })
            })
          }) : item.result === null && item.status === "order" ? /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
            className: "text-center pt-1 border-t-1",
            children: "V\xE9 mua \u0111ang \u0111\u01B0\u1EE3c x\u1EED l\xFD"
          }) : /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
            className: "text-center",
            children: "Mua v\xE9 kh\xF4ng th\xE0nh c\xF4ng"
          })]
        })]
      })
    })
  });
};

/* harmony default export */ __webpack_exports__["Z"] = (/*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_5___default().memo(Content));

/***/ }),

/***/ 630:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);



const Numbers = ({
  borderColor = "border-blue-500",
  borderWidth = "border-2",
  bgColor = "bg-transparent",
  width = "w-3",
  height = "h-3",
  index,
  num,
  isPower,
  sameNumber = []
}) => {
  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
    className: `${num % 2 === 0 ? `${bgColor}` : null} ${isPower && index === 6 ? `bg-blue-200 border-blue-200` : null}  rounded-full ${height} ${width} ${borderWidth} ${sameNumber.includes(Number(num)) ? "border-red-400 bg-white" : `${borderColor}`} mr-0.2 mb-0.5 flex items-center justify-center`,
    children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
      className: "text-xl font-normal",
      children: num
    })
  }, index);
};

/* harmony default export */ __webpack_exports__["Z"] = (/*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default().memo(Numbers));

/***/ }),

/***/ 5878:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _utils_functions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6187);
/* harmony import */ var _Number__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(630);







const BoxNumber = ({
  index,
  item,
  nums,
  isResultKeno,
  isPower,
  isTitleName,
  borderColor,
  borderWidth,
  subGame = "",
  sameNumber,
  listOptions
}) => {
  const listBigNumbers = numbers => {
    return numbers.filter(num => num > 40);
  };

  const listSmallNumbers = numbers => {
    return numbers.filter(num => num <= 40);
  };

  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
    children: [isTitleName && nums.length || isTitleName && subGame ? /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("p", {
        className: "py-0.5",
        children: ["B\u1EA3ng ", (0,_utils_functions__WEBPACK_IMPORTED_MODULE_2__/* .title */ .TN)(index, subGame)]
      })
    }) : null, item.win[index] > 0 && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("p", {
      className: "pb-0.5",
      children: ["\u0110\xE3 tr\xFAng", /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
        className: "text-red-500",
        children: ` ${(0,_utils_functions__WEBPACK_IMPORTED_MODULE_2__/* .formatCurrency */ .xG)(item.win[index])}`
      })]
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
      className: "flex w-full",
      children: isResultKeno ? /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
        className: "flex flex-col w-full",
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
          className: "flex flex-col",
          children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
            className: "pb-0.5",
            children: "Nh\u1ECF"
          }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            className: "flex flex-wrap",
            children: listSmallNumbers(nums).map((num, index) => /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_Number__WEBPACK_IMPORTED_MODULE_3__/* .default */ .Z, {
              num: num,
              index: index,
              borderColor: borderColor,
              borderWidth: borderWidth,
              sameNumber: sameNumber,
              bgColor: "bg-blue-color"
            }, index))
          })]
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
          className: "flex flex-col",
          children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
            className: "py-0.5",
            children: "L\u1EDBn"
          }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            className: "flex flex-wrap",
            children: listBigNumbers(nums).map((num, index) => /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_Number__WEBPACK_IMPORTED_MODULE_3__/* .default */ .Z, {
              num: num,
              index: index,
              borderColor: borderColor,
              borderWidth: borderWidth,
              sameNumber: sameNumber,
              bgColor: "bg-blue-color"
            }, index))
          })]
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
          className: "flex justify-center py-1 w-full",
          children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            className: "border-2 border-yellow-500 rounded-lg py-0.5 px-1 mr-0.5 ",
            children: item.result.chan >= 10 ? /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
              children: ["Ch\u1EB5n ", item.result.chan]
            }) : /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
              children: ["L\u1EBB ", item.result.le]
            })
          }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            className: "border-2 border-green-500 rounded-md py-0.5 px-1",
            children: item.result.lon >= 10 ? /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
              children: ["L\u1EDBn ", item.result.lon]
            }) : /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("span", {
              children: ["Nh\u1ECF ", item.result.nho]
            })
          })]
        })]
      }) : /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
        className: "flex flex-wrap",
        children: [nums.slice().sort((a, b) => a - b).map((num, index) => /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
          children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_Number__WEBPACK_IMPORTED_MODULE_3__/* .default */ .Z, {
            num: num,
            index: index,
            isPower: isPower,
            borderWidth: borderWidth,
            borderColor: borderColor,
            sameNumber: sameNumber
          }, index)
        })), subGame.length > 0 && /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
          className: "text-bold p-1 border-2 rounded-md border-purple-800 font-medium",
          children: listOptions.find(option => option.value == subGame).display
        })]
      })
    })]
  });
};

/* harmony default export */ __webpack_exports__["Z"] = (/*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_1___default().memo(BoxNumber));

/***/ }),

/***/ 2268:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }



const Button = (_ref) => {
  let {
    children,
    isLoading,
    disabled,
    buttonColor = "btn-info",
    colorText = "text-white",
    className
  } = _ref,
      props = _objectWithoutProperties(_ref, ["children", "isLoading", "disabled", "buttonColor", "colorText", "className"]);

  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", _objectSpread(_objectSpread({
    className: `btn rounded border-0 normal-case text-lg ${colorText} ${buttonColor} ${isLoading ? "loading" : ""} ${className}`,
    disabled: disabled
  }, props), {}, {
    children: children
  }));
};

/* harmony default export */ __webpack_exports__["Z"] = (Button);

/***/ }),

/***/ 2811:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": function() { return /* binding */ common_EmptyLoader; }
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(5282);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(9297);
// EXTERNAL MODULE: external "react-lottie"
var external_react_lottie_ = __webpack_require__(9102);
var external_react_lottie_default = /*#__PURE__*/__webpack_require__.n(external_react_lottie_);
;// CONCATENATED MODULE: ./public/images/lotties/empty.json
var empty_namespaceObject = JSON.parse('{"v":"5.5.7","meta":{"g":"LottieFiles AE 0.1.20","a":"","k":"","d":"","tc":""},"fr":30,"ip":0,"op":120,"w":1000,"h":1000,"nm":"Thong bao","ddd":0,"assets":[{"id":"comp_0","layers":[{"ddd":0,"ind":1,"ty":4,"nm":"7 Outlines","sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[644,533,0],"ix":2},"a":{"a":0,"k":[4.25,4.25,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"ef":[{"ty":5,"nm":"Wiggle - position","np":4,"mn":"ADBE CM WigglePosition","ix":1,"en":1,"ef":[{"ty":0,"nm":"Wiggle Speed (wigs/sec)","mn":"ADBE CM WigglePosition-0001","ix":1,"v":{"a":0,"k":1,"ix":1}},{"ty":0,"nm":"Wiggle Amount (pixels)","mn":"ADBE CM WigglePosition-0002","ix":2,"v":{"a":0,"k":3.5,"ix":2}}]},{"ty":5,"nm":"(Transform)","np":14,"mn":"ADBE Geometry2","ix":2,"en":1,"ef":[{"ty":3,"nm":"Anchor Point","mn":"ADBE Geometry2-0001","ix":1,"v":{"a":0,"k":[0,0],"ix":1}},{"ty":3,"nm":"Position","mn":"ADBE Geometry2-0002","ix":2,"v":{"a":0,"k":[0,0],"ix":2,"x":"var $bm_rt;\\n$bm_rt = wiggle(effect(\'Wiggle - position\')(\'Wiggle Speed (wigs/sec)\'), effect(\'Wiggle - position\')(\'Wiggle Amount (pixels)\'));"}},{"ty":7,"nm":"Uniform Scale","mn":"ADBE Geometry2-0011","ix":3,"v":{"a":0,"k":1,"ix":3}},{"ty":0,"nm":"Scale Height","mn":"ADBE Geometry2-0003","ix":4,"v":{"a":0,"k":100,"ix":4}},{"ty":0,"nm":"Scale Width","mn":"ADBE Geometry2-0004","ix":5,"v":{"a":0,"k":100,"ix":5}},{"ty":0,"nm":"Skew","mn":"ADBE Geometry2-0005","ix":6,"v":{"a":0,"k":0,"ix":6}},{"ty":0,"nm":"Skew Axis","mn":"ADBE Geometry2-0006","ix":7,"v":{"a":0,"k":0,"ix":7}},{"ty":0,"nm":"Rotation","mn":"ADBE Geometry2-0007","ix":8,"v":{"a":0,"k":0,"ix":8}},{"ty":0,"nm":"Opacity","mn":"ADBE Geometry2-0008","ix":9,"v":{"a":0,"k":100,"ix":9}},{"ty":7,"nm":"Use Composition’s Shutter Angle","mn":"ADBE Geometry2-0009","ix":10,"v":{"a":0,"k":1,"ix":10}},{"ty":0,"nm":"Shutter Angle","mn":"ADBE Geometry2-0010","ix":11,"v":{"a":0,"k":0,"ix":11}},{"ty":7,"nm":"Sampling","mn":"ADBE Geometry2-0012","ix":12,"v":{"a":0,"k":1,"ix":12}}]}],"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[-2.209,0],[0,-2.209],[2.209,0],[0,2.209]],"o":[[2.209,0],[0,2.209],[-2.209,0],[0,-2.209]],"v":[[0,-4],[4,0],[0,4],[-4,0]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0.847000002394,0.862999949736,0.941000007181,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"bm":0,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[4.25,4.25],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 1","np":2,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false}],"ip":0,"op":180,"st":0,"bm":0},{"ddd":0,"ind":2,"ty":4,"nm":"6 Outlines","sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[528,589,0],"ix":2},"a":{"a":0,"k":[4.5,4.5,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"ef":[{"ty":5,"nm":"Wiggle - position","np":4,"mn":"ADBE CM WigglePosition","ix":1,"en":1,"ef":[{"ty":0,"nm":"Wiggle Speed (wigs/sec)","mn":"ADBE CM WigglePosition-0001","ix":1,"v":{"a":0,"k":1,"ix":1}},{"ty":0,"nm":"Wiggle Amount (pixels)","mn":"ADBE CM WigglePosition-0002","ix":2,"v":{"a":0,"k":3.5,"ix":2}}]},{"ty":5,"nm":"(Transform)","np":14,"mn":"ADBE Geometry2","ix":2,"en":1,"ef":[{"ty":3,"nm":"Anchor Point","mn":"ADBE Geometry2-0001","ix":1,"v":{"a":0,"k":[0,0],"ix":1}},{"ty":3,"nm":"Position","mn":"ADBE Geometry2-0002","ix":2,"v":{"a":0,"k":[0,0],"ix":2,"x":"var $bm_rt;\\n$bm_rt = wiggle(effect(\'Wiggle - position\')(\'Wiggle Speed (wigs/sec)\'), effect(\'Wiggle - position\')(\'Wiggle Amount (pixels)\'));"}},{"ty":7,"nm":"Uniform Scale","mn":"ADBE Geometry2-0011","ix":3,"v":{"a":0,"k":1,"ix":3}},{"ty":0,"nm":"Scale Height","mn":"ADBE Geometry2-0003","ix":4,"v":{"a":0,"k":100,"ix":4}},{"ty":0,"nm":"Scale Width","mn":"ADBE Geometry2-0004","ix":5,"v":{"a":0,"k":100,"ix":5}},{"ty":0,"nm":"Skew","mn":"ADBE Geometry2-0005","ix":6,"v":{"a":0,"k":0,"ix":6}},{"ty":0,"nm":"Skew Axis","mn":"ADBE Geometry2-0006","ix":7,"v":{"a":0,"k":0,"ix":7}},{"ty":0,"nm":"Rotation","mn":"ADBE Geometry2-0007","ix":8,"v":{"a":0,"k":0,"ix":8}},{"ty":0,"nm":"Opacity","mn":"ADBE Geometry2-0008","ix":9,"v":{"a":0,"k":100,"ix":9}},{"ty":7,"nm":"Use Composition’s Shutter Angle","mn":"ADBE Geometry2-0009","ix":10,"v":{"a":0,"k":1,"ix":10}},{"ty":0,"nm":"Shutter Angle","mn":"ADBE Geometry2-0010","ix":11,"v":{"a":0,"k":0,"ix":11}},{"ty":7,"nm":"Sampling","mn":"ADBE Geometry2-0012","ix":12,"v":{"a":0,"k":1,"ix":12}}]}],"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[-1.933,0],[0,-1.933],[1.933,0],[0,1.933]],"o":[[1.933,0],[0,1.933],[-1.933,0],[0,-1.933]],"v":[[0,-3.5],[3.5,0],[0,3.5],[-3.5,0]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[0.847000002394,0.862999949736,0.941000007181,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":1,"ix":5},"lc":1,"lj":1,"ml":4,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[4.5,4.5],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 1","np":2,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false}],"ip":0,"op":180,"st":0,"bm":0},{"ddd":0,"ind":3,"ty":4,"nm":"5 Outlines","sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[409,560,0],"ix":2},"a":{"a":0,"k":[4.25,4.25,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"ef":[{"ty":5,"nm":"Wiggle - position","np":4,"mn":"ADBE CM WigglePosition","ix":1,"en":1,"ef":[{"ty":0,"nm":"Wiggle Speed (wigs/sec)","mn":"ADBE CM WigglePosition-0001","ix":1,"v":{"a":0,"k":1,"ix":1}},{"ty":0,"nm":"Wiggle Amount (pixels)","mn":"ADBE CM WigglePosition-0002","ix":2,"v":{"a":0,"k":3.5,"ix":2}}]},{"ty":5,"nm":"(Transform)","np":14,"mn":"ADBE Geometry2","ix":2,"en":1,"ef":[{"ty":3,"nm":"Anchor Point","mn":"ADBE Geometry2-0001","ix":1,"v":{"a":0,"k":[0,0],"ix":1}},{"ty":3,"nm":"Position","mn":"ADBE Geometry2-0002","ix":2,"v":{"a":0,"k":[0,0],"ix":2,"x":"var $bm_rt;\\n$bm_rt = wiggle(effect(\'Wiggle - position\')(\'Wiggle Speed (wigs/sec)\'), effect(\'Wiggle - position\')(\'Wiggle Amount (pixels)\'));"}},{"ty":7,"nm":"Uniform Scale","mn":"ADBE Geometry2-0011","ix":3,"v":{"a":0,"k":1,"ix":3}},{"ty":0,"nm":"Scale Height","mn":"ADBE Geometry2-0003","ix":4,"v":{"a":0,"k":100,"ix":4}},{"ty":0,"nm":"Scale Width","mn":"ADBE Geometry2-0004","ix":5,"v":{"a":0,"k":100,"ix":5}},{"ty":0,"nm":"Skew","mn":"ADBE Geometry2-0005","ix":6,"v":{"a":0,"k":0,"ix":6}},{"ty":0,"nm":"Skew Axis","mn":"ADBE Geometry2-0006","ix":7,"v":{"a":0,"k":0,"ix":7}},{"ty":0,"nm":"Rotation","mn":"ADBE Geometry2-0007","ix":8,"v":{"a":0,"k":0,"ix":8}},{"ty":0,"nm":"Opacity","mn":"ADBE Geometry2-0008","ix":9,"v":{"a":0,"k":100,"ix":9}},{"ty":7,"nm":"Use Composition’s Shutter Angle","mn":"ADBE Geometry2-0009","ix":10,"v":{"a":0,"k":1,"ix":10}},{"ty":0,"nm":"Shutter Angle","mn":"ADBE Geometry2-0010","ix":11,"v":{"a":0,"k":0,"ix":11}},{"ty":7,"nm":"Sampling","mn":"ADBE Geometry2-0012","ix":12,"v":{"a":0,"k":1,"ix":12}}]}],"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"v":[[1,-1],[1,-4],[-1,-4],[-1,-1],[-4,-1],[-4,1],[-1,1],[-1,4],[1,4],[1,1],[4,1],[4,-1]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0.847000002394,0.862999949736,0.941000007181,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"bm":0,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[4.25,4.25],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 1","np":2,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false}],"ip":0,"op":180,"st":0,"bm":0},{"ddd":0,"ind":4,"ty":4,"nm":"4 Outlines","sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[392,456,0],"ix":2},"a":{"a":0,"k":[4.25,4.25,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"ef":[{"ty":5,"nm":"Wiggle - position","np":4,"mn":"ADBE CM WigglePosition","ix":1,"en":1,"ef":[{"ty":0,"nm":"Wiggle Speed (wigs/sec)","mn":"ADBE CM WigglePosition-0001","ix":1,"v":{"a":0,"k":1,"ix":1}},{"ty":0,"nm":"Wiggle Amount (pixels)","mn":"ADBE CM WigglePosition-0002","ix":2,"v":{"a":0,"k":3.5,"ix":2}}]},{"ty":5,"nm":"(Transform)","np":14,"mn":"ADBE Geometry2","ix":2,"en":1,"ef":[{"ty":3,"nm":"Anchor Point","mn":"ADBE Geometry2-0001","ix":1,"v":{"a":0,"k":[0,0],"ix":1}},{"ty":3,"nm":"Position","mn":"ADBE Geometry2-0002","ix":2,"v":{"a":0,"k":[0,0],"ix":2,"x":"var $bm_rt;\\n$bm_rt = wiggle(effect(\'Wiggle - position\')(\'Wiggle Speed (wigs/sec)\'), effect(\'Wiggle - position\')(\'Wiggle Amount (pixels)\'));"}},{"ty":7,"nm":"Uniform Scale","mn":"ADBE Geometry2-0011","ix":3,"v":{"a":0,"k":1,"ix":3}},{"ty":0,"nm":"Scale Height","mn":"ADBE Geometry2-0003","ix":4,"v":{"a":0,"k":100,"ix":4}},{"ty":0,"nm":"Scale Width","mn":"ADBE Geometry2-0004","ix":5,"v":{"a":0,"k":100,"ix":5}},{"ty":0,"nm":"Skew","mn":"ADBE Geometry2-0005","ix":6,"v":{"a":0,"k":0,"ix":6}},{"ty":0,"nm":"Skew Axis","mn":"ADBE Geometry2-0006","ix":7,"v":{"a":0,"k":0,"ix":7}},{"ty":0,"nm":"Rotation","mn":"ADBE Geometry2-0007","ix":8,"v":{"a":0,"k":0,"ix":8}},{"ty":0,"nm":"Opacity","mn":"ADBE Geometry2-0008","ix":9,"v":{"a":0,"k":100,"ix":9}},{"ty":7,"nm":"Use Composition’s Shutter Angle","mn":"ADBE Geometry2-0009","ix":10,"v":{"a":0,"k":1,"ix":10}},{"ty":0,"nm":"Shutter Angle","mn":"ADBE Geometry2-0010","ix":11,"v":{"a":0,"k":0,"ix":11}},{"ty":7,"nm":"Sampling","mn":"ADBE Geometry2-0012","ix":12,"v":{"a":0,"k":1,"ix":12}}]}],"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[-2.209,0],[0,-2.209],[2.209,0],[0,2.209]],"o":[[2.209,0],[0,2.209],[-2.209,0],[0,-2.209]],"v":[[0,-4],[4,0],[0,4],[-4,0]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0.847000002394,0.862999949736,0.941000007181,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"bm":0,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[4.25,4.25],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 1","np":2,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false}],"ip":0,"op":180,"st":0,"bm":0},{"ddd":0,"ind":5,"ty":4,"nm":"3 Outlines","sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[471,381,0],"ix":2},"a":{"a":0,"k":[4.5,4.5,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"ef":[{"ty":5,"nm":"Wiggle - position","np":4,"mn":"ADBE CM WigglePosition","ix":1,"en":1,"ef":[{"ty":0,"nm":"Wiggle Speed (wigs/sec)","mn":"ADBE CM WigglePosition-0001","ix":1,"v":{"a":0,"k":1,"ix":1}},{"ty":0,"nm":"Wiggle Amount (pixels)","mn":"ADBE CM WigglePosition-0002","ix":2,"v":{"a":0,"k":3.5,"ix":2}}]},{"ty":5,"nm":"(Transform)","np":14,"mn":"ADBE Geometry2","ix":2,"en":1,"ef":[{"ty":3,"nm":"Anchor Point","mn":"ADBE Geometry2-0001","ix":1,"v":{"a":0,"k":[0,0],"ix":1}},{"ty":3,"nm":"Position","mn":"ADBE Geometry2-0002","ix":2,"v":{"a":0,"k":[0,0],"ix":2,"x":"var $bm_rt;\\n$bm_rt = wiggle(effect(\'Wiggle - position\')(\'Wiggle Speed (wigs/sec)\'), effect(\'Wiggle - position\')(\'Wiggle Amount (pixels)\'));"}},{"ty":7,"nm":"Uniform Scale","mn":"ADBE Geometry2-0011","ix":3,"v":{"a":0,"k":1,"ix":3}},{"ty":0,"nm":"Scale Height","mn":"ADBE Geometry2-0003","ix":4,"v":{"a":0,"k":100,"ix":4}},{"ty":0,"nm":"Scale Width","mn":"ADBE Geometry2-0004","ix":5,"v":{"a":0,"k":100,"ix":5}},{"ty":0,"nm":"Skew","mn":"ADBE Geometry2-0005","ix":6,"v":{"a":0,"k":0,"ix":6}},{"ty":0,"nm":"Skew Axis","mn":"ADBE Geometry2-0006","ix":7,"v":{"a":0,"k":0,"ix":7}},{"ty":0,"nm":"Rotation","mn":"ADBE Geometry2-0007","ix":8,"v":{"a":0,"k":0,"ix":8}},{"ty":0,"nm":"Opacity","mn":"ADBE Geometry2-0008","ix":9,"v":{"a":0,"k":100,"ix":9}},{"ty":7,"nm":"Use Composition’s Shutter Angle","mn":"ADBE Geometry2-0009","ix":10,"v":{"a":0,"k":1,"ix":10}},{"ty":0,"nm":"Shutter Angle","mn":"ADBE Geometry2-0010","ix":11,"v":{"a":0,"k":0,"ix":11}},{"ty":7,"nm":"Sampling","mn":"ADBE Geometry2-0012","ix":12,"v":{"a":0,"k":1,"ix":12}}]}],"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[-1.933,0],[0,-1.933],[1.933,0],[0,1.933]],"o":[[1.933,0],[0,1.933],[-1.933,0],[0,-1.933]],"v":[[0,-3.5],[3.5,0],[0,3.5],[-3.5,0]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[0.847000002394,0.862999949736,0.941000007181,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":1,"ix":5},"lc":1,"lj":1,"ml":4,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[4.5,4.5],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 1","np":2,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false}],"ip":0,"op":180,"st":0,"bm":0},{"ddd":0,"ind":6,"ty":4,"nm":"2 Outlines","sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[563,373,0],"ix":2},"a":{"a":0,"k":[4.5,4.5,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"ef":[{"ty":5,"nm":"Wiggle - position","np":4,"mn":"ADBE CM WigglePosition","ix":1,"en":1,"ef":[{"ty":0,"nm":"Wiggle Speed (wigs/sec)","mn":"ADBE CM WigglePosition-0001","ix":1,"v":{"a":0,"k":1,"ix":1}},{"ty":0,"nm":"Wiggle Amount (pixels)","mn":"ADBE CM WigglePosition-0002","ix":2,"v":{"a":0,"k":3.5,"ix":2}}]},{"ty":5,"nm":"(Transform)","np":14,"mn":"ADBE Geometry2","ix":2,"en":1,"ef":[{"ty":3,"nm":"Anchor Point","mn":"ADBE Geometry2-0001","ix":1,"v":{"a":0,"k":[0,0],"ix":1}},{"ty":3,"nm":"Position","mn":"ADBE Geometry2-0002","ix":2,"v":{"a":0,"k":[0,0],"ix":2,"x":"var $bm_rt;\\n$bm_rt = wiggle(effect(\'Wiggle - position\')(\'Wiggle Speed (wigs/sec)\'), effect(\'Wiggle - position\')(\'Wiggle Amount (pixels)\'));"}},{"ty":7,"nm":"Uniform Scale","mn":"ADBE Geometry2-0011","ix":3,"v":{"a":0,"k":1,"ix":3}},{"ty":0,"nm":"Scale Height","mn":"ADBE Geometry2-0003","ix":4,"v":{"a":0,"k":100,"ix":4}},{"ty":0,"nm":"Scale Width","mn":"ADBE Geometry2-0004","ix":5,"v":{"a":0,"k":100,"ix":5}},{"ty":0,"nm":"Skew","mn":"ADBE Geometry2-0005","ix":6,"v":{"a":0,"k":0,"ix":6}},{"ty":0,"nm":"Skew Axis","mn":"ADBE Geometry2-0006","ix":7,"v":{"a":0,"k":0,"ix":7}},{"ty":0,"nm":"Rotation","mn":"ADBE Geometry2-0007","ix":8,"v":{"a":0,"k":0,"ix":8}},{"ty":0,"nm":"Opacity","mn":"ADBE Geometry2-0008","ix":9,"v":{"a":0,"k":100,"ix":9}},{"ty":7,"nm":"Use Composition’s Shutter Angle","mn":"ADBE Geometry2-0009","ix":10,"v":{"a":0,"k":1,"ix":10}},{"ty":0,"nm":"Shutter Angle","mn":"ADBE Geometry2-0010","ix":11,"v":{"a":0,"k":0,"ix":11}},{"ty":7,"nm":"Sampling","mn":"ADBE Geometry2-0012","ix":12,"v":{"a":0,"k":1,"ix":12}}]}],"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[-1.933,0],[0,-1.933],[1.933,0],[0,1.933]],"o":[[1.933,0],[0,1.933],[-1.933,0],[0,-1.933]],"v":[[0,-3.5],[3.5,0],[0,3.5],[-3.5,0]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[0.847000002394,0.862999949736,0.941000007181,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":1,"ix":5},"lc":1,"lj":1,"ml":4,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[4.5,4.5],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 1","np":2,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false}],"ip":0,"op":180,"st":0,"bm":0},{"ddd":0,"ind":7,"ty":4,"nm":"1 Outlines","sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[628,414,0],"ix":2},"a":{"a":0,"k":[4.25,4.25,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"ef":[{"ty":5,"nm":"Wiggle - position","np":4,"mn":"ADBE CM WigglePosition","ix":1,"en":1,"ef":[{"ty":0,"nm":"Wiggle Speed (wigs/sec)","mn":"ADBE CM WigglePosition-0001","ix":1,"v":{"a":0,"k":1,"ix":1}},{"ty":0,"nm":"Wiggle Amount (pixels)","mn":"ADBE CM WigglePosition-0002","ix":2,"v":{"a":0,"k":3.5,"ix":2}}]},{"ty":5,"nm":"(Transform)","np":14,"mn":"ADBE Geometry2","ix":2,"en":1,"ef":[{"ty":3,"nm":"Anchor Point","mn":"ADBE Geometry2-0001","ix":1,"v":{"a":0,"k":[0,0],"ix":1}},{"ty":3,"nm":"Position","mn":"ADBE Geometry2-0002","ix":2,"v":{"a":0,"k":[0,0],"ix":2,"x":"var $bm_rt;\\n$bm_rt = wiggle(effect(\'Wiggle - position\')(\'Wiggle Speed (wigs/sec)\'), effect(\'Wiggle - position\')(\'Wiggle Amount (pixels)\'));"}},{"ty":7,"nm":"Uniform Scale","mn":"ADBE Geometry2-0011","ix":3,"v":{"a":0,"k":1,"ix":3}},{"ty":0,"nm":"Scale","mn":"ADBE Geometry2-0003","ix":4,"v":{"a":0,"k":100,"ix":4}},{"ty":0,"nm":" ","mn":"ADBE Geometry2-0004","ix":5,"v":{"a":0,"k":100,"ix":5}},{"ty":0,"nm":"Skew","mn":"ADBE Geometry2-0005","ix":6,"v":{"a":0,"k":0,"ix":6}},{"ty":0,"nm":"Skew Axis","mn":"ADBE Geometry2-0006","ix":7,"v":{"a":0,"k":0,"ix":7}},{"ty":0,"nm":"Rotation","mn":"ADBE Geometry2-0007","ix":8,"v":{"a":0,"k":0,"ix":8}},{"ty":0,"nm":"Opacity","mn":"ADBE Geometry2-0008","ix":9,"v":{"a":0,"k":100,"ix":9}},{"ty":7,"nm":"Use Composition’s Shutter Angle","mn":"ADBE Geometry2-0009","ix":10,"v":{"a":0,"k":1,"ix":10}},{"ty":0,"nm":"Shutter Angle","mn":"ADBE Geometry2-0010","ix":11,"v":{"a":0,"k":0,"ix":11}},{"ty":7,"nm":"Sampling","mn":"ADBE Geometry2-0012","ix":12,"v":{"a":0,"k":1,"ix":12}}]}],"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"o":[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]],"v":[[1,-1],[1,-4],[-1,-4],[-1,-1],[-4,-1],[-4,1],[-1,1],[-1,4],[1,4],[1,1],[4,1],[4,-1]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0.847000002394,0.862999949736,0.941000007181,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"bm":0,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[4.25,4.25],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 1","np":2,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false}],"ip":0,"op":180,"st":0,"bm":0},{"ddd":0,"ind":8,"ty":4,"nm":"Card1 Outlines","sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":1,"k":[{"i":{"x":0.833,"y":0.833},"o":{"x":0.167,"y":0.167},"t":0,"s":[529.5,504.5,0],"to":[0,1.833,0],"ti":[0,0,0]},{"i":{"x":0.833,"y":0.833},"o":{"x":0.167,"y":0.167},"t":30,"s":[529.5,515.5,0],"to":[0,0,0],"ti":[0,1.833,0]},{"t":60,"s":[529.5,504.5,0]}],"ix":2,"x":"var $bm_rt;\\n$bm_rt = loopOut();"},"a":{"a":0,"k":[87,45,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"ef":[{"ty":25,"nm":"Drop Shadow","np":8,"mn":"ADBE Drop Shadow","ix":1,"en":1,"ef":[{"ty":2,"nm":"Shadow Color","mn":"ADBE Drop Shadow-0001","ix":1,"v":{"a":0,"k":[0.117647059262,0.533333361149,0.898039221764,1],"ix":1}},{"ty":0,"nm":"Opacity","mn":"ADBE Drop Shadow-0002","ix":2,"v":{"a":0,"k":63.75,"ix":2}},{"ty":0,"nm":"Direction","mn":"ADBE Drop Shadow-0003","ix":3,"v":{"a":0,"k":135,"ix":3}},{"ty":0,"nm":"Distance","mn":"ADBE Drop Shadow-0004","ix":4,"v":{"a":0,"k":5,"ix":4}},{"ty":0,"nm":"Softness","mn":"ADBE Drop Shadow-0005","ix":5,"v":{"a":0,"k":53,"ix":5}},{"ty":7,"nm":"Shadow Only","mn":"ADBE Drop Shadow-0006","ix":6,"v":{"a":0,"k":0,"ix":6}}]}],"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[-1.657,0],[0,0],[0,-1.657],[1.657,0],[0,0],[0,1.657]],"o":[[0,0],[1.657,0],[0,1.657],[0,0],[-1.657,0],[0,-1.657]],"v":[[-54,-3],[54,-3],[57,0],[54,3],[-54,3],[-57,0]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0.948999980852,0.948999980852,0.948999980852,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"bm":0,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[81.5,54.5],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 1","np":2,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[-1.657,0],[0,0],[0,-1.657],[1.657,0],[0,0],[0,1.657]],"o":[[0,0],[1.657,0],[0,1.657],[0,0],[-1.657,0],[0,-1.657]],"v":[[-22.5,-3],[22.5,-3],[25.5,0],[22.5,3],[-22.5,3],[-25.5,0]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0.769000004787,0.769000004787,0.769000004787,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"bm":0,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[50,36.5],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 2","np":2,"cix":2,"bm":0,"ix":2,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[-3.36,2.84],[0,0],[0,4.75],[0,0]],"o":[[0,0],[-3.36,-2.84],[0,0],[0,-4.75]],"v":[[2.75,-40.34],[2.75,40.34],[-2.75,28.5],[-2.75,-28.5]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0.118000000598,0.532999973671,0.898000021542,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"bm":0,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[3.75,45],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 3","np":2,"cix":2,"bm":0,"ix":3,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,-8.56],[0,0],[8.56,0],[0,0],[2.69,2.28],[0,4.75],[0,0],[-3.36,2.84],[-3.81,0],[0,0]],"o":[[0,0],[0,8.56],[0,0],[-3.81,0],[-3.36,-2.84],[0,0],[0,-4.75],[2.69,-2.28],[0,0],[8.56,0]],"v":[[86,-28.5],[86,28.5],[70.5,44],[-70.5,44],[-80.5,40.34],[-86,28.5],[-86,-28.5],[-80.5,-40.34],[-70.5,-44],[70.5,-44]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[0.847000002394,0.862999949736,0.941000007181,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":1,"ix":5},"lc":1,"lj":1,"ml":4,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"fl","c":{"a":0,"k":[1,1,1,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"bm":0,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[87,45],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 4","np":3,"cix":2,"bm":0,"ix":4,"mn":"ADBE Vector Group","hd":false}],"ip":0,"op":180,"st":0,"bm":0},{"ddd":0,"ind":9,"ty":4,"nm":"Card2 Outlines","parent":8,"sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[60,-5.733,0],"ix":2},"a":{"a":0,"k":[87,45,0],"ix":1},"s":{"a":0,"k":[100,100,100],"ix":6}},"ao":0,"ef":[{"ty":25,"nm":"Drop Shadow","np":8,"mn":"ADBE Drop Shadow","ix":1,"en":1,"ef":[{"ty":2,"nm":"Shadow Color","mn":"ADBE Drop Shadow-0001","ix":1,"v":{"a":0,"k":[0.117647059262,0.533333361149,0.898039221764,1],"ix":1}},{"ty":0,"nm":"Opacity","mn":"ADBE Drop Shadow-0002","ix":2,"v":{"a":0,"k":63.75,"ix":2}},{"ty":0,"nm":"Direction","mn":"ADBE Drop Shadow-0003","ix":3,"v":{"a":0,"k":135,"ix":3}},{"ty":0,"nm":"Distance","mn":"ADBE Drop Shadow-0004","ix":4,"v":{"a":0,"k":5,"ix":4}},{"ty":0,"nm":"Softness","mn":"ADBE Drop Shadow-0005","ix":5,"v":{"a":0,"k":53,"ix":5}},{"ty":7,"nm":"Shadow Only","mn":"ADBE Drop Shadow-0006","ix":6,"v":{"a":0,"k":0,"ix":6}}]}],"shapes":[{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[-1.657,0],[0,0],[0,-1.657],[1.657,0],[0,0],[0,1.657]],"o":[[0,0],[1.657,0],[0,1.657],[0,0],[-1.657,0],[0,-1.657]],"v":[[-54,-3],[54,-3],[57,0],[54,3],[-54,3],[-57,0]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0.948999980852,0.948999980852,0.948999980852,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"bm":0,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[81.5,54.5],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 1","np":2,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[-1.657,0],[0,0],[0,-1.657],[1.657,0],[0,0],[0,1.657]],"o":[[0,0],[1.657,0],[0,1.657],[0,0],[-1.657,0],[0,-1.657]],"v":[[-22.5,-3],[22.5,-3],[25.5,0],[22.5,3],[-22.5,3],[-25.5,0]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0.769000004787,0.769000004787,0.769000004787,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"bm":0,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[50,36.5],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 2","np":2,"cix":2,"bm":0,"ix":2,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[-3.36,2.84],[0,0],[0,4.75],[0,0]],"o":[[0,0],[-3.36,-2.84],[0,0],[0,-4.75]],"v":[[2.75,-40.34],[2.75,40.34],[-2.75,28.5],[-2.75,-28.5]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"fl","c":{"a":0,"k":[0.118000000598,0.532999973671,0.898000021542,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"bm":0,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[3.75,45],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 3","np":2,"cix":2,"bm":0,"ix":3,"mn":"ADBE Vector Group","hd":false},{"ty":"gr","it":[{"ind":0,"ty":"sh","ix":1,"ks":{"a":0,"k":{"i":[[0,-8.56],[0,0],[8.56,0],[0,0],[2.69,2.28],[0,4.75],[0,0],[-3.36,2.84],[-3.81,0],[0,0]],"o":[[0,0],[0,8.56],[0,0],[-3.81,0],[-3.36,-2.84],[0,0],[0,-4.75],[2.69,-2.28],[0,0],[8.56,0]],"v":[[86,-28.5],[86,28.5],[70.5,44],[-70.5,44],[-80.5,40.34],[-86,28.5],[-86,-28.5],[-80.5,-40.34],[-70.5,-44],[70.5,-44]],"c":true},"ix":2},"nm":"Path 1","mn":"ADBE Vector Shape - Group","hd":false},{"ty":"st","c":{"a":0,"k":[0.847000002394,0.862999949736,0.941000007181,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":1,"ix":5},"lc":1,"lj":1,"ml":4,"bm":0,"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"fl","c":{"a":0,"k":[1,1,1,1],"ix":4},"o":{"a":0,"k":100,"ix":5},"r":1,"bm":0,"nm":"Fill 1","mn":"ADBE Vector Graphic - Fill","hd":false},{"ty":"tr","p":{"a":0,"k":[87,45],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Group 4","np":3,"cix":2,"bm":0,"ix":4,"mn":"ADBE Vector Group","hd":false}],"ip":0,"op":180,"st":0,"bm":0},{"ddd":0,"ind":10,"ty":4,"nm":"Shape Layer 1","sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[596,430,0],"ix":2},"a":{"a":0,"k":[119,-105,0],"ix":1},"s":{"a":0,"k":[82,82,100],"ix":6}},"ao":0,"shapes":[{"ty":"gr","it":[{"d":1,"ty":"el","s":{"a":0,"k":[206,206],"ix":2},"p":{"a":0,"k":[0,0],"ix":3},"nm":"Ellipse Path 1","mn":"ADBE Vector Shape - Ellipse","hd":false},{"ty":"st","c":{"a":0,"k":[0.847058823529,0.862745098039,0.941176470588,1],"ix":3},"o":{"a":0,"k":100,"ix":4},"w":{"a":0,"k":2,"ix":5},"lc":1,"lj":1,"ml":4,"bm":0,"d":[{"n":"d","nm":"dash","v":{"a":0,"k":10,"ix":1}},{"n":"o","nm":"offset","v":{"a":1,"k":[{"i":{"x":[0.833],"y":[0.833]},"o":{"x":[0.167],"y":[0.167]},"t":0,"s":[0]},{"i":{"x":[0.833],"y":[0.833]},"o":{"x":[0.167],"y":[0.167]},"t":30,"s":[10]},{"i":{"x":[0.833],"y":[0.833]},"o":{"x":[0.167],"y":[0.167]},"t":60,"s":[0]},{"i":{"x":[0.833],"y":[0.833]},"o":{"x":[0.167],"y":[0.167]},"t":90,"s":[10]},{"t":120,"s":[0]}],"ix":7}}],"nm":"Stroke 1","mn":"ADBE Vector Graphic - Stroke","hd":false},{"ty":"tr","p":{"a":0,"k":[119,-105],"ix":2},"a":{"a":0,"k":[0,0],"ix":1},"s":{"a":0,"k":[100,100],"ix":3},"r":{"a":0,"k":0,"ix":6},"o":{"a":0,"k":100,"ix":7},"sk":{"a":0,"k":0,"ix":4},"sa":{"a":0,"k":0,"ix":5},"nm":"Transform"}],"nm":"Ellipse 1","np":3,"cix":2,"bm":0,"ix":1,"mn":"ADBE Vector Group","hd":false}],"ip":0,"op":180,"st":0,"bm":0}]}],"layers":[{"ddd":0,"ind":1,"ty":0,"nm":"ThongBaoElement","refId":"comp_0","sr":1,"ks":{"o":{"a":0,"k":100,"ix":11},"r":{"a":0,"k":0,"ix":10},"p":{"a":0,"k":[436,563,0],"ix":2},"a":{"a":0,"k":[500,500,0],"ix":1},"s":{"a":0,"k":[243,243,100],"ix":6}},"ao":0,"tm":{"a":1,"k":[{"i":{"x":[0.833],"y":[0.833]},"o":{"x":[0.167],"y":[0.167]},"t":0,"s":[0]},{"i":{"x":[0.833],"y":[0.833]},"o":{"x":[0.167],"y":[0.167]},"t":60,"s":[2]},{"i":{"x":[0.833],"y":[0.833]},"o":{"x":[0.167],"y":[0.167]},"t":120,"s":[0]},{"t":180,"s":[6]}],"ix":2},"w":1000,"h":1000,"ip":0,"op":180,"st":0,"bm":0}],"markers":[]}');
;// CONCATENATED MODULE: ./components/share/common/EmptyLoader.js


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }





const EmptyLoader = (_ref) => {
  let {
    size = 100
  } = _ref,
      props = _objectWithoutProperties(_ref, ["size"]);

  return /*#__PURE__*/jsx_runtime_.jsx("div", {
    className: "flex justify-center",
    children: /*#__PURE__*/jsx_runtime_.jsx((external_react_lottie_default()), _objectSpread({
      height: size,
      width: size,
      options: {
        loop: true,
        autoplay: true,
        animationData: empty_namespaceObject
      }
    }, props))
  });
};

/* harmony default export */ var common_EmptyLoader = (EmptyLoader);

/***/ }),

/***/ 8656:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var eva_icons__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1765);
/* harmony import */ var eva_icons__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(eva_icons__WEBPACK_IMPORTED_MODULE_2__);




const IconEva = ({
  name,
  color,
  height,
  width
}) => {
  (0,react__WEBPACK_IMPORTED_MODULE_1__.useEffect)(() => {
    eva_icons__WEBPACK_IMPORTED_MODULE_2__.replace();
  }, []);
  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
    children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("i", {
      "data-eva": name,
      "data-eva-fill": color,
      "data-eva-height": height,
      "data-eva-width": width
    })
  });
};

/* harmony default export */ __webpack_exports__["Z"] = (IconEva);

/***/ }),

/***/ 4020:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": function() { return /* binding */ common_NavBar; }
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(5282);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(9297);
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);
;// CONCATENATED MODULE: external "@headlessui/react"
var react_namespaceObject = require("@headlessui/react");;
// EXTERNAL MODULE: external "eva-icons"
var external_eva_icons_ = __webpack_require__(1765);
// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(6731);
// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__(1664);
// EXTERNAL MODULE: ./utils/functions.js
var functions = __webpack_require__(6187);
// EXTERNAL MODULE: ./components/share/common/IconEva.js
var IconEva = __webpack_require__(8656);
;// CONCATENATED MODULE: ./components/share/common/NavBar.js










const NavBar = () => {
  const router = (0,router_.useRouter)();
  const navigation = [{
    name: "Trang Chủ",
    href: "/"
  }, {
    name: "Lịch Sử",
    href: "/history"
  }, {
    name: "Kết Quả",
    href: "/result"
  }, {
    name: "Tài Khoản",
    href: "/profile"
  }];

  const handleClickNav = (item, index) => () => {
    router.push(item.href);
  };

  const iconEva = (name, color) => {
    return /*#__PURE__*/jsx_runtime_.jsx("i", {
      "data-eva": name,
      "data-eva-fill": color
    });
  };

  const iconsNavbar = [iconEva, iconEva, iconEva, iconEva];
  const nameIcons = ["home-outline", "clock-outline", "tv-outline", "heart-outline"];
  (0,external_react_.useEffect)(() => {
    external_eva_icons_.replace();
  }, []);
  return /*#__PURE__*/jsx_runtime_.jsx(react_namespaceObject.Disclosure, {
    as: "nav",
    className: "fixed bottom-0 left-0 right-0 z-40 bg-white",
    children: /*#__PURE__*/jsx_runtime_.jsx("div", {
      className: "container mx-auto border-b-1 sm:border-0 sm:px-6 lg:px-8",
      children: /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: "flex ",
        children: navigation.map((item, index) => /*#__PURE__*/jsx_runtime_.jsx(next_link.default, {
          href: item.href,
          className: "w-1/4",
          children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("a", {
            onClick: handleClickNav(item, index),
            className: (0,functions/* classNames */.AK)(item.href === router.route ? "text-blue-700 border-t-4 border-blue-700 " : "text-gray-500 border-t-4 border-white ", "w-full text-xl py-0.5 text-center font-normal flex flex-col items-center"),
            "aria-current": item.current ? "page" : undefined,
            children: [iconsNavbar[index](nameIcons[index], item.href === router.route ? "#1D4ED8" : "#6b7280"), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
              className: "pt-0.2",
              children: [" ", item.name]
            })]
          })
        }, index))
      })
    })
  });
};

/* harmony default export */ var common_NavBar = (/*#__PURE__*/external_react_default().memo(NavBar));

/***/ }),

/***/ 9900:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": function() { return /* binding */ BaseApi; }
});

;// CONCATENATED MODULE: external "axios"
var external_axios_namespaceObject = require("axios");;
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_namespaceObject);
// EXTERNAL MODULE: external "js-cookie"
var external_js_cookie_ = __webpack_require__(6155);
var external_js_cookie_default = /*#__PURE__*/__webpack_require__.n(external_js_cookie_);
// EXTERNAL MODULE: external "store"
var external_store_ = __webpack_require__(7405);
var external_store_default = /*#__PURE__*/__webpack_require__.n(external_store_);
// EXTERNAL MODULE: ./store/user.js
var user = __webpack_require__(5175);
// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(6731);
var router_default = /*#__PURE__*/__webpack_require__.n(router_);
// EXTERNAL MODULE: external "lodash"
var external_lodash_ = __webpack_require__(3804);
;// CONCATENATED MODULE: ./config/BaseApi.js






const BaseAPI = external_axios_default().create({
  baseURL: "https://apis.kenoclub.net",
  timeout: 12000
});
BaseAPI.interceptors.response.use(config => {
  const token = external_js_cookie_default().get("token");
  const authDevice = "d04f817cf80fb5cad2eb936fa2aba25ae6d1a77dab4e5bf4662099072cd9f6d1";
  if (!!token) config.headers.Authorization = `Bearer ${token}`;
  config.headers.authdevice = authDevice;
  const {
    status
  } = config.data;

  if (status) {
    return config.data;
  }

  return config;
}, error => {
  if ((0,external_lodash_.get)(error, "response.status", "") === 401) {
    external_store_default().dispatch((0,user/* logOut */.ni)());
    router_default().push("/auth/login");
  }

  Promise.reject((0,external_lodash_.get)(error, "response.data", {}));
});
BaseAPI.interceptors.request.use(config => {
  const token = external_js_cookie_default().get("token");
  const authDevice = "d04f817cf80fb5cad2eb936fa2aba25ae6d1a77dab4e5bf4662099072cd9f6d1";
  if (!!token) config.headers.Authorization = `Bearer ${token}`;
  config.headers.authdevice = authDevice;
  return config;
}, err => Promise.reject(err));
/* harmony default export */ var BaseApi = (BaseAPI);

/***/ }),

/***/ 1377:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "wO": function() { return /* binding */ lotteryType; },
/* harmony export */   "ZW": function() { return /* binding */ LOTTERY_DATA; },
/* harmony export */   "Po": function() { return /* binding */ megaPrice; },
/* harmony export */   "bD": function() { return /* binding */ powerPrice; },
/* harmony export */   "G7": function() { return /* binding */ LIST_TYPE_LOTTERY; }
/* harmony export */ });
/* unused harmony exports KENO, MAX_3D, MAX_4D, POWER, MEGA, TOGETHER, LOTO, BIG, EQUAL_BIG, SMALL, EVEN, EVEN_11, ODD_11, EQUAL_EVEN, ODD, BIG_SMALL, ODD_EVEN */
const KENO = "keno";
const MAX_3D = "3d";
const MAX_4D = "4d";
const POWER = "power";
const MEGA = "mega";
const TOGETHER = "together";
const LOTO = "loto";
const lotteryType = {
  keno: "keno",
  power: "power",
  mega: "mega"
}; // options plus keno

const BIG = "lon";
const EQUAL_BIG = "hoa-lonnho";
const SMALL = "nho";
const EVEN = "chan";
const EVEN_11 = "chan-11-12";
const ODD_11 = "le-11-12";
const EQUAL_EVEN = "hoa-chanle";
const ODD = "le";
const BIG_SMALL = "BIG_SMALL";
const ODD_EVEN = "ODD_EVEN";
const LOTTERY_DATA = {
  currentPeriod: 0,
  listSelect: [],
  listSelectedGroup: [],
  // use in buy lottery group
  isShowAlert: false,
  type: "",
  period: 1,
  money: 10000,
  level: 10,
  subGame: [],
  subGameName: {}
};
const megaPrice = [{
  text: "Bao 5",
  value: 5,
  money: 400000
}, {
  text: "Bao 6",
  value: 6,
  money: 10000
}, {
  text: "Bao 7",
  value: 7,
  money: 70000
}, {
  text: "Bao 8",
  value: 8,
  money: 280000
}, {
  text: "Bao 9",
  value: 9,
  money: 840000
}, {
  text: "Bao 10",
  value: 10,
  money: 2100000
}, {
  text: "Bao 11",
  value: 11,
  money: 4620000
}, {
  text: "Bao 12",
  value: 12,
  money: 9240000
}, {
  text: "Bao 13",
  value: 13,
  money: 17160000
}, {
  text: "Bao 14",
  value: 14,
  money: 30030000
}, {
  text: "Bao 15",
  value: 15,
  money: 50050000
}, {
  text: "Bao 18",
  value: 18,
  money: 185640000
}];
const powerPrice = [{
  text: "Bao 5",
  value: 5,
  money: 500000
}, {
  text: "Bao 6",
  value: 6,
  money: 10000
}, {
  text: "Bao 7",
  value: 7,
  money: 70000
}, {
  text: "Bao 8",
  value: 8,
  money: 280000
}, {
  text: "Bao 9",
  value: 9,
  money: 840000
}, {
  text: "Bao 10",
  value: 10,
  money: 2100000
}, {
  text: "Bao 11",
  value: 11,
  money: 4620000
}, {
  text: "Bao 12",
  value: 12,
  money: 9240000
}, {
  text: "Bao 13",
  value: 13,
  money: 17160000
}, {
  text: "Bao 14",
  value: 14,
  money: 30030000
}, {
  text: "Bao 15",
  value: 15,
  money: 50050000
}, {
  text: "Bao 18",
  value: 18,
  money: 185640000
}];
const LIST_TYPE_LOTTERY = {
  keno: {
    image: "/images/logo/keno_logo.png",
    name: "Keno",
    slug: "keno",
    number: new Array(80),
    date: "Từ 6:00 đến 22:00 hằng ngày",
    time: "Mỗi 10 phút",
    period: [{
      text: "1 Kỳ",
      value: 1
    }, {
      text: "2 Kỳ",
      value: 2
    }, {
      text: "3 Kỳ",
      value: 3
    }, {
      text: "4 Kỳ",
      value: 4
    }, {
      text: "5 Kỳ",
      value: 5
    }, {
      text: "10 Kỳ",
      value: 10
    }, {
      text: "15 Kỳ",
      value: 15
    }, {
      text: "20 Kỳ",
      value: 20
    }, {
      text: "30 Kỳ",
      value: 30
    }],
    levels: [{
      text: "1",
      value: 1
    }, {
      text: "2",
      value: 2
    }, {
      text: "3",
      value: 3
    }, {
      text: "4",
      value: 4
    }, {
      text: "5",
      value: 5
    }, {
      text: "6",
      value: 6
    }, {
      text: "7",
      value: 7
    }, {
      text: "8",
      value: 8
    }, {
      text: "9",
      value: 9
    }, {
      text: "10",
      value: 10
    }],
    prices: [{
      text: "10.000",
      value: 10000
    }, {
      text: "20.000",
      value: 20000
    }, {
      text: "30.000",
      value: 30000
    }, {
      text: "40.000",
      value: 40000
    }, {
      text: "50.000",
      value: 50000
    }, {
      text: "100.000",
      value: 100000
    }, {
      text: "150.000",
      value: 150000
    }, {
      text: "200.000",
      value: 200000
    }, {
      text: "500.000",
      value: 500000
    }],
    listOptions: [{
      name: "Lớn: có 11 số trở lên từ 41 đến 80",
      display: "Lớn",
      value: BIG,
      type: BIG_SMALL
    }, {
      name: "Hoà Lớn-Nhỏ: có 10 số từ 01 đến 40 và 10 số từ 41-80",
      display: "Hoà Lớn-Nhỏ",
      value: EQUAL_BIG,
      type: BIG_SMALL
    }, {
      name: "Nhỏ: có 11 số trở lên từ 01 đến 40",
      display: "Nhỏ",
      value: SMALL,
      type: BIG_SMALL
    }, {
      name: "Chẵn: có 13 số trở lên là số chẵn",
      display: "Chẵn",
      value: EVEN,
      type: ODD_EVEN
    }, {
      name: "Chẵn 11-12: có 11 hoặc 12 số chẵn",
      display: "Chẵn 11-12",
      value: EVEN_11,
      type: ODD_EVEN
    }, {
      name: "Hoà Chẵn-Lẻ: có 10 số chẵn và 10 số lẻ",
      display: "Hoà Chẵn-Lẻ",
      value: EQUAL_EVEN,
      type: ODD_EVEN
    }, {
      name: "Lẻ 11-12: có 11 hoặc 12 số lẻ",
      display: "Lẻ 11-12",
      value: ODD_11,
      type: ODD_EVEN
    }, {
      name: "Lẻ: có 13 số trở lên là số lẻ",
      display: "Lẻ",
      value: ODD,
      type: ODD_EVEN
    }]
  },
  power: {
    image: "/images/logo/power655_logo.png",
    name: "Power 6/55",
    number: new Array(55),
    slug: "power",
    date: "Thứ 3 - Thứ 5 - Thứ 7",
    time: "Vào lúc 18:00",
    period: [{
      text: "1 Kỳ",
      value: 1
    }, {
      text: "2 Kỳ",
      value: 2
    }, {
      text: "3 Kỳ",
      value: 3
    }, {
      text: "4 Kỳ",
      value: 4
    }, {
      text: "5 Kỳ",
      value: 5
    }, {
      text: "6 Kỳ",
      value: 6
    }],
    lottery: powerPrice
  },
  mega: {
    image: "/images/logo/mega645_logo.png",
    name: "Mega 6/45",
    number: new Array(45),
    slug: "mega",
    date: "Thứ 4 - Thứ 6 - Chủ Nhật",
    time: "Vào lúc 18:00",
    lottery: megaPrice,
    period: [{
      text: "1 Kỳ",
      value: 1
    }, {
      text: "2 Kỳ",
      value: 2
    }, {
      text: "3 Kỳ",
      value: 3
    }, {
      text: "4 Kỳ",
      value: 4
    }, {
      text: "5 Kỳ",
      value: 5
    }, {
      text: "6 Kỳ",
      value: 6
    }]
  },
  TOGETHER: {
    image: "/images/logo/together.jpg",
    name: "Bao 3 Miền",
    type: ["mega", "power"],
    slug: "together"
  },
  LOTO: {
    image: "/images/logo/lotodongnai.jpg",
    name: "Lô Tô Đồng Nai",
    type: [],
    slug: "loto"
  }
};

/***/ }),

/***/ 6071:
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";
var __webpack_unused_export__;


var _interopRequireWildcard = __webpack_require__(9448);

__webpack_unused_export__ = true;
exports.default = void 0;

var _react = _interopRequireWildcard(__webpack_require__(9297));

var _router = __webpack_require__(1689);

var _router2 = __webpack_require__(2441);

var _useIntersection = __webpack_require__(5749);

const prefetched = {};

function prefetch(router, href, as, options) {
  if (true) return;
  if (!(0, _router.isLocalURL)(href)) return; // Prefetch the JSON page if asked (only in the client)
  // We need to handle a prefetch error here since we may be
  // loading with priority which can reject but we don't
  // want to force navigation since this is only a prefetch

  router.prefetch(href, as, options).catch(err => {
    if (false) {}
  });
  const curLocale = options && typeof options.locale !== 'undefined' ? options.locale : router && router.locale; // Join on an invalid URI character

  prefetched[href + '%' + as + (curLocale ? '%' + curLocale : '')] = true;
}

function isModifiedEvent(event) {
  const {
    target
  } = event.currentTarget;
  return target && target !== '_self' || event.metaKey || event.ctrlKey || event.shiftKey || event.altKey || // triggers resource download
  event.nativeEvent && event.nativeEvent.which === 2;
}

function linkClicked(e, router, href, as, replace, shallow, scroll, locale) {
  const {
    nodeName
  } = e.currentTarget;

  if (nodeName === 'A' && (isModifiedEvent(e) || !(0, _router.isLocalURL)(href))) {
    // ignore click for browser’s default behavior
    return;
  }

  e.preventDefault(); //  avoid scroll for urls with anchor refs

  if (scroll == null) {
    scroll = as.indexOf('#') < 0;
  } // replace state instead of push if prop is present


  router[replace ? 'replace' : 'push'](href, as, {
    shallow,
    locale,
    scroll
  });
}

function Link(props) {
  if (false) {}

  const p = props.prefetch !== false;
  const router = (0, _router2.useRouter)();
  const pathname = router && router.pathname || '/';

  const {
    href,
    as
  } = _react.default.useMemo(() => {
    const [resolvedHref, resolvedAs] = (0, _router.resolveHref)(pathname, props.href, true);
    return {
      href: resolvedHref,
      as: props.as ? (0, _router.resolveHref)(pathname, props.as) : resolvedAs || resolvedHref
    };
  }, [pathname, props.href, props.as]);

  let {
    children,
    replace,
    shallow,
    scroll,
    locale
  } = props; // Deprecated. Warning shown by propType check. If the children provided is a string (<Link>example</Link>) we wrap it in an <a> tag

  if (typeof children === 'string') {
    children = /*#__PURE__*/_react.default.createElement("a", null, children);
  } // This will return the first child, if multiple are provided it will throw an error


  const child = _react.Children.only(children);

  const childRef = child && typeof child === 'object' && child.ref;
  const [setIntersectionRef, isVisible] = (0, _useIntersection.useIntersection)({
    rootMargin: '200px'
  });

  const setRef = _react.default.useCallback(el => {
    setIntersectionRef(el);

    if (childRef) {
      if (typeof childRef === 'function') childRef(el);else if (typeof childRef === 'object') {
        childRef.current = el;
      }
    }
  }, [childRef, setIntersectionRef]);

  (0, _react.useEffect)(() => {
    const shouldPrefetch = isVisible && p && (0, _router.isLocalURL)(href);
    const curLocale = typeof locale !== 'undefined' ? locale : router && router.locale;
    const isPrefetched = prefetched[href + '%' + as + (curLocale ? '%' + curLocale : '')];

    if (shouldPrefetch && !isPrefetched) {
      prefetch(router, href, as, {
        locale: curLocale
      });
    }
  }, [as, href, isVisible, locale, p, router]);
  const childProps = {
    ref: setRef,
    onClick: e => {
      if (child.props && typeof child.props.onClick === 'function') {
        child.props.onClick(e);
      }

      if (!e.defaultPrevented) {
        linkClicked(e, router, href, as, replace, shallow, scroll, locale);
      }
    }
  };

  childProps.onMouseEnter = e => {
    if (!(0, _router.isLocalURL)(href)) return;

    if (child.props && typeof child.props.onMouseEnter === 'function') {
      child.props.onMouseEnter(e);
    }

    prefetch(router, href, as, {
      priority: true
    });
  }; // If child is an <a> tag and doesn't have a href attribute, or if the 'passHref' property is
  // defined, we specify the current 'href', so that repetition is not needed by the user


  if (props.passHref || child.type === 'a' && !('href' in child.props)) {
    const curLocale = typeof locale !== 'undefined' ? locale : router && router.locale; // we only render domain locales if we are currently on a domain locale
    // so that locale links are still visitable in development/preview envs

    const localeDomain = router && router.isLocaleDomain && (0, _router.getDomainLocale)(as, curLocale, router && router.locales, router && router.domainLocales);
    childProps.href = localeDomain || (0, _router.addBasePath)((0, _router.addLocale)(as, curLocale, router && router.defaultLocale));
  }

  return /*#__PURE__*/_react.default.cloneElement(child, childProps);
}

var _default = Link;
exports.default = _default;

/***/ }),

/***/ 6528:
/***/ (function(__unused_webpack_module, exports) {

"use strict";


exports.__esModule = true;
exports.removePathTrailingSlash = removePathTrailingSlash;
exports.normalizePathTrailingSlash = void 0;
/**
* Removes the trailing slash of a path if there is one. Preserves the root path `/`.
*/

function removePathTrailingSlash(path) {
  return path.endsWith('/') && path !== '/' ? path.slice(0, -1) : path;
}
/**
* Normalizes the trailing slash of a path according to the `trailingSlash` option
* in `next.config.js`.
*/


const normalizePathTrailingSlash =  false ? 0 : removePathTrailingSlash;
exports.normalizePathTrailingSlash = normalizePathTrailingSlash;

/***/ }),

/***/ 8391:
/***/ (function(__unused_webpack_module, exports) {

"use strict";


exports.__esModule = true;
exports.cancelIdleCallback = exports.requestIdleCallback = void 0;

const requestIdleCallback = typeof self !== 'undefined' && self.requestIdleCallback || function (cb) {
  let start = Date.now();
  return setTimeout(function () {
    cb({
      didTimeout: false,
      timeRemaining: function () {
        return Math.max(0, 50 - (Date.now() - start));
      }
    });
  }, 1);
};

exports.requestIdleCallback = requestIdleCallback;

const cancelIdleCallback = typeof self !== 'undefined' && self.cancelIdleCallback || function (id) {
  return clearTimeout(id);
};

exports.cancelIdleCallback = cancelIdleCallback;

/***/ }),

/***/ 7599:
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(2426);

exports.__esModule = true;
exports.markAssetError = markAssetError;
exports.isAssetError = isAssetError;
exports.getClientBuildManifest = getClientBuildManifest;
exports.default = void 0;

var _getAssetPathFromRoute = _interopRequireDefault(__webpack_require__(2238));

var _requestIdleCallback = __webpack_require__(8391); // 3.8s was arbitrarily chosen as it's what https://web.dev/interactive
// considers as "Good" time-to-interactive. We must assume something went
// wrong beyond this point, and then fall-back to a full page transition to
// show the user something of value.


const MS_MAX_IDLE_DELAY = 3800;

function withFuture(key, map, generator) {
  let entry = map.get(key);

  if (entry) {
    if ('future' in entry) {
      return entry.future;
    }

    return Promise.resolve(entry);
  }

  let resolver;
  const prom = new Promise(resolve => {
    resolver = resolve;
  });
  map.set(key, entry = {
    resolve: resolver,
    future: prom
  });
  return generator ? // eslint-disable-next-line no-sequences
  generator().then(value => (resolver(value), value)) : prom;
}

function hasPrefetch(link) {
  try {
    link = document.createElement('link');
    return (// detect IE11 since it supports prefetch but isn't detected
      // with relList.support
      !!window.MSInputMethodContext && !!document.documentMode || link.relList.supports('prefetch')
    );
  } catch (_unused) {
    return false;
  }
}

const canPrefetch = hasPrefetch();

function prefetchViaDom(href, as, link) {
  return new Promise((res, rej) => {
    if (document.querySelector(`link[rel="prefetch"][href^="${href}"]`)) {
      return res();
    }

    link = document.createElement('link'); // The order of property assignment here is intentional:

    if (as) link.as = as;
    link.rel = `prefetch`;
    link.crossOrigin = undefined;
    link.onload = res;
    link.onerror = rej; // `href` should always be last:

    link.href = href;
    document.head.appendChild(link);
  });
}

const ASSET_LOAD_ERROR = Symbol('ASSET_LOAD_ERROR'); // TODO: unexport

function markAssetError(err) {
  return Object.defineProperty(err, ASSET_LOAD_ERROR, {});
}

function isAssetError(err) {
  return err && ASSET_LOAD_ERROR in err;
}

function appendScript(src, script) {
  return new Promise((resolve, reject) => {
    script = document.createElement('script'); // The order of property assignment here is intentional.
    // 1. Setup success/failure hooks in case the browser synchronously
    //    executes when `src` is set.

    script.onload = resolve;

    script.onerror = () => reject(markAssetError(new Error(`Failed to load script: ${src}`))); // 2. Configure the cross-origin attribute before setting `src` in case the
    //    browser begins to fetch.


    script.crossOrigin = undefined; // 3. Finally, set the source and inject into the DOM in case the child
    //    must be appended for fetching to start.

    script.src = src;
    document.body.appendChild(script);
  });
} // Resolve a promise that times out after given amount of milliseconds.


function resolvePromiseWithTimeout(p, ms, err) {
  return new Promise((resolve, reject) => {
    let cancelled = false;
    p.then(r => {
      // Resolved, cancel the timeout
      cancelled = true;
      resolve(r);
    }).catch(reject);
    (0, _requestIdleCallback.requestIdleCallback)(() => setTimeout(() => {
      if (!cancelled) {
        reject(err);
      }
    }, ms));
  });
} // TODO: stop exporting or cache the failure
// It'd be best to stop exporting this. It's an implementation detail. We're
// only exporting it for backwards compatibilty with the `page-loader`.
// Only cache this response as a last resort if we cannot eliminate all other
// code branches that use the Build Manifest Callback and push them through
// the Route Loader interface.


function getClientBuildManifest() {
  if (self.__BUILD_MANIFEST) {
    return Promise.resolve(self.__BUILD_MANIFEST);
  }

  const onBuildManifest = new Promise(resolve => {
    // Mandatory because this is not concurrent safe:
    const cb = self.__BUILD_MANIFEST_CB;

    self.__BUILD_MANIFEST_CB = () => {
      resolve(self.__BUILD_MANIFEST);
      cb && cb();
    };
  });
  return resolvePromiseWithTimeout(onBuildManifest, MS_MAX_IDLE_DELAY, markAssetError(new Error('Failed to load client build manifest')));
}

function getFilesForRoute(assetPrefix, route) {
  if (false) {}

  return getClientBuildManifest().then(manifest => {
    if (!(route in manifest)) {
      throw markAssetError(new Error(`Failed to lookup route: ${route}`));
    }

    const allFiles = manifest[route].map(entry => assetPrefix + '/_next/' + encodeURI(entry));
    return {
      scripts: allFiles.filter(v => v.endsWith('.js')),
      css: allFiles.filter(v => v.endsWith('.css'))
    };
  });
}

function createRouteLoader(assetPrefix) {
  const entrypoints = new Map();
  const loadedScripts = new Map();
  const styleSheets = new Map();
  const routes = new Map();

  function maybeExecuteScript(src) {
    let prom = loadedScripts.get(src);

    if (prom) {
      return prom;
    } // Skip executing script if it's already in the DOM:


    if (document.querySelector(`script[src^="${src}"]`)) {
      return Promise.resolve();
    }

    loadedScripts.set(src, prom = appendScript(src));
    return prom;
  }

  function fetchStyleSheet(href) {
    let prom = styleSheets.get(href);

    if (prom) {
      return prom;
    }

    styleSheets.set(href, prom = fetch(href).then(res => {
      if (!res.ok) {
        throw new Error(`Failed to load stylesheet: ${href}`);
      }

      return res.text().then(text => ({
        href: href,
        content: text
      }));
    }).catch(err => {
      throw markAssetError(err);
    }));
    return prom;
  }

  return {
    whenEntrypoint(route) {
      return withFuture(route, entrypoints);
    },

    onEntrypoint(route, execute) {
      Promise.resolve(execute).then(fn => fn()).then(exports => ({
        component: exports && exports.default || exports,
        exports: exports
      }), err => ({
        error: err
      })).then(input => {
        const old = entrypoints.get(route);
        entrypoints.set(route, input);
        if (old && 'resolve' in old) old.resolve(input);
      });
    },

    loadRoute(route, prefetch) {
      return withFuture(route, routes, () => {
        return resolvePromiseWithTimeout(getFilesForRoute(assetPrefix, route).then(({
          scripts,
          css
        }) => {
          return Promise.all([entrypoints.has(route) ? [] : Promise.all(scripts.map(maybeExecuteScript)), Promise.all(css.map(fetchStyleSheet))]);
        }).then(res => {
          return this.whenEntrypoint(route).then(entrypoint => ({
            entrypoint,
            styles: res[1]
          }));
        }), MS_MAX_IDLE_DELAY, markAssetError(new Error(`Route did not complete loading: ${route}`))).then(({
          entrypoint,
          styles
        }) => {
          const res = Object.assign({
            styles: styles
          }, entrypoint);
          return 'error' in entrypoint ? entrypoint : res;
        }).catch(err => {
          if (prefetch) {
            // we don't want to cache errors during prefetch
            throw err;
          }

          return {
            error: err
          };
        });
      });
    },

    prefetch(route) {
      // https://github.com/GoogleChromeLabs/quicklink/blob/453a661fa1fa940e2d2e044452398e38c67a98fb/src/index.mjs#L115-L118
      // License: Apache 2.0
      let cn;

      if (cn = navigator.connection) {
        // Don't prefetch if using 2G or if Save-Data is enabled.
        if (cn.saveData || /2g/.test(cn.effectiveType)) return Promise.resolve();
      }

      return getFilesForRoute(assetPrefix, route).then(output => Promise.all(canPrefetch ? output.scripts.map(script => prefetchViaDom(script, 'script')) : [])).then(() => {
        (0, _requestIdleCallback.requestIdleCallback)(() => this.loadRoute(route, true).catch(() => {}));
      }).catch( // swallow prefetch errors
      () => {});
    }

  };
}

var _default = createRouteLoader;
exports.default = _default;

/***/ }),

/***/ 2441:
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var _interopRequireWildcard = __webpack_require__(9448);

var _interopRequireDefault = __webpack_require__(2426);

exports.__esModule = true;
exports.useRouter = useRouter;
exports.makePublicRouterInstance = makePublicRouterInstance;
exports.createRouter = exports.withRouter = exports.default = void 0;

var _react = _interopRequireDefault(__webpack_require__(9297));

var _router2 = _interopRequireWildcard(__webpack_require__(1689));

exports.Router = _router2.default;
exports.NextRouter = _router2.NextRouter;

var _routerContext = __webpack_require__(8417);

var _withRouter = _interopRequireDefault(__webpack_require__(3168));

exports.withRouter = _withRouter.default;
/* global window */

const singletonRouter = {
  router: null,
  // holds the actual router instance
  readyCallbacks: [],

  ready(cb) {
    if (this.router) return cb();

    if (false) {}
  }

}; // Create public properties and methods of the router in the singletonRouter

const urlPropertyFields = ['pathname', 'route', 'query', 'asPath', 'components', 'isFallback', 'basePath', 'locale', 'locales', 'defaultLocale', 'isReady', 'isPreview', 'isLocaleDomain'];
const routerEvents = ['routeChangeStart', 'beforeHistoryChange', 'routeChangeComplete', 'routeChangeError', 'hashChangeStart', 'hashChangeComplete'];
const coreMethodFields = ['push', 'replace', 'reload', 'back', 'prefetch', 'beforePopState']; // Events is a static property on the router, the router doesn't have to be initialized to use it

Object.defineProperty(singletonRouter, 'events', {
  get() {
    return _router2.default.events;
  }

});
urlPropertyFields.forEach(field => {
  // Here we need to use Object.defineProperty because, we need to return
  // the property assigned to the actual router
  // The value might get changed as we change routes and this is the
  // proper way to access it
  Object.defineProperty(singletonRouter, field, {
    get() {
      const router = getRouter();
      return router[field];
    }

  });
});
coreMethodFields.forEach(field => {
  // We don't really know the types here, so we add them later instead
  ;

  singletonRouter[field] = (...args) => {
    const router = getRouter();
    return router[field](...args);
  };
});
routerEvents.forEach(event => {
  singletonRouter.ready(() => {
    _router2.default.events.on(event, (...args) => {
      const eventField = `on${event.charAt(0).toUpperCase()}${event.substring(1)}`;
      const _singletonRouter = singletonRouter;

      if (_singletonRouter[eventField]) {
        try {
          _singletonRouter[eventField](...args);
        } catch (err) {
          console.error(`Error when running the Router event: ${eventField}`);
          console.error(`${err.message}\n${err.stack}`);
        }
      }
    });
  });
});

function getRouter() {
  if (!singletonRouter.router) {
    const message = 'No router instance found.\n' + 'You should only use "next/router" inside the client side of your app.\n';
    throw new Error(message);
  }

  return singletonRouter.router;
} // Export the singletonRouter and this is the public API.


var _default = singletonRouter; // Reexport the withRoute HOC

exports.default = _default;

function useRouter() {
  return _react.default.useContext(_routerContext.RouterContext);
} // INTERNAL APIS
// -------------
// (do not use following exports inside the app)
// Create a router and assign it as the singleton instance.
// This is used in client side when we are initilizing the app.
// This should **not** use inside the server.


const createRouter = (...args) => {
  singletonRouter.router = new _router2.default(...args);
  singletonRouter.readyCallbacks.forEach(cb => cb());
  singletonRouter.readyCallbacks = [];
  return singletonRouter.router;
}; // This function is used to create the `withRouter` router instance


exports.createRouter = createRouter;

function makePublicRouterInstance(router) {
  const _router = router;
  const instance = {};

  for (const property of urlPropertyFields) {
    if (typeof _router[property] === 'object') {
      instance[property] = Object.assign(Array.isArray(_router[property]) ? [] : {}, _router[property]); // makes sure query is not stateful

      continue;
    }

    instance[property] = _router[property];
  } // Events is a static property on the router, the router doesn't have to be initialized to use it


  instance.events = _router2.default.events;
  coreMethodFields.forEach(field => {
    instance[field] = (...args) => {
      return _router[field](...args);
    };
  });
  return instance;
}

/***/ }),

/***/ 5749:
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.useIntersection = useIntersection;

var _react = __webpack_require__(9297);

var _requestIdleCallback = __webpack_require__(8391);

const hasIntersectionObserver = typeof IntersectionObserver !== 'undefined';

function useIntersection({
  rootMargin,
  disabled
}) {
  const isDisabled = disabled || !hasIntersectionObserver;
  const unobserve = (0, _react.useRef)();
  const [visible, setVisible] = (0, _react.useState)(false);
  const setRef = (0, _react.useCallback)(el => {
    if (unobserve.current) {
      unobserve.current();
      unobserve.current = undefined;
    }

    if (isDisabled || visible) return;

    if (el && el.tagName) {
      unobserve.current = observe(el, isVisible => isVisible && setVisible(isVisible), {
        rootMargin
      });
    }
  }, [isDisabled, rootMargin, visible]);
  (0, _react.useEffect)(() => {
    if (!hasIntersectionObserver) {
      if (!visible) {
        const idleCallback = (0, _requestIdleCallback.requestIdleCallback)(() => setVisible(true));
        return () => (0, _requestIdleCallback.cancelIdleCallback)(idleCallback);
      }
    }
  }, [visible]);
  return [setRef, visible];
}

function observe(element, callback, options) {
  const {
    id,
    observer,
    elements
  } = createObserver(options);
  elements.set(element, callback);
  observer.observe(element);
  return function unobserve() {
    elements.delete(element);
    observer.unobserve(element); // Destroy observer when there's nothing left to watch:

    if (elements.size === 0) {
      observer.disconnect();
      observers.delete(id);
    }
  };
}

const observers = new Map();

function createObserver(options) {
  const id = options.rootMargin || '';
  let instance = observers.get(id);

  if (instance) {
    return instance;
  }

  const elements = new Map();
  const observer = new IntersectionObserver(entries => {
    entries.forEach(entry => {
      const callback = elements.get(entry.target);
      const isVisible = entry.isIntersecting || entry.intersectionRatio > 0;

      if (callback && isVisible) {
        callback(isVisible);
      }
    });
  }, options);
  observers.set(id, instance = {
    id,
    observer,
    elements
  });
  return instance;
}

/***/ }),

/***/ 3168:
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__(2426);

exports.__esModule = true;
exports.default = withRouter;

var _react = _interopRequireDefault(__webpack_require__(9297));

var _router = __webpack_require__(2441);

function withRouter(ComposedComponent) {
  function WithRouterWrapper(props) {
    return /*#__PURE__*/_react.default.createElement(ComposedComponent, Object.assign({
      router: (0, _router.useRouter)()
    }, props));
  }

  WithRouterWrapper.getInitialProps = ComposedComponent.getInitialProps // This is needed to allow checking for custom getInitialProps in _app
  ;
  WithRouterWrapper.origGetInitialProps = ComposedComponent.origGetInitialProps;

  if (false) {}

  return WithRouterWrapper;
}

/***/ }),

/***/ 1253:
/***/ (function(__unused_webpack_module, exports) {

"use strict";


exports.__esModule = true;
exports.normalizeLocalePath = normalizeLocalePath;

function normalizeLocalePath(pathname, locales) {
  let detectedLocale; // first item will be empty string from splitting at first char

  const pathnameParts = pathname.split('/');
  (locales || []).some(locale => {
    if (pathnameParts[1].toLowerCase() === locale.toLowerCase()) {
      detectedLocale = locale;
      pathnameParts.splice(1, 1);
      pathname = pathnameParts.join('/') || '/';
      return true;
    }

    return false;
  });
  return {
    pathname,
    detectedLocale
  };
}

/***/ }),

/***/ 7332:
/***/ (function(__unused_webpack_module, exports) {

"use strict";


exports.__esModule = true;
exports.default = mitt;
/*
MIT License
Copyright (c) Jason Miller (https://jasonformat.com/)
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
// This file is based on https://github.com/developit/mitt/blob/v1.1.3/src/index.js
// It's been edited for the needs of this script
// See the LICENSE at the top of the file

function mitt() {
  const all = Object.create(null);
  return {
    on(type, handler) {
      ;
      (all[type] || (all[type] = [])).push(handler);
    },

    off(type, handler) {
      if (all[type]) {
        all[type].splice(all[type].indexOf(handler) >>> 0, 1);
      }
    },

    emit(type, ...evts) {
      // eslint-disable-next-line array-callback-return
      ;
      (all[type] || []).slice().map(handler => {
        handler(...evts);
      });
    }

  };
}

/***/ }),

/***/ 1689:
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.getDomainLocale = getDomainLocale;
exports.addLocale = addLocale;
exports.delLocale = delLocale;
exports.hasBasePath = hasBasePath;
exports.addBasePath = addBasePath;
exports.delBasePath = delBasePath;
exports.isLocalURL = isLocalURL;
exports.interpolateAs = interpolateAs;
exports.resolveHref = resolveHref;
exports.default = void 0;

var _normalizeTrailingSlash = __webpack_require__(6528);

var _routeLoader = __webpack_require__(7599);

var _denormalizePagePath = __webpack_require__(9320);

var _normalizeLocalePath = __webpack_require__(1253);

var _mitt = _interopRequireDefault(__webpack_require__(7332));

var _utils = __webpack_require__(3937);

var _isDynamic = __webpack_require__(3288);

var _parseRelativeUrl = __webpack_require__(4436);

var _querystring = __webpack_require__(4915);

var _resolveRewrites = _interopRequireDefault(__webpack_require__(4453));

var _routeMatcher = __webpack_require__(7451);

var _routeRegex = __webpack_require__(8193);

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    default: obj
  };
} // tslint:disable:no-console


let detectDomainLocale;

if (false) {}

const basePath =  false || '';

function buildCancellationError() {
  return Object.assign(new Error('Route Cancelled'), {
    cancelled: true
  });
}

function addPathPrefix(path, prefix) {
  return prefix && path.startsWith('/') ? path === '/' ? (0, _normalizeTrailingSlash.normalizePathTrailingSlash)(prefix) : `${prefix}${pathNoQueryHash(path) === '/' ? path.substring(1) : path}` : path;
}

function getDomainLocale(path, locale, locales, domainLocales) {
  if (false) {}

  return false;
}

function addLocale(path, locale, defaultLocale) {
  if (false) {}

  return path;
}

function delLocale(path, locale) {
  if (false) {}

  return path;
}

function pathNoQueryHash(path) {
  const queryIndex = path.indexOf('?');
  const hashIndex = path.indexOf('#');

  if (queryIndex > -1 || hashIndex > -1) {
    path = path.substring(0, queryIndex > -1 ? queryIndex : hashIndex);
  }

  return path;
}

function hasBasePath(path) {
  path = pathNoQueryHash(path);
  return path === basePath || path.startsWith(basePath + '/');
}

function addBasePath(path) {
  // we only add the basepath on relative urls
  return addPathPrefix(path, basePath);
}

function delBasePath(path) {
  path = path.slice(basePath.length);
  if (!path.startsWith('/')) path = `/${path}`;
  return path;
}
/**
* Detects whether a given url is routable by the Next.js router (browser only).
*/


function isLocalURL(url) {
  // prevent a hydration mismatch on href for url with anchor refs
  if (url.startsWith('/') || url.startsWith('#')) return true;

  try {
    // absolute urls can be local if they are on the same origin
    const locationOrigin = (0, _utils.getLocationOrigin)();
    const resolved = new URL(url, locationOrigin);
    return resolved.origin === locationOrigin && hasBasePath(resolved.pathname);
  } catch (_) {
    return false;
  }
}

function interpolateAs(route, asPathname, query) {
  let interpolatedRoute = '';
  const dynamicRegex = (0, _routeRegex.getRouteRegex)(route);
  const dynamicGroups = dynamicRegex.groups;
  const dynamicMatches = // Try to match the dynamic route against the asPath
  (asPathname !== route ? (0, _routeMatcher.getRouteMatcher)(dynamicRegex)(asPathname) : '') || // Fall back to reading the values from the href
  // TODO: should this take priority; also need to change in the router.
  query;
  interpolatedRoute = route;
  const params = Object.keys(dynamicGroups);

  if (!params.every(param => {
    let value = dynamicMatches[param] || '';
    const {
      repeat,
      optional
    } = dynamicGroups[param]; // support single-level catch-all
    // TODO: more robust handling for user-error (passing `/`)

    let replaced = `[${repeat ? '...' : ''}${param}]`;

    if (optional) {
      replaced = `${!value ? '/' : ''}[${replaced}]`;
    }

    if (repeat && !Array.isArray(value)) value = [value];
    return (optional || param in dynamicMatches) && ( // Interpolate group into data URL if present
    interpolatedRoute = interpolatedRoute.replace(replaced, repeat ? value.map( // these values should be fully encoded instead of just
    // path delimiter escaped since they are being inserted
    // into the URL and we expect URL encoded segments
    // when parsing dynamic route params
    segment => encodeURIComponent(segment)).join('/') : encodeURIComponent(value)) || '/');
  })) {
    interpolatedRoute = ''; // did not satisfy all requirements
    // n.b. We ignore this error because we handle warning for this case in
    // development in the `<Link>` component directly.
  }

  return {
    params,
    result: interpolatedRoute
  };
}

function omitParmsFromQuery(query, params) {
  const filteredQuery = {};
  Object.keys(query).forEach(key => {
    if (!params.includes(key)) {
      filteredQuery[key] = query[key];
    }
  });
  return filteredQuery;
}
/**
* Resolves a given hyperlink with a certain router state (basePath not included).
* Preserves absolute urls.
*/


function resolveHref(currentPath, href, resolveAs) {
  // we use a dummy base url for relative urls
  const base = new URL(currentPath, 'http://n');
  const urlAsString = typeof href === 'string' ? href : (0, _utils.formatWithValidation)(href); // Return because it cannot be routed by the Next.js router

  if (!isLocalURL(urlAsString)) {
    return resolveAs ? [urlAsString] : urlAsString;
  }

  try {
    const finalUrl = new URL(urlAsString, base);
    finalUrl.pathname = (0, _normalizeTrailingSlash.normalizePathTrailingSlash)(finalUrl.pathname);
    let interpolatedAs = '';

    if ((0, _isDynamic.isDynamicRoute)(finalUrl.pathname) && finalUrl.searchParams && resolveAs) {
      const query = (0, _querystring.searchParamsToUrlQuery)(finalUrl.searchParams);
      const {
        result,
        params
      } = interpolateAs(finalUrl.pathname, finalUrl.pathname, query);

      if (result) {
        interpolatedAs = (0, _utils.formatWithValidation)({
          pathname: result,
          hash: finalUrl.hash,
          query: omitParmsFromQuery(query, params)
        });
      }
    } // if the origin didn't change, it means we received a relative href


    const resolvedHref = finalUrl.origin === base.origin ? finalUrl.href.slice(finalUrl.origin.length) : finalUrl.href;
    return resolveAs ? [resolvedHref, interpolatedAs || resolvedHref] : resolvedHref;
  } catch (_) {
    return resolveAs ? [urlAsString] : urlAsString;
  }
}

function stripOrigin(url) {
  const origin = (0, _utils.getLocationOrigin)();
  return url.startsWith(origin) ? url.substring(origin.length) : url;
}

function prepareUrlAs(router, url, as) {
  // If url and as provided as an object representation,
  // we'll format them into the string version here.
  let [resolvedHref, resolvedAs] = resolveHref(router.pathname, url, true);
  const origin = (0, _utils.getLocationOrigin)();
  const hrefHadOrigin = resolvedHref.startsWith(origin);
  const asHadOrigin = resolvedAs && resolvedAs.startsWith(origin);
  resolvedHref = stripOrigin(resolvedHref);
  resolvedAs = resolvedAs ? stripOrigin(resolvedAs) : resolvedAs;
  const preparedUrl = hrefHadOrigin ? resolvedHref : addBasePath(resolvedHref);
  const preparedAs = as ? stripOrigin(resolveHref(router.pathname, as)) : resolvedAs || resolvedHref;
  return {
    url: preparedUrl,
    as: asHadOrigin ? preparedAs : addBasePath(preparedAs)
  };
}

function resolveDynamicRoute(pathname, pages) {
  const cleanPathname = (0, _normalizeTrailingSlash.removePathTrailingSlash)((0, _denormalizePagePath.denormalizePagePath)(pathname));

  if (cleanPathname === '/404' || cleanPathname === '/_error') {
    return pathname;
  } // handle resolving href for dynamic routes


  if (!pages.includes(cleanPathname)) {
    // eslint-disable-next-line array-callback-return
    pages.some(page => {
      if ((0, _isDynamic.isDynamicRoute)(page) && (0, _routeRegex.getRouteRegex)(page).re.test(cleanPathname)) {
        pathname = page;
        return true;
      }
    });
  }

  return (0, _normalizeTrailingSlash.removePathTrailingSlash)(pathname);
}

const manualScrollRestoration = (/* unused pure expression or super */ null && ( false && 0));
const SSG_DATA_NOT_FOUND = Symbol('SSG_DATA_NOT_FOUND');

function fetchRetry(url, attempts) {
  return fetch(url, {
    // Cookies are required to be present for Next.js' SSG "Preview Mode".
    // Cookies may also be required for `getServerSideProps`.
    //
    // > `fetch` won’t send cookies, unless you set the credentials init
    // > option.
    // https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
    //
    // > For maximum browser compatibility when it comes to sending &
    // > receiving cookies, always supply the `credentials: 'same-origin'`
    // > option instead of relying on the default.
    // https://github.com/github/fetch#caveats
    credentials: 'same-origin'
  }).then(res => {
    if (!res.ok) {
      if (attempts > 1 && res.status >= 500) {
        return fetchRetry(url, attempts - 1);
      }

      if (res.status === 404) {
        return res.json().then(data => {
          if (data.notFound) {
            return {
              notFound: SSG_DATA_NOT_FOUND
            };
          }

          throw new Error(`Failed to load static props`);
        });
      }

      throw new Error(`Failed to load static props`);
    }

    return res.json();
  });
}

function fetchNextData(dataHref, isServerRender) {
  return fetchRetry(dataHref, isServerRender ? 3 : 1).catch(err => {
    // We should only trigger a server-side transition if this was caused
    // on a client-side transition. Otherwise, we'd get into an infinite
    // loop.
    if (!isServerRender) {
      (0, _routeLoader.markAssetError)(err);
    }

    throw err;
  });
}

class Router {
  /**
  * Map of all components loaded in `Router`
  */
  // Static Data Cache
  // In-flight Server Data Requests, for deduping
  constructor(_pathname, _query, _as, {
    initialProps,
    pageLoader,
    App,
    wrapApp,
    Component,
    err,
    subscription,
    isFallback,
    locale,
    locales,
    defaultLocale,
    domainLocales,
    isPreview
  }) {
    this.route = void 0;
    this.pathname = void 0;
    this.query = void 0;
    this.asPath = void 0;
    this.basePath = void 0;
    this.components = void 0;
    this.sdc = {};
    this.sdr = {};
    this.sub = void 0;
    this.clc = void 0;
    this.pageLoader = void 0;
    this._bps = void 0;
    this.events = void 0;
    this._wrapApp = void 0;
    this.isSsr = void 0;
    this.isFallback = void 0;
    this._inFlightRoute = void 0;
    this._shallow = void 0;
    this.locale = void 0;
    this.locales = void 0;
    this.defaultLocale = void 0;
    this.domainLocales = void 0;
    this.isReady = void 0;
    this.isPreview = void 0;
    this.isLocaleDomain = void 0;
    this._idx = 0;

    this.onPopState = e => {
      const state = e.state;

      if (!state) {
        // We get state as undefined for two reasons.
        //  1. With older safari (< 8) and older chrome (< 34)
        //  2. When the URL changed with #
        //
        // In the both cases, we don't need to proceed and change the route.
        // (as it's already changed)
        // But we can simply replace the state with the new changes.
        // Actually, for (1) we don't need to nothing. But it's hard to detect that event.
        // So, doing the following for (1) does no harm.
        const {
          pathname,
          query
        } = this;
        this.changeState('replaceState', (0, _utils.formatWithValidation)({
          pathname: addBasePath(pathname),
          query
        }), (0, _utils.getURL)());
        return;
      }

      if (!state.__N) {
        return;
      }

      let forcedScroll;
      const {
        url,
        as,
        options,
        idx
      } = state;

      if (false) {}

      this._idx = idx;
      const {
        pathname
      } = (0, _parseRelativeUrl.parseRelativeUrl)(url); // Make sure we don't re-render on initial load,
      // can be caused by navigating back from an external site

      if (this.isSsr && as === this.asPath && pathname === this.pathname) {
        return;
      } // If the downstream application returns falsy, return.
      // They will then be responsible for handling the event.


      if (this._bps && !this._bps(state)) {
        return;
      }

      this.change('replaceState', url, as, Object.assign({}, options, {
        shallow: options.shallow && this._shallow,
        locale: options.locale || this.defaultLocale
      }), forcedScroll);
    }; // represents the current component key


    this.route = (0, _normalizeTrailingSlash.removePathTrailingSlash)(_pathname); // set up the component cache (by route keys)

    this.components = {}; // We should not keep the cache, if there's an error
    // Otherwise, this cause issues when when going back and
    // come again to the errored page.

    if (_pathname !== '/_error') {
      this.components[this.route] = {
        Component,
        initial: true,
        props: initialProps,
        err,
        __N_SSG: initialProps && initialProps.__N_SSG,
        __N_SSP: initialProps && initialProps.__N_SSP
      };
    }

    this.components['/_app'] = {
      Component: App,
      styleSheets: [
        /* /_app does not need its stylesheets managed */
      ]
    }; // Backwards compat for Router.router.events
    // TODO: Should be remove the following major version as it was never documented

    this.events = Router.events;
    this.pageLoader = pageLoader;
    this.pathname = _pathname;
    this.query = _query; // if auto prerendered and dynamic route wait to update asPath
    // until after mount to prevent hydration mismatch

    const autoExportDynamic = (0, _isDynamic.isDynamicRoute)(_pathname) && self.__NEXT_DATA__.autoExport;

    this.asPath = autoExportDynamic ? _pathname : _as;
    this.basePath = basePath;
    this.sub = subscription;
    this.clc = null;
    this._wrapApp = wrapApp; // make sure to ignore extra popState in safari on navigating
    // back from external site

    this.isSsr = true;
    this.isFallback = isFallback;
    this.isReady = !!(self.__NEXT_DATA__.gssp || self.__NEXT_DATA__.gip || !autoExportDynamic && !self.location.search && !false);
    this.isPreview = !!isPreview;
    this.isLocaleDomain = false;

    if (false) {}

    if (false) {}
  }

  reload() {
    window.location.reload();
  }
  /**
  * Go back in history
  */


  back() {
    window.history.back();
  }
  /**
  * Performs a `pushState` with arguments
  * @param url of the route
  * @param as masks `url` for the browser
  * @param options object you can define `shallow` and other options
  */


  push(url, as, options = {}) {
    if (false) {}

    ;
    ({
      url,
      as
    } = prepareUrlAs(this, url, as));
    return this.change('pushState', url, as, options);
  }
  /**
  * Performs a `replaceState` with arguments
  * @param url of the route
  * @param as masks `url` for the browser
  * @param options object you can define `shallow` and other options
  */


  replace(url, as, options = {}) {
    ;
    ({
      url,
      as
    } = prepareUrlAs(this, url, as));
    return this.change('replaceState', url, as, options);
  }

  async change(method, url, as, options, forcedScroll) {
    var _options$scroll;

    if (!isLocalURL(url)) {
      window.location.href = url;
      return false;
    } // for static pages with query params in the URL we delay
    // marking the router ready until after the query is updated


    if (options._h) {
      this.isReady = true;
    } // Default to scroll reset behavior unless explicitly specified to be
    // `false`! This makes the behavior between using `Router#push` and a
    // `<Link />` consistent.


    options.scroll = !!((_options$scroll = options.scroll) != null ? _options$scroll : true);
    let localeChange = options.locale !== this.locale;

    if (false) { var _this$locales; }

    if (!options._h) {
      this.isSsr = false;
    } // marking route changes as a navigation start entry


    if (_utils.ST) {
      performance.mark('routeChange');
    }

    const {
      shallow = false
    } = options;
    const routeProps = {
      shallow
    };

    if (this._inFlightRoute) {
      this.abortComponentLoad(this._inFlightRoute, routeProps);
    }

    as = addBasePath(addLocale(hasBasePath(as) ? delBasePath(as) : as, options.locale, this.defaultLocale));
    const cleanedAs = delLocale(hasBasePath(as) ? delBasePath(as) : as, this.locale);
    this._inFlightRoute = as; // If the url change is only related to a hash change
    // We should not proceed. We should only change the state.
    // WARNING: `_h` is an internal option for handing Next.js client-side
    // hydration. Your app should _never_ use this property. It may change at
    // any time without notice.

    if (!options._h && this.onlyAHashChange(cleanedAs)) {
      this.asPath = cleanedAs;
      Router.events.emit('hashChangeStart', as, routeProps); // TODO: do we need the resolved href when only a hash change?

      this.changeState(method, url, as, options);
      this.scrollToHash(cleanedAs);
      this.notify(this.components[this.route], null);
      Router.events.emit('hashChangeComplete', as, routeProps);
      return true;
    }

    let parsed = (0, _parseRelativeUrl.parseRelativeUrl)(url);
    let {
      pathname,
      query
    } = parsed; // The build manifest needs to be loaded before auto-static dynamic pages
    // get their query parameters to allow ensuring they can be parsed properly
    // when rewritten to

    let pages, rewrites;

    try {
      pages = await this.pageLoader.getPageList();
      ({
        __rewrites: rewrites
      } = await (0, _routeLoader.getClientBuildManifest)());
    } catch (err) {
      // If we fail to resolve the page list or client-build manifest, we must
      // do a server-side transition:
      window.location.href = as;
      return false;
    } // If asked to change the current URL we should reload the current page
    // (not location.reload() but reload getInitialProps and other Next.js stuffs)
    // We also need to set the method = replaceState always
    // as this should not go into the history (That's how browsers work)
    // We should compare the new asPath to the current asPath, not the url


    if (!this.urlIsNew(cleanedAs) && !localeChange) {
      method = 'replaceState';
    } // we need to resolve the as value using rewrites for dynamic SSG
    // pages to allow building the data URL correctly


    let resolvedAs = as; // url and as should always be prefixed with basePath by this
    // point by either next/link or router.push/replace so strip the
    // basePath from the pathname to match the pages dir 1-to-1

    pathname = pathname ? (0, _normalizeTrailingSlash.removePathTrailingSlash)(delBasePath(pathname)) : pathname;

    if (pathname !== '/_error') {
      if (false) {} else {
        parsed.pathname = resolveDynamicRoute(pathname, pages);

        if (parsed.pathname !== pathname) {
          pathname = parsed.pathname;
          url = (0, _utils.formatWithValidation)(parsed);
        }
      }
    }

    const route = (0, _normalizeTrailingSlash.removePathTrailingSlash)(pathname);

    if (!isLocalURL(as)) {
      if (false) {}

      window.location.href = as;
      return false;
    }

    resolvedAs = delLocale(delBasePath(resolvedAs), this.locale);

    if ((0, _isDynamic.isDynamicRoute)(route)) {
      const parsedAs = (0, _parseRelativeUrl.parseRelativeUrl)(resolvedAs);
      const asPathname = parsedAs.pathname;
      const routeRegex = (0, _routeRegex.getRouteRegex)(route);
      const routeMatch = (0, _routeMatcher.getRouteMatcher)(routeRegex)(asPathname);
      const shouldInterpolate = route === asPathname;
      const interpolatedAs = shouldInterpolate ? interpolateAs(route, asPathname, query) : {};

      if (!routeMatch || shouldInterpolate && !interpolatedAs.result) {
        const missingParams = Object.keys(routeRegex.groups).filter(param => !query[param]);

        if (missingParams.length > 0) {
          if (false) {}

          throw new Error((shouldInterpolate ? `The provided \`href\` (${url}) value is missing query values (${missingParams.join(', ')}) to be interpolated properly. ` : `The provided \`as\` value (${asPathname}) is incompatible with the \`href\` value (${route}). `) + `Read more: https://nextjs.org/docs/messages/${shouldInterpolate ? 'href-interpolation-failed' : 'incompatible-href-as'}`);
        }
      } else if (shouldInterpolate) {
        as = (0, _utils.formatWithValidation)(Object.assign({}, parsedAs, {
          pathname: interpolatedAs.result,
          query: omitParmsFromQuery(query, interpolatedAs.params)
        }));
      } else {
        // Merge params into `query`, overwriting any specified in search
        Object.assign(query, routeMatch);
      }
    }

    Router.events.emit('routeChangeStart', as, routeProps);

    try {
      var _self$__NEXT_DATA__$p, _self$__NEXT_DATA__$p2;

      let routeInfo = await this.getRouteInfo(route, pathname, query, as, resolvedAs, routeProps);
      let {
        error,
        props,
        __N_SSG,
        __N_SSP
      } = routeInfo; // handle redirect on client-transition

      if ((__N_SSG || __N_SSP) && props) {
        if (props.pageProps && props.pageProps.__N_REDIRECT) {
          const destination = props.pageProps.__N_REDIRECT; // check if destination is internal (resolves to a page) and attempt
          // client-navigation if it is falling back to hard navigation if
          // it's not

          if (destination.startsWith('/')) {
            const parsedHref = (0, _parseRelativeUrl.parseRelativeUrl)(destination);
            parsedHref.pathname = resolveDynamicRoute(parsedHref.pathname, pages);

            if (pages.includes(parsedHref.pathname)) {
              const {
                url: newUrl,
                as: newAs
              } = prepareUrlAs(this, destination, destination);
              return this.change(method, newUrl, newAs, options);
            }
          }

          window.location.href = destination;
          return new Promise(() => {});
        }

        this.isPreview = !!props.__N_PREVIEW; // handle SSG data 404

        if (props.notFound === SSG_DATA_NOT_FOUND) {
          let notFoundRoute;

          try {
            await this.fetchComponent('/404');
            notFoundRoute = '/404';
          } catch (_) {
            notFoundRoute = '/_error';
          }

          routeInfo = await this.getRouteInfo(notFoundRoute, notFoundRoute, query, as, resolvedAs, {
            shallow: false
          });
        }
      }

      Router.events.emit('beforeHistoryChange', as, routeProps);
      this.changeState(method, url, as, options);

      if (false) {} // shallow routing is only allowed for same page URL changes.


      const isValidShallowRoute = options.shallow && this.route === route;

      if (options._h && pathname === '/_error' && ((_self$__NEXT_DATA__$p = self.__NEXT_DATA__.props) == null ? void 0 : (_self$__NEXT_DATA__$p2 = _self$__NEXT_DATA__$p.pageProps) == null ? void 0 : _self$__NEXT_DATA__$p2.statusCode) === 500 && props != null && props.pageProps) {
        // ensure statusCode is still correct for static 500 page
        // when updating query information
        props.pageProps.statusCode = 500;
      }

      await this.set(route, pathname, query, cleanedAs, routeInfo, forcedScroll || (isValidShallowRoute || !options.scroll ? null : {
        x: 0,
        y: 0
      })).catch(e => {
        if (e.cancelled) error = error || e;else throw e;
      });

      if (error) {
        Router.events.emit('routeChangeError', error, cleanedAs, routeProps);
        throw error;
      }

      if (false) {}

      Router.events.emit('routeChangeComplete', as, routeProps);
      return true;
    } catch (err) {
      if (err.cancelled) {
        return false;
      }

      throw err;
    }
  }

  changeState(method, url, as, options = {}) {
    if (false) {}

    if (method !== 'pushState' || (0, _utils.getURL)() !== as) {
      this._shallow = options.shallow;
      window.history[method]({
        url,
        as,
        options,
        __N: true,
        idx: this._idx = method !== 'pushState' ? this._idx : this._idx + 1
      }, // Most browsers currently ignores this parameter, although they may use it in the future.
      // Passing the empty string here should be safe against future changes to the method.
      // https://developer.mozilla.org/en-US/docs/Web/API/History/replaceState
      '', as);
    }
  }

  async handleRouteInfoError(err, pathname, query, as, routeProps, loadErrorFail) {
    if (err.cancelled) {
      // bubble up cancellation errors
      throw err;
    }

    if ((0, _routeLoader.isAssetError)(err) || loadErrorFail) {
      Router.events.emit('routeChangeError', err, as, routeProps); // If we can't load the page it could be one of following reasons
      //  1. Page doesn't exists
      //  2. Page does exist in a different zone
      //  3. Internal error while loading the page
      // So, doing a hard reload is the proper way to deal with this.

      window.location.href = as; // Changing the URL doesn't block executing the current code path.
      // So let's throw a cancellation error stop the routing logic.

      throw buildCancellationError();
    }

    try {
      let Component;
      let styleSheets;
      let props;

      if (typeof Component === 'undefined' || typeof styleSheets === 'undefined') {
        ;
        ({
          page: Component,
          styleSheets
        } = await this.fetchComponent('/_error'));
      }

      const routeInfo = {
        props,
        Component,
        styleSheets,
        err,
        error: err
      };

      if (!routeInfo.props) {
        try {
          routeInfo.props = await this.getInitialProps(Component, {
            err,
            pathname,
            query
          });
        } catch (gipErr) {
          console.error('Error in error page `getInitialProps`: ', gipErr);
          routeInfo.props = {};
        }
      }

      return routeInfo;
    } catch (routeInfoErr) {
      return this.handleRouteInfoError(routeInfoErr, pathname, query, as, routeProps, true);
    }
  }

  async getRouteInfo(route, pathname, query, as, resolvedAs, routeProps) {
    try {
      const existingRouteInfo = this.components[route];

      if (routeProps.shallow && existingRouteInfo && this.route === route) {
        return existingRouteInfo;
      }

      const cachedRouteInfo = existingRouteInfo && 'initial' in existingRouteInfo ? undefined : existingRouteInfo;
      const routeInfo = cachedRouteInfo ? cachedRouteInfo : await this.fetchComponent(route).then(res => ({
        Component: res.page,
        styleSheets: res.styleSheets,
        __N_SSG: res.mod.__N_SSG,
        __N_SSP: res.mod.__N_SSP
      }));
      const {
        Component,
        __N_SSG,
        __N_SSP
      } = routeInfo;

      if (false) {}

      let dataHref;

      if (__N_SSG || __N_SSP) {
        dataHref = this.pageLoader.getDataHref((0, _utils.formatWithValidation)({
          pathname,
          query
        }), resolvedAs, __N_SSG, this.locale);
      }

      const props = await this._getData(() => __N_SSG ? this._getStaticData(dataHref) : __N_SSP ? this._getServerData(dataHref) : this.getInitialProps(Component, // we provide AppTree later so this needs to be `any`
      {
        pathname,
        query,
        asPath: as
      }));
      routeInfo.props = props;
      this.components[route] = routeInfo;
      return routeInfo;
    } catch (err) {
      return this.handleRouteInfoError(err, pathname, query, as, routeProps);
    }
  }

  set(route, pathname, query, as, data, resetScroll) {
    this.isFallback = false;
    this.route = route;
    this.pathname = pathname;
    this.query = query;
    this.asPath = as;
    return this.notify(data, resetScroll);
  }
  /**
  * Callback to execute before replacing router state
  * @param cb callback to be executed
  */


  beforePopState(cb) {
    this._bps = cb;
  }

  onlyAHashChange(as) {
    if (!this.asPath) return false;
    const [oldUrlNoHash, oldHash] = this.asPath.split('#');
    const [newUrlNoHash, newHash] = as.split('#'); // Makes sure we scroll to the provided hash if the url/hash are the same

    if (newHash && oldUrlNoHash === newUrlNoHash && oldHash === newHash) {
      return true;
    } // If the urls are change, there's more than a hash change


    if (oldUrlNoHash !== newUrlNoHash) {
      return false;
    } // If the hash has changed, then it's a hash only change.
    // This check is necessary to handle both the enter and
    // leave hash === '' cases. The identity case falls through
    // and is treated as a next reload.


    return oldHash !== newHash;
  }

  scrollToHash(as) {
    const [, hash] = as.split('#'); // Scroll to top if the hash is just `#` with no value or `#top`
    // To mirror browsers

    if (hash === '' || hash === 'top') {
      window.scrollTo(0, 0);
      return;
    } // First we check if the element by id is found


    const idEl = document.getElementById(hash);

    if (idEl) {
      idEl.scrollIntoView();
      return;
    } // If there's no element with the id, we check the `name` property
    // To mirror browsers


    const nameEl = document.getElementsByName(hash)[0];

    if (nameEl) {
      nameEl.scrollIntoView();
    }
  }

  urlIsNew(asPath) {
    return this.asPath !== asPath;
  }
  /**
  * Prefetch page code, you may wait for the data during page rendering.
  * This feature only works in production!
  * @param url the href of prefetched page
  * @param asPath the as path of the prefetched page
  */


  async prefetch(url, asPath = url, options = {}) {
    let parsed = (0, _parseRelativeUrl.parseRelativeUrl)(url);
    let {
      pathname
    } = parsed;

    if (false) {}

    const pages = await this.pageLoader.getPageList();
    let resolvedAs = asPath;

    if (false) {} else {
      parsed.pathname = resolveDynamicRoute(parsed.pathname, pages);

      if (parsed.pathname !== pathname) {
        pathname = parsed.pathname;
        url = (0, _utils.formatWithValidation)(parsed);
      }
    }

    const route = (0, _normalizeTrailingSlash.removePathTrailingSlash)(pathname); // Prefetch is not supported in development mode because it would trigger on-demand-entries

    if (false) {}

    await Promise.all([this.pageLoader._isSsg(route).then(isSsg => {
      return isSsg ? this._getStaticData(this.pageLoader.getDataHref(url, resolvedAs, true, typeof options.locale !== 'undefined' ? options.locale : this.locale)) : false;
    }), this.pageLoader[options.priority ? 'loadPage' : 'prefetch'](route)]);
  }

  async fetchComponent(route) {
    let cancelled = false;

    const cancel = this.clc = () => {
      cancelled = true;
    };

    const componentResult = await this.pageLoader.loadPage(route);

    if (cancelled) {
      const error = new Error(`Abort fetching component for route: "${route}"`);
      error.cancelled = true;
      throw error;
    }

    if (cancel === this.clc) {
      this.clc = null;
    }

    return componentResult;
  }

  _getData(fn) {
    let cancelled = false;

    const cancel = () => {
      cancelled = true;
    };

    this.clc = cancel;
    return fn().then(data => {
      if (cancel === this.clc) {
        this.clc = null;
      }

      if (cancelled) {
        const err = new Error('Loading initial props cancelled');
        err.cancelled = true;
        throw err;
      }

      return data;
    });
  }

  _getStaticData(dataHref) {
    const {
      href: cacheKey
    } = new URL(dataHref, window.location.href);

    if ( true && !this.isPreview && this.sdc[cacheKey]) {
      return Promise.resolve(this.sdc[cacheKey]);
    }

    return fetchNextData(dataHref, this.isSsr).then(data => {
      this.sdc[cacheKey] = data;
      return data;
    });
  }

  _getServerData(dataHref) {
    const {
      href: resourceKey
    } = new URL(dataHref, window.location.href);

    if (this.sdr[resourceKey]) {
      return this.sdr[resourceKey];
    }

    return this.sdr[resourceKey] = fetchNextData(dataHref, this.isSsr).then(data => {
      delete this.sdr[resourceKey];
      return data;
    }).catch(err => {
      delete this.sdr[resourceKey];
      throw err;
    });
  }

  getInitialProps(Component, ctx) {
    const {
      Component: App
    } = this.components['/_app'];

    const AppTree = this._wrapApp(App);

    ctx.AppTree = AppTree;
    return (0, _utils.loadGetInitialProps)(App, {
      AppTree,
      Component,
      router: this,
      ctx
    });
  }

  abortComponentLoad(as, routeProps) {
    if (this.clc) {
      Router.events.emit('routeChangeError', buildCancellationError(), as, routeProps);
      this.clc();
      this.clc = null;
    }
  }

  notify(data, resetScroll) {
    return this.sub(data, this.components['/_app'].Component, resetScroll);
  }

}

exports.default = Router;
Router.events = (0, _mitt.default)();

/***/ }),

/***/ 7687:
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.formatUrl = formatUrl;

var querystring = _interopRequireWildcard(__webpack_require__(4915));

function _getRequireWildcardCache() {
  if (typeof WeakMap !== "function") return null;
  var cache = new WeakMap();

  _getRequireWildcardCache = function () {
    return cache;
  };

  return cache;
}

function _interopRequireWildcard(obj) {
  if (obj && obj.__esModule) {
    return obj;
  }

  if (obj === null || typeof obj !== "object" && typeof obj !== "function") {
    return {
      default: obj
    };
  }

  var cache = _getRequireWildcardCache();

  if (cache && cache.has(obj)) {
    return cache.get(obj);
  }

  var newObj = {};
  var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor;

  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null;

      if (desc && (desc.get || desc.set)) {
        Object.defineProperty(newObj, key, desc);
      } else {
        newObj[key] = obj[key];
      }
    }
  }

  newObj.default = obj;

  if (cache) {
    cache.set(obj, newObj);
  }

  return newObj;
} // Format function modified from nodejs
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.


const slashedProtocols = /https?|ftp|gopher|file/;

function formatUrl(urlObj) {
  let {
    auth,
    hostname
  } = urlObj;
  let protocol = urlObj.protocol || '';
  let pathname = urlObj.pathname || '';
  let hash = urlObj.hash || '';
  let query = urlObj.query || '';
  let host = false;
  auth = auth ? encodeURIComponent(auth).replace(/%3A/i, ':') + '@' : '';

  if (urlObj.host) {
    host = auth + urlObj.host;
  } else if (hostname) {
    host = auth + (~hostname.indexOf(':') ? `[${hostname}]` : hostname);

    if (urlObj.port) {
      host += ':' + urlObj.port;
    }
  }

  if (query && typeof query === 'object') {
    query = String(querystring.urlQueryToSearchParams(query));
  }

  let search = urlObj.search || query && `?${query}` || '';
  if (protocol && protocol.substr(-1) !== ':') protocol += ':';

  if (urlObj.slashes || (!protocol || slashedProtocols.test(protocol)) && host !== false) {
    host = '//' + (host || '');
    if (pathname && pathname[0] !== '/') pathname = '/' + pathname;
  } else if (!host) {
    host = '';
  }

  if (hash && hash[0] !== '#') hash = '#' + hash;
  if (search && search[0] !== '?') search = '?' + search;
  pathname = pathname.replace(/[?#]/g, encodeURIComponent);
  search = search.replace('#', '%23');
  return `${protocol}${host}${pathname}${search}${hash}`;
}

/***/ }),

/***/ 3288:
/***/ (function(__unused_webpack_module, exports) {

"use strict";


exports.__esModule = true;
exports.isDynamicRoute = isDynamicRoute; // Identify /[param]/ in route string

const TEST_ROUTE = /\/\[[^/]+?\](?=\/|$)/;

function isDynamicRoute(route) {
  return TEST_ROUTE.test(route);
}

/***/ }),

/***/ 4436:
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.parseRelativeUrl = parseRelativeUrl;

var _utils = __webpack_require__(3937);

var _querystring = __webpack_require__(4915);
/**
* Parses path-relative urls (e.g. `/hello/world?foo=bar`). If url isn't path-relative
* (e.g. `./hello`) then at least base must be.
* Absolute urls are rejected with one exception, in the browser, absolute urls that are on
* the current origin will be parsed as relative
*/


function parseRelativeUrl(url, base) {
  const globalBase = new URL( true ? 'http://n' : 0);
  const resolvedBase = base ? new URL(base, globalBase) : globalBase;
  const {
    pathname,
    searchParams,
    search,
    hash,
    href,
    origin
  } = new URL(url, resolvedBase);

  if (origin !== globalBase.origin) {
    throw new Error(`invariant: invalid relative URL, router received ${url}`);
  }

  return {
    pathname,
    query: (0, _querystring.searchParamsToUrlQuery)(searchParams),
    search,
    hash,
    href: href.slice(globalBase.origin.length)
  };
}

/***/ }),

/***/ 4915:
/***/ (function(__unused_webpack_module, exports) {

"use strict";


exports.__esModule = true;
exports.searchParamsToUrlQuery = searchParamsToUrlQuery;
exports.urlQueryToSearchParams = urlQueryToSearchParams;
exports.assign = assign;

function searchParamsToUrlQuery(searchParams) {
  const query = {};
  searchParams.forEach((value, key) => {
    if (typeof query[key] === 'undefined') {
      query[key] = value;
    } else if (Array.isArray(query[key])) {
      ;
      query[key].push(value);
    } else {
      query[key] = [query[key], value];
    }
  });
  return query;
}

function stringifyUrlQueryParam(param) {
  if (typeof param === 'string' || typeof param === 'number' && !isNaN(param) || typeof param === 'boolean') {
    return String(param);
  } else {
    return '';
  }
}

function urlQueryToSearchParams(urlQuery) {
  const result = new URLSearchParams();
  Object.entries(urlQuery).forEach(([key, value]) => {
    if (Array.isArray(value)) {
      value.forEach(item => result.append(key, stringifyUrlQueryParam(item)));
    } else {
      result.set(key, stringifyUrlQueryParam(value));
    }
  });
  return result;
}

function assign(target, ...searchParamsList) {
  searchParamsList.forEach(searchParams => {
    Array.from(searchParams.keys()).forEach(key => target.delete(key));
    searchParams.forEach((value, key) => target.append(key, value));
  });
  return target;
}

/***/ }),

/***/ 7451:
/***/ (function(__unused_webpack_module, exports) {

"use strict";


exports.__esModule = true;
exports.getRouteMatcher = getRouteMatcher;

function getRouteMatcher(routeRegex) {
  const {
    re,
    groups
  } = routeRegex;
  return pathname => {
    const routeMatch = re.exec(pathname);

    if (!routeMatch) {
      return false;
    }

    const decode = param => {
      try {
        return decodeURIComponent(param);
      } catch (_) {
        const err = new Error('failed to decode param');
        err.code = 'DECODE_FAILED';
        throw err;
      }
    };

    const params = {};
    Object.keys(groups).forEach(slugName => {
      const g = groups[slugName];
      const m = routeMatch[g.pos];

      if (m !== undefined) {
        params[slugName] = ~m.indexOf('/') ? m.split('/').map(entry => decode(entry)) : g.repeat ? [decode(m)] : decode(m);
      }
    });
    return params;
  };
}

/***/ }),

/***/ 8193:
/***/ (function(__unused_webpack_module, exports) {

"use strict";


exports.__esModule = true;
exports.getRouteRegex = getRouteRegex; // this isn't importing the escape-string-regex module
// to reduce bytes

function escapeRegex(str) {
  return str.replace(/[|\\{}()[\]^$+*?.-]/g, '\\$&');
}

function parseParameter(param) {
  const optional = param.startsWith('[') && param.endsWith(']');

  if (optional) {
    param = param.slice(1, -1);
  }

  const repeat = param.startsWith('...');

  if (repeat) {
    param = param.slice(3);
  }

  return {
    key: param,
    repeat,
    optional
  };
}

function getRouteRegex(normalizedRoute) {
  const segments = (normalizedRoute.replace(/\/$/, '') || '/').slice(1).split('/');
  const groups = {};
  let groupIndex = 1;
  const parameterizedRoute = segments.map(segment => {
    if (segment.startsWith('[') && segment.endsWith(']')) {
      const {
        key,
        optional,
        repeat
      } = parseParameter(segment.slice(1, -1));
      groups[key] = {
        pos: groupIndex++,
        repeat,
        optional
      };
      return repeat ? optional ? '(?:/(.+?))?' : '/(.+?)' : '/([^/]+?)';
    } else {
      return `/${escapeRegex(segment)}`;
    }
  }).join(''); // dead code eliminate for browser since it's only needed
  // while generating routes-manifest

  if (true) {
    let routeKeyCharCode = 97;
    let routeKeyCharLength = 1; // builds a minimal routeKey using only a-z and minimal number of characters

    const getSafeRouteKey = () => {
      let routeKey = '';

      for (let i = 0; i < routeKeyCharLength; i++) {
        routeKey += String.fromCharCode(routeKeyCharCode);
        routeKeyCharCode++;

        if (routeKeyCharCode > 122) {
          routeKeyCharLength++;
          routeKeyCharCode = 97;
        }
      }

      return routeKey;
    };

    const routeKeys = {};
    let namedParameterizedRoute = segments.map(segment => {
      if (segment.startsWith('[') && segment.endsWith(']')) {
        const {
          key,
          optional,
          repeat
        } = parseParameter(segment.slice(1, -1)); // replace any non-word characters since they can break
        // the named regex

        let cleanedKey = key.replace(/\W/g, '');
        let invalidKey = false; // check if the key is still invalid and fallback to using a known
        // safe key

        if (cleanedKey.length === 0 || cleanedKey.length > 30) {
          invalidKey = true;
        }

        if (!isNaN(parseInt(cleanedKey.substr(0, 1)))) {
          invalidKey = true;
        }

        if (invalidKey) {
          cleanedKey = getSafeRouteKey();
        }

        routeKeys[cleanedKey] = key;
        return repeat ? optional ? `(?:/(?<${cleanedKey}>.+?))?` : `/(?<${cleanedKey}>.+?)` : `/(?<${cleanedKey}>[^/]+?)`;
      } else {
        return `/${escapeRegex(segment)}`;
      }
    }).join('');
    return {
      re: new RegExp(`^${parameterizedRoute}(?:/)?$`),
      groups,
      routeKeys,
      namedRegex: `^${namedParameterizedRoute}(?:/)?$`
    };
  }

  return {
    re: new RegExp(`^${parameterizedRoute}(?:/)?$`),
    groups
  };
}

/***/ }),

/***/ 3937:
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.execOnce = execOnce;
exports.getLocationOrigin = getLocationOrigin;
exports.getURL = getURL;
exports.getDisplayName = getDisplayName;
exports.isResSent = isResSent;
exports.loadGetInitialProps = loadGetInitialProps;
exports.formatWithValidation = formatWithValidation;
exports.ST = exports.SP = exports.urlObjectKeys = void 0;

var _formatUrl = __webpack_require__(7687);
/**
* Utils
*/


function execOnce(fn) {
  let used = false;
  let result;
  return (...args) => {
    if (!used) {
      used = true;
      result = fn(...args);
    }

    return result;
  };
}

function getLocationOrigin() {
  const {
    protocol,
    hostname,
    port
  } = window.location;
  return `${protocol}//${hostname}${port ? ':' + port : ''}`;
}

function getURL() {
  const {
    href
  } = window.location;
  const origin = getLocationOrigin();
  return href.substring(origin.length);
}

function getDisplayName(Component) {
  return typeof Component === 'string' ? Component : Component.displayName || Component.name || 'Unknown';
}

function isResSent(res) {
  return res.finished || res.headersSent;
}

async function loadGetInitialProps(App, ctx) {
  if (false) { var _App$prototype; } // when called from _app `ctx` is nested in `ctx`


  const res = ctx.res || ctx.ctx && ctx.ctx.res;

  if (!App.getInitialProps) {
    if (ctx.ctx && ctx.Component) {
      // @ts-ignore pageProps default
      return {
        pageProps: await loadGetInitialProps(ctx.Component, ctx.ctx)
      };
    }

    return {};
  }

  const props = await App.getInitialProps(ctx);

  if (res && isResSent(res)) {
    return props;
  }

  if (!props) {
    const message = `"${getDisplayName(App)}.getInitialProps()" should resolve to an object. But found "${props}" instead.`;
    throw new Error(message);
  }

  if (false) {}

  return props;
}

const urlObjectKeys = ['auth', 'hash', 'host', 'hostname', 'href', 'path', 'pathname', 'port', 'protocol', 'query', 'search', 'slashes'];
exports.urlObjectKeys = urlObjectKeys;

function formatWithValidation(url) {
  if (false) {}

  return (0, _formatUrl.formatUrl)(url);
}

const SP = typeof performance !== 'undefined';
exports.SP = SP;
const ST = SP && typeof performance.mark === 'function' && typeof performance.measure === 'function';
exports.ST = ST;

/***/ }),

/***/ 1752:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var components_BoxNumber__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5878);
/* harmony import */ var components_BoxNumber_Content__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5404);
/* harmony import */ var components_share_common_EmptyLoader__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(2811);
/* harmony import */ var components_share_common_NavBar__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(4020);
/* harmony import */ var config_BaseApi__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(9900);
/* harmony import */ var constants_lottery__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(1377);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(1664);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var react_infinite_scroll_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(4885);
/* harmony import */ var react_infinite_scroll_component__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react_infinite_scroll_component__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(79);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var store__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(7405);
/* harmony import */ var store__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(store__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var store_lottery__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(2360);
/* harmony import */ var utils_functions__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(6187);




function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }














const PAGE_NUMBER = 1;

const History = () => {
  let state = (0,react_redux__WEBPACK_IMPORTED_MODULE_10__.useSelector)(state => state.lottery.lotteryHistory);

  const listType = _objectSpread({}, constants_lottery__WEBPACK_IMPORTED_MODULE_6__/* .LIST_TYPE_LOTTERY */ .G7);

  const {
    0: dataTable,
    1: setDataTable
  } = (0,react__WEBPACK_IMPORTED_MODULE_8__.useState)([]);
  const {
    0: page,
    1: setPage
  } = (0,react__WEBPACK_IMPORTED_MODULE_8__.useState)(PAGE_NUMBER);
  const {
    0: variableTab,
    1: setVariableTab
  } = (0,react__WEBPACK_IMPORTED_MODULE_8__.useState)(state.variableTab);
  const {
    0: totalResult,
    1: setTotalResult
  } = (0,react__WEBPACK_IMPORTED_MODULE_8__.useState)(null);
  const {
    0: tabs,
    1: setTabs
  } = (0,react__WEBPACK_IMPORTED_MODULE_8__.useState)([{
    title: "Keno",
    value: "keno"
  }, {
    title: "Mega",
    value: "mega"
  }, {
    title: "Power",
    value: "power"
  }, {
    title: "3 Miền",
    value: "share"
  }]);

  const handleClickButton = (item, index) => () => {
    try {
      fetchApi(item.value);
      setVariableTab(item.value);
      setPage(1);
      store__WEBPACK_IMPORTED_MODULE_11___default().dispatch((0,store_lottery__WEBPACK_IMPORTED_MODULE_12__/* .setLotteryHistory */ .CC)({
        variableTab: item.value
      }));
    } catch (error) {
      console.log(error);
    }
  };

  const fetchApi = async variableTab => {
    try {
      const data = await config_BaseApi__WEBPACK_IMPORTED_MODULE_5__/* .default.get */ .Z.get("lottery/filter", {
        params: {
          lotteryType: variableTab,
          sort: "-createdAt",
          limit: 20,
          page: page,
          isFromWeb: true
        }
      });
      setDataTable(data.data.docs);

      if (page === 1) {
        setDataTable(data.data.docs);
        setTotalResult(data.total);
      } else {
        setDataTable([...dataTable, ...data.data.docs]);
        setTotalResult(data.total);
      }
    } catch (error) {
      console.log(error);
    }
  };

  const mergeArray = listArray => {
    let myArray = [];
    listArray.forEach((item, index) => myArray.push(...item));
    return myArray;
  };

  const findTheSameNumber = (array, childArr) => {
    return childArr.filter(element => array.includes(element));
  };

  (0,react__WEBPACK_IMPORTED_MODULE_8__.useEffect)(() => {
    fetchApi(variableTab);
  }, [variableTab, page]);
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    className: "container mx-auto px-1.5",
    children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(components_share_common_NavBar__WEBPACK_IMPORTED_MODULE_4__/* .default */ .Z, {}), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("nav", {
      className: "flex sm:flex-row bg-white rounded mt-1.5",
      children: tabs.map((item, index) => /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_link__WEBPACK_IMPORTED_MODULE_7__.default, {
        href: "/history",
        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
          onClick: handleClickButton(item, index),
          className: (0,utils_functions__WEBPACK_IMPORTED_MODULE_13__/* .classNames */ .AK)(`${item.value === variableTab ? "transition ease-in-out duration-700 w-1/4 py-1 px-1 block rounded hover:text-blue-500 focus:outline-none text-blue-500 border-4 font-medium border-blue-500" : "text-gray-600 w-1/4 py-1 px-1 block hover:text-blue-500 focus:outline-none font-medium"} `),
          children: item.title
        }, index)
      }))
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
      className: "w-full flex flex-col justify-center mt-1.5 mb-4.5",
      children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((react_infinite_scroll_component__WEBPACK_IMPORTED_MODULE_9___default()), {
        dataLength: dataTable.length,
        next: () => setPage(page + 1),
        hasMore: totalResult > dataTable.length ? true : false,
        loader: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h4", {
          className: "text-center py-0.5",
          children: "\u0110ang t\u1EA3i th\xEAm d\u1EEF li\u1EC7u"
        }),
        endMessage: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h4", {
          className: "text-center mb-1.5",
          children: dataTable.length ? "Đã hiển thị tất cả kết quả" : ""
        }),
        children: dataTable.length ? /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((react__WEBPACK_IMPORTED_MODULE_8___default().Fragment), {
          children: dataTable.map((infoLottery, index) => /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(components_BoxNumber_Content__WEBPACK_IMPORTED_MODULE_2__/* .default */ .Z, {
            item: infoLottery,
            isKenoTab: variableTab == "keno",
            children: [infoLottery.lottery.map((lottery, index) => lottery && /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.Fragment, {
              children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(components_BoxNumber__WEBPACK_IMPORTED_MODULE_1__/* .default */ .Z, {
                index: index,
                nums: lottery.nums,
                item: infoLottery,
                isTitleName: true,
                subGame: lottery.subGame ? lottery.subGame : "",
                listOptions: lottery.subGame ? listType.keno.listOptions : [],
                sameNumber: infoLottery.result !== null ? findTheSameNumber(lottery.nums, infoLottery.result.result) : []
              }, index)
            })), infoLottery.result && infoLottery.isTC === false && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
              className: "mt-1 border-t border-b pb-0.5 mb-0.5",
              children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                className: "py-1",
                children: "K\u1EBFt qu\u1EA3 x\u1ED5 s\u1ED1"
              }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(components_BoxNumber__WEBPACK_IMPORTED_MODULE_1__/* .default */ .Z, {
                nums: infoLottery.result.result,
                item: infoLottery,
                borderColor: "border-blue-500",
                borderWidth: "border-2",
                isTitleName: false,
                status: infoLottery.status,
                isResultKeno: variableTab == "keno",
                sameNumber: infoLottery.lottery.length ? findTheSameNumber(mergeArray(infoLottery.lottery.map((num, index) => num.nums)), infoLottery.result.result) : []
              }, index)]
            })]
          }, index))
        }) : /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
          className: "flex flex-col items-center",
          children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(components_share_common_EmptyLoader__WEBPACK_IMPORTED_MODULE_3__/* .default */ .Z, {}), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
            children: "Hi\u1EC7n Ch\u01B0a C\xF3 D\u1EEF Li\u1EC7u"
          })]
        })
      })
    })]
  });
};

/* harmony default export */ __webpack_exports__["default"] = (/*#__PURE__*/react__WEBPACK_IMPORTED_MODULE_8___default().memo(History));

/***/ }),

/***/ 2360:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Vg": function() { return /* binding */ addListCopy; },
/* harmony export */   "CF": function() { return /* binding */ setRooms; },
/* harmony export */   "$L": function() { return /* binding */ addSelectLotteryKeno; },
/* harmony export */   "$2": function() { return /* binding */ addSelectPriceKeno; },
/* harmony export */   "HL": function() { return /* binding */ setDataKeno; },
/* harmony export */   "yS": function() { return /* binding */ setDataMega; },
/* harmony export */   "pG": function() { return /* binding */ setDataPower; },
/* harmony export */   "CC": function() { return /* binding */ setLotteryHistory; },
/* harmony export */   "Nf": function() { return /* binding */ addCopyLotteryKeno; }
/* harmony export */ });
/* unused harmony exports lotteryModule, addLottery */
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6139);
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__);

const lotteryModule = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createSlice)({
  name: "lottery",
  initialState: {
    user: {},
    listCopy: [],
    roomShare: {},
    selectLotteryKeno: {},
    selectPriceKeno: {},
    kenoData: {},
    megaData: {},
    powerData: {},
    lotteryHistory: {
      variableTab: "keno"
    },
    copyLotteryKeno: {}
  },
  reducers: {
    addListCopy: (state, action) => {
      state.listCopy = action.payload;
    },
    addSelectLotteryKeno: (state, action) => {
      state.selectLotteryKeno = action.payload;
    },
    addCopyLotteryKeno: (state, action) => {
      state.copyLotteryKeno = action.payload;
    },
    addSelectPriceKeno: (state, action) => {
      state.selectPriceKeno = action.payload;
    },
    addLottery: (state, action) => {
      state.infoLottery.push(action.payload);
    },
    setRooms: (state, action) => {
      state.roomShare = action.payload;
    },
    setDataKeno: (state, action) => {
      state.kenoData = action.payload;
    },
    setDataMega: (state, action) => {
      state.megaData = action.payload;
    },
    setDataPower: (state, action) => {
      state.powerData = action.payload;
    },
    setLotteryHistory: (state, action) => {
      state.lotteryHistory = action.payload;
    }
  }
}); // Action creators are generated for each case reducer function

const {
  addListCopy,
  addLottery,
  setRooms,
  addSelectLotteryKeno,
  addSelectPriceKeno,
  setDataKeno,
  setDataMega,
  setDataPower,
  setLotteryHistory,
  addCopyLotteryKeno
} = lotteryModule.actions;
/* harmony default export */ __webpack_exports__["ZP"] = (lotteryModule.reducer);

/***/ }),

/***/ 5175:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "av": function() { return /* binding */ setUser; },
/* harmony export */   "ni": function() { return /* binding */ logOut; }
/* harmony export */ });
/* unused harmony export userModule */
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6139);
/* harmony import */ var _reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6155);
/* harmony import */ var js_cookie__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(js_cookie__WEBPACK_IMPORTED_MODULE_1__);


const userModule = (0,_reduxjs_toolkit__WEBPACK_IMPORTED_MODULE_0__.createSlice)({
  name: "user",
  initialState: {
    user: {}
  },
  reducers: {
    setUser: (state, action) => {
      const {
        token,
        user
      } = action.payload;
      state.user = user;

      if (token) {
        js_cookie__WEBPACK_IMPORTED_MODULE_1___default().set("token", token);
      }
    },
    logOut: () => {
      js_cookie__WEBPACK_IMPORTED_MODULE_1___default().remove("token");
    }
  }
}); // Action creators are generated for each case reducer function

const {
  setUser,
  logOut
} = userModule.actions;
/* harmony default export */ __webpack_exports__["ZP"] = (userModule.reducer);

/***/ }),

/***/ 6187:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "xG": function() { return /* binding */ formatCurrency; },
/* harmony export */   "AK": function() { return /* binding */ classNames; },
/* harmony export */   "TN": function() { return /* binding */ title; },
/* harmony export */   "p6": function() { return /* binding */ formatDate; },
/* harmony export */   "pw": function() { return /* binding */ totalLotteryWin; }
/* harmony export */ });
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(2470);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_0__);

const formatCurrency = money => {
  const num = parseInt(money);
  return `${(num || 0).toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.")}đ`;
};
const classNames = (...classes) => {
  return classes.filter(Boolean).join(" ");
};
const title = (index, subGame) => {
  const objTitle = {
    0: "A",
    1: "B",
    2: `${subGame ? "Trò Chơi Phụ" : "C"}`,
    3: "D",
    4: "E",
    5: "F"
  };
  return objTitle[index];
};
const formatDate = date => moment__WEBPACK_IMPORTED_MODULE_0___default()(date).locale("vi").format("L");
const totalLotteryWin = listNumber => {
  return listNumber.reduce((a, b) => a + b, 0);
};

/***/ }),

/***/ 9320:
/***/ (function(__unused_webpack_module, exports) {

"use strict";
exports.__esModule=true;exports.normalizePathSep=normalizePathSep;exports.denormalizePagePath=denormalizePagePath;function normalizePathSep(path){return path.replace(/\\/g,'/');}function denormalizePagePath(page){page=normalizePathSep(page);if(page.startsWith('/index/')){page=page.slice(6);}else if(page==='/index'){page='/';}return page;}
//# sourceMappingURL=denormalize-page-path.js.map

/***/ }),

/***/ 1664:
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

module.exports = __webpack_require__(6071)


/***/ }),

/***/ 2426:
/***/ (function(module) {

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

module.exports = _interopRequireDefault;

/***/ }),

/***/ 9448:
/***/ (function(module, __unused_webpack_exports, __webpack_require__) {

var _typeof = __webpack_require__(7917);

function _getRequireWildcardCache() {
  if (typeof WeakMap !== "function") return null;
  var cache = new WeakMap();

  _getRequireWildcardCache = function _getRequireWildcardCache() {
    return cache;
  };

  return cache;
}

function _interopRequireWildcard(obj) {
  if (obj && obj.__esModule) {
    return obj;
  }

  if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") {
    return {
      "default": obj
    };
  }

  var cache = _getRequireWildcardCache();

  if (cache && cache.has(obj)) {
    return cache.get(obj);
  }

  var newObj = {};
  var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor;

  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null;

      if (desc && (desc.get || desc.set)) {
        Object.defineProperty(newObj, key, desc);
      } else {
        newObj[key] = obj[key];
      }
    }
  }

  newObj["default"] = obj;

  if (cache) {
    cache.set(obj, newObj);
  }

  return newObj;
}

module.exports = _interopRequireWildcard;

/***/ }),

/***/ 7917:
/***/ (function(module) {

function _typeof(obj) {
  "@babel/helpers - typeof";

  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    module.exports = _typeof = function _typeof(obj) {
      return typeof obj;
    };
  } else {
    module.exports = _typeof = function _typeof(obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

module.exports = _typeof;

/***/ }),

/***/ 6139:
/***/ (function(module) {

"use strict";
module.exports = require("@reduxjs/toolkit");;

/***/ }),

/***/ 1765:
/***/ (function(module) {

"use strict";
module.exports = require("eva-icons");;

/***/ }),

/***/ 6155:
/***/ (function(module) {

"use strict";
module.exports = require("js-cookie");;

/***/ }),

/***/ 3804:
/***/ (function(module) {

"use strict";
module.exports = require("lodash");;

/***/ }),

/***/ 2470:
/***/ (function(module) {

"use strict";
module.exports = require("moment");;

/***/ }),

/***/ 8417:
/***/ (function(module) {

"use strict";
module.exports = require("next/dist/next-server/lib/router-context.js");;

/***/ }),

/***/ 2238:
/***/ (function(module) {

"use strict";
module.exports = require("next/dist/next-server/lib/router/utils/get-asset-path-from-route.js");;

/***/ }),

/***/ 6731:
/***/ (function(module) {

"use strict";
module.exports = require("next/router");;

/***/ }),

/***/ 9297:
/***/ (function(module) {

"use strict";
module.exports = require("react");;

/***/ }),

/***/ 4885:
/***/ (function(module) {

"use strict";
module.exports = require("react-infinite-scroll-component");;

/***/ }),

/***/ 9102:
/***/ (function(module) {

"use strict";
module.exports = require("react-lottie");;

/***/ }),

/***/ 79:
/***/ (function(module) {

"use strict";
module.exports = require("react-redux");;

/***/ }),

/***/ 5282:
/***/ (function(module) {

"use strict";
module.exports = require("react/jsx-runtime");;

/***/ }),

/***/ 7405:
/***/ (function(module) {

"use strict";
module.exports = require("store");;

/***/ }),

/***/ 4453:
/***/ (function() {

/* (ignored) */

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = function(moduleId) { return __webpack_require__(__webpack_require__.s = moduleId); }
var __webpack_exports__ = (__webpack_exec__(1752));
module.exports = __webpack_exports__;

})();